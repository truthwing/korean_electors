package com.haesinit.common;

public class Common {

	private static Common _instance;

	public static final String TAG = "HSSocket";

	static {
		_instance = new Common();
	}

	public static Common getInstance() {
		return _instance;
	}
}
