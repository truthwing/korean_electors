package com.haesinit.common;
/******************************************************************************************
!! Copyrightⓒ 1999-2004 KICA Inc. All rights reserved.
!! Product                     : KICA SecuKit
!! Program                     : Base64Util.java
!! Author                      : Hwang, Young Jun
!! Creation date : 15/JUL/1999
!! Update History
!! Version           Modifier              Date                 Change Reason
!! 1.1               yjHwang              12/dec/2001           Add New function
!! 1.2               yjHwang              22/mar/2002           Modify function
!! 1.3               yjHwang              22/jul/2003           Modify function
!! 1.4               yjHwang              12/dec/2003           Add New function
!! 1.5               yjHwang              22/mar/2004           Modify function
*******************************************************************************************/

import java.util.StringTokenizer;


/**
 * Base64는 바이트 배열을 base64형식에 아스키 문자로 표현하기 위한 클래스이다.
 * <p>
 * 이것은 7비트 아스키 이외에는 어떤 것도 지원하진 않는 이메일과 같은
 * <p>
 * 매체를 통해 원래 바이트 데이터를 전송하고자 할 때 유용하다.
 * <p>
 * 8진수가 자리당 3비트, 16진수가 자리당 4비트를 사용한다면
 * <p>
 * Base64는 자리당 6비트를 사용한다.
 * <p>
 * <b>예제</b>
 * <p>
 * String testData = "12345abcdefghij테스트!@#$%^&*()";
 * <p>
 * 	String b64String = Base64Util.encode(testData.getBytes());  or String b64String = Base64Util.encode(testData);
 * <p>
 * 	System.out.println(b64String);
 * <p>
 * 	byte[] plain = Base64Util.decode(b64String);
 * <p>
 * 	System.out.println(new String(plain));
 * <p>
 * <b>결과</b><p>
 * MTIzNDVhYmNkZWZnaGlqxde9usauIUAjJCVeJiooKQ==
 * <p>
 * 12345abcdefghij테스트!@#$%^&*()
 * <p>
 */

public class Base64Util
{
/**
  * String을 인자로 받아서 Base64로 인코딩된 String을 리턴한다.
  * <p>
  * @param raw String을 인자로 받음.
  * @return String
  */
	public static String encode(String raw)
	{
		return encode( raw.getBytes(), true );
	}
/**
  * String을 인자로 받아서 Base64로 인코딩된 String을 리턴한다.<br>
  * 두번째 파라미터는 Base64인코딩된 리턴값의 길이가 길 경우<br>
  * true일경우에는 64자 단위로 캐리지 리턴과 라인피드를 삽입하고<br>
  * false일 경우에는 삽입하지 않는다.<br>
  * <p>
  * @param raw String을 인자로 받음.
  * @param insertCRLF boolean
  * @return String
  */
	public static String encode(String raw, boolean insertCRLF)
	{
		return encode( raw.getBytes(), insertCRLF );
	}

/**
  * byte[]을 인자로 받아서 Base64로 인코딩된 String을 리턴한다.
  * <p>
  * @param raw byte[]을 인자로 받음.
  * @return String
  */
	public static String encode(byte[] raw)
	{
		return encode(raw, true);
	}

/**
  * byte[]을 인자로 받아서 Base64로 인코딩된 String을 리턴한다.
  * <p>
  * @param raw byte[]을 인자로 받음.
  * <p>
  * @return String
  */
    public static String encode(byte[] raw, boolean insertCRLF)
    {
    	StringBuffer encoded = new StringBuffer();
    	if (insertCRLF)
    	{
			for(int i=0; i<raw.length; i+=3)
			{
				if( i%48==0 && i!=0 )
					encoded.append("\n");

				encoded.append(encodedBlock(raw, i));
			}
		}
		else
		{
			for(int i=0; i<raw.length; i+=3)
			{
				encoded.append(encodedBlock(raw, i));
			}
		}

		return encoded.toString();
    }

    protected static char[] encodedBlock(byte[] raw, int offset)
    {
		int block=0;
		int slack = raw.length - offset - 1 ;
		int end = (slack >= 2) ? 2 : slack ;

		for( int i=0; i<=end; i++)
		{
			byte b = raw[offset + i];
			int neuter = (b<0) ? b+256 : b;
			block += neuter << (8*(2-i));
		}

		char[] base64 = new char[4];
		for(int i=0; i<4; i++)
		{
			int sixbit = (block >>> (6*(3-i)))&0x3f;
			base64[i] = getChar(sixbit);
		}

		if(slack<1)
			base64[2] = '=';
		if(slack<2)
			base64[3] = '=';

		return base64;
    }

    protected static char getChar(int sixBit)
    {
		if(sixBit >=0 && sixBit <= 25)
			return (char)('A' + sixBit);
		if(sixBit>=26 && sixBit<=51)
			return (char)('a'+(sixBit-26));
		if(sixBit>=52 && sixBit<=61)
			return (char)('0'+(sixBit-52));
		if(sixBit==62)
			return '+';
		if(sixBit == 63)
			return '/';

		return '?';
    }
/**
  * Base64 String을 인자로 받아서 Base64로 인코딩 되기 이전의 byte[]을 리턴한다.
  * <p>
  * ※ Base64로 인코딩된 스트링이 아닐 경우 Exception이 발생한다.
  *
  * @param base64Src   Base64 인코딩 String
  * <p>
  * @return byte [].
  * <p>
  * @exception InvalidBase64Exception
  */
    public static byte[] decode(String base64Src) throws Exception
    {
		int pad = 0;

		String base64 = delCRLF( reform(base64Src) );

		if( base64.length()%4 != 0 )
			throw new Exception("Data is not Base64 encoding type.(Data length error)");

		if( !isBase64(base64.getBytes()) )
			throw new Exception("Data is not Base64 encoding type.(String set error)");

		for(int i=base64.length()-1; base64.charAt(i) =='='; i--)
			pad++;

		int base64Length = base64.length()*6/8 - pad;

		byte[] raw = new byte[base64Length];

		int rawIndex = 0;
		for(int i=0; i<base64.length(); i+=4)
		{
			int block = (getValue(base64.charAt(i))<<18)
				+ (getValue(base64.charAt(i+1))<<12)
				+ (getValue(base64.charAt(i+2))<<6)
				+ (getValue(base64.charAt(i+3)));

			for(int j=0; j<3 && rawIndex + j<raw.length; j++)
			raw[rawIndex+j] = (byte)((block >> (8*(2-j)))& 0xff);
			rawIndex +=3 ;
		}

		return raw;
    }

    protected static int getValue( char c )
    {
		if( c>='A' && c<='Z')
			return c-'A';
		if( c>='a' && c<='z')
			return c-'a'+26;
		if( c>='0' && c<='9')
			return c-'0'+52;
		if( c=='+')
			return 62;
		if( c=='/')
			return 63;
		if( c=='=')
			return 0;

		return -1;
    }
/**
  * 인자로 주어진 바이트 배열이 base64로 인코딩된 바이트 배열인가를 확인한다.
  *
  * @param data   Base64 인코딩된 스트링의 바이트 값
  * <p>
  * @return boolean
  */
	public static boolean isBase64( byte[] data )
	{
		data = (delCRLF(new String(data))).getBytes();
		for(int i=0; i<data.length; i++)
		{
			char c = (char)data[i];

			if( (c>='A'&&c<='Z') || (c>='a' && c<='z') ||
				(c>='0' && c<='9') || (c=='+') || (c=='/') || (c=='=') )
				continue;
			else
				return false;
		}

		return true;
	}

	protected static String reform( String data )
	{
		StringBuffer sb = new StringBuffer();

		StringTokenizer st = new StringTokenizer( data );
		while(st.hasMoreTokens())
			sb.append( st.nextToken() );

		return sb.toString();
	}

	protected static String delCRLF( String data )
	{
		StringBuffer sb = new StringBuffer();
		StringTokenizer st = new StringTokenizer( data, "\n\r" );
		while(st.hasMoreTokens())
			sb.append( st.nextToken() );

		return sb.toString();
	}
}
