package com.haesinit.ultralite.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

import org.json.JSONObject;

import com.haesinit.common.Common;
import com.haesinit.common.NetworkUtil;
import com.haesinit.ultralite.database.UltraliteManager;

import android.content.Context;
import android.util.Log;

public class SocketServer {
	private Context mContext;
	private int mServerPort;
	private boolean mSocketServerThreadFlag;
	private ServerSocket mServerSocket;
	private UltraliteManager mMngr;

	private OnEventListener mEventListener;

	public SocketServer(Context context, int port, OnEventListener event) {
		mContext = context;
		mServerPort = port;
		mEventListener = event;
	}

	// 생성된 서버의 IP를 조회
	public String getLocalIP() {
		String ip = NetworkUtil.getIPAddress(true);
		Log.i(Common.TAG, "[SERVERSOCKET] " + ip);

		return ip;
	}

	// 소켓서버 시작
	public void startServer(String databasePath) {
		Log.i(Common.TAG, "[SERVERSOCKET] START");

		mMngr = new UltraliteManager(mContext);
		mMngr.openDatabase(databasePath);

		mSocketServerThreadFlag = true;
		Thread socketServerThread = new Thread(new SocketServerThread());
		socketServerThread.start();
	}

	public int getStatus() {
		return mMngr.getStatus();
	}

	// 소켓서버 종료
	public void stopServer() {
		Log.i(Common.TAG, "[SERVERSOCKET] STOP");
		mSocketServerThreadFlag = false;

		try {
			if (mMngr != null) {
				Log.i(Common.TAG, "[SERVERSOCKET] mMngr != null");
				mMngr.closeDatabase();
			} else {
				Log.i(Common.TAG, "[SERVERSOCKET] mMngr == null");
			}
		} catch (Exception e2) {
			Log.i(Common.TAG, "[SERVERSOCKET] mMngr exception:" + e2.toString());
		}

		try {
			if (mServerSocket != null) {
				try {
					Log.i(Common.TAG, "[SERVERSOCKET] mServerSocket != null");
					mServerSocket.close();
				} catch (final IOException e) {
					Log.e(Common.TAG, "[SERVERSOCKET] E1:" + e.getMessage());
				}
			} else {
				Log.i(Common.TAG, "[SERVERSOCKET] mServerSocket == null");
			}
		} catch (Exception e3) {
			Log.i(Common.TAG, "[SERVERSOCKET] mServerSocket exception:" + e3.toString());
		}
	}

	Socket mSocket = null;
	// 소켓서버 Recv/Send 스레드
	private class SocketServerThread extends Thread {
		String msg = "";

		@Override
		public void run() {

			try {
				
				mServerSocket = new ServerSocket(mServerPort);
				Log.i(Common.TAG, "[SERVERSOCKET] PORT : " + mServerSocket.getLocalPort());

				Socket socket = null;
				while (mSocketServerThreadFlag) {
					Log.i(Common.TAG, "[SERVERSOCKET] THREAD: SocketServerThread");
					socket = mServerSocket.accept();

					int qno = 0;
					try {
						// Recv
						BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
						msg = in.readLine();
						Log.i(Common.TAG, "[SERVERSOCKET] RECV:" + msg);

//						try {
//							if (mEventListener != null) {
//								mEventListener.onEvent(msg);
//							}
//						} catch (Exception ee) {
//						}

						// Query
						String resultQuery = mMngr.executeQuery(msg);

						// Send
						OutputStream outputStream;
						JSONObject jsonData = new JSONObject();

						JSONObject jsonParser = new JSONObject(msg);
						qno = jsonParser.getInt("qn");

						/*-------------------------------------------------------------------------------------
						 * SELECT
						 * -------------------------------------------------------------------------------------
						 * {"r":"", "c":"-1"} 오류 r : result, c: result code
						 * {"r":"{"rec":{"<필드1>":"<데이터1>","<필드2>":"<데이터2>"},{ <rec more> }, 
						 *  "et":"<쿼리시간>", "rc":"<record count>", "cc":"<column count>"}, "c":"0"} 정상
						 */

						/*
						 * ------------------------------------------------------
						 * ------------------------------- INSERT, UPDATE,
						 * DELETE
						 * ------------------------------------------------
						 * ------------------------------------- {"r":"",
						 * "c":"-1"} 오류 r:result, c: result code
						 * {"r":"{"et":"<쿼리시간>", "uc":"<update count>"}",
						 * "c":"0"} 정상
						 */

						if (resultQuery == null || resultQuery.equals("")) {
							jsonData.put("c", -1);
						} else {
							jsonData.put("c", 0);
						}
						jsonData.put("qn", qno);
						jsonData.put("r", resultQuery);

						Log.i(Common.TAG, "[SERVERSOCKET] SEND LEN:" + jsonData.toString().length());
							outputStream = socket.getOutputStream();
							PrintStream printStream = new PrintStream(outputStream);
							printStream.print(jsonData.toString());
							printStream.close();
							Log.i(Common.TAG, "[SERVERSOCKET] SEND:" + jsonData.toString());
					} catch (final Exception e) {
						e.printStackTrace();
					} finally {
						socket.close();
					}
				}

				Log.i(Common.TAG, "While out....");

			} catch (final IOException e) {
				Log.e(Common.TAG, "[SERVERSOCKET] E3:" + e.getMessage());
			}
		}
	}
	
}
