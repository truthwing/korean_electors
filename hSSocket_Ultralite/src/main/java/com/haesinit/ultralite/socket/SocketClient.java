package com.haesinit.ultralite.socket;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;

import org.json.JSONException;
import org.json.JSONObject;

import com.haesinit.common.Common;
import com.haesinit.ultralite.database.SQLNO;

import android.util.Log;

public class SocketClient {
	private String mServerIP;
	private int mServerPort;
	private int mConnectionTimeout;
	private int mSoTimeout;
	
	private OnEventListener mEventListener;
	
	public SocketClient(String ip, int port, int connectionTimeout, int soTimeout, OnEventListener event) {
		mServerIP = ip;
		mServerPort = port;
		mEventListener = event;
		mConnectionTimeout = connectionTimeout;
		mSoTimeout = soTimeout;
		
		Log.i(Common.TAG, "[CLIENTSOCKET] SERVER_INFO:" + mServerIP + ":" + mServerPort);
	}
	
	public void executeQuery(SQLNO sqlno, String[] params, int queryType) {
		int qno = sqlno.ordinal();
		int len = params.length;
		JSONObject jsonData = new JSONObject();

		Log.i(Common.TAG, "[CLIENTSOCKET] EXECUTEQUERY:" + qno + " : " + params.length);

		// {"qn": "<query no>", "pc": "<parameter count>", "t": "<query type [S|U]>",
		// "pn": "<parameter n>"}
		try {
			jsonData.put("qn", qno);
			jsonData.put("t", (queryType == 0) ? "S" : (queryType == 1) ? "I" : "U");
			jsonData.put("pc", len);

			for (int i = 1; i <= len; i++) {
				jsonData.put("p" + i, params[i - 1]);
			}
		} catch (JSONException e) {
			Log.e(Common.TAG, "[CLIENTSOCKET] E3:" + e.getMessage());
			return;
		}

		TCPclient tcpThread = new TCPclient(qno, jsonData.toString());
		Thread thread = new Thread(tcpThread);
		thread.start();
	}

	private class TCPclient implements Runnable {
		private Socket inetSocket = null;
		private SocketAddress socketAddress = null;
		private String msg = "";
		private int qno = 0;

		public TCPclient(int qno, String msg) {
			this.qno = qno;
			this.msg = msg;
		}
		
		public void run() {
			Log.i(Common.TAG, "[CLIENTSOCKET] THREAD:TCPclient");

			try {
				//inetSocket = new Socket(mServerIP, mServerPort);
				socketAddress = new InetSocketAddress(mServerIP, mServerPort);
				inetSocket = new Socket();
								
				JSONObject jsonData = new JSONObject();
				
				try {
					inetSocket.setSoTimeout(mSoTimeout);
					inetSocket.connect(socketAddress, mConnectionTimeout);
					
					// Send
					PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(inetSocket.getOutputStream())),
							true);
					out.println(msg);

					Log.i(Common.TAG, "[CLIENTSOCKET] SEND:" + msg);

					// Recv
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
					byte[] buffer = new byte[1024];

					int bytesRead;
					InputStream inputStream = inetSocket.getInputStream();

					// notice: inputStream.read() will block if no data return
					msg = "";
					while ((bytesRead = inputStream.read(buffer)) != -1) {
						byteArrayOutputStream.write(buffer, 0, bytesRead);
					}
					msg = byteArrayOutputStream.toString("UTF-8");
					
					Log.i(Common.TAG, "[CLIENTSOCKET] RECV:" + msg);
			
					try {
						if(mEventListener != null) {
							mEventListener.onEvent(msg);
						}
					} catch(Exception ee) {
					}

				} catch (SocketException e) {
					Log.e(Common.TAG, "[CLIENTSOCKET] E4:" + e.getMessage());
					jsonData.put("r", "-8888");
					jsonData.put("e", e.getMessage());
					
					try {
						if(mEventListener != null) {
							mEventListener.onEvent(jsonData.toString());
						}
					} catch(Exception ee) {
					}
					
				} catch (final Exception e) {
					Log.e(Common.TAG, "[CLIENTSOCKET] E1:" + e.getMessage());
					jsonData.put("r", "-9999");
					jsonData.put("e", e.getMessage());
					
					try {
						if(mEventListener != null) {
							mEventListener.onEvent(jsonData.toString());
						}
					} catch(Exception ee) {
					}
					
				} finally {
					inetSocket.close();
				}
			
			} catch (final Exception e) {
				Log.e(Common.TAG, "[CLIENTSOCKET] E2:" + e.getMessage());
			}
		}
	}
}
