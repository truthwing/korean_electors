package com.haesinit.ultralite.socket;

public interface OnEventListener {
	void onEvent(String msg);
}
