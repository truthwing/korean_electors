package com.haesinit.ultralite.database;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.haesinit.common.Base64Util;
import com.haesinit.common.Common;
import com.ianywhere.ultralitejni16.ConfigFileAndroid;
import com.ianywhere.ultralitejni16.DatabaseManager;
import com.ianywhere.ultralitejni16.ResultSetMetadata;
import com.ianywhere.ultralitejni16.ULjException;

import android.content.Context;
import android.util.Log;
public class UltraliteManager {
	private Context mContext;
	private com.ianywhere.ultralitejni16.Connection mConnect;

	public UltraliteManager(Context context) {
		mContext = context;
	}

	public void openDatabase(String databasePath) {
		//String StoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();

		try {
			ConfigFileAndroid config = DatabaseManager.createConfigurationFileAndroid(databasePath, mContext);
			config.setUserName("dba");
			config.setPassword("sql");
			mConnect = DatabaseManager.connect(config);

			mConnect.prepareStatement("ALTER TABLE BK_SGI_NOF alter SEAL_IMG LONG VARCHAR");
			mConnect.prepareStatement("ALTER TABLE EN_TPG alter IMG_MGR_STAMP LONG VARCHAR");
			mConnect.commit();
			Log.i(Common.TAG, "[QUERYMANAGER] OPEN DATABASE");
		} catch (ULjException e) {
			Log.e(Common.TAG, "[QUERYMANAGER] E1:" + e.toString());
			e.printStackTrace();
		}

	}

	public int getStatus() {
		byte state;
		try {
			state = mConnect.getState();
			Log.i(Common.TAG, "state:" + state);
		} catch (ULjException e) {
			return -1;
		} catch(NullPointerException e) {
			return -2;
		} catch(Exception e) {
			return -3;
		}
		
		if(state == mConnect.CONNECTED) {
			return 0;
		} else {
			return 1;
		}
	}
	
	public String executeQuery(String request) {

		int qno = -1;
		int paramCnt = 0;
		String result = "";
		String queryType = "";
		
		// JSON Parsing
		JSONObject jsonParser = null;
		try {
			jsonParser = new JSONObject(request);
			qno = jsonParser.getInt("qn");
			queryType = jsonParser.getString("t");
			paramCnt = jsonParser.getInt("pc");
			
			Log.i(Common.TAG, "[QUERYMANAGER] QueryNo:" + qno);
			Log.i(Common.TAG, "[QUERYMANAGER] QueryType:" + queryType);
			Log.i(Common.TAG, "[QUERYMANAGER] ParamCnt:" + paramCnt);

			if(qno == 0) {
				byte state = mConnect.getState();
				if(state == mConnect.CONNECTED) {
					return "0";
				} else {
					return "1";
				}
				
			}
		} catch (JSONException e1) {
			Log.e(Common.TAG, "[QUERYMANAGER] E2:" + e1.toString());
		} catch (ULjException e) {
			if(qno == 0) {
				return "-1";
			}
		}

		com.ianywhere.ultralitejni16.PreparedStatement ps = null;
		com.ianywhere.ultralitejni16.ResultSet rs = null;
		
		JSONObject jsonResult = null;
		JSONObject jsonData = null;

		try {
			ps = mConnect.prepareStatement(ULTRALITE.Query[qno]);
			Log.i(Common.TAG, "[QUERYMANAGER] QUERY:" + ULTRALITE.Query[qno]);
			
			for (int i = 1; i <= paramCnt; i++) {
				ps.set(i, jsonParser.getString("p" + i));
				Log.i(Common.TAG, "[QUERYMANAGER] PARAM:[" + jsonParser.getString("p" + i)+"]");
			}

			long t1 = 0;
			long t2 = 0;
			if (queryType.equals("S")) {

				// SELECT 쿼리
				t1 = System.currentTimeMillis();
				rs = ps.executeQuery();
				t2 = System.currentTimeMillis();
				
				ResultSetMetadata meta = rs.getResultSetMetadata();
				int metaCnt = meta.getColumnCount();
				long recordCnt = rs.getRowCount(0);
				Log.i(Common.TAG, "[QUERYMANAGER] META COUNT: " + metaCnt);
				Log.i(Common.TAG, "[QUERYMANAGER] RECORD COUNT: " + String.valueOf(recordCnt));

				jsonResult = new JSONObject();
				JSONArray jsonArray = new JSONArray();
								
				jsonResult.put("et", t2 - t1);
				jsonResult.put("cc", metaCnt);
				jsonResult.put("rc", recordCnt);

				if (recordCnt > 0) {
					rs.first();
					do {
						jsonData = new JSONObject();
						for (int i = 1; i <= metaCnt; i++) {
							String columnName = meta.getTableColumnName(i);
							String columnData = "";
							
							try {
								if(columnName!=null&&columnName.equals("SGG_BPS_STAMP"))
									columnData = Base64Util.encode(rs.getBytes("SGG_BPS_STAMP"));
								else
									columnData = rs.getString(i);
							}  catch (ULjException e) {
								byte[] b = rs.getBytes(i);
								Log.i(Common.TAG, "BYTE LEN: "  + b.length);
								try {
									columnData = new String(b,"utf-8");
									
								} catch (Exception e1) {
									Log.i(Common.TAG, e1.toString());
								}
							}
							
							if(columnData == null) {
								columnData = "";
							}
							
							if(columnName == null || columnName.equals("")) {
								columnName = meta.getAliasName(i);
							} 
							
							jsonData.put(columnName, columnData);
						}
						
						jsonArray.put(jsonData);

					} while (rs.next());
				}

				jsonResult.put("rec", jsonArray);
				result = jsonResult.toString();
			
			} else if (queryType.equals("U") || queryType.equals("I")) { 
				
				// UPDATE, INSERT, DELETE 쿼리
				t1 = System.currentTimeMillis();
				boolean isSuccess = ps.execute();
				t2 = System.currentTimeMillis();
				
				int updateCnt = ps.getUpdateCount();
				
				
				if(isSuccess) 
					mConnect.commit();
				
				jsonData = new JSONObject();
				jsonData.put("et", t2 - t1);
				jsonData.put("uc", updateCnt);
				
				return (isSuccess == false) ? "" : jsonData.toString();
			}

		} catch (ULjException e) {
			Log.e(Common.TAG, "[QUERYMANAGER] E2:" + e.toString());
		} catch (JSONException e) {
			Log.e(Common.TAG, "[QUERYMANAGER] E3:" + e.toString());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (ULjException e) {
				Log.e(Common.TAG, "[QUERYMANAGER] E4:" + e.toString());
			}
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (ULjException e) {
				Log.e(Common.TAG, "[QUERYMANAGER] E2:" + e.toString());
			}
		}

		return result;
	}

	public void closeDatabase() {
		if (mConnect != null) {
			try {
				Log.i(Common.TAG, "[QUERYMANAGER] mConnect != null");
				mConnect.release();
				Log.i(Common.TAG, "[QUERYMANAGER] CLOSE DATABASE");

			} catch (ULjException e) {
				Log.e(Common.TAG, "[QUERYMANAGER] E9:" + e.toString());
			}
		} else {
			Log.i(Common.TAG, "[QUERYMANAGER] mConnect == null");
		}
	}
}
