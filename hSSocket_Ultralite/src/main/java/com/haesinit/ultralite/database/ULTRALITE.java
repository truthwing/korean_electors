package com.haesinit.ultralite.database;

public class ULTRALITE {

	private static ULTRALITE _instance;

	public static final String[] Query = new String[78];

	static {
		_instance = new ULTRALITE();

		makeQuery();
	}

	public static ULTRALITE getInstance() {
		return _instance;
	}

	private static void makeQuery() {
		int no = 0;
		StringBuilder sb = new StringBuilder();

		// QUERY 0 : TEST CODE
		sb = new StringBuilder();
		sb.append("SELECT OBJECT_ID, TABLE_NAME FROM systable");
		Query[no++] = sb.toString();

		// QUERY 1
		// FIX: CERT -> ISNULL(CERT,'') AS CERT
		sb = new StringBuilder();
		sb.append("SELECT ISNULL(CERT,'') AS CERT FROM EN_CERT ");
		sb.append("WHERE CERT_GUBUN = 'T' ");
		sb.append("AND VALID_YN = 'Y' ");
		sb.append("AND CERT_ES = 'S' ");
		sb.append("AND TG_DATE = ? ");
		sb.append("AND CERT_CD = ?");
		Query[no++] = sb.toString();

		// QUERY 2
		sb = new StringBuilder();
		sb.append("SELECT (SELECT FIRST S_NAME ");
		sb.append("FROM EN_S ");
		sb.append("WHERE S_TYPE = 'N' ");
		sb.append("ORDER BY S_ID) S_NAME, ");
		sb.append("(SELECT FIRST END_GUBN FROM EN_S WHERE ");
		sb.append("      S_TYPE = 'N' ) END_GUBN, ");
		sb.append("      A.STATUS, C.TPG_NAME, ");
		sb.append("      DATEFORMAT(NOW(*), 'YYYY-MM-DD') SERVER_DATE, ");
		sb.append("      DATEFORMAT(NOW(*), 'HH:NN:SS') SERVER_TIME ");
		sb.append("FROM VT_TPG A, ");
		sb.append("      EN_TPG C ");
		sb.append("WHERE A.TPG_ID = C.TPG_ID ");
		sb.append("AND A.TPG_ID = ? ");
		sb.append("AND A.VT_DATE = DATE(?)");
		Query[no++] = sb.toString();

		// QUERY 3
		// FIX: @@VERSION DB_VERSION -> 'ASA 16.0.0.2043' AS DB_VERSION
		sb = new StringBuilder();
		sb.append("SELECT STRING(VER_DATE) VER_DATE, 'ASA 16.0.0.2043' AS DB_VERSION FROM EN_VERSION ORDER BY VER_SNO DESC");
		Query[no++] = sb.toString();

		// QUERY 4
		sb = new StringBuilder();
		sb.append("SELECT STRING(DATACHECK_DATE) DATACHECK_DATE FROM EN_S");
		Query[no++] = sb.toString();

		// QUERY 5
		// FIX : COUNT(*) -> COUNT(*) CNT
		sb = new StringBuilder();
		sb.append("SELECT COUNT(*) AS CNT FROM BK_SGI_NOF WHERE TPG_ID IS NULL");
		Query[no++] = sb.toString();

		// QUERY 6
		// FIX : COUNT(*) ->
		sb = new StringBuilder();
		// sb.append("IF (SELECT COUNT(*) FROM EN_S) > 0 ");
		// sb.append("THEN ");
		sb.append("SELECT CASE WHEN (SELECT COUNT(*) FROM EN_S) > 0 THEN Count(*) ELSE 0 END AS CNT ");
		sb.append("FROM BK_SGI_NOF B, EN_S S ");
		sb.append("WHERE B.SGIG_ID IS NOT NULL AND B.SGIG_ID NOT IN ");
		sb.append("( SELECT B.SGIG_ID FROM BK_SGI_NOF B, EN_SGIG S WHERE S.SGIG_ID = B.SGIG_ID ) ");
		// sb.append("END IF");
		Query[no++] = sb.toString();

		// QUERY 7
		// FIX: COUNT(*) -> COUNT(*) AS CNT
		sb = new StringBuilder();
		sb.append("SELECT COUNT(*) AS CNT FROM BK_SGI_NOF ");
		sb.append("WHERE SGIG_TPG_ID IS NOT NULL ");
		sb.append("AND SGIG_TPG_ID NOT IN ");
		sb.append("(SELECT TPG_ID FROM EN_TPG WHERE PRE_YN='Y')");
		Query[no++] = sb.toString();

		// QUERY 8
		sb = new StringBuilder();
		sb.append("UPDATE BK_SGI_NOF ");
		sb.append("SET VT_YN = 'Y', ");
		sb.append("VT_DATE = NULL, ");
		sb.append("VT_METHOD = NULL, ");
		sb.append("VT_TPG_ID = CASE WHEN TPG_ID_YN = 'N' THEN  NULL ELSE VT_TPG_ID END , ");
		sb.append("SEAL_CD = NULL, ");
		sb.append("SEAL_IMG = NULL, ");
		sb.append("SEAL_POINT = NULL, ");
		sb.append("ISSUED_LOCATION = NULL, ");
		sb.append("ISSUE_CNT = NULL ");
		sb.append("WHERE    VT_TPG_ID = ?");
		Query[no++] = sb.toString();

		// QUERY 9
		sb = new StringBuilder();
		sb.append("UPDATE VT_TPG ");
		sb.append("SET STATUS = 'A', ");
		sb.append("START_DATE = NULL, ");
		sb.append("CLOSE_DATE = NULL, ");
		sb.append("VTR_ALL = NULL, ");
		sb.append("VTR_ABS = NULL, ");
		sb.append("BK_MCNT = NULL, ");
		sb.append("VT_MCNT = NULL, ");
		sb.append("VT_XCNT = NULL, ");
		sb.append("VT_PCNT = NULL, ");
		sb.append("VT_CCNT = NULL, ");
		sb.append("VT_DCNT = NULL, ");
		sb.append("VT_ECNT = NULL, ");
		sb.append("NVT_CCNT = NULL ");
		sb.append("WHERE   TPG_ID = ?");
		Query[no++] = sb.toString();

		// QUERY 10
		// FIX: GROUP BY VT_TPG_ID) C -> GROUP BY VT_TPG_ID) C (VT_TPG_ID,
		// VT_CCNT)
		sb = new StringBuilder();
		sb.append("SELECT A.TPG_ID, A.VT_DATE, ");
		sb.append("D.NAME AS STATUS, ");
		sb.append("A.START_DATE, A.CLOSE_DATE, ");
		sb.append("IFNULL(A.VTR_ALL, 0, A.VTR_ALL) VTR_ALL, ");
		sb.append("IFNULL(A.VTR_ABS, 0, A.VTR_ABS) VTR_ABS, ");
		sb.append("IFNULL(A.BK_MCNT, 0, A.BK_MCNT) BK_MCNT, ");
		sb.append("IFNULL(A.VT_MCNT, 0, A.VT_MCNT) VT_MCNT, ");
		sb.append("IFNULL(A.VT_XCNT, 0, A.VT_XCNT) VT_XCNT, ");
		sb.append("IFNULL(A.VT_PCNT, 0, A.VT_PCNT) VT_PCNT, ");
		sb.append("IFNULL(C.VT_CCNT, 0, C.VT_CCNT) VT_CCNT, ");
		sb.append("IFNULL(A.VT_DCNT, 0, A.VT_DCNT) VT_DCNT, ");
		sb.append("IFNULL(A.VT_ECNT, 0, A.VT_ECNT) VT_ECNT, ");
		sb.append("IFNULL(A.NVT_CCNT, 0, A.NVT_CCNT) NVT_CCNT, ");
		sb.append("B.TPG_NAME ");
		sb.append("FROM VT_TPG A ");
		sb.append("LEFT JOIN EN_TPG B ON A.TPG_ID = B.TPG_ID ");
		sb.append("LEFT OUTER JOIN ");
		sb.append("(SELECT VT_TPG_ID, COUNT(*) VT_CCNT ");
		sb.append("FROM BK_SGI_NOF ");
		sb.append("WHERE VT_YN <> 'Y' ");
		sb.append("GROUP BY VT_TPG_ID) C (VT_TPG_ID, VT_CCNT) ON A.TPG_ID = C.VT_TPG_ID ");
		sb.append("LEFT JOIN EN_ETC_CD D ON A.STATUS = D.CODE AND D.GUBUN  = 'ING_STATUS' ");
		sb.append("WHERE A.VT_DATE = DATE(?) ");
		sb.append("ORDER BY A.TPG_ID");
		Query[no++] = sb.toString();

		// QUERY 11
		sb = new StringBuilder();
		sb.append("SELECT COUNT(*) VTR_ALL, NOW(*) NOW FROM BK_SGI_NOF");
		Query[no++] = sb.toString();

		// QUERY 12
		// FIX: 주석된 것을 변경
		sb = new StringBuilder();
		// sb.append("SELECT ROUND(SGI_CNT / TPG_CNT, 0) + 1 ");
		// sb.append("FROM (SELECT COUNT(*) SGI_CNT ");
		// sb.append("FROM BK_SGI_NOF) A, ");
		// sb.append("(SELECT COUNT(*) TPG_CNT ");
		// sb.append("FROM VT_TPG ");
		// sb.append("WHERE VT_DATE = DATE(?)) B");
		sb.append("SELECT (SELECT COUNT(*) FROM BK_SGI_NOF) AS SGI_CNT, ");
		sb.append(" (SELECT CASE WHEN COUNT(*) = 0 THEN 1 ELSE COUNT(*) END FROM VT_TPG WHERE VT_DATE = DATE(?)) AS TPG_CNT, ");
		sb.append(" ROUND(SGI_CNT/TPG_CNT,0) + 1 AS CNT ");
		Query[no++] = sb.toString();

		// QUERY 13
		sb = new StringBuilder();
		// sb.append("SELECT A.DJ_NO, A.ADDR, A.NAME, A.JUMIN_NO, ");
		// sb.append("A.ETC ETC, A.TPG_ID, ");
		// sb.append("A.SGIG_ID, A.VT_YN, A.VT_DATE, B.TPG_NAME, ");
		// sb.append("ISNULL(A.ISSUED_LOCATION, '') IP_ADDR, ");
		// sb.append("CASE WHEN A.VT_YN = 'Y' THEN '발급가능' ELSE '기발급' END VT_YN_DESC, ");
		// sb.append("SUBSTRING(A.JUMIN_NO, 1, 2) + '/' + ");
		// sb.append("SUBSTRING(A.JUMIN_NO, 3, 2) + '/' + ");
		// sb.append("SUBSTRING(A.JUMIN_NO, 5, 2) BIRTH_DATE ");
		// sb.append("FROM BK_SGI_NOF A ");
		// sb.append("LEFT OUTER JOIN EN_TPG B ON A.VT_TPG_ID = B.TPG_ID ");
		// sb.append("WHERE A.NAME LIKE ? ");
		// sb.append("AND A.JUMIN_NO LIKE ? ");
		// sb.append("AND A.TPG_ID IN (SELECT TPG_ID FROM EN_TPG WHERE PRE_YN = 'N') ");
		// sb.append("ORDER BY A.DJ_NO");
		sb.append("SELECT DJ_NO, ADDR, NAME, JUMIN_NO, ETC, SGIG_ID, VT_YN, VT_DATE, TPG_NAME, A.TPG_ID, A.VT_TPG_ID, A.TPG_ID_YN, ");
		sb.append("SUBSTRING(JUMIN_NO, 1, 2) + '/' + SUBSTRING(JUMIN_NO, 3, 2) + '/' + SUBSTRING(JUMIN_NO, 5, 2) BIRTH_DATE, ");
		sb.append("A.ISSUED_LOCATION ");
		sb.append("FROM BK_SGI_NOF A  ");
		sb.append("LEFT OUTER JOIN EN_TPG B ON A.VT_TPG_ID = B.TPG_ID ");
		sb.append("WHERE DJ_NO = ?  ");
		sb.append("OR NAME LIKE ? ");
		sb.append("OR JUMIN_NO LIKE ? ");
		sb.append("ORDER BY DJ_NO");
		Query[no++] = sb.toString();

		// QUERY 14
		sb = new StringBuilder();
		sb.append("SELECT BYTE_LENGTH(A.SEAL_IMG) IMG_LEN, ");
		sb.append("IFNULL(BYTE_LENGTH(A.SEAL_POINT), 0, BYTE_LENGTH(A.SEAL_POINT)) MIN_LEN, ");
		//sb.append("CONVERT(VARCHAR, CONVERT(BINARY(32767), A.SEAL_IMG)),0, 32767) AS SEAL_IMG, ");
		sb.append("A.SEAL_IMG, ");
		sb.append("A.SEAL_CD, A.SEAL_POINT ");
		sb.append("FROM BK_SGI_NOF A, EN_TPG B ");
		sb.append("WHERE A.VT_TPG_ID = B.TPG_ID ");
		sb.append("AND A.DJ_NO = ? ");
		sb.append("AND A.TPG_ID = ?");
		Query[no++] = sb.toString();

		// QUERY 15
		sb = new StringBuilder();
		sb.append("SELECT A.DJ_NO, A.ADDR, A.NAME, A.JUMIN_NO, A.TPG_ID, ");
		sb.append("ISNULL(A.ISSUED_LOCATION, '') IP_ADDR, ");
		sb.append("A.ETC ETC, ");
		sb.append("A.SGIG_ID, A.VT_YN, A.VT_DATE, B.TPG_NAME, ");
		sb.append("CASE WHEN A.VT_YN = 'Y' THEN '발급가능' ELSE '기발급' END VT_YN_DESC, ");
		sb.append("SUBSTRING(A.JUMIN_NO, 1, 2) + '/' + ");
		sb.append("SUBSTRING(A.JUMIN_NO, 3, 2) + '/' + ");
		sb.append("SUBSTRING(A.JUMIN_NO, 5, 2) BIRTH_DATE ");
		sb.append("FROM BK_SGI_NOF A ");
		sb.append("LEFT OUTER JOIN EN_TPG B ON A.VT_TPG_ID = B.TPG_ID ");
		sb.append("WHERE A.NAME LIKE ? ");
		sb.append("AND A.JUMIN_NO LIKE ? ");
		sb.append("AND A.TPG_ID IN (SELECT TPG_ID FROM EN_TPG WHERE PRE_YN = 'N') ");
		sb.append("ORDER BY A.DJ_NO");
		Query[no++] = sb.toString();

		// QUERY 16
		sb = new StringBuilder();
		sb.append("SELECT DJ_NO, NAME, JUMIN_NO, ADDR, ETC, ");
		sb.append("VT_DATE, NOW(*) NOW, ");
		sb.append("VT_TPG_ID, ");
		sb.append("TPG_ID ");
		sb.append("FROM BK_SGI_NOF ");
		sb.append("WHERE DJ_NO = ? ");
		sb.append("AND VT_YN <> 'Y' ");
		sb.append("AND TPG_ID = ? ");
		sb.append("AND IFNULL(VT_TPG_ID, ?, VT_TPG_ID) = ? ");
		sb.append("AND TPG_ID IN (SELECT TPG_ID FROM EN_TPG WHERE PRE_YN = 'N') ");
		sb.append("AND ( IFNULL(SGIG_TPG_ID, 'NULL', SGIG_TPG_ID) = '{0}' ");
		sb.append("OR IFNULL(SGIG_TPG_ID, 'NULL', SGIG_TPG_ID) = 'NULL' ");
		sb.append("OR 'NULL' = '{1}' )");
		Query[no++] = sb.toString();

		// QUERY 17
		sb = new StringBuilder();
		sb.append("INSERT INTO BK_LOG_NOF ");
		sb.append("(LOG_TPG_ID, TPG_ID, LOG_DATE, BK_LOG_GUBUN, ");
		sb.append("DJ_NO, LOG_TEXT, BAK_TEXT, BAK_IMG) ");
		sb.append("SELECT VT_TPG_ID, TPG_ID, NOW(*), ?, DJ_NO, ?, ");
		sb.append("'VT_DATE:' + DATEFORMAT(VT_DATE, 'YYYYMMDD HHNNSS') + ', ' + ");
		sb.append("'VT_METHOD:' + VT_METHOD + ', ' + ");
		sb.append("'SEAL_CD:' + SEAL_CD, ");
		sb.append("SEAL_IMG ");
		sb.append("FROM BK_SGI_NOF ");
		sb.append("WHERE DJ_NO = ? ");
		sb.append("AND TPG_ID = ? ");
		sb.append("AND VT_YN <> 'Y' ");
		sb.append("AND ( IFNULL(SGIG_TPG_ID, 'NULL', SGIG_TPG_ID) = '{0}' ");
		sb.append("OR IFNULL(SGIG_TPG_ID, 'NULL', SGIG_TPG_ID) = 'NULL' ");
		sb.append("OR 'NULL' = '{1}' )");
		Query[no++] = sb.toString();

		// QUERY 18
		sb = new StringBuilder();
		sb.append("UPDATE BK_SGI_NOF ");
		sb.append("SET ETC = ?, ");
		sb.append("VT_YN = 'Y', ");
		sb.append("VT_DATE = NULL, ");
		sb.append("VT_METHOD = NULL, ");
		sb.append("VT_TPG_ID = CASE WHEN TPG_ID_YN = 'N' THEN  null ELSE VT_TPG_ID END , ");
		sb.append("SEAL_CD = NULL, ");
		sb.append("SEAL_IMG = NULL, ");
		sb.append("SEAL_POINT = NULL, ");
		sb.append("ISSUED_LOCATION = NULL ");
		sb.append("WHERE DJ_NO = ? ");
		sb.append("AND TPG_ID = ? ");
		sb.append("AND VT_YN <> 'Y' ");
		sb.append("AND ( IFNULL(SGIG_TPG_ID, 'NULL', SGIG_TPG_ID) = '{0}' ");
		sb.append("OR IFNULL(SGIG_TPG_ID, 'NULL', SGIG_TPG_ID) = 'NULL' ");
		sb.append("OR 'NULL' = '{1}' )");
		Query[no++] = sb.toString();

		// QUERY 19
		sb = new StringBuilder();
		sb.append("SELECT TPG_ID, TPG_NAME FROM EN_TPG WHERE TPG_ID IS NOT NULL AND PRE_YN='N' ORDER BY TPG_ID");
		Query[no++] = sb.toString();

		// QUERY 20
		// FIX: COUNT(*) -> COUNT(*) AS CNT
		sb = new StringBuilder();
		sb.append("SELECT COUNT(*) AS CNT FROM EN_TPG WHERE TPG_ID IS NOT NULL AND PRE_YN='N'");
		Query[no++] = sb.toString();

		// QUERY 21
		sb = new StringBuilder();
		sb.append("SELECT TPG_ID, TPG_NAME FROM EN_TPG WHERE TPG_ID IS NOT NULL AND PRE_YN='Y'  ORDER BY TPG_ID");
		Query[no++] = sb.toString();

		// QUERY 22
		// FIX: COUNT(*) -> COUNT(*) AS CNT
		sb = new StringBuilder();
		sb.append("SELECT COUNT(*) AS CNT FROM EN_TPG WHERE TPG_ID IS NOT NULL AND PRE_YN='Y'");
		Query[no++] = sb.toString();

		// QUERY 23
		sb = new StringBuilder();
		sb.append("SELECT SGIG_ID, SGIG_NAME FROM EN_SGIG  WHERE SGIG_ID IS NOT NULL ORDER BY SGIG_ID");
		Query[no++] = sb.toString();

		// QUERY 24
		// FIX: COUNT(*) -> COUNT(*) AS CNT
		sb = new StringBuilder();
		sb.append("SELECT COUNT(*) AS CNT FROM EN_SGIG WHERE SGIG_ID IS NOT NULL");
		Query[no++] = sb.toString();

		// QUERY 25
		sb = new StringBuilder();
		sb.append("SELECT COUNT(*) DUP FROM BK_SGI_NOF WHERE JUMIN_NO = ? AND NAME = ?");
		Query[no++] = sb.toString();

		// QUERY 26
		sb = new StringBuilder();
		sb.append("INSERT INTO BK_SGI_NOF ");
		sb.append("(DJ_NO, ADDR, NAME, JUMIN_NO, ETC, TPG_ID, VT_TPG_ID, SGIG_ID, VT_YN, TPG_ID_YN) ");
		sb.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, 'Y', 'Y')");
		Query[no++] = sb.toString();

		// QUERY 27
		// FIX: LOG_SNO는 autoincrement이므로 쿼리에서 제거
		sb = new StringBuilder();
		sb.append("INSERT INTO BK_LOG_NOF ");
		sb.append("(TPG_ID, LOG_TPG_ID, LOG_DATE, BK_LOG_GUBUN, DJ_NO, LOG_TEXT) ");
		sb.append("VALUES (?, ?, NOW(*), 'C', ?, ?)");
		Query[no++] = sb.toString();

		// QUERY 28
		// FIX: ISNULL(MAX(DJ_NO), 0) + 1 -> ISNULL(MAX(DJ_NO), 0) + 1 AS
		// DJ_MAX_NO
		sb = new StringBuilder();
		sb.append("SELECT ISNULL(MAX(DJ_NO), 0) + 1 AS DJ_MAX_NO FROM BK_SGI_NOF");
		Query[no++] = sb.toString();

		// QUERY 29
		sb = new StringBuilder();
		sb.append("SELECT BYTE_LENGTH(A.SEAL_IMG) IMG_LEN, ");
		sb.append("A.SEAL_IMG, ");
		sb.append("B.TPG_NAME, ");
		sb.append("ISNULL(A.ISSUED_LOCATION, '') IP_ADDR ");
		sb.append("FROM BK_SGI_NOF A, EN_TPG B ");
		sb.append("WHERE A.VT_TPG_ID = B.TPG_ID ");
		sb.append("AND A.DJ_NO = ? ");
		sb.append("AND A.TPG_ID = ?");
		Query[no++] = sb.toString();

		// QUERY 30
		// FIX: 주석된 쿼리대신 수정된 쿼리로 대체
		sb = new StringBuilder();
		sb.append("SELECT AA.DJ_NO, AA.ADDR, AA.NAME, AA.JUMIN_NO, AA.ETC , ");
		sb.append("AA.SGIG_ID, AA.VT_YN, AA.VT_DATE, AA.TPG_ID, AA.VT_TPG_ID, ");
		sb.append("CASE WHEN AA.VT_YN = 'Y' THEN '발급가능' ELSE '기발급' END VT_YN_DESC, ");
		sb.append("SUBSTRING(AA.JUMIN_NO, 1, 2) + '/' + ");
		sb.append("SUBSTRING(AA.JUMIN_NO, 3, 2) + '/' + ");
		sb.append("SUBSTRING(AA.JUMIN_NO, 5, 2) BIRTH_DATE ");
		sb.append("FROM BK_SGI_NOF AA ");
		// sb.append(",(SELECT distinct CASE TPG_ID_YN WHEN 'Y' THEN ? ELSE ISNULL(VT_TPG_ID,'NULL') END AS VT_TPG_ID FROM BK_SGI_NOF WHERE JUMIN_NO LIKE ? AND NAME LIKE ?) BB ");
		sb.append(",(SELECT distinct CASE ISNULL(TPG_ID_YN,'N') WHEN 'Y' THEN ? ELSE ISNULL(VT_TPG_ID,'NULL') END AS VT_TPG_ID FROM BK_SGI_NOF WHERE JUMIN_NO LIKE ? AND NAME LIKE ?) BB (VT_TPG_ID) ");
		sb.append("WHERE AA.JUMIN_NO LIKE ? ");
		sb.append("AND AA.NAME LIKE ? ");
		sb.append("AND ISNULL(AA.VT_TPG_ID,'NULL') = BB.VT_TPG_ID ");
		sb.append("AND AA.TPG_ID IN (SELECT TPG_ID FROM EN_TPG WHERE PRE_YN = 'N') ");
		sb.append("AND ( IFNULL(AA.SGIG_TPG_ID, 'NULL', AA.SGIG_TPG_ID) = '{0}' ");
		sb.append("OR IFNULL(AA.SGIG_TPG_ID, 'NULL', AA.SGIG_TPG_ID) = 'NULL' ");
		sb.append("OR 'NULL' = '{1}' ) ");
		sb.append("ORDER BY DJ_NO   DESC");
		Query[no++] = sb.toString();

		// QUERY 31
		// FIX: 주석된 쿼리대신 수정된 쿼리로 대체
		sb = new StringBuilder();
		sb.append("SELECT DJ_NO, ADDR, NAME, JUMIN_NO, ETC, ");
		sb.append("SGIG_ID, VT_YN, VT_DATE, TPG_ID, VT_TPG_ID, ");
		sb.append("CASE WHEN VT_YN = 'Y' THEN '발급가능' ELSE '기발급' END VT_YN_DESC, ");
		sb.append("SUBSTRING(JUMIN_NO, 1, 2) + '/' + ");
		sb.append("SUBSTRING(JUMIN_NO, 3, 2) + '/' + ");
		sb.append("SUBSTRING(JUMIN_NO, 5, 2) BIRTH_DATE ");
		sb.append("FROM BK_SGI_NOF ");
		sb.append("WHERE DJ_NO = ? ");
		// sb.append("AND ISNULL(VT_TPG_ID,'NULL') IN (SELECT CASE TPG_ID_YN WHEN 'Y' THEN ? ELSE ISNULL(VT_TPG_ID,'NULL') END FROM BK_SGI_NOF WHERE DJ_NO = ?) ");
		sb.append("AND ISNULL(VT_TPG_ID,'NULL') IN (SELECT CASE ISNULL(TPG_ID_YN,'N') WHEN 'Y' THEN ? ELSE ISNULL(VT_TPG_ID,'NULL') END FROM BK_SGI_NOF WHERE DJ_NO = ?) ");
		sb.append("AND TPG_ID IN (SELECT TPG_ID FROM EN_TPG WHERE PRE_YN = 'N') ");
		sb.append("AND ( IFNULL(SGIG_TPG_ID, 'NULL', SGIG_TPG_ID) = '{0}' ");
		sb.append("OR IFNULL(SGIG_TPG_ID, 'NULL', SGIG_TPG_ID) = 'NULL' ");
		sb.append("OR 'NULL' = '{1}' ) ");
		sb.append("ORDER BY TPG_ID");
		Query[no++] = sb.toString();

		// QUERY 32
		sb = new StringBuilder();
		sb.append("SELECT STATUS FROM VT_TPG WHERE TPG_ID = ? AND VT_DATE = DATE(?)");
		Query[no++] = sb.toString();

		// QUERY 33
		// FIX: 주석된 쿼리대신 수정된 쿼리로 대체
		sb = new StringBuilder();
		sb.append("SELECT DJ_NO, ADDR, NAME, JUMIN_NO, ETC, ");
		sb.append("SGIG_ID, VT_YN, VT_TPG_ID, ");
		sb.append("VT_DATE, TPG_ID ");
		sb.append("FROM BK_SGI_NOF ");
		sb.append("WHERE DJ_NO = ? ");
		sb.append("AND TPG_ID IN (SELECT TPG_ID FROM EN_TPG WHERE PRE_YN = 'N') ");
		sb.append("AND TPG_ID = ? ");
		// sb.append("AND ISNULL(VT_TPG_ID,'NULL') IN (SELECT CASE TPG_ID_YN WHEN 'Y' THEN ? ELSE ISNULL(VT_TPG_ID,'NULL') END FROM BK_SGI_NOF WHERE DJ_NO= ?) ");
		sb.append("AND ISNULL(VT_TPG_ID,'NULL') IN (SELECT CASE ISNULL(TPG_ID_YN,'N') WHEN 'Y' THEN ? ELSE ISNULL(VT_TPG_ID,'NULL') END FROM BK_SGI_NOF WHERE DJ_NO= ?) ");
		sb.append("AND ( IFNULL(SGIG_TPG_ID, 'NULL', SGIG_TPG_ID) = '{0}' ");
		sb.append("OR IFNULL(SGIG_TPG_ID, 'NULL', SGIG_TPG_ID) = 'NULL' ");
		sb.append("OR 'NULL' = '{1}' )");
		Query[no++] = sb.toString();

		// QUERY 34
		// FIX: 주석된 쿼리대신 수정된 쿼리로 대체
		sb = new StringBuilder();
		sb.append("SELECT DJ_NO, ADDR, NAME, JUMIN_NO, ETC, ");
		sb.append("SGIG_ID, VT_YN, VT_TPG_ID, ");
		sb.append("VT_DATE, TPG_ID ");
		sb.append("FROM BK_SGI_NOF ");
		sb.append("WHERE DJ_NO = ? ");
		sb.append("AND TPG_ID IN (SELECT TPG_ID FROM EN_TPG WHERE PRE_YN = 'N') ");
		// sb.append("AND ISNULL(VT_TPG_ID,'NULL') IN (SELECT CASE TPG_ID_YN WHEN 'Y' THEN ? ELSE ISNULL(VT_TPG_ID,'NULL') END FROM BK_SGI_NOF WHERE DJ_NO= ?) ");
		sb.append("AND ISNULL(VT_TPG_ID,'NULL') IN (SELECT CASE ISNULL(TPG_ID_YN,'N') WHEN 'Y' THEN ? ELSE ISNULL(VT_TPG_ID,'NULL') END FROM BK_SGI_NOF WHERE DJ_NO= ?) ");
		sb.append("AND ( IFNULL(SGIG_TPG_ID, 'NULL', SGIG_TPG_ID) = '{0}' ");
		sb.append("OR IFNULL(SGIG_TPG_ID, 'NULL', SGIG_TPG_ID) = 'NULL' ");
		sb.append("OR 'NULL' = '{1}' )");
		Query[no++] = sb.toString();

		// QUERY 35
		// FIX: 주석된 쿼리대신 수정된 쿼리로 대체
		sb = new StringBuilder();
		sb.append("SELECT AA.DJ_NO, AA.ADDR, AA.NAME, AA.JUMIN_NO, AA.ETC, ");
		sb.append("AA.SGIG_ID, AA.VT_YN, AA.VT_TPG_ID, ");
		sb.append("AA.VT_DATE, AA.TPG_ID ");
		sb.append("FROM BK_SGI_NOF AA ");
		// sb.append(",(SELECT distinct CASE TPG_ID_YN WHEN 'Y' THEN ? ELSE ISNULL(VT_TPG_ID,'NULL') END AS VT_TPG_ID FROM BK_SGI_NOF WHERE  JUMIN_NO LIKE ? AND NAME LIKE ? ) BB ");
		sb.append(",(SELECT distinct CASE ISNULL(TPG_ID_YN,'N') WHEN 'Y' THEN ? ELSE ISNULL(VT_TPG_ID,'NULL') END AS VT_TPG_ID FROM BK_SGI_NOF WHERE  JUMIN_NO LIKE ? AND NAME LIKE ? ) BB (VT_TPG_ID) ");
		sb.append("WHERE  AA.JUMIN_NO LIKE ? ");
		sb.append("AND AA.NAME LIKE ? ");
		sb.append("AND AA.TPG_ID IN (SELECT TPG_ID FROM EN_TPG WHERE PRE_YN = 'N') ");
		sb.append("AND ISNULL(AA.VT_TPG_ID,'NULL') = BB.VT_TPG_ID ");
		sb.append("AND ( IFNULL(AA.SGIG_TPG_ID, 'NULL', AA.SGIG_TPG_ID) = '{0}' ");
		sb.append("OR IFNULL(AA.SGIG_TPG_ID, 'NULL', AA.SGIG_TPG_ID) = 'NULL' ");
		sb.append("OR 'NULL' = '{1}' )");
		Query[no++] = sb.toString();

		// QUERY 36
		// FIX: LOG_SNO는 autoincrement이므로 쿼리에서 제거
		sb = new StringBuilder();
		sb.append("INSERT INTO BK_LOG_NOF ");
		sb.append("(LOG_TPG_ID, TPG_ID, LOG_DATE, BK_LOG_GUBUN, LOG_TEXT) ");
		sb.append("VALUES (?, ?, NOW(*), 'D', ? )");
		Query[no++] = sb.toString();

		// QUERY 37
		// FIX: LOG_SNO는 autoincrement이므로 쿼리에서 제거
		sb = new StringBuilder();
		sb.append("INSERT INTO BK_LOG_NOF ");
		sb.append("(LOG_TPG_ID, TPG_ID, LOG_DATE, BK_LOG_GUBUN, DJ_NO,LOG_TEXT) ");
		sb.append("VALUES (?, ?, NOW(*), ?, ?, ?)");
		Query[no++] = sb.toString();

		// QUERY 38
		sb = new StringBuilder();
		sb.append("UPDATE BK_SGI_NOF ");
		sb.append("SET VT_YN = ?, ");
		sb.append("VT_DATE = NOW(*), ");
		sb.append("VT_METHOD = ?, ");
		sb.append("VT_TPG_ID = ?, ");
		sb.append("SEAL_CD = ?, ");
		sb.append("SEAL_IMG = ?, ");
		sb.append("SEAL_POINT = ?, ");
		sb.append("ISSUED_LOCATION = ?, ");
		sb.append("ISSUE_CNT = IFNULL(ISSUE_CNT, 0, ISSUE_CNT) + 1 ");
		sb.append("WHERE DJ_NO = ? ");
		sb.append("AND TPG_ID = ? ");
		sb.append("AND VT_YN = 'Y' ");
		sb.append("AND ( IFNULL(SGIG_TPG_ID, 'NULL', SGIG_TPG_ID) = '{0}' ");
		sb.append("OR IFNULL(SGIG_TPG_ID, 'NULL', SGIG_TPG_ID) = 'NULL' ");
		sb.append("OR 'NULL' = '{1}' )");
		Query[no++] = sb.toString();

		// QUERY 39
		// FIX: 주석된 쿼리대신 수정된 쿼리로 대체
		sb = new StringBuilder();
		sb.append("UPDATE BK_SGI_NOF ");
		sb.append("SET VT_YN = 'Y', ");
		sb.append("VT_DATE = null, ");
		sb.append("VT_METHOD = null, ");
		// sb.append("VT_TPG_ID=CASE WHEN TPG_ID_YN = 'N' THEN  null ELSE VT_TPG_ID END , ");
		sb.append("VT_TPG_ID=CASE WHEN ISNULL(TPG_ID_YN,'N') = 'N' THEN  null ELSE VT_TPG_ID END , ");
		sb.append("SEAL_CD = null, ");
		sb.append("SEAL_IMG = null, ");
		sb.append("SEAL_POINT = null, ");
		sb.append("ISSUED_LOCATION = null, ");
		sb.append("ISSUE_CNT = null ");
		sb.append("WHERE DJ_NO = ? ");
		sb.append("AND TPG_ID = ? ");
		sb.append("AND VT_YN <> 'Y' ");
		sb.append("AND ( IFNULL(SGIG_TPG_ID, 'NULL', SGIG_TPG_ID) = '{0}' ");
		sb.append("OR IFNULL(SGIG_TPG_ID, 'NULL', SGIG_TPG_ID) = 'NULL' ");
		sb.append("OR 'NULL' = '{1}' )");
		Query[no++] = sb.toString();

		// QUERY 40
		sb = new StringBuilder();
		sb.append("SELECT (SELECT COUNT(*) ");
		sb.append("FROM BK_SGI_NOF) VTR_ALL, ");
		sb.append("(SELECT COUNT(*) ");
		sb.append("FROM BK_SGI_NOF ");
		sb.append("WHERE VT_YN = 'C') VT_CCNT, ");
		sb.append("(SELECT COUNT(*) ");
		sb.append("FROM BK_SGI_NOF ");
		sb.append("WHERE VT_YN = 'P') VT_PCNT, ");
		sb.append("(SELECT COUNT(*) ");
		sb.append("FROM BK_SGI_NOF ");
		sb.append("WHERE VT_YN = 'N') VT_NCNT");
		Query[no++] = sb.toString();

		// QUERY 41
		sb = new StringBuilder();
		sb.append("SELECT DJ_NO, ADDR, NAME, ");
		sb.append("SUBSTRING(JUMIN_NO, 1, 2) + '/' + ");
		sb.append("SUBSTRING(JUMIN_NO, 3, 2) + '/' + ");
		sb.append("SUBSTRING(JUMIN_NO, 5, 2) BIRTH_DATE, ");
		sb.append("ETC, ");
		sb.append("CASE WHEN VT_YN = 'Y' THEN '' ELSE DATEFORMAT(VT_DATE, 'YYYY/MM/DD HH:NN:SS') END VT_YN, ");
		sb.append("NOW(*) NOW ");
		sb.append("FROM BK_SGI_NOF ");
		sb.append("WHERE DJ_NO >= ? ");
		sb.append("AND DJ_NO <= ? ");
		sb.append("ORDER BY DJ_NO");
		Query[no++] = sb.toString();

		// QUERY 42
		sb = new StringBuilder();
		sb.append("SELECT (SELECT COUNT(*) FROM BK_SGI_NOF) TOTAL_SGI, ");
		sb.append("(SELECT COUNT(*) FROM BK_SGI_NOF ");
		sb.append("WHERE VT_TPG_ID = ?) TOTAL_SGI_TPG, ");
		sb.append("(SELECT COUNT(*) FROM BK_SGI_NOF ");
		sb.append("WHERE VT_YN <> 'Y') TOTAL_ISSUE, ");
		sb.append("(SELECT COUNT(*) FROM BK_SGI_NOF ");
		sb.append("WHERE VT_YN = 'C' ");
		sb.append("AND VT_TPG_ID = ?) TPG_VCARD_ISSUE, ");
		sb.append("(SELECT COUNT(*) FROM BK_SGI_NOF ");
		sb.append("WHERE VT_YN = 'P' ");
		sb.append("AND VT_TPG_ID = ?) TPG_VPAPER_ISSUE, ");
		sb.append("(SELECT COUNT(*) FROM BK_SGI_NOF ");
		sb.append("WHERE VT_YN = 'N' ");
		sb.append("AND VT_TPG_ID = ?) TPG_ETC_ISSUE");
		Query[no++] = sb.toString();

		// QUERY 43
		// FIX: 주석된 쿼리대신 수정된 쿼리로 대체
		sb = new StringBuilder();
		// sb.append("SELECT   A.TPG_ID, (select tpg_name from en_tpg where A.tpg_id=tpg_id) as TPG_NAME, GUBUN, ");
		// sb.append("A.GUBUN_SGI_CNT, ");
		// sb.append("A.GUBUN_ISS_CNT, STR(CAST(A.GUBUN_ISS_CNT AS NUMERIC)/CAST(A.GUBUN_SGI_CNT AS NUMERIC) * 100,6,2) + ' %'  AS GUBUN_ISS_RATIO ");
		// sb.append("FROM ( ");
		// sb.append("SELECT  TPG_ID, GUBUN, ");
		// sb.append("CASE WHEN GUBUN = '남' THEN 1 ELSE ");
		// sb.append("CASE WHEN GUBUN = '여' THEN 2 ELSE 3 END END GUBUN_ORDER, ");
		// sb.append("SUM(SGI_CNT) GUBUN_SGI_CNT, ");
		// sb.append("SUM(ISSUE_CNT) GUBUN_ISS_CNT ");
		// sb.append("FROM ( ");
		// sb.append("SELECT  TPG_ID, ");
		// sb.append("CASE WHEN MOD(CAST(SUBSTRING(JUMIN_NO, 7, 1) AS INTEGER), 2) = 1 ");
		// sb.append("THEN '남' ELSE '여' END GUBUN, ");
		// sb.append("CASE WHEN NOT VT_YN = 'Y' AND VT_TPG_ID = ? THEN 1 ");
		// sb.append("ELSE 0 END AS ISSUE_CNT, ");
		// sb.append("CASE WHEN VT_YN = 'Y' THEN 1 ");
		// sb.append("ELSE 1 END AS SGI_CNT ");
		// sb.append("FROM BK_SGI_NOF ");
		// sb.append(") M ");
		// sb.append("GROUP BY ROLLUP (TPG_ID, GUBUN) ");
		// sb.append(") A ");
		// sb.append("LEFT OUTER JOIN EN_TPG B ON A.TPG_ID = B.TPG_ID ");
		// sb.append("WHERE NOT A.GUBUN IS NULL AND A.TPG_ID IN (SELECT TPG_ID FROM EN_TPG WHERE PRE_YN = 'N') ");
		// sb.append("ORDER BY A.TPG_ID DESC, A.GUBUN_ORDER");
		sb.append("SELECT   A.TPG_ID AS TPG_ID, ISNULL(A.TPG_NAME,'') AS TPG_NAME, GUBUN AS GUBUN, ");
		sb.append("A.GUBUN_SGI_CNT AS GUBUN_SGI_CNT, ");
		sb.append("A.GUBUN_ISS_CNT AS GUBUN_ISS_CNT, STR(CAST(A.GUBUN_ISS_CNT AS NUMERIC)/CAST(A.GUBUN_SGI_CNT AS NUMERIC) * 100,6,2) + ' %'  AS GUBUN_ISS_RATIO ");
		sb.append("FROM ( ");
		sb.append("SELECT  M.TPG_ID, M.TPG_NAME, GUBUN, ");
		sb.append("CASE WHEN GUBUN = '남' THEN 1 ELSE ");
		sb.append("CASE WHEN GUBUN = '여' THEN 2 ELSE 3 END END GUBUN_ORDER, ");
		sb.append("SUM(M.SGI_CNT) GUBUN_SGI_CNT, ");
		sb.append("SUM(M.ISSUE_CNT) GUBUN_ISS_CNT ");
		sb.append("FROM ( ");
		sb.append("SELECT  TPG_ID, ");
		sb.append("(SELECT TPG_NAME FROM EN_TPG WHERE TPG_ID=M.TPG_ID) AS TPG_NAME, ");
		sb.append("CASE WHEN MOD(CAST(SUBSTRING(JUMIN_NO, 7, 1) AS INTEGER), 2) = 1 ");
		sb.append("THEN '남' ELSE '여' END GUBUN, ");
		sb.append("CASE WHEN NOT VT_YN = 'Y' AND VT_TPG_ID = ? THEN 1 ");
		sb.append("ELSE 0 END AS ISSUE_CNT, ");
		sb.append("CASE WHEN VT_YN = 'Y' THEN 1 ");
		sb.append("ELSE 1 END AS SGI_CNT ");
		sb.append("FROM BK_SGI_NOF ");
		sb.append(") M (TPG_ID, TPG_NAME, GUBUN, ISSUE_CNT, SGI_CNT) ");
		sb.append("GROUP BY M.TPG_ID, M.TPG_NAME, M.GUBUN ");
		sb.append(") A (TPG_ID, TPG_NAME, GUBUN, GUBUN_ORDER, GUBUN_SGI_CNT, GUBUN_ISS_CNT) ");
		sb.append("LEFT OUTER JOIN EN_TPG B ON A.TPG_ID = B.TPG_ID ");
		sb.append("WHERE NOT A.GUBUN IS NULL AND A.TPG_ID IN (SELECT TPG_ID FROM EN_TPG WHERE PRE_YN = 'N') ");
		sb.append("ORDER BY A.TPG_ID DESC, A.GUBUN_ORDER ");
		Query[no++] = sb.toString();

		// QUERY 44
		// FIX: 주석된 쿼리대신 수정된 쿼리로 대체
		sb = new StringBuilder();
		sb.append("SELECT TPG_ID, VT_TPG_ID, VT_YN,");
		sb.append("CAST(DATEFORMAT(NOW(*), 'YYYY') AS INTEGER) - ");
		sb.append(" CAST(CASE WHEN CAST(SUBSTRING(JUMIN_NO, 7, 1) AS INTEGER) < 3 ");
		sb.append("THEN '19' ELSE '20' END + SUBSTRING(JUMIN_NO, 1, 2) AS INTEGER)  AGE, ");
		sb.append(" CASE WHEN              AGE < 20 THEN '20세 미만' ELSE ");
		sb.append(" CASE WHEN AGE >=20 AND AGE < 30 THEN '20대'  ELSE ");
		sb.append(" CASE WHEN AGE >=30 AND AGE < 40 THEN '30대'  ELSE ");
		sb.append(" CASE WHEN AGE >=40 AND AGE < 50 THEN '40대' ELSE ");
		sb.append(" CASE WHEN AGE >=50 AND AGE < 60 THEN '50대' ELSE ");
		sb.append("  CASE WHEN AGE >=60              THEN '60세 이상' END END END END END END GUBUN, ");
		sb.append(" CASE WHEN GUBUN = '20세 미만' THEN 1 ");
		sb.append("  WHEN GUBUN = '20대' THEN 2 ");
		sb.append("   WHEN GUBUN = '30대' THEN 3 ");
		sb.append("    WHEN GUBUN = '40대' THEN 4 ");
		sb.append("   WHEN GUBUN = '50대' THEN 5 ");
		sb.append("  WHEN GUBUN = '60세 이상' THEN 6 ELSE 7  END GUBUN_ORDER ");
		sb.append(" FROM BK_SGI_NOF");
		// sb.append("SELECT   A.TPG_ID, (select tpg_name from en_tpg where A.tpg_id=tpg_id) as TPG_NAME, GUBUN, ");
		// sb.append("A.GUBUN_SGI_CNT, ");
		// sb.append("A.GUBUN_ISS_CNT , ");
		// sb.append("STR(CAST(A.GUBUN_ISS_CNT AS NUMERIC)/CAST(A.GUBUN_SGI_CNT AS NUMERIC) * 100,6,2) + ' %'  AS GUBUN_ISS_RATIO ");
		// sb.append("FROM ( ");
		// sb.append("SELECT  TPG_ID, GUBUN, ");
		// sb.append("CASE WHEN GUBUN = '20세 미만' THEN 1 ");
		// sb.append("WHEN GUBUN = '20대' THEN 2 ");
		// sb.append("WHEN GUBUN = '30대' THEN 3 ");
		// sb.append("WHEN GUBUN = '40대' THEN 4 ");
		// sb.append("WHEN GUBUN = '50대' THEN 5 ");
		// sb.append("WHEN GUBUN = '60세 이상' THEN 6 ELSE 7  END GUBUN_ORDER, ");
		// sb.append("SUM(SGI_CNT) GUBUN_SGI_CNT, ");
		// sb.append("SUM(ISSUE_CNT) GUBUN_ISS_CNT ");
		// sb.append("FROM ( ");
		// sb.append("SELECT  TPG_ID, ");
		// sb.append("CASE WHEN              AGE < 20 THEN '20세 미만' ELSE ");
		// sb.append("CASE WHEN AGE >=20 AND AGE < 30 THEN '20대'  ELSE ");
		// sb.append("CASE WHEN AGE >=30 AND AGE < 40 THEN '30대'  ELSE ");
		// sb.append("CASE WHEN AGE >=40 AND AGE < 50 THEN '40대' ELSE ");
		// sb.append("CASE WHEN AGE >=50 AND AGE < 60 THEN '50대' ELSE ");
		// sb.append("CASE WHEN AGE >=60              THEN '60세 이상' END END END END END END GUBUN, ");
		// sb.append("CAST(DATEFORMAT(NOW(*), 'YYYY') AS INTEGER) - ");
		// sb.append("CAST(CASE WHEN CAST(SUBSTRING(JUMIN_NO, 7, 1) AS INTEGER) < 3 ");
		// sb.append("THEN '19' ELSE '20' END + SUBSTRING(JUMIN_NO, 1, 2) AS INTEGER)  AGE, ");
		// sb.append("CASE WHEN NOT VT_YN = 'Y' AND VT_TPG_ID = ? THEN 1 ");
		// sb.append("ELSE 0 END AS ISSUE_CNT, ");
		// sb.append("CASE WHEN VT_YN = 'Y'                   THEN 1 ");
		// sb.append("ELSE 1 END AS SGI_CNT ");
		// sb.append("FROM BK_SGI_NOF) M ");
		// sb.append("GROUP BY ROLLUP (TPG_ID, GUBUN) ");
		// sb.append(") A ");
		// sb.append("LEFT OUTER JOIN EN_TPG B ON A.TPG_ID = B.TPG_ID ");
		// sb.append("WHERE NOT A.GUBUN IS NULL AND A.TPG_ID IN (SELECT TPG_ID FROM EN_TPG WHERE PRE_YN = 'N') ");
		// sb.append("ORDER BY A.TPG_ID DESC, A.GUBUN_ORDER");
//		sb.append("SELECT   A.TPG_ID AS TPG_ID, ISNULL(A.TPG_NAME,'') AS TPG_NAME, GUBUN AS GUBUN, ");
//		sb.append("A.GUBUN_SGI_CNT AS GUBUN_SGI_CNT, ");
//		sb.append("A.GUBUN_ISS_CNT AS GUBUN_ISS_CNT , ");
//		sb.append("STR(CAST(A.GUBUN_ISS_CNT AS NUMERIC)/CAST(A.GUBUN_SGI_CNT AS NUMERIC) * 100,6,2) + ' %'  AS GUBUN_ISS_RATIO ");
//		sb.append("FROM ( ");
//		sb.append("SELECT  M.TPG_ID, M.TPG_NAME, GUBUN, ");
//		sb.append("CASE WHEN GUBUN = '20세 미만' THEN 1 ");
//		sb.append("WHEN GUBUN = '20대' THEN 2 ");
//		sb.append("WHEN GUBUN = '30대' THEN 3 ");
//		sb.append("WHEN GUBUN = '40대' THEN 4 ");
//		sb.append("WHEN GUBUN = '50대' THEN 5 ");
//		sb.append("WHEN GUBUN = '60세 이상' THEN 6 ELSE 7  END GUBUN_ORDER, ");
//		sb.append("SUM(M.SGI_CNT) GUBUN_SGI_CNT, ");
//		sb.append("SUM(M.ISSUE_CNT) GUBUN_ISS_CNT ");
//		sb.append("FROM ( ");
//		sb.append("SELECT  TPG_ID, ");
//		sb.append("(SELECT TPG_NAME FROM EN_TPG WHERE TPG_ID=A.TPG_ID), ");
//		sb.append("CAST(DATEFORMAT(NOW(*), 'YYYY') AS INTEGER) - ");
//		sb.append("CAST(CASE WHEN CAST(SUBSTRING(JUMIN_NO, 7, 1) AS INTEGER) < 3 ");
//		sb.append("THEN '19' ELSE '20' END + SUBSTRING(JUMIN_NO, 1, 2) AS INTEGER)  AGE, ");
//		sb.append("CASE WHEN              AGE < 20 THEN '20세 미만' ELSE ");
//		sb.append("CASE WHEN AGE >=20 AND AGE < 30 THEN '20대'  ELSE ");
//		sb.append("CASE WHEN AGE >=30 AND AGE < 40 THEN '30대'  ELSE ");
//		sb.append("CASE WHEN AGE >=40 AND AGE < 50 THEN '40대' ELSE ");
//		sb.append("CASE WHEN AGE >=50 AND AGE < 60 THEN '50대' ELSE ");
//		sb.append("CASE WHEN AGE >=60              THEN '60세 이상' END END END END END END GUBUN, ");
//		sb.append("CASE WHEN NOT VT_YN = 'Y' AND VT_TPG_ID = ? THEN 1 ");
//		sb.append("ELSE 0 END AS ISSUE_CNT, ");
//		sb.append("CASE WHEN VT_YN = 'Y' THEN 1 ");
//		sb.append("ELSE 1 END AS SGI_CNT ");
//		sb.append("FROM BK_SGI_NOF) M (TPG_ID, TPG_NAME, AGE, GUBUN, ISSUE_CNT, SGI_CNT) ");
//		sb.append("GROUP BY M.TPG_ID, M.TPG_NAME, M.GUBUN ");
//		sb.append(") A (TPG_ID, TPG_NAME, GUBUN, GUBUN_ORDER, GUBUN_SGI_CNT, GUBUN_ISS_CNT) ");
//		sb.append("LEFT OUTER JOIN EN_TPG B ON A.TPG_ID = B.TPG_ID ");
//		sb.append("WHERE NOT A.GUBUN IS NULL AND A.TPG_ID IN (SELECT TPG_ID FROM EN_TPG WHERE PRE_YN = 'N') ");
//		sb.append("ORDER BY A.TPG_ID DESC, A.GUBUN_ORDER");
		Query[no++] = sb.toString();

		// QUERY 45
		// FIX: 주석된 쿼리대신 수정된 쿼리로 대체
		sb = new StringBuilder();
		sb.append("SELECT TPG_ID, VT_YN, VT_TPG_ID, ");
		sb.append("CAST(DATEFORMAT(VT_DATE, 'HH') AS INTEGER) UPDATE_TIME, ");
		sb.append("CASE WHEN  UPDATE_TIME < 10 THEN '10시 이전' ELSE  ");
		sb.append("CASE WHEN  UPDATE_TIME = 10 THEN '10시 ~ 11시 사이' ELSE  ");
		sb.append("                     CASE WHEN  UPDATE_TIME = 11 THEN '11시 ~ 12시 사이' ELSE  ");
		sb.append("  CASE WHEN  UPDATE_TIME = 12 THEN '12시 ~ 13시 사이' ELSE  ");
		sb.append("                     CASE WHEN  UPDATE_TIME = 13 THEN '13시 ~ 14시 사이' ELSE  ");
		sb.append("                     CASE WHEN  UPDATE_TIME = 14 THEN '14시 ~ 15시 사이' ELSE  ");
		sb.append("  CASE WHEN  UPDATE_TIME = 15 THEN '15시 ~ 16시 사이' ELSE  ");
		sb.append("                     CASE WHEN  UPDATE_TIME = 16 THEN '16시 ~ 17시 사이' ELSE  ");
		sb.append("  CASE WHEN  UPDATE_TIME >= 17 THEN '17시 이후' END END END END END END END END END GUBUN,");
		sb.append("      CASE WHEN  UPDATE_TIME < 10 THEN 1 ELSE ");
		sb.append("CASE WHEN  UPDATE_TIME = 10 THEN 2 ELSE ");
		sb.append("CASE WHEN  UPDATE_TIME = 11 THEN 3 ELSE ");
		sb.append("CASE WHEN  UPDATE_TIME = 12 THEN 4 ELSE ");
		sb.append("CASE WHEN  UPDATE_TIME = 13 THEN 5 ELSE ");
		sb.append("CASE WHEN  UPDATE_TIME = 14 THEN 6 ELSE ");
		sb.append("CASE WHEN  UPDATE_TIME = 15 THEN 7 ELSE ");
		sb.append("CASE WHEN  UPDATE_TIME = 16 THEN 8 ELSE ");
		sb.append("CASE WHEN  UPDATE_TIME >= 17 THEN 9 END END END END END END END END END GUBUN_ORDER,   (SELECT COUNT(*) FROM BK_SGI_NOF) AS SGI_TOTAL ");
		sb.append("FROM BK_SGI_NOF ");;
		// sb.append("SELECT   A.TPG_ID, (select tpg_name from en_tpg where A.tpg_id=tpg_id) as TPG_NAME, GUBUN, ");
		// sb.append("A.GUBUN_ISS_CNT , ");
		// sb.append("(SELECT COUNT(*) FROM BK_SGI_NOF) SGI_TOTAL, ");
		// sb.append("STR(CAST(A.GUBUN_ISS_CNT AS NUMERIC)/CAST(SGI_TOTAL AS NUMERIC) * 100,6,2) + ' %'  AS GUBUN_ISS_RATIO ");
		// sb.append("FROM ( SELECT   TPG_ID, GUBUN, ");
		// sb.append("CASE ");
		// sb.append("WHEN GUBUN = '10시 이전'        THEN 1 ");
		// sb.append("WHEN GUBUN = '10시 ~ 11시 사이' THEN 2 ");
		// sb.append("WHEN GUBUN = '11시 ~ 12시 사이' THEN 3 ");
		// sb.append("WHEN GUBUN = '12시 ~ 13시 사이' THEN 4 ");
		// sb.append("WHEN GUBUN = '13시 ~ 14시 사이' THEN 5 ");
		// sb.append("WHEN GUBUN = '14시 ~ 15시 사이' THEN 6 ");
		// sb.append("WHEN GUBUN = '15시 ~ 16시 사이' THEN 7 ");
		// sb.append("WHEN GUBUN = '16시 ~ 17시 사이' THEN 8 ");
		// sb.append("WHEN GUBUN = '17시 이후'        THEN 9 ELSE 10  END GUBUN_ORDER, ");
		// sb.append("SUM(ISSUE_CNT) GUBUN_ISS_CNT ");
		// sb.append("FROM ( SELECT  TPG_ID, ");
		// sb.append("CASE WHEN  UPDATE_TIME < 10 THEN '10시 이전'        ELSE ");
		// sb.append("CASE WHEN  UPDATE_TIME = 10 THEN '10시 ~ 11시 사이' ELSE ");
		// sb.append("CASE WHEN  UPDATE_TIME = 11 THEN '11시 ~ 12시 사이' ELSE ");
		// sb.append("CASE WHEN  UPDATE_TIME = 12 THEN '12시 ~ 13시 사이' ELSE ");
		// sb.append("CASE WHEN  UPDATE_TIME = 13 THEN '13시 ~ 14시 사이' ELSE ");
		// sb.append("CASE WHEN  UPDATE_TIME = 14 THEN '14시 ~ 15시 사이' ELSE ");
		// sb.append("CASE WHEN  UPDATE_TIME = 15 THEN '15시 ~ 16시 사이' ELSE ");
		// sb.append("CASE WHEN  UPDATE_TIME = 16 THEN '16시 ~ 17시 사이' ELSE ");
		// sb.append("CASE WHEN  UPDATE_TIME >= 17 THEN '17시 이후' END END END END END END END END END GUBUN, ");
		// sb.append("CAST(DATEFORMAT(VT_DATE, 'HH') AS INTEGER) UPDATE_TIME, ");
		// sb.append("CASE WHEN NOT VT_YN = 'Y' AND VT_TPG_ID = ? THEN 1 ");
		// sb.append("ELSE 0 END AS ISSUE_CNT ");
		// sb.append("FROM BK_SGI_NOF) M ");
		// sb.append("GROUP BY ROLLUP (TPG_ID, GUBUN) ");
		// sb.append(") A ");
		// sb.append("LEFT OUTER JOIN EN_TPG B ON A.TPG_ID = B.TPG_ID ");
		// sb.append("WHERE NOT A.GUBUN IS NULL AND A.TPG_ID IN (SELECT TPG_ID FROM EN_TPG WHERE PRE_YN = 'N') ");
		// sb.append("ORDER BY A.TPG_ID DESC, A.GUBUN_ORDER");
//		sb.append("SELECT   A.TPG_ID AS TPG_ID, ISNULL(A.TPG_NAME,'') AS TPG_NAME, GUBUN AS GUBUN, ");
//		sb.append("A.GUBUN_ISS_CNT AS GUBUN_ISS_CNT , ");
//		sb.append("(SELECT COUNT(*) FROM BK_SGI_NOF) AS SGI_TOTAL, ");
//		sb.append("STR(CAST(A.GUBUN_ISS_CNT AS NUMERIC)/CAST(SGI_TOTAL AS NUMERIC) * 100,6,2) + ' %'  AS GUBUN_ISS_RATIO ");
//		sb.append("FROM ( ");
//		sb.append("SELECT   M.TPG_ID, TPG_NAME, GUBUN, ");
//		sb.append("CASE ");
//		sb.append("WHEN GUBUN = '10시 이전'        THEN 1 ");
//		sb.append("WHEN GUBUN = '10시 ~ 11시 사이' THEN 2 ");
//		sb.append("WHEN GUBUN = '11시 ~ 12시 사이' THEN 3 ");
//		sb.append("WHEN GUBUN = '12시 ~ 13시 사이' THEN 4 ");
//		sb.append("WHEN GUBUN = '13시 ~ 14시 사이' THEN 5 ");
//		sb.append("WHEN GUBUN = '14시 ~ 15시 사이' THEN 6 ");
//		sb.append("WHEN GUBUN = '15시 ~ 16시 사이' THEN 7 ");
//		sb.append("WHEN GUBUN = '16시 ~ 17시 사이' THEN 8 ");
//		sb.append("WHEN GUBUN = '17시 이후'        THEN 9 ELSE 10  END GUBUN_ORDER, ");
//		sb.append("SUM(M.ISSUE_CNT) GUBUN_ISS_CNT ");
//		sb.append("FROM ( ");
//		sb.append("SELECT  TPG_ID, ");
//		sb.append("(SELECT TPG_NAME FROM EN_TPG WHERE TPG_ID = M.TPG_ID) AS TPG_NAME, ");
//		sb.append("CAST(DATEFORMAT(VT_DATE, 'HH') AS INTEGER) UPDATE_TIME, ");
//		sb.append("CASE WHEN  UPDATE_TIME < 10 THEN '10시 이전'        ELSE ");
//		sb.append("CASE WHEN  UPDATE_TIME = 10 THEN '10시 ~ 11시 사이' ELSE ");
//		sb.append("CASE WHEN  UPDATE_TIME = 11 THEN '11시 ~ 12시 사이' ELSE ");
//		sb.append("CASE WHEN  UPDATE_TIME = 12 THEN '12시 ~ 13시 사이' ELSE ");
//		sb.append("CASE WHEN  UPDATE_TIME = 13 THEN '13시 ~ 14시 사이' ELSE ");
//		sb.append("CASE WHEN  UPDATE_TIME = 14 THEN '14시 ~ 15시 사이' ELSE ");
//		sb.append("CASE WHEN  UPDATE_TIME = 15 THEN '15시 ~ 16시 사이' ELSE ");
//		sb.append("CASE WHEN  UPDATE_TIME = 16 THEN '16시 ~ 17시 사이' ELSE ");
//		sb.append("CASE WHEN  UPDATE_TIME >= 17 THEN '17시 이후' END END END END END END END END END GUBUN, ");
//		sb.append("CASE WHEN NOT VT_YN = 'Y' AND VT_TPG_ID =? THEN 1 ");
//		sb.append("ELSE 0 END AS ISSUE_CNT ");
//		sb.append("FROM BK_SGI_NOF ");
//		sb.append(") M (TPG_ID, TPG_NAME, UPDATE_TIME, GUBUN, ISSUE_CNT) ");
//		sb.append("GROUP BY M.TPG_ID, TPG_NAME, GUBUN ");
//		sb.append(") A (TPG_ID, TPG_NAME, GUBUN, GUBUN_ORDER, GUBUN_ISS_CNT) ");
//		sb.append("LEFT OUTER JOIN EN_TPG B ON A.TPG_ID = B.TPG_ID ");
//		sb.append("WHERE NOT A.GUBUN IS NULL AND A.TPG_ID IN (SELECT TPG_ID FROM EN_TPG WHERE PRE_YN = 'N') ");
//		sb.append("ORDER BY A.TPG_ID DESC, A.GUBUN_ORDER");
		
		Query[no++] = sb.toString();

		// QUERY 46
		sb = new StringBuilder();
		sb.append("SELECT TPG_NAME FROM EN_TPG WHERE TPG_ID = ?");
		Query[no++] = sb.toString();

		// QUERY 47
		sb = new StringBuilder();
		sb.append("SELECT STATUS FROM VT_TPG WHERE TPG_ID = ? AND VT_DATE = DATE(?)");
		Query[no++] = sb.toString();

		// QUERY 48
		sb = new StringBuilder();
		sb.append("SELECT 'DN' DN, ");
		sb.append("TPG_ID, ");
		sb.append("DATEFORMAT(VT_DATE, 'YYYYMMDD') VT_DATE, ");
		sb.append("STATUS, ");
		sb.append("DATEFORMAT(START_DATE, 'HHNNSS') START_TIME, ");
		sb.append("DATEFORMAT(NOW(*), 'HHNNSS') CLOSE_TIME, ");
		sb.append("ISNULL(VTR_ALL, 0) VTR_ALL, ");
		sb.append("ISNULL(VTR_ABS, 0) VTR_ABS, ");
		sb.append("ISNULL(BK_MCNT, 0) BK_MCNT, ");
		sb.append("ISNULL(VT_MCNT, 0) VT_MCNT, ");
		sb.append("ISNULL(VT_XCNT, 0) VT_XCNT, ");
		sb.append("ISNULL(VT_PCNT, 0) VT_PCNT, ");
		sb.append("ISNULL(VT_CCNT, 0) VT_CCNT, ");
		sb.append("ISNULL(VT_DCNT, 0) VT_DCNT, ");
		sb.append("ISNULL(VT_ECNT, 0) VT_ECNT, ");
		sb.append("ISNULL(NVT_CCNT, 0) NVT_CCNT ");
		sb.append("FROM VT_TPG ");
		sb.append("WHERE TPG_ID = ? ");
		sb.append("AND VT_DATE = DATE(?)");
		Query[no++] = sb.toString();

		// QUERY 49
		sb = new StringBuilder();
		sb.append("SELECT COUNT(*) VT_CCNT ");
		sb.append("FROM BK_SGI_NOF ");
		sb.append("WHERE VT_TPG_ID = ? ");
		sb.append("AND VT_YN <> 'Y'");
		Query[no++] = sb.toString();

		// QUERY 50
		sb = new StringBuilder();
		sb.append("SELECT COUNT(*) NVT_CCNT ");
		sb.append("FROM BK_LOG_NOF ");
		sb.append("WHERE BK_LOG_GUBUN = 'D' ");
		sb.append("AND LOG_TPG_ID = ?");
		Query[no++] = sb.toString();

		// QUERY 51
		sb = new StringBuilder();
		sb.append("UPDATE VT_TPG ");
		sb.append("SET STATUS = 'C', ");
		sb.append("CLOSE_DATE = ?, ");
		sb.append("BK_MCNT = ?, ");
		sb.append("VT_MCNT = ?, ");
		sb.append("VT_XCNT = ?, ");
		sb.append("VT_PCNT = ?, ");
		sb.append("VT_CCNT = ?, ");
		sb.append("VT_ECNT = ?, ");
		sb.append("NVT_CCNT = ? ");
		sb.append("WHERE TPG_ID = ? ");
		sb.append("AND VT_DATE = DATE(?) ");
		sb.append("AND STATUS = 'B'");
		Query[no++] = sb.toString();

		// QUERY 52
		sb = new StringBuilder();
		sb.append("SELECT STATUS ");
		sb.append("FROM VT_TPG ");
		sb.append("WHERE TPG_ID = ? ");
		sb.append("AND VT_DATE = DATE(?)");
		Query[no++] = sb.toString();

		// QUERY 53
		sb = new StringBuilder();
		sb.append("SELECT NOW(*) NOW, COUNT(*) VT_CCNT ");
		sb.append("FROM BK_SGI_NOF ");
		sb.append("WHERE VT_TPG_ID = ? AND VT_YN <> 'Y'");
		Query[no++] = sb.toString();

		// QUERY 54
		sb = new StringBuilder();
		sb.append("UPDATE VT_TPG ");
		sb.append("SET STATUS = 'B', ");
		sb.append("START_DATE = ?, ");
		sb.append("BK_MCNT = ?, ");
		sb.append("VT_MCNT = ? ");
		sb.append("WHERE TPG_ID = ? ");
		sb.append("AND VT_DATE = DATE(?) ");
		sb.append("AND STATUS = 'A'");
		Query[no++] = sb.toString();

		// QUERY 55
		sb = new StringBuilder();
		sb.append("");
		Query[no++] = sb.toString();

		// QUERY 56
		sb = new StringBuilder();
		sb.append("SELECT  DISTINCT A.SGG_ID           SGG_ID, ");
		sb.append("A.SGG_NAME        SGG_NAME,   ISNULL(A.SGG_BPS_NAME,'')    SGG_BPS_NAME, ");
		sb.append("K.FOR_AGAINST   FOR_AGAINST ");
		sb.append("FROM    EN_SGG  A, ");
		sb.append("EN_SG   B, ");
		sb.append("EN_SGG_SKIN   K ");
		sb.append("WHERE   SUBSTR(A.SGG_ID, 1, 1) = B.SG_ID ");
		sb.append("AND K.SGG_ID = A.SGG_ID");
		Query[no++] = sb.toString();

		// QUERY 57
		sb = new StringBuilder();
		sb.append("SELECT  A.HBJ_GIHO  HBJ_GIHO, ");
		sb.append("ISNULL(A.HBJ_NAME, '')  HBJ_NAME, ");
		sb.append("A.SGG_ID    SGG_ID, ");
		sb.append("ISNULL(A.cancel_cd, '0') cancel_cd, ");
		sb.append("ISNULL(K.for_against,'0') for_against, ");
		sb.append("ISNULL(B.JD_NAME, '') JD_NAME ");
		sb.append("FROM    EN_SGG_SKIN K, EN_HBJ A  LEFT OUTER JOIN EN_JD B ON A.JD_ID=B.JD_ID ");
		sb.append("WHERE   K.SGG_ID = A.SGG_ID AND A.SGG_ID = ? ");
		sb.append("ORDER BY A.HBJ_ORDER");
		Query[no++] = sb.toString();

		// QUERY 58
		sb = new StringBuilder();
		sb.append("SELECT ISNULL(MAX(PRT_SN), 0) PRT_SN FROM BK_SGG_PRT_SN WHERE SGG_ID = ?");
		Query[no++] = sb.toString();

		// QUERY 59
		sb = new StringBuilder();
		sb.append("Select * From EN_SG_PRINTER Where SGG_ID = ?");
		Query[no++] = sb.toString();

		// QUERY 60
		sb = new StringBuilder();
		sb.append("INSERT INTO BK_SGG_PRT_SN(SGG_ID, PRT_SN) VALUES (?, ?)");
		Query[no++] = sb.toString();

		// QUERY 61
		sb = new StringBuilder();
		sb.append("SELECT distinct d.hbj_giho, f.sg_order, ");
		sb.append("a.tpg_id tpg_id, ");
		sb.append("c.sgg_id sgg_id, ");
		sb.append("d.hbj_id hbj_id, ");
		sb.append("d.hbj_name hbj_name, ");
		sb.append("ISNULL(d.hbj_order, 1) hbj_order, ");
		sb.append("ISNULL(d.cancel_cd, '0') cancel_cd, ");
		sb.append("ISNULL(j.jd_name, '') jd_name, ");
		sb.append("ISNULL(k.for_against,'0') for_against ");
		sb.append("FROM EN_TPG a, EN_SG f, EN_TPG_SGG c, EN_SGG_SKIN k, ");
		sb.append("EN_HBJ d left outer join EN_JD j on d.jd_id=j.jd_id ");
		sb.append("WHERE c.tpg_id = a.tpg_id ");
		sb.append("AND d.sgg_id = c.sgg_id ");
		sb.append("AND f.sg_id = substr(c.sgg_id,1,1) ");
		sb.append("AND k.sgg_id = c.sgg_id ");
		sb.append("AND a.tpg_id = ? ");
		sb.append("ORDER BY sg_order, hbj_order");
		Query[no++] = sb.toString();

		// QUERY 62
		sb = new StringBuilder();
		sb.append("SELECT  a.tpg_id tpg_id, ");
		sb.append("a.tpg_name tpg_name, ");
		sb.append("c.sgg_id sgg_id, ");
		sb.append("e.sgg_name sgg_name, ");
		sb.append("e.sgg_bps_name sgg_bps_name, ");
		sb.append("e.sgg_sname sgg_sname, ");
		sb.append("e.sgg_bps_stamp sgg_bps_stamp ");
		sb.append("FROM EN_TPG a, EN_TPG_SGG c, ");
		sb.append("EN_SG d, ");
		sb.append("EN_SGG e left outer join EN_SGG_SKIN f on f.sgg_id = e.sgg_id ");
		sb.append("WHERE(c.tpg_id = a.tpg_id) ");
		sb.append("and substr(e.sgg_id,1,1) = d.sg_id ");
		sb.append("and e.sgg_id = c.sgg_id ");
		sb.append("and a.tpg_id = ? ");
		sb.append("ORDER BY d.sg_order");
		Query[no++] = sb.toString();

		// QUERY 63
		sb = new StringBuilder();
		sb.append("SELECT  * FROM EN_SG_PRINTER");
		Query[no++] = sb.toString();

		// QUERY 64
		sb = new StringBuilder();
		sb.append("SELECT ISNULL(MAX(PRT_SN), 0) PRT_SN FROM BK_SGG_PRT_SN WHERE SGG_ID = ?");
		Query[no++] = sb.toString();

		// QUERY 65
		sb = new StringBuilder();
		sb.append("SELECT SGG_ID,SGG_NAME,SGG_BPS_STAMP FROM EN_SGG WHERE SGG_ID = ?");
		Query[no++] = sb.toString();

		// QUERY 66
		sb = new StringBuilder();
		sb.append("INSERT INTO BK_SGG_PRT_SN(SGG_ID, PRT_SN) VALUES (?, ?)");
		Query[no++] = sb.toString();

		// QUERY 67
		sb = new StringBuilder();
		sb.append("INSERT INTO BK_SGI_IMG (TPG_ID, DJ_NO, IDCARD_IMG) VALUES (?, ?, ?)");
		Query[no++] = sb.toString();
		
		// QUERY 68
		sb = new StringBuilder();
		sb.append("UPDATE en_tpg set IMG_MGR_STAMP=? where TPG_ID=?");
		Query[no++] = sb.toString();
	
		// QUERY 69
		sb = new StringBuilder();
		sb.append("SELECT IMG_MGR_STAMP FROM EN_TPG WHERE TPG_ID=?");
		Query[no++] = sb.toString();
		// QUERY 70
		sb = new StringBuilder();
		sb.append("SELECT SG_DID, DJ_NO, NAME, JUMIN_NO, REMARK, "+
				"VT_YN, VT_DATE, VT_TPG_ID, ETC, "+          
                 "IFNULL(ISSUE_CNT, 0, ISSUE_CNT) ISSUE_CNT "+
            "FROM BK_SGI_DEMO"+                               
          " WHERE DJ_NO = ? OR NAME LIKE ? OR JUMIN_NO LIKE ?");
		Query[no++] = sb.toString();
		// QUERY 71
		sb = new StringBuilder();
		sb.append("UPDATE BK_SGI_DEMO"+     
           " SET VT_YN = ?,     "+
            "    VT_DATE = ?,"+
             "   VT_METHOD = ?,"+ 
              "  VT_TPG_ID = ?,"+
               " SEAL_CD = ?, "+
                "SEAL_IMG = ?, "+
                "SEAL_POINT = ?, "+
                "ISSUE_CNT = IFNULL(ISSUE_CNT, 0, ISSUE_CNT) + 1"+
          " WHERE DJ_NO = ?");
		Query[no++] = sb.toString();
		// QUERY 72
		sb = new StringBuilder();
		sb.append("SELECT BYTE_LENGTH(A.SEAL_IMG) IMG_LEN,"
				+ "IFNULL(BYTE_LENGTH(A.SEAL_POINT), 0, "
				+ "BYTE_LENGTH(A.SEAL_POINT)) MIN_LEN, "
				+ "A.SEAL_IMG,A.SEAL_CD, A.SEAL_POINT "
				+ "FROM BK_SGI_DEMO A "
				+ "WHERE A.DJ_NO = ?");
		Query[no++] = sb.toString();
		// QUERY 73
		sb = new StringBuilder();
		sb.append("SELECT SG_DID, SG_PLACE, SG_NAME FROM EN_SG_DEMO ORDER BY SG_DID DESC");
		Query[no++] = sb.toString();
		// QUERY 74
		sb = new StringBuilder();
		sb.append("SELECT ISNULL(MAX(DJ_NO), 0) + 1 AS DJ_MAX_NO FROM BK_SGI_DEMO");
		Query[no++] = sb.toString();
		// QUERY 75
		sb = new StringBuilder();
		sb.append("SELECT COUNT(*) DUP FROM BK_SGI_DEMO WHERE JUMIN_NO = ?");
		Query[no++] = sb.toString();
		// QUERY 76
		sb = new StringBuilder();
		sb.append("INSERT INTO BK_SGI_DEMO (DJ_NO, REMARK, NAME, JUMIN_NO, ETC, VT_TPG_ID, SG_DID, VT_YN)"
				+ " VALUES (?, ?, ?, ?, ?, ?, ?, \'Y\')");
		Query[no++] = sb.toString();
		// QUERY 77
		sb = new StringBuilder();
		sb.append("UPDATE BK_SGI_DEMO SET ETC = ?, VT_YN = \'Y\', VT_DATE = NULL, VT_METHOD = NULL, SEAL_CD = NULL, SEAL_IMG = NULL, SEAL_POINT = NULL WHERE DJ_NO = ? AND VT_YN <> \'Y\'");
		Query[no++] = sb.toString();
	}
}
