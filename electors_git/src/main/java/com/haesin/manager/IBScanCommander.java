package com.haesin.manager;

import com.integratedbiometrics.ibscancommon.IBCommon.FingerPosition;
import com.integratedbiometrics.ibscancommon.IBCommon.ImageDataExt;
import com.integratedbiometrics.ibscanultimate.IBScan;
import com.integratedbiometrics.ibscanultimate.IBScanDevice;
import com.integratedbiometrics.ibscanultimate.IBScanDevice.FingerCountState;
import com.integratedbiometrics.ibscanultimate.IBScanDevice.FingerQualityState;
import com.integratedbiometrics.ibscanultimate.IBScanDevice.ImageData;
import com.integratedbiometrics.ibscanultimate.IBScanDevice.ImageResolution;
import com.integratedbiometrics.ibscanultimate.IBScanDevice.ImageType;
import com.integratedbiometrics.ibscanultimate.IBScanDevice.PlatenState;
import com.integratedbiometrics.ibscanultimate.IBScanDevice.PropertyId;
import com.integratedbiometrics.ibscanultimate.IBScanDevice.SegmentPosition;
import com.integratedbiometrics.ibscanultimate.IBScanDeviceListener;
import com.integratedbiometrics.ibscanultimate.IBScanException;
import com.integratedbiometrics.ibscanultimate.IBScanListener;
import com.mirusys.electors.ResourceFactory;
import com.mirusys.electors.ResourceFactory.Log;

import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
public class IBScanCommander implements IBScanDeviceListener, IBScanListener {
	private static final String TAG = "IBScanCommander";
	private static int STARTCAPTURE=0;
	private boolean mListeningInCheck = true;
	private Thread mibThread = null;
	/**
	 * A handle to the open IBScanDevice (if any) that will be the interface for
	 * getting data from the open scanner, including capturing the image
	 * (beginCaptureImage(), cancelCaptureImage()), and the type of image being
	 * captured.
	 */
	private IBScanDevice mibScanDevice;
	private IBScan mibScan;
	private ImageType mImageType = ImageType.TYPE_NONE;

	private OnStateListener mListener = null;

	public IBScanCommander(){
		Context ctx = ResourceFactory.getInstance().getApplicationContext();
		mibScan = IBScan.getInstance(ctx);
		mibScan.setScanListener(IBScanCommander.this);
	}

	public void setOnStateListener(OnStateListener l) {
		mListener = l;
	}

	public void forceOpen() {
		if (mibThread == null) {
			mibThread = new Thread() {
				public void run() {
					Context ctx = ResourceFactory.getInstance().getApplicationContext();
//			        	mibScan.updateUsbPermission(); // USB bus 퍼미션 추가.
						/*
						 * Make sure there are no USB devices attached that are IB scanners for which permission has
						 * not been granted.  For any that are found, request permission; we should receive a
						 * callback when permission is granted or denied and then when IBScan recognizes that
						 * new devices are connected, which will result in another refresh.
						 */
						final UsbManager manager = (UsbManager)ctx.getSystemService(Context.USB_SERVICE);
						for (UsbDevice device : manager.getDeviceList().values()) {
						    if (IBScan.isScanDevice(device) && !manager.hasPermission(device)) {
						    	mibScan.requestPermission(device.getDeviceId());
						    }
						}

						try {
				        	mibScan.enableTraceLog(ResourceFactory.IB_LOG);
							if (mibScan.getDeviceCount() < 1)
								Log.e(TAG, "No avaliable fingerprint device");
							else {
								mibScanDevice = mibScan.openDevice(0);
								if (mibScanDevice == null)
									Log.e(TAG, "Device 0 could not be initialized");
								else {
										// Enable power save mode.
										mibScanDevice.setProperty(PropertyId.ENABLE_POWER_SAVE_MODE, "TRUE");

										for (ImageType imageType : ImageType.values()) {
											if (mibScanDevice.isCaptureAvailable(imageType, ImageResolution.RESOLUTION_500)) {
												mImageType = imageType;
												break;
											}
										}

									if (mImageType == ImageType.TYPE_NONE)
										mibScanDevice.close();
									else {
										// We will start receiving callbacks for preview images and finger count and quality changes.
										mibScanDevice.setScanDeviceListener(IBScanCommander.this);
										if (mListeningInCheck) {
											mListeningInCheck = false;
											if (mListener != null) mListener.onStatus(IBScanCommander.State.IBSCAN_CONNECTED);
										}
									}
								}

								if (mListeningInCheck) {
									mListeningInCheck = false;
									if (mListener != null) mListener.onStatus(IBScanCommander.State.IBSCAN_UNKNOWN);
								}
							}
						} catch (IBScanException ibse) {
							Log.e(TAG, "Received exception getting device count " + ibse.getType().toString());
							Log.e(TAG, "Could not initialize device with exception " + ibse.getType().toString());
							if (mListener != null) mListener.onStatus(IBScanCommander.State.IBSCAN_BROKEN);
							this.interrupt();
							if(!this.isAlive())
								this.start();
						}

						if (mListeningInCheck == false)
							mListeningInCheck = true;
						else if(mListener != null)
							mListener.onStatus(IBScanCommander.State.IBSCAN_NODEVICE);

					synchronized(mibThread) {
						mibThread.notify();
					}
				}
			};
		}

		mibThread.start();
	}

	/**
	 *  Connect to IBScan device with asynchronous
	 */
	public void open() {
		if (mibThread == null) {
				mibThread = new Thread() {
					public void run() {
				        	Context ctx = ResourceFactory.getInstance().getApplicationContext();
	//			        	mibScan.updateUsbPermission(); // USB bus 퍼미션 추가.
							/*
							 * Make sure there are no USB devices attached that are IB scanners for which permission has
							 * not been granted.  For any that are found, request permission; we should receive a
							 * callback when permission is granted or denied and then when IBScan recognizes that
							 * new devices are connected, which will result in another refresh.
							 */
							final UsbManager manager = (UsbManager)ctx.getSystemService(Context.USB_SERVICE);
							for (UsbDevice device : manager.getDeviceList().values()) {
							    if (IBScan.isScanDevice(device) && !manager.hasPermission(device)) {
							    	mibScan.requestPermission(device.getDeviceId());
							    }
							}

					        	try {
									mibScan.enableTraceLog(ResourceFactory.IB_LOG);
									int deviceCount = mibScan.getDeviceCount();
									if (mibScan.getDeviceCount() < 1)
										Log.e(TAG, "No avaliable fingerprint device");
									else {
										IBScan.DeviceDesc deviceDesc = null;
										int devId = 0;
										for (int i = 0; i < deviceCount; i++) {
											synchronized (mibScan) {
												deviceDesc = mibScan.getDeviceDescription(i);
												devId = i;
											}
										}
										synchronized (mibScan) {
											if(deviceDesc != null)
												mibScan.openDeviceAsync(devId);
										}
									}
								} catch (IBScanException e) {
									if(e.getType().toString().equals("NOT_SURPPORTED")){
										mListener.onStatus(IBScanCommander.State.IBSCAN_NOTSURPPORTED);
										this.interrupt();
									}
									Log.e(TAG, "Received exception getting device count " + e.getType().toString());
									Log.e(TAG, "Could not initialize device with exception " + e.getType().toString());
									e.printStackTrace();
								}
						synchronized(mibThread) {
							mibThread.notify();
						}
				}
			};
		}
		mibThread.start();
	}

	public boolean isPreparing() {
		return mibThread != null && mibThread.isAlive();
	}

	public boolean isOpened() {
		return mibScanDevice != null;
	}

	/**
	 *  Close & null device.
	 */
	public void close() {
		if (mibThread != null) {
			mListeningInCheck = false;
			synchronized (mibThread) {
				if (mibThread.isAlive()) {
					try { mibThread.wait(); } catch(InterruptedException ie) {}
				}
			}
			mListeningInCheck = true;
		}

		try {
			if (mibScanDevice != null) {
				synchronized (mibScanDevice) {
						if(mibScanDevice.isCaptureActive()) {
							stopCapture();
						}
				}
				mibScanDevice.close();
				Log.d(TAG, "Closed");
				mibScanDevice = null;
				mImageType = ImageType.TYPE_NONE;
			}
		} catch (IBScanException ibse) {
			Log.e(TAG, "Could not close device " + ibse.getType().toString());
		}

	}

	/**
	 * Begin capturing an image.  While the image is being captured,
	 * we will receive preview images through callbacks.
	 * At the end of the capture, we will recieve a final image.
	 */
	public boolean startCapture() {
		if (mibScanDevice == null)
			return false;
		try {
			if (mibScanDevice.isOpened() && !mibScanDevice.isCaptureActive()) {
				Log.d(TAG, "start capture..."+(STARTCAPTURE++));
			mibScanDevice.beginCaptureImage(mImageType, ImageResolution.RESOLUTION_500,
					IBScanDevice.OPTION_AUTO_CAPTURE | IBScanDevice.OPTION_AUTO_CONTRAST);
			}else if(mListener!=null){
				mListener.onStatus(State.IBSCAN_FAILED_CAPTURING);
			}
		} catch (IBScanException ibse) {
			Log.e(TAG, "Could not begin capturing with error " + ibse.getType().toString());
			if(ibse.getType().toString().equals("NOT_SUPPORTED"))
				if (mListener != null) mListener.onStatus(IBScanCommander.State.IBSCAN_NOTSURPPORTED);
			else
			if (mListener != null) mListener.onStatus(IBScanCommander.State.IBSCAN_BROKEN);
			ibse.printStackTrace();
			return false;
		}

		Log.i(TAG, "Now capturing...");
		return true;
	}

	public boolean isCapturing() {
		try {
			return mibScanDevice != null && mibScanDevice.isCaptureActive();
		} catch (IBScanException ibse) {
			Log.e(TAG, "Could not check if capturing: " + ibse.getType().toString());
		}

		return false;
	}

	/**
	 * Cancel capturing the image.
	 */
	public boolean stopCapture() {
			try {

				if (mibScanDevice != null && mibScanDevice.isCaptureActive()) {
					Log.d(TAG, "cancle capture");
					mibScanDevice.cancelCaptureImage();
				}
				return true;
			} catch (IBScanException e) {
				Log.e(TAG, "Could not cancel capturing with error " + e.getType().toString());
				e.printStackTrace();
				if (mListener != null) mListener.onStatus(IBScanCommander.State.IBSCAN_BROKEN);
				return false;
			}

	}

	/* *********************************************************************************************
	 * IBScanDeviceListener METHODS
	 ******************************************************************************************** */

	@Override
	public void deviceCommunicationBroken(final IBScanDevice device) {
		Log.e(TAG, "Communication break with device");
		if (mListener != null) mListener.onStatus(State.IBSCAN_BROKEN);
	}

	@Override
	public void deviceFingerCountChanged(final IBScanDevice device, final FingerCountState fingerState) {
		Log.i(TAG, "deviceFingerCountChanged...");
	}

	@Override
	public void deviceAcquisitionBegun(final IBScanDevice device, final ImageType imageType) {
		Log.i(TAG, "deviceAcquisitionBegun...");
	}

	@Override
	public void deviceAcquisitionCompleted(final IBScanDevice device, final ImageType imageType) {
		Log.i(TAG, "deviceAcquisitionCompleted...");
	}

	@Override
	public void deviceFingerQualityChanged(final IBScanDevice device, final FingerQualityState[] fingerQualities) {
		Log.i(TAG, "deviceFingerQualityChanged...");
	}

	@Override
	public void deviceImagePreviewAvailable(final IBScanDevice device, final ImageData image) {
//		if (mListener != null) mListener.onPreview(image.toBitmap());
	}

	@Override
	public void deviceImageResultAvailable( final IBScanDevice device, final ImageData image,
											final ImageType imageType, final ImageData[] splitImageArray ) {
		Log.i(TAG, "deviceImageResultAvailable...");
	}

	@Override
    public void deviceImageResultExtendedAvailable(IBScanDevice device, IBScanException imageStatus,
    		final ImageData image, final ImageType imageType, final int detectedFingerCount,
    		final ImageData[] segmentImageArray, final SegmentPosition[] segmentPositionArray) {
		try {
			if(device!=null && image!=null)
				Log.i(TAG, "Score : "+device.calculateNfiqScore(image));
			if (imageStatus != null)
				throw new Exception(imageStatus.getType().toString());
			else {
				Log.i(TAG, "Image result available");
//				deviceImagePreviewAvailable(device, image);
				if (mListener != null) mListener.onCompleted((ImageDataExt)mibScanDevice.getResultImageExt(FingerPosition.UNKNOWN)[0]);
			}
		} catch(Exception e) {
			Log.e(TAG, "Image capture ended with error: " + e.toString());
			if (mListener != null) mListener.onStatus(State.IBSCAN_FAILED_CAPTURING);
		}
    }

	@Override
	public void devicePlatenStateChanged(final IBScanDevice device, final PlatenState platenState) {}

	@Override
	public void deviceWarningReceived(final IBScanDevice device, final IBScanException warning) {}

	public enum State {
		IBSCAN_NODEVICE, IBSCAN_CONNECTED, IBSCAN_BROKEN, IBSCAN_FAILED_CAPTURING, IBSCAN_UNKNOWN, IBSCAN_NOTSURPPORTED;
	}

	public interface OnStateListener {
		public void onStatus(State state);
		public void onPreview(Bitmap bitmap);
		public void onCompleted(ImageDataExt image);
	}

	@Override
	public void devicePressedKeyButtons(IBScanDevice arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scanDeviceAttached(int arg0) {
		// TODO Auto-generated method stub
		open();
//		boolean hasPermission = mibScan.hasPermission(arg0);
//		if (!hasPermission)
//			mibScan.requestPermission(arg0);
	}

	@Override
	public void scanDeviceCountChanged(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scanDeviceDetached(int arg0) {
		// TODO Auto-generated method stub
		Log.i(TAG, "detached USB...");
	}

	@Override
	public void scanDeviceInitProgress(int arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scanDeviceOpenComplete(int arg0, IBScanDevice arg1, IBScanException arg2) {
		// TODO Auto-generated method stub
		try{
			mibScanDevice= arg1;
			if (mibScanDevice == null)
				Log.e(TAG, "Device 0 could not be initialized");
			else {
					// Enable power save mode.
					mibScanDevice.setProperty(PropertyId.ENABLE_POWER_SAVE_MODE, "TRUE");
					for (ImageType imageType : ImageType.values()) {
						Log.i(TAG, "ImageType : "+imageType.name());
						if (mibScanDevice.isCaptureAvailable(imageType, ImageResolution.RESOLUTION_500)) {
							mImageType = imageType;
							break;
						}
					}

				if (mImageType == ImageType.TYPE_NONE)
					mibScanDevice.close();
				else {
					// We will start receiving callbacks for preview images and finger count and quality changes.
					mibScanDevice.setScanDeviceListener(IBScanCommander.this);
					if (mListeningInCheck) {
						mListeningInCheck = false;
						if (mListener != null) mListener.onStatus(IBScanCommander.State.IBSCAN_CONNECTED);
					}
				}
			}

			if (mListeningInCheck) {
				mListeningInCheck = false;
				if (mListener != null) mListener.onStatus(IBScanCommander.State.IBSCAN_UNKNOWN);
			}

			} catch (IBScanException ibse) {
				Log.e(TAG, "Received exception getting device count " + ibse.getType().toString());
				Log.e(TAG, "Could not initialize device with exception " + ibse.getType().toString());
				if (mListener != null) mListener.onStatus(IBScanCommander.State.IBSCAN_BROKEN);
			}

		if (mListeningInCheck == false)
			mListeningInCheck = true;
		else if(mListener != null)
			mListener.onStatus(IBScanCommander.State.IBSCAN_NODEVICE);
	}

	@Override
	public void scanDevicePermissionGranted(int arg0, boolean arg1) {
		// TODO Auto-generated method stub

	}

}
