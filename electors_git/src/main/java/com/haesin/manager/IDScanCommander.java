package com.haesin.manager;

import java.io.UnsupportedEncodingException;
import java.util.Timer;
import java.util.TimerTask;

import com.mirusys.electors.ResourceFactory;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.voting.MiruScanner;
import com.pacesystem.lib.PaceRecID;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;

public class IDScanCommander {
	private final static String TAG = "IDScanCommander";

	private final static char BUSY = '3';
	// private final static char DONE = '5';
	private final static int WIDTH = 642;
	private final static int HEIGHT = 1110;

	private MiruScanner mScanner = null;

	private OnStateListener mListener;
	private boolean mRecorgnizable;
	private Timer mTimer = null;
	private boolean mForward = false;

	// double side 스캔을 하기 때문에 *2를 한다.
	private byte[] mScanBytes;
	private int[] mUpsideColors;
//	private int[] mDownsideColors = new int[HEIGHT * WIDTH];
	private boolean isScan = false;

	public IDScanCommander(OnStateListener l) {
		mListener = l;
	}

	public boolean open() {
		close();
		 mScanBytes= new byte[WIDTH * HEIGHT * 3 * 2];
		try {
			if ((mScanner != null || (mScanner = new MiruScanner()) != null)
					&& mScanner.Open("/dev/miru0") == false)
				Log.e(TAG, "Failed open");
			else {
				mRecorgnizable = false;
				mTimer = new Timer();
				mTimer.schedule(new ScanTask(), 10, 1200);
				Log.i(TAG, "Opened");
				return true;
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception: " + e.getMessage());
		}
		return false;
	}

	public void closeTimer() {
		if(mTimer!=null)
		synchronized (mTimer) {
			mTimer.cancel();
			mTimer.purge();
			mTimer = null;
		}
	}

	public void close() {
		if (isOpened()) {
			if (mTimer != null) {
				synchronized (mTimer) {
					closeTimer();
				}
			}

			if (mScanner != null) {
				synchronized (this) {
					mScanner.Close();
				}
			}

			mForward = false;
			Log.i(TAG, "Closed");
		}
	}

	public boolean isOpened() {
		return mScanner != null && mScanner.IsClosed() == false;
	}

	public boolean setScannable(boolean allow) {
		if (isOpened() == false || mTimer == null)
			return false;
		else
			synchronized (mTimer) {
				mRecorgnizable = allow;
			}
		return true;
	}

	public boolean forward() {
		if (isOpened() && mTimer != null) {
			synchronized (mTimer) {
				mForward = true;
			}
			return true;
		}
		return false;
	}

	/**
	 * 	지문 장비 전원 on, off 작업.<br>
	 * @param isOff true=off, false=on
	 * @return received 정상 여부
     */
	public boolean fingerOn(boolean isOff) {
		if (isOpened()) {
			byte[] buffer = new byte[10];
			byte[] command = isOff?"FP010\r".getBytes():"FP011\r".getBytes();
			System.arraycopy(command, 0, buffer, 0, command.length);

			int received = 0;
//			if (card.CheckCardInOut() == MiruSmartCard.SUCCESS)
				synchronized (this) {
					received = mScanner.SendCommand(buffer, command.length);
				}

			if (received == 5
					&& "FP010".equals(new String(buffer, 0, received))){
				Log.i(TAG, "FP on!");
				return true;
			}

			Log.d(TAG, "return (FP): " + String.valueOf(received));
		}
		return false;
	}

//	AnalysTask at;
//
//	private void recognize() {
//		try {
//			mRecorgnizable = false;
//			at = new AnalysTask();
//			at.execute();
//			if (!at.getStatus().equals(Status.RUNNING)) {
//				at.notify();
//			}
//		} catch (Exception e) {
//			Log.e(TAG, "Exception: " + e.getMessage());
//			mListener.onError(State.IDSCAN_OCR_ERROR);
//		}
//	}

	// 스캐너 모니터링 및 스캔까지 처리하는 procedure
	private class ScanTask extends TimerTask {
		private byte[] mBuffer = null;
		private String mLastState = "";
		private boolean mScanning;
		private boolean mNoCover = true;
		private boolean mNoJam = true;
// Scan 완료후 backward되어도 sensor에 걸려 재스캔되는 경우가 발생하여 카드를 사용자가 뽑는 시간을 고려하여 스캔 동작을 지연함 (센서가 너무 민감한 듯)
		private int mScanDelay = 0;

		@Override
		public void run() {
			synchronized (mTimer) {
				if (mScanner.IsClosed())
					closeTimer();
				else {
					int received;
					if (mBuffer == null) {
						mBuffer = String
								.format("W4600300030000000000%04d%04d002400040001003000300030002000200020\r",
										WIDTH, HEIGHT).getBytes();
						synchronized (mScanner) {
							received = mScanner.SendCommand(mBuffer,
									mBuffer.length);
						}
						Log.e(TAG, "received:"+received +" mBuffer:"+new String(mBuffer));
						if (received != 5 || "W4010".equals(new String(mBuffer, 0,received)) == false) {
							if (received == 5 && mBuffer[4] == '3') {
								Log.e(TAG, "Scanner busy");
								mBuffer = null;
							} else {
								Log.e(TAG, "Can't configuable");
								mListener.onError(State.IDSCAN_IO_ERROR);
							}
							return;
						}
					}

					byte[] state_paper = "RA00\r".getBytes();
					System.arraycopy(state_paper, 0, mBuffer, 0,
							state_paper.length);

					synchronized (mScanner) {
						received = mScanner.SendCommand(mBuffer,
								state_paper.length);
					}

					if (received < 1)
						Log.d(TAG, "return (RA): " + String.valueOf(received));
					else {
						String state = new String(mBuffer, 0, received);

						// RA07xxxxxxx
						if (state.length() != 11
								|| state.startsWith("RA07") == false)
							Log.e(TAG, "mismatch: " + state);
						else {
							if (state.equals(mLastState) == false) {
								Log.i(TAG, "state: " + state);
								mLastState = state;
							}

							if (mScanning /* && state.charAt(6) == DONE */) {
								// miru에서 스캐너 펌웨어 변경하여 스캐너 상태가 아닌 이미지 요청후 응답에 따라
								// 판단해야 함
								try {
									String command = String.format(
											"RS1600000000%08d\r",
											mScanBytes.length);
									byte[] read_image = command.getBytes();

									synchronized (mScanner) {
										received = mScanner.SendCommand(
												read_image, read_image.length);
									}

									if (received != 5)
										throw new Exception("return ("
												+ String.valueOf(received)
												+ "): " + command);
									else if ("RS010".equals(state = new String(
											read_image, 0, received)) == false)
										Log.d(TAG,
												"RS013".equals(state) ? "SCANNING"
														: "WRONG COMMAND: "
																+ state);
									else {
										synchronized (mScanner) {
											received = mScanner
													.ReadImage(mScanBytes);
										}

										mScanning = false;
										// state: RA070100F00 일 경우 자동으로 BF 작업
										// 실행.
										// backward();
										Log.d(TAG, "scan length = "
												+ mScanBytes.length);
										if (received == mScanBytes.length) {
											isScan = true;
											mRecorgnizable = false;
											new AnalysThread().start();
										}

										else
											throw new Exception(
													String.format(
															"Failed reading %d not eq. %d (W:%d H:%d 24 bps)",
															received,
															mScanBytes.length,
															WIDTH, HEIGHT));
									}
								} catch (Exception e) {
									Log.e(TAG, e.getMessage());
									mListener.onError(State.IDSCAN_IO_ERROR);
								}
							} else if (state.charAt(6) == BUSY)
								mListener.onError(State.IDSCAN_BUSY);
							else if (state.charAt(7) < '8') { // cover open ?
								mNoCover = true;

								if (state.charAt(8) < '8') { // jam ?
									mNoJam = true;
									// RA00를 던져서 카드가 입구에서 대기중일경우 return값의 10번째
									// 숫자가 3이됨
									if (mScanning == false
											&& state.charAt(10) == '3') {
										if (mForward) {
											mForward = false;
											// FF00 command를 통해 스마트카드 발급기까지 밀어넣는
											// 작업
											state_paper = "FF00\r".getBytes();
											System.arraycopy(state_paper, 0,
													mBuffer, 0,
													state_paper.length);

											synchronized (mScanner) {
												received = mScanner
														.SendCommand(
																mBuffer,
																state_paper.length);
											}
											// return에 아무런 데이터가 없을 경우 error
											if (received < 1)
												Log.e(TAG,
														"SCANNER FORWARD ERROR : "
																+ String.valueOf(received));
											else
												Log.d(TAG,
														"SCANNER FORWARD STATE : "
																+ new String(
																		mBuffer,
																		0,
																		received));
										} else if (mRecorgnizable
												&& ++mScanDelay > 0) {
											byte[] scanning = "W5015\r"
													.getBytes();

											mScanDelay = 0;

											synchronized (mScanner) {
												received = mScanner
														.SendCommand(scanning,
																scanning.length);
											}

											if (received == 5
													&& "W5010"
															.equals(new String(
																	scanning,
																	0, received)))
												mScanning = true;
											else
												Log.d(TAG,
														"return (W5): "
																+ String.valueOf(received));
										}
									}
								} else if (mNoJam) {
									// backward();
									mNoJam = false;
									mListener.onError(State.IDSCAN_PAPERJAM);
								}
							} else if (mNoCover) {
								// backward();
								mNoCover = false;
								mListener.onError(State.IDSCAN_COVEROPEN);
							}
						}
					}
				}
			}
		}
	};

	boolean mSuccess = true;
	String mPeopleName;
	String mSocialNo = null;
	Bitmap bitmap;
	int hRECID;
	private boolean recognizeImg(int[] data) {

		byte idnum[] = new byte[256];
		byte name[] = new byte[256];

		int id_rect[] = new int[5];
		int name_rect[] = new int[5];

		String enc_text = null;
		String enc_num = null;


		if (hRECID != 0) {
			Log.d(TAG, "OPEN RECID success!!");
			int ret = PaceRecID.SetImageInt(hRECID, 0, HEIGHT, WIDTH,
					HEIGHT * 4, data, 5); // height 1110, width 642
			mSuccess = false;
			if (ret > 0) {
				Log.d(TAG, "SetImageInt success!!");
				ret = PaceRecID.Recog(hRECID);
				if (ret % 2 == 1) { // 1:주민증 앞면, 3:면허증 앞면, 5:공무원증 앞면
					// if(ret>0){ //1:주민증 앞면, 3:면허증 앞면, 5:공무원증 앞면
					Log.d(TAG, "image rec success = " + ret);
					// 등록 번호 얻기
					PaceRecID.GetResult(hRECID, 1, id_rect, idnum, 256);
					// 등록 번호 얻기
					PaceRecID.GetResult(hRECID, 2, name_rect, name, 256);
					Log.d("Recog", "image is " + ret);
					try {
						enc_num = new String(idnum, 0, 13, "EUC-KR");
						enc_text = new String(name, 0, 26, "EUC-KR");
						StringBuffer strNum = new StringBuffer(enc_num);
						StringBuffer strName = new StringBuffer(enc_text);
						// strNum.insert(6, "-");
						strName.deleteCharAt(1);
						// tvIdcard.setText(strNum.toString());
						mPeopleName = strName.toString();
						mSocialNo = strNum.toString();
						Log.i(TAG, strNum.toString());
						Log.i(TAG, strName.toString());
					} catch (UnsupportedEncodingException e) {
						Log.e(TAG, "ID Scan Error : "+e.getMessage());
						e.printStackTrace();
					}
					PaceRecID.SaveResultImage(hRECID,
							ResourceFactory.PATH_TRAINING + "/idcard");
					Log.i(TAG, "path = " + ResourceFactory.PATH_TRAINING
							+ "/idcard");
					// String imgpath = path+"/idcard.jpg";
					mSuccess = true;
				} else {
					Log.d(TAG, "image downside = " + ret);
					mSuccess = false;
				}
			} else {
				Log.d(TAG, "Set Image error");
			}
			PaceRecID.FreeImage(hRECID);

		} else {
			Log.d(TAG, "Open Failed");
			mSuccess = false;
		}
		return mSuccess;
	}

	public class AnalysThread extends Thread{
		public void run() {
			if (isScan) {
				if (mScanner.IsClosed())
					closeTimer();
					if (
//						mDownsideColors != null ||
						(mUpsideColors = new int[HEIGHT * WIDTH]) != null) {
					// flip (horizontal) & rotate (+90': upside)
					Log.i(TAG, "Image making");
					int s = 0, d = 0;
					final int matrix = WIDTH * HEIGHT;
					for (int i = 0; i < HEIGHT; ++i, ++d) {
						for (int c = 0; c < 4; ++c) {
							int below = d, above = matrix - (d + 1), shift = (c - 1) << 3;
							for (int j = 0; j < WIDTH; ++j, above -= HEIGHT, below += HEIGHT) {
								if (c == 0) { // fill alpha
//									mDownsideColors[below] = 0xFF000000;
									mUpsideColors[above] = 0xFF000000;
								} else {
//									mDownsideColors[below] |= (mScanBytes[s++] & 0xff) << shift;
									s++;
									mUpsideColors[above] |= (mScanBytes[s++] & 0xff) << shift;
								}
							}
						}
					}
					hRECID = PaceRecID.Open(ResourceFactory.PATH_TRAINING);
					if (recognizeImg(mUpsideColors))
						bitmap = Bitmap.createBitmap(mUpsideColors, HEIGHT,
								WIDTH, Bitmap.Config.ARGB_8888);
					else {
						s = 0; d = 0;
						for (int i = 0; i < HEIGHT; ++i, ++d) {
							for (int c = 0; c < 4; ++c) {
								int below = d, above = matrix - (d + 1), shift = (c - 1) << 3;
								for (int j = 0; j < WIDTH; ++j, above -= HEIGHT, below += HEIGHT) {
									if (c == 0) { // fill alpha
									// mDownsideColors[below] = 0xFF000000;
										mUpsideColors[below] = 0xFF000000;
									} else {
										// mDownsideColors[below] |=
										// (mScanBytes[s++] & 0xff) << shift;
										mUpsideColors[below] |= (mScanBytes[s++] & 0xff) << shift;
										s++;
									}
								}
							}
						}
					}
					PaceRecID.Close(hRECID);
					Log.i(TAG, "Image maked");
				}
			}
			if (mSuccess){
				bitmap = BitmapFactory.decodeFile(ResourceFactory.PATH_TRAINING+"/idcard");
				mHandler.sendEmptyMessage(1);
			}else {
				if (bitmap != null) {
					bitmap.recycle();
					bitmap = null;
				}
				mHandler.sendEmptyMessage(2);

			}
			mUpsideColors=null;
		}
	};
	Handler mHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 1:
				mListener.onCompleted(bitmap, mSocialNo, mPeopleName);
				break;
			case 2:
				mListener.onError(State.IDSCAN_IO_ERROR);
				break;
			default:
				break;
			}
		};
	};
//	private class AnalysTask extends AsyncTask<Void, Void, Bitmap> {
//
//
//		@Override
//		protected Bitmap doInBackground(Void... params) {
//
//		}
//		@Override
//		protected void onPostExecute(Bitmap bitmap) {
//
//		}
//	};

	public enum State {
		IDSCAN_BUSY, IDSCAN_COVEROPEN, IDSCAN_PAPERJAM, IDSCAN_OCR_ERROR, IDSCAN_IO_ERROR;
	}

	public interface OnStateListener {
		/**
		 * 스캐너의 상태 및 오류 여부를 통지합니다. COVEROPEN, PAPERJAM일 경우에 한해 강제적으로 backward
		 * 명령이 진행됩니다. (미루 권고사항) BUSY는 상황에 따라 오류일 수도 있고 스캐너가 작업중일 수도 있으므로 작업 요청한
		 * 개체에서 판단해야 합니다.
		 *
		 * @param state
		 *            IDSCAN_ 열거형 상수
		 */
		public void onError(State state);

		/**
		 * 스캔 및 OCR처리가 완료되었을 시 (UI thread상에서) 호출됩니다. 정상 완료되지 않을 시
		 * IDSCAN_OCR_ERROR 이벤트(onError callback)가 발생합니다. 스캔이 정상 완료되었더라도 (미디어
		 * 선명도에 따라) OCR 인식이 안되어 주민등록번호 및 성명이 null일 수 있습니다.
		 *
		 * @param bitmap
		 *            스캔된 앞면 이미지이거나 null
		 * @param socialNo
		 *            이미지로부터 인식된 주민등록번호('-'제외된 숫자열)이거나 null
		 * @param peopleName
		 *            이미지로부터 인식된 성명(공백으로 '성'과 '이름'구분)이거나 null
		 */
		public void onCompleted(Bitmap bitmap, String socialNo,
				String peopleName);
	}

}
