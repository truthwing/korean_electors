package com.haesin.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.spec.EncodedKeySpec;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class XmlParsing {
private DocumentBuilder mDocumentBuilder = null;
private Document mDoc = null;
	public XmlParsing() {
		try {
			mDocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			mDoc = mDocumentBuilder.newDocument();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	public boolean closingXml(String filename, String dn, String tpg_id, String vt_date, String status, String start_time, String close_time, String vtr_all, String vtr_abs, String bk_mcnt
			, String vt_mcnt, String vt_xcnt, String vt_pcnt, String vt_ccnt, String vt_dcnt, String vt_ecnt, String nvt_ccnt){
		Element vt_tpg = mDoc.createElement("VT_TPG");
		mDoc.appendChild(vt_tpg);
		String[] elementNames = {"DN","TPG_ID", "VT_DATE", "STATUS", "START_TIME", 
				"CLOSE_TIME", "VTR_ALL", "VTR_ABS", "BK_MCNT", "VT_MCNT", "VT_XCNT",
				"VT_PCNT", "VT_CCNT", "VT_DCNT", "VT_ECNT", "NVT_CCNT"};
		String[] elementValues = {dn, tpg_id, vt_date, status, start_time, close_time, 
				vtr_all, vtr_abs, bk_mcnt, vt_mcnt, vt_xcnt, vt_pcnt, vt_ccnt, 
				vt_dcnt, vt_ecnt, nvt_ccnt};
		mDoc.setXmlVersion("1.0");
		for (int i = 0; i < elementNames.length; i++) {
			Element data = mDoc.createElement(elementNames[i]);
			data.appendChild(mDoc.createTextNode(elementValues[i]));
			vt_tpg.appendChild(data);
		}
		Transformer transformer;
		try {
			transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "ks_c_5601-1987");
			DOMSource source = new DOMSource(mDoc);
			StreamResult result = new StreamResult(new File(filename));
			transformer.transform(source, result);
			try {new Thread().sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
			transformer.clearParameters();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
			return false;
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
			return false;
		} catch (TransformerException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean closingSign(String filename){
		
		return true;
	}
}
