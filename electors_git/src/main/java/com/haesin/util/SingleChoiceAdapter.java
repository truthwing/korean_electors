package com.haesin.util;

import java.util.ArrayList;

import com.mirusys.electors.MainActivity;
import com.mirusys.electors.R;
import com.mirusys.electors.ResourceFactory;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class SingleChoiceAdapter extends ArrayAdapter<Object[]> {
	private int mSelectedIndex;
	private Context mContext;
	private GestureDetector mGestureItem;
	public SingleChoiceAdapter(Context context, ArrayList<Object[]> electors) {
		super(context, 0, electors);
		mContext = context;
		mSelectedIndex = -1;
	}

	public void setSelectedIndex(int index) {
		mSelectedIndex = index;
		notifyDataSetChanged();
	}

	public int getSelectedIndex() {
		return mSelectedIndex;
	}

	@Override
	@SuppressLint("InflateParams")
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView[] holder;
		
		if (convertView != null)
			holder = (TextView[])convertView.getTag();
		else {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row_issue_voting_ticket, null);

			holder = new TextView[] {
				(TextView)convertView.findViewById(R.id.textview01),
				(TextView)convertView.findViewById(R.id.textview02),
				(TextView)convertView.findViewById(R.id.textview03),
				(TextView)convertView.findViewById(R.id.textview04),
				(TextView)convertView.findViewById(R.id.textview05),
				(TextView)convertView.findViewById(R.id.textview06)
			};

			convertView.setTag(holder);
		}

		final int color = mSelectedIndex < 0 || mSelectedIndex != position? Color.WHITE : ResourceFactory.getInstance().getResources().getColor(R.color.article_active);
		final Object[] elector = getItem(position);
		for(int i = 0; i < holder.length; ++i) {
			holder[i].setTextColor(color);
			if (i == 4) {
				holder[i].setText(elector[i] == null? null : (elector[i].toString().equals("Y")?R.string.menu_issue_01_issue_flag_Y:R.string.menu_issue_01_issue_flag_N));
			}
			else {
				holder[i].setText(elector[i] == null? null : elector[i].toString());
			}
		}
		convertView.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				((MainActivity)mContext).setPosition(((ListView)v.getParent()).getPositionForView(v));
				return mGestureItem.onTouchEvent(event);
			}
		});
		return convertView;
	}

	public GestureDetector getGestureItem() {
		return mGestureItem;
	}

	public void setGestureItem(GestureDetector mGestureItem) {
		this.mGestureItem = mGestureItem;
	}
	
}
