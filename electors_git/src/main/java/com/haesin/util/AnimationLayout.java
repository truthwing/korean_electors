package com.haesin.util;

import com.mirusys.electors.R;
import com.mirusys.electors.ResourceFactory.Log;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

public class AnimationLayout extends ViewGroup
{
    public final static String TAG = "AnimationLayout";
    public final static int DURATION = 250;

    private boolean mPressed = false;
    private boolean mClosed = true;
    private int mSidebarWidth = 0;

    private View mSidebar = null;
    private View mContent = null;
    private Animation mOpenAnimation = null, mCloseAnimation = null;
    private Listener mListener;

    public AnimationLayout(Context context) {
        this(context, null);
    }

    public AnimationLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();

        mSidebar = findViewById(R.id.animation_layout_sidebar);
        mContent = findViewById(R.id.animation_layout_content);

        if (mSidebar != null) {
	        if (mClosed) {
	        	mSidebar.setVisibility(View.INVISIBLE);
	        }
        }
    }

    @Override
    public void onLayout(boolean changed, int l, int t, int r, int b) {
        /* the title bar assign top padding, drop it */
        if (mSidebar != null) {
            int sidebarLeft = l;
        	mSidebar.layout(sidebarLeft, 0, sidebarLeft + mSidebarWidth, 0 + mSidebar.getMeasuredHeight());
        }

        if (mContent != null) {
	        if (mClosed)
	            mContent.layout(l, 0, r, b);
	        else
	            mContent.layout(l + mSidebarWidth, 0, r + mSidebarWidth, b);
        }
    }

    @Override
    public void onMeasure(int w, int h) {
        super.onMeasure(w, h);
        super.measureChildren(w, h);

        if (mSidebar != null) mSidebarWidth = mSidebar.getMeasuredWidth();

    	mOpenAnimation = null;
    	mCloseAnimation = null;
    }

    @Override
    protected void measureChild(View child, int parentWSpec, int parentHSpec) {
        /* the max width of Sidebar is 90% of Parent */
        if (child != mSidebar)
            super.measureChild(child, parentWSpec, parentHSpec);
        else {
            int mode = MeasureSpec.getMode(parentWSpec);
            int width = (int)(getMeasuredWidth() * 0.9);
            super.measureChild(child, MeasureSpec.makeMeasureSpec(width, mode), parentHSpec);
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (isClosed() == false) {
	        final int action = ev.getAction();

	        if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_DOWN) {
		        /* if user press and release both on Sidebar while was opened, call listener.
		         * otherwise, pass the event to child. */
		        final int x = (int)ev.getX();
		        final int y = (int)ev.getY();

		        if (mSidebar.getLeft() > x || mSidebar.getRight() < x || mSidebar.getTop() > y || mSidebar.getBottom() < y)
		            mPressed = false;
		        else if(action == MotionEvent.ACTION_DOWN)
		            mPressed = true;
		        else if(mPressed) {
		            mPressed = false;
		            mListener.onTouchedSidebarWhenOpened();
		        }
	        }
        }

        return false;
    }

    public void setListener(Listener l) {
        mListener = l;
    }

    public boolean isClosed() {
        return mClosed;
    }

    public void toggle() {
        if (mContent.getAnimation() != null)
            Log.d(TAG, "Is being animation");
        else {
        	if (mOpenAnimation == null && mCloseAnimation == null) {
            	mOpenAnimation = new TranslateAnimation(0, mSidebarWidth, 0, 0);
            	mCloseAnimation = new TranslateAnimation(0, -mSidebarWidth, 0, 0);

                Animation.AnimationListener l = new Animation.AnimationListener() {
			        @Override
			        public void onAnimationStart(Animation animation) {}
			        @Override
			        public void onAnimationRepeat(Animation animation) {}
			        @Override
			        public void onAnimationEnd(Animation animation) {
				        mContent.clearAnimation();

	                    mClosed = !mClosed;
            	        mSidebar.setVisibility(mClosed? View.INVISIBLE : View.VISIBLE);

	                    requestLayout();
	                    mListener.onSidebarClosed();
			        }
		        };

		        mOpenAnimation.setAnimationListener(l);
                mOpenAnimation.setDuration(DURATION);
                mOpenAnimation.setFillAfter(true);
                mOpenAnimation.setFillEnabled(true);

                mCloseAnimation.setAnimationListener(l);
                mCloseAnimation.setDuration(DURATION);
                mCloseAnimation.setFillAfter(true);
                mCloseAnimation.setFillEnabled(true);
			}

        	mContent.startAnimation(mClosed? mOpenAnimation : mCloseAnimation);
		}
    }

    public interface Listener {
        public void onSidebarOpened();
        public void onSidebarClosed();
        public void onTouchedSidebarWhenOpened();
    }
}
