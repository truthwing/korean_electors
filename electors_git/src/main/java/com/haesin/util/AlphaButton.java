package com.haesin.util;

import com.mirusys.electors.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.widget.Button;

public class AlphaButton extends Button
{
	private int mAlphaNormal = (int)(255 * .8);
	private int mAlphaPressed = (int)(255 * .7);
	private int mAlphaDisabled = (int)(255 * .1);

    public AlphaButton(Context context) {
        this(context, null);
    }

    public AlphaButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        parseAttributes(attrs);
    }

    public AlphaButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        parseAttributes(attrs);
    }

    private void parseAttributes(AttributeSet attrs) {
        if (attrs != null) {
	        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.AlphaButton);

	        mAlphaNormal =  Math.min(Math.max(a.getInteger(R.styleable.AlphaButton_alphaNormal, mAlphaNormal), 0), 255);
	        mAlphaPressed = Math.min(Math.max(a.getInteger(R.styleable.AlphaButton_alphaPressed, mAlphaPressed), 0), 255);
	        mAlphaDisabled = Math.min(Math.max(a.getInteger(R.styleable.AlphaButton_alphaDisabled, mAlphaDisabled), 0), 255);

	        a.recycle();
        }
    }

    @Override
    public void setBackground(Drawable d) {
    	// Replace the original background drawable (e.g. image) with a LayerDrawable that
    	// contains the original drawable.
    	super.setBackground(d != null? new AlphaBackgroundDrawable(d) : null);
    }

    public void setAlphaNormal(int alpha) {
    	mAlphaNormal = Math.min(Math.max(alpha, 0), 255);
    }

    public int getAlphaNormal() {
    	return mAlphaNormal;
    }

    public void setAlphaPressed(int alpha) {
    	mAlphaPressed = Math.min(Math.max(alpha, 0), 255);
    }

    public int getAlphaPressed() {
    	return mAlphaPressed;
    }

    public void setAlphaDisabled(int alpha) {
    	mAlphaDisabled = Math.min(Math.max(alpha, 0), 255);
    }

    public int getAlphaDisabled() {
    	return mAlphaDisabled;
    }

    public class AlphaBackgroundDrawable extends LayerDrawable {
    	public AlphaBackgroundDrawable(Drawable d) {
    		super(new Drawable[] { d });
    	}

    	@Override
    	protected boolean onStateChange(int[] states) {
    		boolean enabled = false;
    		boolean pressed = false;

    		for(int state : states) {
    			if (state == android.R.attr.state_enabled)
    				enabled = true;
    			else if(state == android.R.attr.state_pressed)
    				pressed = true;
    		}

    		mutate();
			setAlpha(enabled? (pressed? getAlphaPressed() : getAlphaNormal()) : getAlphaDisabled());
    		invalidateSelf();

    		return super.onStateChange(states);
    	}

    	@Override
    	public boolean isStateful() {
    		return true;
    	}
    }
}