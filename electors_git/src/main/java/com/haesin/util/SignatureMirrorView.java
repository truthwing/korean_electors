package com.haesin.util;

import java.util.ArrayList;

import com.haesin.entities.SignatureObject;
import com.mirusys.electors.ResourceFactory.Log;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.widget.FrameLayout;

public class SignatureMirrorView extends FrameLayout implements Callback {
	private static final String TAG ="SignatureMirrorView";
	private static Paint paint = new Paint();
	private ArrayList<Path> paths;
	private ArrayList<Float> parray;

	private float ratio;
	private Matrix scaleMatrix;
	private SurfaceView mTextureView;
	private RenderingThread mThread;
	public SignatureMirrorView(Context context, AttributeSet atts) {
		super(context, atts);
		mTextureView = new SurfaceView(context);
		mTextureView.getHolder().addCallback(this);
		mTextureView.setZOrderOnTop(true);    // necessary
		SurfaceHolder sfhTrackHolder = mTextureView.getHolder();
		sfhTrackHolder.setFormat(PixelFormat.TRANSPARENT);

		addView(mTextureView, new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}

	private void setPresentation(){
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeJoin(Paint.Join.ROUND);
		paint.setStrokeWidth(10.0f);
	}

	@Override
	protected void onAttachedToWindow() {
		// TODO Auto-generated method stub
		super.onAttachedToWindow();
		LocalBroadcastManager.getInstance(getContext()).registerReceiver(signatureReceiver, new IntentFilter(SignatureView.ACTION_SIGNATURE_MIRROR));
	}

	@Override
	protected void onDetachedFromWindow() {
		// TODO Auto-generated method stub
		super.onDetachedFromWindow();
		LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(signatureReceiver);
	}

	void debug(String msg){
		Log.d("Chimgee", msg);
	}

	private BroadcastReceiver signatureReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			if (paths == null || parray == null) {
				SignatureObject sign = (SignatureObject) intent.getSerializableExtra("sign");
				if (ratio == 0) ratio = (float)(getMeasuredWidth()-getPaddingLeft()-getPaddingRight())/(float)sign.size.x;
				if (scaleMatrix == null) {
					RectF drawableRect = new RectF(0, 0, sign.size.x, sign.size.y);
					RectF viewRect = new RectF(getPaddingLeft(), getPaddingTop(), getMeasuredWidth()-getPaddingLeft()-getPaddingRight(), getMeasuredHeight()-getPaddingTop()-getPaddingBottom());
					scaleMatrix = new Matrix();
					scaleMatrix.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.CENTER);
				}
				
				paths = sign.paths;
				parray = sign.parray;
			}
		}
	};


	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		Log.i(TAG, "surfaceChange : "+arg1+" arg2 : "+arg2+" arg3 : "+arg3);
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		setPresentation();
		mThread = new RenderingThread(arg0);
		mThread.start();
		stopThread();
	}
	
	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		if (mThread != null) mThread.stopRendering();
	}
	public void startThread(){
		if (mThread != null) mThread.setRunning(true);
	}
	public void stopThread(){
		if (mThread != null) mThread.setRunning(false);
	}
	
	private class RenderingThread extends Thread {
		private final SurfaceHolder mSurface;
		private volatile boolean mRunning = true;
		private volatile boolean mWaiting = true;
		private int size = 0;

		public RenderingThread(SurfaceHolder surface) {
			mSurface = surface;
		}

		@Override
		public void run() {
			while (mRunning&&!Thread.interrupted()) {
				if (paths != null && parray != null && parray.size() != size) {
					Canvas canvas = mSurface.lockCanvas(null);
					try {
						size = parray.size();
						canvas.drawColor(Color.WHITE, PorterDuff.Mode.CLEAR);
						for(int i=0;i<paths.size();i++){
							Path path = new Path(paths.get(i));
							path.transform(scaleMatrix);
							if(parray.size()>i)
							paint.setStrokeWidth(parray.get(i)*ratio);
							canvas.drawPath(path, paint);
						}
					} finally {
						mSurface.unlockCanvasAndPost(canvas);
					}
				}

				if(!mWaiting)
					synchronized (this) {
						try {
							this.wait();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
								try {
									Thread.sleep(10);
								} catch (InterruptedException e) {
									// Interrupted
								}
			}
		}

		void stopRendering() {
			interrupt();
			mRunning = false;
		}
		void setRunning(boolean running){
			mWaiting = running;
			if(running)
				synchronized (this){
				this.notify();}
		}
	}
	
}