package com.haesin.util;

import java.util.ArrayList;

import com.haesin.entities.SignatureObject;
import com.mirusys.electors.ResourceFactory.Log;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.widget.FrameLayout;

public class SignatureView extends FrameLayout implements Callback {
	private static final String TAG ="SignatureView";
	public final static String ACTION_SIGNATURE_MIRROR = "com.mirusys.electors.ACTION.SIGNATURE_MIRROR";

	private final float STROKE_MUL = 7.5f;
	private final float VELOCITY_FILTER_WEIGHT = 0.4f;
	private static Paint paint = new Paint();
	private static ArrayList<Path> paths = new ArrayList<Path>();
	private static ArrayList<Float> parray = new ArrayList<Float>();
	private Path path;
	private float mx, my;
	private float lastStroke = 0f;
	private boolean mIsDrawingComplete = false;
	private SurfaceView mTextureView;
	private RenderingThread mThread;
	private boolean isDrawing=false;
	private boolean isStart = false;
	private static long timeCheck;

	public SignatureView(Context context, AttributeSet atts) {
		super(context, atts);
		mTextureView = new SurfaceView(context);
		mTextureView.getHolder().addCallback(this);
		mTextureView.setZOrderOnTop(true);    // necessary
		SurfaceHolder sfhTrackHolder = mTextureView.getHolder();
		sfhTrackHolder.setFormat(PixelFormat.TRANSPARENT);
		
		addView(mTextureView, new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}
	
	private void setPresentation(){
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeJoin(Paint.Join.ROUND);
		paint.setStrokeWidth(10.0f);
		clearCanvas();
	}

	public void clearCanvas(){
		paths.clear();
		parray.clear();
		isDrawing=true;

		SignatureObject sign = new SignatureObject();
		sign.paths = paths;
		sign.parray = parray;
		sign.size = new Point(getMeasuredWidth(), getMeasuredHeight());

		Intent intent = new Intent(ACTION_SIGNATURE_MIRROR);
		intent.putExtra("clear", true);
		intent.putExtra("sign", sign);
		LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(!isDrawing) return true;

		timeCheck = System.currentTimeMillis();

		float eventX = event.getX();
		float eventY = event.getY();
		float p = event.getPressure();
		//
		if(p>0.6)
			p=(p-0.6f);
		/*
		 * 
		 * */
		if(p > 0.003){  
			float eventStroke = (VELOCITY_FILTER_WEIGHT*p*STROKE_MUL+
					(1-VELOCITY_FILTER_WEIGHT)*lastStroke);

			switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				//				debug("ACTION_DOWN", "eventx= "+eventX+" eventy="+eventY);
				if(mThread != null) mThread.setRunning(isStart);
				path = new Path();
				path.moveTo(eventX, eventY);
				paths.add(path);
				parray.add(eventStroke);
				mx = eventX;
				my = eventY;
				break;
			case MotionEvent.ACTION_MOVE:
				//				debug("ACTION_MOVE", "eventx= "+eventX+" eventy="+eventY);
				int historySize = event.getHistorySize();
//				if(lastStroke!=eventStroke){
					path=new Path();
					path.moveTo(mx, my);
					paths.add(path);
					//					debug("ACTION_MOVE","eventStroke "+eventStroke);
					parray.add(eventStroke);
//				}
				//				debug("ACTION_MOVE", "mx= "+mx+" my="+my);
				float x = mx;
				float y = my;
				for(int i=0; i<historySize; i++){
					float hx = event.getHistoricalX(i);
					float hy = event.getHistoricalY(i);

					path.quadTo(x, y, (hx+x)/2, (hy+y)/2);
					path.quadTo((hx+x)/2, (hy+y)/2, hx, hy);
					
					x=hx;
					y=hy;
				}
				path.quadTo(x, y, (eventX+x)/2,(eventY+y)/2);
				path.quadTo((eventX+x)/2,(eventY+y)/2, eventX,eventY);
				mx = eventX;
				my = eventY;
				break;
			case MotionEvent.ACTION_UP:
				//				debug("ACTION_UP","eventx= "+eventX+" eventy="+eventY);
				path.quadTo(mx, my, eventX, eventY);
				mIsDrawingComplete = true;
				break;
			default:
				return false;
			}
			lastStroke = eventStroke;
			debug("OnDraw", (mThread.isAlive()?"true":"false") + "/timediff="+ String.valueOf((System.currentTimeMillis() - timeCheck)));
			
			SignatureObject sign = new SignatureObject();
			sign.paths = paths;
			sign.parray = parray;
			sign.size = new Point(getMeasuredWidth(), getMeasuredHeight());

			Intent intent = new Intent(ACTION_SIGNATURE_MIRROR);
			intent.putExtra("clear", false);
			intent.putExtra("sign", sign);
			LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);	

			return true;
		}
		return false;
	}

	public boolean isDrawing() {
		return isDrawing;
	}

	public void setDrawing(boolean isDrawing) {
		this.isDrawing = isDrawing;
	}

	public Bitmap getBitmap() {
	    Bitmap bitmap = Bitmap.createBitmap(mTextureView.getWidth(), mTextureView.getHeight(), Bitmap.Config.ARGB_8888);
	    Canvas canvas = new Canvas(bitmap);
	   	canvas.drawColor(Color.WHITE, PorterDuff.Mode.CLEAR);
		for(int i=0;i<parray.size();i++){
			paint.setStrokeWidth(parray.get(i));
			canvas.drawPath(paths.get(i), paint);
		}
	    return bitmap;
	}
	
	private static void debug(String tag, String msg){
		Log.d(tag, msg);
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		Log.i(TAG, "surfaceChange : "+arg1+" arg2 : "+arg2+" arg3 : "+arg3);
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		Log.i(TAG, "surfaceCreated");
		setPresentation();
		mThread = new RenderingThread(arg0);
		mThread.start();
		mThread.setRunning(false);
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		Log.i(TAG, "surfaceDestroy");
		if (mThread != null) mThread.stopRendering();
	}
	public void startThread(){
		Log.i(TAG, "StartThread");
		isStart=true;
	}
	
	public void initCanvas(){
		if (mThread != null) {
			mThread.setRunning(true);
			mThread.setRunning(false);
			}
	}
	
	public void stopThread(){
		Log.i(TAG, "StopThread");
		isStart=false;
		if (mThread != null) mThread.setRunning(false);
	}
	
	public void destroy(){
		Log.i(TAG, "Destory");
		if(mThread != null) mThread.stopRendering();
	}
	
	private class RenderingThread extends Thread {
		private final SurfaceHolder mSurface;
		private volatile boolean mRunning = true;
		private volatile boolean mWaiting = true;
		private int size = 0;

		public RenderingThread(SurfaceHolder surface) {
			mSurface = surface;
		}

		@Override
		public void run() {
			while (mRunning&&!Thread.interrupted()) {
				if (parray.size() != size) {
					Canvas canvas = mSurface.lockCanvas(null);
					try {
						size = parray.size();
						canvas.drawColor(Color.WHITE, PorterDuff.Mode.CLEAR);
						for(int i=0;i<parray.size();i++){
							paint.setStrokeWidth(parray.get(i));
							canvas.drawPath(paths.get(i), paint);
						}

						debug("OnDraw", "pathSize ="+paths.size() + "/timediff="+ String.valueOf((System.currentTimeMillis() - timeCheck)));
					} finally {
						try {
							mSurface.unlockCanvasAndPost(canvas);
						} catch (Exception e) {
							e.printStackTrace();
							Log.e(TAG, "sign exception");
						}
					}
				}
				if(!mWaiting)
					synchronized (this) {
						try {
							this.wait();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// Interrupted
				}
			}
		}

		void stopRendering() {
			synchronized (this) {
				this.notify();
				interrupt();
			}
            mRunning = false;
		}
		
		void setRunning(boolean running){
			mWaiting = running;
			if(running)
				synchronized (this) {
					this.notify();
				}
		}
	}
	
	public boolean isDrawingComplete() {
		return mIsDrawingComplete;
	}

	public void setDrawingComplete(boolean isDrawingComplete) {
		this.mIsDrawingComplete = isDrawingComplete;
	}

}