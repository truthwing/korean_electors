package com.haesin.util;

import java.util.ArrayList;
import java.util.List;

import com.haesin.entities.CardBackupObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class CardBackupDataSQLite extends SQLiteOpenHelper {
	private final String TAG = "CardBackupDataSQLite";
	public CardBackupDataSQLite(Context context, String name,
			CursorFactory factory, int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("create table Cardbackup (cId integer primary key autoincrement, "
				+ "cDate character(8) not null, "
				+ "cTime character(6) not null, "
				+ "cTPGID character(10) not null, "
				+ "cStatus character(1) not null);");
		db.execSQL("create table CardTPG (tTPGID character(10) not null, cId integer not null, tStatus character(24) not null);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}
	
	public List<CardBackupObject> select() {
		Cursor c = getReadableDatabase().query("Cardbackup", null, null, null, null, null, null);
		List<CardBackupObject> cardBackups = new ArrayList<CardBackupObject>();
		while (c.moveToNext()) {
			Cursor c2 = getReadableDatabase().query("CardTPG", null, null, null, null, null, null);
			CardBackupObject cardBackup = new CardBackupObject();
			cardBackup.setId(c.getInt(c.getColumnIndex("cId")));
			cardBackup.setDate(c.getString(c.getColumnIndex("cDate")));
			cardBackup.setTime(c.getString(c.getColumnIndex("cTime")));
			cardBackup.settPGID(c.getString(c.getColumnIndex("cTPGID")));
			cardBackup.setStatus(c.getString(c.getColumnIndex("cStatus")));
			ArrayList<String> tmp = new ArrayList<String>();
			while(c2.moveToNext()) {
				if(c2.getColumnCount()>0
						&&c2.getInt(c2.getColumnIndex("cId"))==cardBackup.getId()){
						tmp.add(c2.getString(c2.getColumnIndex("tStatus")));
				}
			}
			String[] tmp2 = new String[20];
			for (int i = 0; i<(tmp2.length==tmp.size()?20:0);i++) {
				String string = tmp.get(i)!=null?tmp.get(i):"";
				tmp2[i]=string;
			}
			cardBackup.settPGStatus(tmp2);
			cardBackups.add(cardBackup);
		}
//		db.close();
		return cardBackups;
	}
	
	public List<CardBackupObject> cardTPGSelect(List<CardBackupObject> cbos) {
		
		return  cbos;
	}
	public void insert(CardBackupObject cb) {
		ContentValues cv = new ContentValues();
		cv.put("cDate", cb.getDate());
		cv.put("cTime", cb.getTime());
		cv.put("cTPGID", cb.gettPGID());
		cv.put("cStatus", cb.getStatus());
		getWritableDatabase().insert("Cardbackup", "", cv);
		Cursor c = getReadableDatabase().query("Cardbackup", null, null, null, null, null, null);
		if(c.moveToLast()&&c.getColumnCount()>0)
			cb.setId(c.getInt(c.getColumnIndex("cId")));
		for (int i = 0; i < cb.gettPGStatus().length; i++) {
			ContentValues cv2 = new ContentValues();
			cv2.put("tStatus",cb.gettPGStatus()[i]);
			cv2.put("tTPGID",cb.gettPGID());
			cv2.put("cId", cb.getId());
			getWritableDatabase().insert("CardTPG", "", cv2);
		}
//		db.close();
	}

	public void update(CardBackupObject cb) {
		ContentValues cv = new ContentValues();
		cv.put("cDate", cb.getDate());
		cv.put("cTime", cb.getTime());
		cv.put("cTPGID", cb.gettPGID());
		cv.put("cStatus", cb.getStatus());
		for (int i = 0; i < cb.gettPGStatus().length; i++) {
			ContentValues cv2 = new ContentValues();
			cv2.put("tStatus",cb.gettPGStatus()[i]);
			cv2.put("tTPGID",cb.gettPGID());
		}
		getWritableDatabase().update("Cardbackup", cv, "cId =" + cb.getId(), null);
//		db.close();
	}

	public void delete() {
		getWritableDatabase().delete("Cardbackup", null, null);
		getWritableDatabase().delete("CardTPG", null, null);
//		db.close();
	}
}
