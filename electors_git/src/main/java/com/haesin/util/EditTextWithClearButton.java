package com.haesin.util;

import com.mirusys.electors.ResourceFactory;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class EditTextWithClearButton extends EditText {
	private Drawable mImgClear = null;

	public EditTextWithClearButton(Context context) {
		this(context, null);
	}
	public EditTextWithClearButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public EditTextWithClearButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
    }

	void init() {
		// 초기 문자열이 없으면 clear button 비활성화
		if((mImgClear = getCompoundDrawables()[2]) != null && getText().length() == 0) {
			setCompoundDrawables(getCompoundDrawables()[0], getCompoundDrawables()[1], null, getCompoundDrawables()[3]);
		}

		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (mImgClear != null &&
					event.getAction() == MotionEvent.ACTION_UP &&
					event.getX() > (getWidth() - getPaddingRight() - mImgClear.getIntrinsicWidth())) {
					setText("");
					setFocusable(true);
				}

				return false;
			}
		});

		addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable arg0) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				setCompoundDrawables(getCompoundDrawables()[0], getCompoundDrawables()[1], s.length() > 0? mImgClear : null, getCompoundDrawables()[3]);
			}
		});
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);
		InputMethodManager imm = (InputMethodManager) ResourceFactory.getInstance().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getWindowToken(), 0);
		return true;
	}
	@Override
	protected void onFocusChanged(boolean focused, int direction,
			Rect previouslyFocusedRect) {
		super.onFocusChanged(focused, direction, previouslyFocusedRect);
	}
}
