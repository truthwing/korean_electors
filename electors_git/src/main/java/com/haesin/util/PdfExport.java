package com.haesin.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.haesin.entities.PDFTPGGubunObject;
import com.haesin.entities.PDFVTPeopleObj;
import com.haesin.entities.VoteStatusObject;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.mirusys.electors.R;
import com.mirusys.electors.ResourceFactory;
import com.mirusys.electors.ResourceFactory.Log;

public class PdfExport {
	private static final String TAG = "PdfExport";
	public static boolean createExportFile(String dest, String filename) {
		File dir = new File(dest);
		if (!dir.exists())
			dir.mkdirs();
		File file = new File(dest+"/" + filename);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				Log.e(TAG, "File Export Error : "+e.getMessage());
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 선거인명부 pdf를 생성
	 *  파일명은 vto.pdf로 생성할 예정입니다.
	 *  폰트는 나눔 고딕 명조만 있으면 됩니다.
	 *  sdcard/cache/font/ 폴더에  nanum 폰트 파일들을 넣어놨습니다.
	 * @param dest 
	 * 			저장경로로써 pdf파일을 저장할 경로 
	 * @param filename
	 * 			저장하고 싶은 경로에 지정할 파일명
	 * @param destFont
	 * 			현재 사용중인 폰트의 경로
	 * @param vtName
	 * 			선거 이름
	 * @param vtPeople
	 * 			선거인 명부 리스트
	 * @return
	 */
	public static boolean createPdfStyle1(String dest, String filename, String destFont, String vtName,
			List<PDFVTPeopleObj> vtPeople) {
		ArrayList<String> arrYName = new ArrayList<String>();
		arrYName.add(ResourceFactory.getInstance().getString(R.string.menu_issue_01_regno));
		arrYName.add(ResourceFactory.getInstance().getString(R.string.menu_issue_01_search_by_name_hint));
		arrYName.add(ResourceFactory.getInstance().getString(R.string.menu_issue_01_address));
		arrYName.add(ResourceFactory.getInstance().getString(R.string.menu_issue_01_birthday));
		arrYName.add(ResourceFactory.getInstance().getString(R.string.menu_issue_01_issue_date));
		String date = new SimpleDateFormat("yyyy-MM-dd, hh:mm:ss")
				.format(new Date());
		boolean isCreate = createExportFile(dest, filename);
		dest = dest+"/"+ filename;
		if (isCreate) {
			Document document = new Document(PageSize.A4);
			PdfWriter writer = null;
			try {
				writer = PdfWriter.getInstance(document, new FileOutputStream(
						dest));
				
				//page handler
				PdfPageEventHelper footerSet = new PdfPageEventHelper() {
					PdfTemplate total;

					@Override
					public void onCloseDocument(PdfWriter writer,
							Document document) {
						// TODO Auto-generated method stub
						ColumnText.showTextAligned(
								total,
								Element.ALIGN_LEFT,
								new Phrase(String.valueOf(writer
										.getPageNumber() - 1)), 2, 2, 0);
					}

					@Override
					public void onOpenDocument(PdfWriter writer,
							Document document) {
						// TODO Auto-generated method stub
						total = writer.getDirectContent()
								.createTemplate(30, 16);
					}

					@Override
					public void onEndPage(PdfWriter writer, Document document) {
						// TODO Auto-generated method stub
						PdfPTable table = new PdfPTable(2);
						try {
							table.setWidths(new int[] { 1, 1 });
							table.setTotalWidth(527);
							table.setLockedWidth(true);
							table.getDefaultCell().setFixedHeight(20);
							table.getDefaultCell().setBorder(0);
							table.getDefaultCell().setHorizontalAlignment(
									Element.ALIGN_RIGHT);
							table.addCell(String.format("Page %d of",
									writer.getPageNumber()));
							PdfPCell cell = new PdfPCell(
									Image.getInstance(total));
							cell.setBorder(0);
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							table.addCell(cell);
							table.writeSelectedRows(0, -1, 34, 33,
									writer.getDirectContent());
						} catch (DocumentException de) {
							throw new ExceptionConverter(de);
						}
					}
				};
				writer.setPageEvent(footerSet);
			} catch (Exception e) {
				Log.e(TAG, "PDF Error : "+e.getMessage());
				e.printStackTrace();
			}
			document.open();
			// font 경로 지정
			FontFactory.registerDirectory(destFont);
			// font 가져오기
			Font font = FontFactory.getFont("NanumGothic", BaseFont.IDENTITY_H,
					BaseFont.EMBEDDED, 10, Font.NORMAL);
			// 제목 font는 좀 다르게 설정
			Font titleFont = FontFactory.getFont("NanumMyeongjo",
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 27, Font.BOLD);

			// 제목 설정
			Paragraph title = new Paragraph(ResourceFactory.getInstance().getString(R.string.voterList), titleFont);
			title.setAlignment(Paragraph.ALIGN_CENTER);
			title.setSpacingBefore(30.0f);
			title.setSpacingAfter(30.0f);

			PdfPTable titleTable = new PdfPTable(2);

			// 선거명
			titleTable.setWidthPercentage(100);
			PdfPCell cell1 = new PdfPCell(new Paragraph(vtName, font));
			cell1.setBackgroundColor(new BaseColor(255, 255, 255));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setBorder(0);

			// 날짜
			PdfPCell cell2 = new PdfPCell(new Paragraph(date, font));
			cell2.setBackgroundColor(new BaseColor(255, 255, 255));
			cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell2.setBorder(0);

			titleTable.addCell(cell1);
			titleTable.addCell(cell2);

			try {
				document.add(title);
				document.add(titleTable);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			// Set Table Data
			PdfPTable table = new PdfPTable(arrYName.size());
			table.setWidthPercentage(100);
			table.setSpacingBefore(10.0f);
			float[] sizes = { 1f, 1f, 4f, 1f, 2f };
			try {
				table.setWidths(sizes);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			for (int j = 0; j < arrYName.size(); j++) {
				PdfPCell cell = new PdfPCell(new Paragraph(arrYName.get(j),
						font));
				cell.setBackgroundColor(new BaseColor(240, 240, 240));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(26);

				table.addCell(cell);
			}
			Paragraph p = new Paragraph(writer.getPageNumber() + "/"
					+ document.getPageNumber());
			p.setAlignment(Paragraph.ALIGN_CENTER);
			PdfPCell footcell = new PdfPCell(p);
			footcell.setBorder(0);
			table.setHeaderRows(1);

			for (int i = 0; i < vtPeople.size(); i++) {
				PDFVTPeopleObj member = vtPeople.get(i);
				arrYName.clear();
				arrYName.add(Integer.toString(member.getNo()));
				arrYName.add(member.getName());
				arrYName.add(member.getAddress());
				arrYName.add(member.getBirth());
				arrYName.add(member.getmDate());
				for (int j = 0; j < arrYName.size(); j++) {
					PdfPCell cell = new PdfPCell(new Paragraph(arrYName.get(j),
							font));
					cell.setBackgroundColor(new BaseColor(255, 255, 255));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(20);
					table.addCell(cell);
				}
			}
			try {
				document.add(table);
			} catch (DocumentException e) {
				Log.e(TAG, "Document Error : "+e.getMessage());
				e.printStackTrace();
				return false;
			}
			document.close();
			return true;
		} else
			return false;
	}
	
	
	/**
	 * 	투표구별 투표 상황을 알려줍니다.
	 *  파일명은 vto.pdf로 생성할 예정입니다.
	 *  폰트는 나눔 고딕 명조만 있으면 됩니다.
	 *  sdcard/cache/font/ 폴더에  nanum 폰트 파일들을 넣어놨습니다.
	 * 
	 * @param dest
	 * 			파일 경로
	 * @param filename
	 * 			경로에 저장할 파일 이름
	 * @param destFont
	 * 			font가 저장된 장소 경로
	 * @param vtName
	 * 			선거 이름
	 * @param vtCnt
	 * 			선거인 수
	 * @param vtPeople
	 * 			투표구의 정보들을 모아놓은 리스트
	 * @return
	 */
	public static boolean createPdfStyle7(String dest, String filename,
			String destFont, String vtName, int vtCnt,
			List<VoteStatusObject> vtPeople) {
		ArrayList<String> arrYName = new ArrayList<String>();
		arrYName.add(ResourceFactory.getInstance().getString(R.string.statstics_print_local));
		arrYName.add(ResourceFactory.getInstance().getString(R.string.print_status_vote_status));
		arrYName.add(ResourceFactory.getInstance().getString(R.string.print_status_people_voting));
		arrYName.add(ResourceFactory.getInstance().getString(R.string.print_status_voter));
		arrYName.add(ResourceFactory.getInstance().getString(R.string.print_status_voter_issued));
		arrYName.add(ResourceFactory.getInstance().getString(R.string.menu_issue_01_desc));
		String date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
				.format(new Date());
		int totBCnt = 0;
		int totMCnt = 0;
		int totTCnt = 0;
		float tpPercent = 0;
		boolean isCreate = createExportFile(dest, filename);
		dest = dest + "/" + filename;
		for (VoteStatusObject member : vtPeople) {
			totBCnt += member.getVtCcnt();
			totMCnt += member.getBkMcnt();
			totTCnt += member.getVtMcnt();
		}
		tpPercent = ((float) totBCnt / (float) vtCnt) * 100;
		if (isCreate) {
			Document document = new Document(PageSize.A4);
			PdfWriter writer = null;
			try {
				writer = PdfWriter.getInstance(document, new FileOutputStream(
						dest));

				// page handler
				PdfPageEventHelper footerSet = new PdfPageEventHelper() {
					PdfTemplate total;

					@Override
					public void onCloseDocument(PdfWriter writer,
							Document document) {
						// TODO Auto-generated method stub
						ColumnText.showTextAligned(
								total,
								Element.ALIGN_LEFT,
								new Phrase(String.valueOf(writer
										.getPageNumber() - 1)), 2, 2, 0);
					}

					@Override
					public void onOpenDocument(PdfWriter writer,
							Document document) {
						// TODO Auto-generated method stub
						total = writer.getDirectContent()
								.createTemplate(30, 16);
					}

					@Override
					public void onEndPage(PdfWriter writer, Document document) {
						// TODO Auto-generated method stub
						PdfPTable table = new PdfPTable(2);
						try {
							table.setWidths(new int[] { 1, 1 });
							table.setTotalWidth(527);
							table.setLockedWidth(true);
							table.getDefaultCell().setFixedHeight(20);
							table.getDefaultCell().setBorder(0);
							table.getDefaultCell().setHorizontalAlignment(
									Element.ALIGN_RIGHT);
							table.addCell(String.format("%d/",
									writer.getPageNumber()));
							PdfPCell cell = new PdfPCell(
									Image.getInstance(total));
							cell.setBorder(0);
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							table.addCell(cell);
							table.writeSelectedRows(0, -1, 34, 33,
									writer.getDirectContent());
						} catch (DocumentException de) {
							throw new ExceptionConverter(de);
						}
					}
				};
				writer.setPageEvent(footerSet);
			} catch (Exception e) {
				Log.e(TAG, "Error msg : "+e.getMessage());
				e.printStackTrace();
			}
			document.open();
			// font 경로 지정
			FontFactory.registerDirectory(destFont);
			// font 가져오기
			Font font = FontFactory.getFont("NanumMyeongjo",
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 10, Font.NORMAL);
			Font headerFont = FontFactory.getFont("NanumMyeongjo",
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 10, Font.BOLD);
			// 제목 font는 좀 다르게 설정
			Font titleFont = FontFactory.getFont("NanumMyeongjo",
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 22, Font.BOLD);

			// 제목 설정
			Paragraph title = new Paragraph(ResourceFactory.getInstance().getString(R.string.menu_manage_02), titleFont);
			title.setAlignment(Paragraph.ALIGN_CENTER);
			title.setSpacingBefore(30.0f);
			title.setSpacingAfter(30.0f);

			PdfPTable titleTable = new PdfPTable(2);

			// 선거명
			titleTable.setWidthPercentage(100);
			PdfPCell cell1 = new PdfPCell(new Paragraph(vtName, headerFont));
			cell1.setBackgroundColor(new BaseColor(255, 255, 255));
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setBorder(0);

			// 날짜
			PdfPCell cell2 = new PdfPCell(new Paragraph(date, headerFont));
			cell2.setBackgroundColor(new BaseColor(255, 255, 255));
			cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell2.setBorder(0);

			titleTable.addCell(cell1);
			titleTable.addCell(cell2);

			PdfPTable percentTable = new PdfPTable(3);

			List<PdfPCell> pdfHPCells = new ArrayList<PdfPCell>();
			pdfHPCells.add(new PdfPCell(new Paragraph(ResourceFactory.getInstance().getString(R.string.statstics_print_total_cnt), headerFont)));
			pdfHPCells.add(new PdfPCell(
					new Paragraph(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_voter_no), headerFont)));
			pdfHPCells.add(new PdfPCell(new Paragraph(ResourceFactory.getInstance().getString(R.string.statstics_print_rating), headerFont)));

			List<PdfPCell> pdfPCells = new ArrayList<PdfPCell>();
			pdfPCells.add(new PdfPCell(new Paragraph(ResourceFactory.getInstance().getString(R.string.format_issued_count,vtCnt), font)));
			pdfPCells.add(new PdfPCell(new Paragraph(ResourceFactory.getInstance().getString(R.string.format_issued_count,totBCnt), font)));
			pdfPCells.add(new PdfPCell(new Paragraph(String.format("%.2f%%",
					tpPercent), font)));

			for (int i = 0; i < 3; i++) {
				PdfPTable percentTable1 = new PdfPTable(2);
				PdfPCell pcell1 = pdfHPCells.get(i);
				pcell1.setVerticalAlignment(Paragraph.ALIGN_MIDDLE);
				pcell1.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
				pcell1.setBorder(PdfPCell.RIGHT);
				pcell1.setFixedHeight(30f);
				percentTable1.addCell(pcell1);
				PdfPCell inPCell1 = pdfPCells.get(i);
				inPCell1.enableBorderSide(PdfPCell.TOP);
				inPCell1.enableBorderSide(PdfPCell.RIGHT);
				inPCell1.enableBorderSide(PdfPCell.LEFT);
				inPCell1.enableBorderSide(PdfPCell.BOTTOM);
				inPCell1.setFixedHeight(30f);
				inPCell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
				inPCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
				inPCell1.setBackgroundColor(new BaseColor(255, 255, 255));
				percentTable1.addCell(inPCell1);
				PdfPCell pCell1 = new PdfPCell(percentTable1);
				pCell1.setBackgroundColor(new BaseColor(240, 240, 240));
				pCell1.setPadding(8.0f);
				percentTable.addCell(pCell1);
			}

			percentTable.setWidthPercentage(100);
			percentTable.setSpacingBefore(3.0f);

			// Set Table Data
			PdfPTable table = new PdfPTable(arrYName.size());
			float[] size = { 1.5f, 1, 1, 1, 1, 1.5f };
			table.setWidthPercentage(100);
			try {
				table.setWidths(size);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			table.setWidthPercentage(100);
			table.setSpacingBefore(10.0f);
			for (int j = 0; j < arrYName.size(); j++) {
				PdfPCell cell = new PdfPCell(new Paragraph(arrYName.get(j),
						headerFont));
				cell.setBackgroundColor(new BaseColor(230, 230, 230));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(26);
				table.addCell(cell);
			}
			Paragraph p = new Paragraph(writer.getPageNumber() + "/"
					+ document.getPageNumber());
			p.setAlignment(Paragraph.ALIGN_CENTER);
			PdfPCell footcell = new PdfPCell(p);
			footcell.setBorder(0);
			table.setHeaderRows(1);

			for (int i = 0; i < vtPeople.size(); i++) {
				VoteStatusObject member = vtPeople.get(i);
				arrYName.clear();
				arrYName.add(member.getTpgName());
				arrYName.add(member.getStatus());
				arrYName.add(Integer.toString(member.getBkMcnt()));
				arrYName.add(Integer.toString(member.getVtMcnt()));
				arrYName.add(Integer.toString(member.getVtCcnt()));
				arrYName.add(member.getTpgId());
				for (int j = 0; j < arrYName.size(); j++) {
					PdfPCell cell = new PdfPCell(new Paragraph(arrYName.get(j),
							font));
					cell.setBackgroundColor(new BaseColor(255, 255, 255));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(20);
					table.addCell(cell);
				}
			}
			arrYName.clear();
			arrYName.add(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_total));
			arrYName.add("");
			arrYName.add(Integer.toString(totMCnt));
			arrYName.add(Integer.toString(totTCnt));
			arrYName.add(Integer.toString(totBCnt));
			arrYName.add("");

			for (int j = 0; j < arrYName.size(); j++) {
				PdfPCell cell = new PdfPCell(new Paragraph(arrYName.get(j),
						headerFont));
				cell.setBackgroundColor(new BaseColor(240, 240, 240));
				if (j == 0)
					cell.disableBorderSide(PdfPCell.RIGHT);
				if (j == 1)
					cell.disableBorderSide(PdfPCell.LEFT);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(26);
				table.addCell(cell);
			}
			try {
				document.add(title);
				document.add(titleTable);
				document.add(percentTable);
				document.add(table);
			} catch (DocumentException e) {
				Log.e(TAG, "Document Error : "+e.getMessage());
				e.printStackTrace();
				return false;
			}
			document.close();
			return true;
		} else
			return false;
	}
	/**
	 *  전체적인 투표 상황을 알려줍니다.
	 *  파일명은 vto.pdf로 생성할 예정입니다.
	 *  폰트는 나눔 고딕 명조만 있으면 됩니다.
	 *  sdcard/cache/font/ 폴더에  nanum 폰트 파일들을 넣어놨습니다.
	 * 
	 * @param dest
	 * 			파일 경로
	 * @param filename
	 * 			경로에 저장할 파일 이름
	 * @param destFont
	 * 			font가 저장된 장소 경로
	 * @param vtName
	 * 			선거 이름
	 * @param vtTCnt
	 * 			투표자 전체 숫자
	 * @param vtCnt
	 * 			발급한 숫자
	 * @param rate
	 * 			발급율
	 * @param genderVtPeople
	 * 			남여가 구분된 발급자 리스트
	 * @param ageVtPeople
	 * 			나이별로 구분된 발급자 리스트
	 * @param timeVtPeople
	 * 			시간대별로 구분된 발급자 리스트
	 * @return
	 */
	public static boolean createPdfStyle8(String dest, String filename,
			String destFont, String vtName,int vtTCnt, int vtCnt,String rate,
			List<PDFTPGGubunObject> genderVtPeople,List<PDFTPGGubunObject> ageVtPeople,List<PDFTPGGubunObject> timeVtPeople) {
		ArrayList<String> arrYName = new ArrayList<String>();
		arrYName.add(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_vote));
		arrYName.add(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_sex));
		arrYName.add(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_total_voter_count));
		arrYName.add(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_issued_voter_count));
		arrYName.add(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_issued_rating));
		String date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
				.format(new Date());
		boolean isCreate = createExportFile(dest, filename);
		dest = dest + "/" + filename;
		if (isCreate) {
			Document document = new Document(PageSize.A4);
			PdfWriter writer = null;
			try {
				writer = PdfWriter.getInstance(document, new FileOutputStream(
						dest));

				// page handler
				PdfPageEventHelper footerSet = new PdfPageEventHelper() {
					PdfTemplate total;

					@Override
					public void onCloseDocument(PdfWriter writer,
							Document document) {
						// TODO Auto-generated method stub
						ColumnText.showTextAligned(
								total,
								Element.ALIGN_LEFT,
								new Phrase(String.valueOf(writer
										.getPageNumber() - 1)), 2, 2, 0);
					}

					@Override
					public void onOpenDocument(PdfWriter writer,
							Document document) {
						// TODO Auto-generated method stub
						total = writer.getDirectContent()
								.createTemplate(30, 16);
					}

					@Override
					public void onEndPage(PdfWriter writer, Document document) {
						// TODO Auto-generated method stub
						PdfPTable table = new PdfPTable(2);
						try {
							table.setWidths(new int[] { 1, 1 });
							table.setTotalWidth(527);
							table.setLockedWidth(true);
							table.getDefaultCell().setFixedHeight(20);
							table.getDefaultCell().setBorder(0);
							table.getDefaultCell().setHorizontalAlignment(
									Element.ALIGN_RIGHT);
							table.addCell(String.format("%d/",
									writer.getPageNumber()));
							PdfPCell cell = new PdfPCell(
									Image.getInstance(total));
							cell.setBorder(0);
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							table.addCell(cell);
							table.writeSelectedRows(0, -1, 34, 33,
									writer.getDirectContent());
						} catch (DocumentException de) {
							throw new ExceptionConverter(de);
						}
					}
				};
				writer.setPageEvent(footerSet);
			} catch (Exception e) {
				Log.e(TAG, "Error msg : "+e.getMessage());
				e.printStackTrace();
			}
			document.open();
			// font 경로 지정
			FontFactory.registerDirectory(destFont);
			// font 가져오기
			Font font = FontFactory.getFont("NanumMyeongjo",
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 10, Font.NORMAL);
			Font headerFont = FontFactory.getFont("NanumMyeongjo",
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 10, Font.BOLD);
			Font headerFont1 = FontFactory.getFont("NanumMyeongjo",
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14, Font.BOLD);
			// 제목 font는 좀 다르게 설정
			Font titleFont = FontFactory.getFont("NanumMyeongjo",
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 25, Font.BOLD);

			// 제목 설정
			Paragraph title = new Paragraph(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_issue_info), titleFont);
			title.setSpacingBefore(30.0f);
			title.setSpacingAfter(30.0f);
			PdfPCell titleCell = new PdfPCell(title);
			titleCell.setPadding(15f);
			titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			PdfPTable tTitleCell = new PdfPTable(1);
			tTitleCell.addCell(titleCell);
			tTitleCell.setWidthPercentage(100f);

			PdfPTable dateTable = new PdfPTable(1);
			dateTable.setWidthPercentage(100f);

			// 투표소 명
			Paragraph tpgName = new Paragraph(vtName, headerFont);
			tpgName.setAlignment(Element.ALIGN_LEFT);

			Paragraph tpgDescription = new Paragraph(
					ResourceFactory.getInstance().getString(R.string.print_paper_pdf_now_issue_intelligence), font);
			tpgDescription.setAlignment(Element.ALIGN_LEFT);
			tpgDescription.setSpacingBefore(5f);

			// 날짜
			PdfPCell dateCell = new PdfPCell(new Paragraph(date, headerFont));
			dateCell.setBackgroundColor(new BaseColor(255, 255, 255));
			dateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			dateCell.setBorder(PdfPCell.BOTTOM);
			dateCell.setPadding(5f);

			dateTable.addCell(dateCell);

			PdfPTable percentTable = new PdfPTable(3);

			List<PdfPCell> pdfHPCells = new ArrayList<PdfPCell>();
			pdfHPCells.add(new PdfPCell(
					new Paragraph(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_total_voter_count), headerFont)));
			pdfHPCells.add(new PdfPCell(new Paragraph(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_issued_voter_count),
					headerFont)));
			pdfHPCells.add(new PdfPCell(new Paragraph(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_issued_rating),
					headerFont)));

			List<PdfPCell> pdfPCells = new ArrayList<PdfPCell>();
			pdfPCells.add(new PdfPCell(new Paragraph(ResourceFactory.getInstance().getString(R.string.format_issued_count,vtTCnt), font)));
			pdfPCells.add(new PdfPCell(new Paragraph(ResourceFactory.getInstance().getString(R.string.format_issued_count,vtCnt), font)));
			pdfPCells.add(new PdfPCell(new Paragraph(rate, font)));

			for (int i = 0; i < 3; i++) {
				PdfPTable percentTable1 = new PdfPTable(2);
				PdfPCell pcell1 = pdfHPCells.get(i);
				pcell1.setVerticalAlignment(Paragraph.ALIGN_MIDDLE);
				pcell1.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
				pcell1.setFixedHeight(30f);
				pcell1.setBackgroundColor(new BaseColor(240, 240, 240));
				percentTable1.addCell(pcell1);
				PdfPCell inPCell1 = pdfPCells.get(i);
				inPCell1.setFixedHeight(30f);
				inPCell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
				inPCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
				inPCell1.setBackgroundColor(new BaseColor(255, 255, 255));
				percentTable1.addCell(inPCell1);
				PdfPCell pCell1 = new PdfPCell(percentTable1);
				pCell1.setPadding(1.0f);
				pCell1.setBorder(0);
				if (i == 1) {
					pCell1.setPaddingLeft(10);
					pCell1.setPaddingRight(10);
				} else if (i == 0) {
					pCell1.setPaddingRight(10);
				} else {
					pCell1.setPaddingLeft(10);
				}
				percentTable.addCell(pCell1);
			}

			percentTable.setWidthPercentage(100);
			percentTable.setSpacingBefore(3.0f);
			// Set Table Data
			PdfPTable genderTable = new PdfPTable(arrYName.size());
			float[] size = { 1.5f, 1, 1, 1, 1.5f };
			genderTable.setWidthPercentage(100);
			try {
				genderTable.setWidths(size);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			genderTable.setSpacingBefore(10.0f);
			genderTable.setSpacingAfter(10.0f);
			for (int j = 0; j < arrYName.size(); j++) {
				PdfPCell cell = new PdfPCell(new Paragraph(arrYName.get(j),
						headerFont));
				cell.setBackgroundColor(new BaseColor(230, 230, 230));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(26);
				genderTable.addCell(cell);
			}
			Paragraph p = new Paragraph(writer.getPageNumber() + "/"
					+ document.getPageNumber());
			p.setAlignment(Paragraph.ALIGN_CENTER);
			PdfPCell footcell = new PdfPCell(p);
			footcell.setBorder(0);
			genderTable.setHeaderRows(1);

			for (int i = 0; i < genderVtPeople.size(); i++) {
				PDFTPGGubunObject member = genderVtPeople.get(i);
				arrYName.clear();
				arrYName.add(member.getTpgName());
				arrYName.add(member.getGubun());
				arrYName.add(Integer.toString(member.getGubunSgiCnt()));
				arrYName.add(Integer.toString(member.getGubunIssCnt()));
				arrYName.add(member.getGubunIssRating());
				for (int j = 0; j < arrYName.size(); j++) {
					PdfPCell cell = new PdfPCell(new Paragraph(arrYName.get(j),
							font));
					cell.setBackgroundColor(new BaseColor(255, 255, 255));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(20);
					genderTable.addCell(cell);
				}
			}
			arrYName.clear();
			arrYName.add(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_vote));
			arrYName.add(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_age));
			arrYName.add(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_total_voter_count));
			arrYName.add(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_issued_voter_count));
			arrYName.add(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_issued_rating));

			PdfPTable ageTable = new PdfPTable(arrYName.size());
			ageTable.setWidthPercentage(100);
			try {
				ageTable.setWidths(size);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ageTable.setSpacingBefore(10.0f);
			ageTable.setSpacingAfter(10.0f);
			for (int j = 0; j < arrYName.size(); j++) {
				PdfPCell cell = new PdfPCell(new Paragraph(arrYName.get(j),
						headerFont));
				cell.setBackgroundColor(new BaseColor(230, 230, 230));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(26);
				ageTable.addCell(cell);
			}
			Paragraph p1 = new Paragraph(writer.getPageNumber() + "/"
					+ document.getPageNumber());
			p1.setAlignment(Paragraph.ALIGN_CENTER);
			PdfPCell footcell1 = new PdfPCell(p1);
			footcell1.setBorder(0);
			ageTable.setHeaderRows(1);

			for (int i = 0; i < ageVtPeople.size(); i++) {
				PDFTPGGubunObject member = ageVtPeople.get(i);
				arrYName.clear();
				arrYName.add(member.getTpgName());
				arrYName.add(member.getGubun());
				arrYName.add(Integer.toString(member.getGubunSgiCnt()));
				arrYName.add(Integer.toString(member.getGubunIssCnt()));
				arrYName.add(member.getGubunIssRating());
				for (int j = 0; j < arrYName.size(); j++) {
					PdfPCell cell = new PdfPCell(new Paragraph(arrYName.get(j),
							font));
					cell.setBackgroundColor(new BaseColor(255, 255, 255));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(20);
					ageTable.addCell(cell);
				}
			}

			arrYName.clear();
			arrYName.add(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_vote));
			arrYName.add(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_time));
			arrYName.add(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_issued_voter_count));
			arrYName.add(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_issued_rating));
			float[] timeSize = { 1.5f, 1, 2, 1.5f };
			PdfPTable timeTable = new PdfPTable(arrYName.size());
			timeTable.setWidthPercentage(100);
			try {
				timeTable.setWidths(timeSize);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			timeTable.setSpacingBefore(10.0f);
			timeTable.setSpacingAfter(10.0f);
			for (int j = 0; j < arrYName.size(); j++) {
				PdfPCell cell = new PdfPCell(new Paragraph(arrYName.get(j),
						headerFont));
				cell.setBackgroundColor(new BaseColor(230, 230, 230));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(26);
				timeTable.addCell(cell);
			}
			Paragraph p11 = new Paragraph(writer.getPageNumber() + "/"
					+ document.getPageNumber());
			p11.setAlignment(Paragraph.ALIGN_CENTER);
			PdfPCell footcell11 = new PdfPCell(p11);
			footcell11.setBorder(0);
			timeTable.setHeaderRows(1);

			for (int i = 0; i < timeVtPeople.size(); i++) {
				PDFTPGGubunObject member = timeVtPeople.get(i);
				arrYName.clear();
				arrYName.add(member.getTpgName());
				arrYName.add(member.getGubun());
				// arrYName.add(Integer.toString(member.getGubunSgiCnt()));
				arrYName.add(Integer.toString(member.getGubunIssCnt()));
				arrYName.add(member.getGubunIssRating());
				for (int j = 0; j < arrYName.size(); j++) {
					PdfPCell cell = new PdfPCell(new Paragraph(arrYName.get(j),
							font));
					cell.setBackgroundColor(new BaseColor(255, 255, 255));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(20);
					timeTable.addCell(cell);
				}
			}
			Paragraph timeTableHeader = new Paragraph(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_timer_issue_info),
					headerFont1);
			PdfPCell pc = new PdfPCell();
			pc.addElement(timeTableHeader);
			pc.setHorizontalAlignment(Element.ALIGN_LEFT);
			pc.setBorder(0);

			PdfPCell pc1 = new PdfPCell();
			pc1.addElement(new Paragraph(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_total_voter_count)+"    :    "
					+ timeVtPeople.get(0).getGubunSgiCnt(), headerFont1));
			pc1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			pc1.setBorder(0);

			PdfPTable pt = new PdfPTable(2);
			pt.setWidthPercentage(100);
			pt.addCell(pc);
			pt.addCell(pc1);

			try {
				document.add(tTitleCell);
				document.add(tpgName);
				document.add(tpgDescription);
				document.add(percentTable);
				document.add(dateTable);
				document.add(new Paragraph(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_sex_issue_info), headerFont1));
				document.add(genderTable);
				document.add(new Paragraph(ResourceFactory.getInstance().getString(R.string.print_paper_pdf_age_issue_info), headerFont1));
				document.add(ageTable);
				document.add(pt);
				document.add(timeTable);
			} catch (DocumentException e) {
				Log.e(TAG, "Error msg : "+e.getMessage());
				e.printStackTrace();
				return false;
			}
			document.close();
			return true;
		} else
			return false;
	}
}
