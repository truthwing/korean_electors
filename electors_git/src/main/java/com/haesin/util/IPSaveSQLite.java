package com.haesin.util;

import java.util.ArrayList;
import java.util.List;

import com.haesin.entities.IPAddressObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class IPSaveSQLite extends SQLiteOpenHelper {

	private String TAG = "IPSaveSQLite";
	
	public IPSaveSQLite(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("create table ipaddress (mId integer primary key autoincrement, "
				+ "ip character(15) not null, subnet character(15) not null, "
				+ "gateway character(15) not null, dns character(15) not null, "
				+ "remark character(20), isserver boolean not null);");
//		db.close();
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	public List<IPAddressObject> select() {
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.query("ipaddress", null, null, null, null, null, null);
		List<IPAddressObject> ipaddresses = new ArrayList<IPAddressObject>();
		while (c.moveToNext()) {
			IPAddressObject ipaddress = new IPAddressObject();
			ipaddress.setId(c.getInt(c.getColumnIndex("mId")));
			ipaddress.setIpaddress(c.getString(c.getColumnIndex("ip")));
			ipaddress.setSubnet(c.getString(c.getColumnIndex("subnet")));
			ipaddress.setGateway(c.getString(c.getColumnIndex("gateway")));
			ipaddress.setDns(c.getString(c.getColumnIndex("dns")));
			ipaddress.setRemark(c.getString(c.getColumnIndex("remark")));
			ipaddresses.add(ipaddress);
		}
//		db.close();
		return ipaddresses;
	}

	public void insert(IPAddressObject ip) {
		ContentValues cv = new ContentValues();
		cv.put("ip", ip.getIpaddress());
		cv.put("subnet", ip.getSubnet());
		cv.put("gateway", ip.getGateway());
		cv.put("dns", ip.getDns());
		cv.put("remark", ip.getRemark());
		cv.put("isserver", ip.isServer());
		getWritableDatabase().insert("ipaddress", "", cv);
//		db.close();
	}

	public void update(IPAddressObject ip) {
		ContentValues cv = new ContentValues();
		cv.put("ip", ip.getIpaddress());
		cv.put("subnet", ip.getSubnet());
		cv.put("gateway", ip.getGateway());
		cv.put("dns", ip.getDns());
		cv.put("remark", ip.getRemark());
		getWritableDatabase().update("ipaddress", cv, "mId =" + ip.getId(), null);
//		db.close();
	}

	public void delete(IPAddressObject ip) {
		getWritableDatabase().delete("ipaddress", "mId =" + ip.getId(), null);
//		db.close();
	}
}
