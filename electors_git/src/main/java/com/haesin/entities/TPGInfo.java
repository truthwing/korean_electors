package com.haesin.entities;

public class TPGInfo {
	private String mTPGID, mTPGName;

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		if(o!=null){
		TPGInfo str = (TPGInfo) o;
		if(str.getmTPGID()!=null)
		return str.getmTPGID().equals(mTPGID);}
		return false;
	}
	
	public String getmTPGID() {
		return mTPGID;
	}

	public void setmTPGID(String mTPGID) {
		this.mTPGID = mTPGID;
	}

	public String getmTPGName() {
		return mTPGName;
	}

	public void setmTPGName(String mTPGName) {
		this.mTPGName = mTPGName;
	}

}
