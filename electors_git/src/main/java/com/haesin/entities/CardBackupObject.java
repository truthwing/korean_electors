package com.haesin.entities;

public class CardBackupObject {
	private int id;
	private String date, tPGID, status, time;
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	private String[] tPGStatus = new String[20];
	public String[] gettPGStatus() {
		return tPGStatus;
	}
	public void settPGStatus(String[] tPGStatus) {
		this.tPGStatus = tPGStatus;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String gettPGID() {
		return tPGID;
	}
	public void settPGID(String tPGID) {
		this.tPGID = tPGID;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String aa = "";
		for (int i = 0; i < tPGStatus.length; i++) {
			aa+=" "+i+" :"+tPGStatus[i];
		}
		return "id : "+id+" date : "+date+" tpg id : "+tPGID+" tpg status "+status+" time : "+time+" "+aa;
	}
}
