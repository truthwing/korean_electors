package com.haesin.entities;

public class SGGInfoObject {
	private String sggName, sggBpsName, sggId;
	private byte[] sggBpsStamp;
	public byte[] getSggBpsStamp() {
		return sggBpsStamp;
	}

	public void setSggBpsStamp(byte[] sggBpsStamp) {
		this.sggBpsStamp = sggBpsStamp;
	}

	public String getSggId() {
		return sggId;
	}

	public void setSggId(String sggId) {
		this.sggId = sggId;
	}

	public String getSggName() {
		return sggName;
	}

	public void setSggName(String sggName) {
		this.sggName = sggName;
	}

	public String getSggBpsName() {
		return sggBpsName;
	}

	public void setSggBpsName(String sggBpsName) {
		this.sggBpsName = sggBpsName;
	}
	
}
