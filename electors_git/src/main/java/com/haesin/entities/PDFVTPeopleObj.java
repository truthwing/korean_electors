package com.haesin.entities;

import java.io.Serializable;

public class PDFVTPeopleObj implements Serializable {

	/**
		 * 
		 */
	private static final long serialVersionUID = -7663224019037769978L;
	// 주소, 이름, 태어난 날짜, 발급 날짜.
	String address, name, birth, mDate;
	// 등재번호.
	int no;

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return no + ", " + address + ", " + name + ", " + birth + ", " + mDate;
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public String getmDate() {
		return mDate;
	}

	public void setmDate(String mDate) {
		this.mDate = mDate;
	}

}
