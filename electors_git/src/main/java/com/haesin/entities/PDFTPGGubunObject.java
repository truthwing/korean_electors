package com.haesin.entities;

import java.io.Serializable;

public class PDFTPGGubunObject implements Serializable{

	/**
	 * tpgName 투표구 이름
	 * gubun 시간, 성별, 연령별
	 * gubunIssRating 발급율
	 * gubunSgiCnt 전체 선거인 수
	 * gubunIssCnt 카드 발급 수
	 */
	private static final long serialVersionUID = -5519515488464828779L;
	public boolean equals(Object o) {
		if(o!=null
				&&((PDFTPGGubunObject)o).getGubun()!=null
				&&((PDFTPGGubunObject)o).getGubun().equals(getGubun()))
				return true;
		return false;
	};
	private String tpgName, gubun, gubunIssRating, vtYn, vtTpgId;
	private int gubunSgiCnt, gubunOtherSgiCnt, gubunIssCnt;
	public String getTpgName() {
		return tpgName;
	}
	public void setTpgName(String tpgName) {
		this.tpgName = tpgName;
	}
	public String getGubun() {
		return gubun;
	}
	public void setGubun(String gubun) {
		this.gubun = gubun;
	}
	public String getGubunIssRating() {
		return gubunIssRating;
	}
	public void setGubunIssRating(String gubunIssRating) {
		this.gubunIssRating = gubunIssRating;
	}
	public void setGubunIssRating() {
		this.gubunIssRating = String.format("%.2f", (float)gubunIssCnt*100/gubunSgiCnt)+" %";
	}
	public int getGubunSgiCnt() {
		return gubunSgiCnt;
	}
	public void setGubunSgiCnt(int gubunSgiCnt) {
		this.gubunSgiCnt = gubunSgiCnt;
	}
	public void addGubunSgiCnt(int num) {
		this.gubunSgiCnt+=num;
	}
	public int getGubunIssCnt() {
		return gubunIssCnt;
	}
	public void setGubunIssCnt(int gubunIssCnt) {
		this.gubunIssCnt = gubunIssCnt;
	}
	public void addGubunIssCnt(int num) {
		this.gubunIssCnt+=num;
	}
	public int getGubunOtherSgiCnt() {
		return gubunOtherSgiCnt;
	}
	public void setGubunOtherSgiCnt(int gubunOtherSgiCnt) {
		this.gubunOtherSgiCnt = gubunOtherSgiCnt;
	}
	public void addGubunOtherSgiCnt(int num) {
		this.gubunOtherSgiCnt+=num;
	}
	public String getVtTpgId() {
		return vtTpgId;
	}
	public void setVtTpgId(String vtTpgId) {
		this.vtTpgId = vtTpgId;
	}
	
}
