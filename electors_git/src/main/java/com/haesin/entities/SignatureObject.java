package com.haesin.entities;

import java.io.Serializable;
import java.util.ArrayList;

import android.graphics.Path;
import android.graphics.Point;

public class SignatureObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4121816129002477639L;
	
	public Point size;
	
	public ArrayList<Path> paths;
	public ArrayList<Float> parray;

}