package com.haesin.entities;

import java.util.ArrayList;

import com.mirusys.epson.dto.Candidate;

public class PrintIntentObject {
	private byte[] stamp, adminstamp;
	private int ballotType, candidateNum, ballotColor, fillType, startSerialNum, endSerialNum;
	private boolean isSerial;
	private Candidate[] candidate;
	private String titleName, subTitleName;
	
	public PrintIntentObject() {
		// TODO Auto-generated constructor stub
	}
	public byte[] getStamp() {
		return stamp;
	}
	public void setStamp(byte[] stamp) {
		this.stamp = stamp;
	}
	public byte[] getAdminstamp() {
		return adminstamp;
	}
	public void setAdminstamp(byte[] adminstamp) {
		this.adminstamp = adminstamp;
	}
	public int getBallotType() {
		return ballotType;
	}
	public void setBallotType(int ballotType) {
		this.ballotType = ballotType;
	}
	public int getCandidateNum() {
		return candidateNum;
	}
	public void setCandidateNum(int candidateNum) {
		this.candidateNum = candidateNum;
	}
	public int getBallotColor() {
		return ballotColor;
	}
	public void setBallotColor(int ballotColor) {
		this.ballotColor = ballotColor;
	}
	public int getFillType() {
		return fillType;
	}
	public void setFillType(int fillType) {
		this.fillType = fillType;
	}
	public boolean isSerial() {
		return isSerial;
	}
	public void setSerial(boolean isSerial) {
		this.isSerial = isSerial;
	}
	public int getStartSerialNum() {
		return startSerialNum;
	}
	public void setStartSerialNum(int startSerialNum) {
		this.startSerialNum = startSerialNum;
	}
	public int getEndSerialNum() {
		return endSerialNum;
	}
	public void setEndSerialNum(int endSerialNum) {
		this.endSerialNum = endSerialNum;
	}
	public Candidate[] getCandidate() {
		return candidate;
	}
	public void setCandidate(ArrayList<Candidate> mCandidates) {
		this.candidate=new Candidate[mCandidates.size()];
		for (int i=0; i<mCandidates.size();i++) {
			this.candidate[i] = mCandidates.get(i);
		}
	}
	public String getTitleName() {
		return titleName;
	}
	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	public String getSubTitleName() {
		return subTitleName;
	}
	public void setSubTitleName(String subTitleName) {
		this.subTitleName = subTitleName;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String str = "TITLE : "+titleName+" subTitle : "+subTitleName+" startserial : "+ startSerialNum+" endserial : "+endSerialNum+
				" ballot color : "+ballotColor+" ballot type : "+ballotType+" isSerial : "+isSerial+" candidateNum : "+candidateNum;
		return str;
	}
}
