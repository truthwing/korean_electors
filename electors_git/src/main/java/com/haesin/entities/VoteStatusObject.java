package com.haesin.entities;

public class VoteStatusObject {
	private String status, tpgName, tpgId;
	private int bkMcnt, vtMcnt, vtCcnt;
	
	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		VoteStatusObject obj = (VoteStatusObject) o;
		if(obj!=null&&obj.getTpgId()!=null){
			if(obj.getTpgId().equals(tpgId))
				return true;
		}
		return false;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTpgName() {
		return tpgName;
	}

	public void setTpgName(String tpgName) {
		this.tpgName = tpgName;
	}

	public String getTpgId() {
		return tpgId;
	}

	public void setTpgId(String tpgId) {
		this.tpgId = tpgId;
	}

	public int getBkMcnt() {
		return bkMcnt;
	}

	public void setBkMcnt(int bkMcnt) {
		this.bkMcnt = bkMcnt;
	}

	public int getVtMcnt() {
		return vtMcnt;
	}

	public void setVtMcnt(int vtMcnt) {
		this.vtMcnt = vtMcnt;
	}

	public int getVtCcnt() {
		return vtCcnt;
	}

	public void setVtCcnt(int vtCcnt) {
		this.vtCcnt = vtCcnt;
	}

}
