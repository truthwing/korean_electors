package com.haesin.entities;

import java.io.Serializable;

public class PDFTPGinfoObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7736373095858714673L;

	// 투표소 이름, 투표소 상태
	private String tpName, tpStatus, remark;
	// 명부기, 투표기, 카드발급
	private int mCnt, tCnt, bCnt;

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTpName() {
		return tpName;
	}

	public void setTpName(String tpName) {
		this.tpName = tpName;
	}

	public String getTpStatus() {
		return tpStatus;
	}

	public void setTpStatus(String tpStatus) {
		this.tpStatus = tpStatus;
	}

	public int getmCnt() {
		return mCnt;
	}

	public void setmCnt(int mCnt) {
		this.mCnt = mCnt;
	}

	public int gettCnt() {
		return tCnt;
	}

	public void settCnt(int tCnt) {
		this.tCnt = tCnt;
	}

	public int getbCnt() {
		return bCnt;
	}

	public void setbCnt(int bCnt) {
		this.bCnt = bCnt;
	}

}
