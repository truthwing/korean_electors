package com.mirusys.vt.fragment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.haesinit.ultralite.database.SQLNO;
import com.mirusys.electors.R;
import com.mirusys.electors.ResourceFactory;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.electors.ResourceFactory.QueryID;
import com.mirusys.electors.SelectDialog;
import com.mirusys.voting.MiruSmartCard;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class AddTicketFragment
extends BasicFragment
implements View.OnClickListener, DialogInterface.OnDismissListener {
	private static final String TAG="AddTicketFragment";
	private View mContentView;
	private TextView mInfoStatus;
	private boolean mReady = false;
	private Button mBtnOkay;

	private boolean mIsDuplicate = false;
	private boolean mCardCheckable = false;
    private boolean mCardAttachable = false;

	private int mSelectedPlace = -1;
	private TextView mVotePlace;
    private ArrayList<String> mPlaceNames = new ArrayList<String>();
    private ArrayList<String> mPlaceCodes = new ArrayList<String>();
	private SelectDialog mPlaceSelector = null;

    private int mSelectedGroup = -1;
	private TextView mVoteGroup;
    private ArrayList<String> mGroupNames = new ArrayList<String>();
    private ArrayList<String> mGroupCodes = new ArrayList<String>();
	private SelectDialog mGroupSelector = null;

	private String mName;
	private String mSocialNo;
	private String mAddress;
	private String mDesc;
	private Integer mDjno;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.frag_vote_add_ticket, container, false);
		mInfoStatus = (TextView)mContentView.findViewById(R.id.infoStatus);

		mVotePlace = (TextView)mContentView.findViewById(R.id.infoVotePlace);
		mVotePlace.setOnClickListener(this);

		mVoteGroup = (TextView)mContentView.findViewById(R.id.infoWeightGroup);
		mVoteGroup.setOnClickListener(this);

		(mBtnOkay = (Button)mContentView.findViewById(R.id.btnOkay)).setOnClickListener(this);
		return mContentView;
	}

	@Override
	public void onStart() {
		super.onStart();

		// 관리자카드용 스마트카드 연결
		if (mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER) != null || ResourceFactory.UI_TEST)
			mCardCheckable = true;
		else
			mInfoStatus.setText(R.string.err_nores_writer);
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
    public void onAttached(MiruSmartCard card, byte type) {
		if (mCardCheckable) {
			byte[] bytesBSection = new byte[51];

			mCardCheckable = false;

			if (ResourceFactory.UI_TEST == false && (
				type != 'T' ||
				card.CardVerify(mFactory.getManagerKey()) != MiruSmartCard.SUCCESS ||
				card.TCardReadBSection(bytesBSection) != MiruSmartCard.SUCCESS ||
				mFactory.getProperty(ResourceFactory.CONFIG_TPGID).equals(ResourceFactory.bytesToString(bytesBSection, 0, 10)) == false ||
				mFactory.getVtOpeningValue().equals(new String(bytesBSection, bytesBSection.length - 10, 10)) == false))
				mCardAttachable = true;
			else if(mFactory.dbQuery(QueryID.Q19, QueryID.Q23)) {
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mInfoStatus.setText(R.string.menu_issue_01_popup_closing_retrieve);
					}
				});
			}
		}
    }

	@Override
	public void onDetached(MiruSmartCard card) {
		if (mCardAttachable) {
			mCardCheckable = true;
			mCardAttachable = false;

			dismissedMessageBox();
		}
    }

	@Override
    public boolean onIdle(MiruSmartCard card) {
    	return mCardCheckable && mFactory.isCardAttached();
    }

	@Override
    public void onSqlStatus(ResourceFactory.SQLState state) {
		if (state != ResourceFactory.SQLState.SQL_CONNECTED) {
			if (mReady)
				showDefaultMessage(R.string.error_database_failed_connection);
			else getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mInfoStatus.setText(R.string.error_database_failed_connection);
				}
			});
		}
    }

	@Override
    public Object[] getParameters(QueryID id) {
		if(id == QueryID.Q25 || id == QueryID.Q75)
			return new Object[] {
				mSocialNo, mName
			};
		else if(id == QueryID.Q26) {
			return mFactory.isJson()?
					new Object[] {
				Integer.toString(mDjno),
				mAddress,
				mName,
				mSocialNo,
				mDesc,
				mPlaceCodes.get(mSelectedPlace),
				mFactory.getProperty(ResourceFactory.CONFIG_TPGID),
				mSelectedGroup < 1? null : mGroupCodes.get(mSelectedGroup)}
				:new Object[] {
				mDjno,
				mAddress,
				mName,
				mSocialNo,
				mDesc,
				mPlaceCodes.get(mSelectedPlace),
				mFactory.getProperty(ResourceFactory.CONFIG_TPGID),
				mSelectedGroup < 1? null : mGroupCodes.get(mSelectedGroup)
			};
		}
		else if(id == QueryID.Q76) {
			return mFactory.isJson()?
					new Object[] {
							Integer.toString(mDjno),
							mAddress,
							mName,
							mSocialNo,
							mDesc,
							mFactory.getProperty(ResourceFactory.CONFIG_TPGID),
							mFactory.getProperty(ResourceFactory.CONFIG_TPGID)}
			:new Object[] {
					mDjno,
					mAddress,
					mName,
					mSocialNo,
					mDesc,
					mFactory.getProperty(ResourceFactory.CONFIG_TPGID),
					mFactory.getProperty(ResourceFactory.CONFIG_TPGID)
							};
		}

		return null; // Query에 요구되는 파라메터가 없을 경우 null을 반환한다.
    }

	@Override
	public void onPrepareUpdate(SQLNO qn, String[] jsonParam) {
		// TODO Auto-generated method stub
		super.onPrepareUpdate(qn, jsonParam);
		switch (qn) {
		case Q76:
		case Q26:
			if(!mIsDuplicate)
			mFactory.jsonCommit(qn, jsonParam);
			else{
				mIsDuplicate = false;
				mBtnOkay.setClickable(true);
				}
			break;

		default:
			break;
		}
	}
	@Override
	public void onEvent(int qn, int updateCount) {
		// TODO Auto-generated method stub
		super.onEvent(qn, updateCount);
		switch (qn) {
		case 76:
		case 26:
			if(updateCount==1){
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					View v = mContentView.findViewById(R.id.btnByeFragment);
					v.setTag(mDjno); // 추가 등재된 선거인을 조회하여 바로 발급 진행한다.
					v.performClick();
				}
			});
			}else{
				showDefaultMessage(R.string.menu_issue_02_result_cant_add);

				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mBtnOkay.setClickable(true);
					}
				});	
			}
			break;

		default:
			break;
		}
	}
	@Override
	public void onEvent(int qn, String arg0) {
		// TODO Auto-generated method stub
		super.onEvent(qn, arg0);
		try {
			JSONArray arr = new JSONArray(arg0);
			JSONObject res;
			switch (qn) {
			case 75:
			case 25:
				for (int i = 0; i < arr.length(); i++) {
					res = arr.getJSONObject(i);
					if(!res.getString("DUP").equals("0") && mFactory.getProperty(ResourceFactory.CONFIG_RECOGNIZE).equals(ResourceFactory.RECOGNIZE_full)){
					showDefaultMessage(R.string.menu_issue_02_result_duplicated);
					mIsDuplicate = true;
					mBtnOkay.setClickable(true);
					}
					}
				break;
			case 74:
			case 28:
				if(arr.length()<1){
					showDefaultMessage(R.string.menu_issue_02_result_unknown_djno);
					mBtnOkay.setClickable(true);}
				else for (int i = 0; i < arr.length(); i++) {
					res = arr.getJSONObject(i);
					mDjno = Integer.valueOf(res.getString("DJ_MAX_NO"));
					if(ResourceFactory.UI_TEST)
						mFactory.dbQuery(QueryID.Q76);
					else
						mFactory.dbQuery(QueryID.Q26);
					}
				break;
			case 19:
				for (int i = 0; i < arr.length(); i++) {
					res = arr.getJSONObject(i);
					if(ResourceFactory.UI_TEST){
						mPlaceCodes.add(mFactory.getProperty(ResourceFactory.CONFIG_TPGID));
						mPlaceNames.add(mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME));
					}
					else{
						mPlaceCodes.add(res.getString("TPG_ID"));
						mPlaceNames.add(res.getString("TPG_NAME"));
						
					}
				}
				break;
			case 23:
				mGroupCodes.add("");
				mGroupNames.add(getString(R.string.menu_issue_02_weight_noselect));

				for (int i = 0; i < arr.length(); i++) {
					res = arr.getJSONObject(i);
					mGroupCodes.add(res.getString("SGIG_ID"));
					mGroupNames.add(res.getString("SGIG_NAME"));
				}

				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (mPlaceCodes.size() == 0)
							mInfoStatus.setText(R.string.menu_issue_02_status_noplace);
						else {
							if (mPlaceCodes.size() == 1) {
								mVotePlace.setText(mPlaceNames.get(0));
								((TextView)mContentView.findViewById(R.id.infoVotePlaceCode)).setText(mPlaceCodes.get(0));
								mSelectedPlace = 0;
							}

							mVotePlace.setEnabled(mPlaceCodes.size() > 1);
							mVoteGroup.setEnabled(mGroupCodes.size() > 1);
							mContentView.findViewById(R.id.contentLayout).setVisibility(View.VISIBLE);
							mContentView.findViewById(R.id.statusLayout).setVisibility(View.GONE);
							mReady = true;
						}
					}
				});
				break;
			default:
				break;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
		}
	}
	
	@Override
	@SuppressWarnings("incomplete-switch")
    public boolean onResultSet(ResultSet rs, QueryID id) throws SQLException {
		boolean moreQuery = true;

		switch(id) {
			case Q75: 
			case Q25:
				if ((rs.next() == false || rs.getInt(1) > 0)&&mFactory.getProperty(ResourceFactory.CONFIG_RECOGNIZE).equals(ResourceFactory.RECOGNIZE_full)) {
					showDefaultMessage(R.string.menu_issue_02_result_duplicated);
					mBtnOkay.setClickable(true);
					moreQuery = false;
				}
				break;
			case Q74:
			case Q28:
				if (rs.next()){
					mDjno = Integer.valueOf(rs.getString(1));
					if(ResourceFactory.UI_TEST)
						mFactory.dbQuery(QueryID.Q76);
					else
						mFactory.dbQuery(QueryID.Q26);
				}else {
					showDefaultMessage(R.string.menu_issue_02_result_unknown_djno);
					mBtnOkay.setClickable(true);
					moreQuery = false;
				}
				break;

			case Q19: // 투표구 목록
				if(ResourceFactory.UI_TEST){
					mPlaceCodes.add(mFactory.getProperty(ResourceFactory.CONFIG_TPGID));
					mPlaceNames.add(mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME));
				}
				else{
					while (rs.next()) {
						mPlaceCodes.add(rs.getString("TPG_ID"));
						mPlaceNames.add(rs.getString("TPG_NAME"));
					}
				}
				break;

			case Q23: // 가중치 그룹
				mGroupCodes.add("");
				mGroupNames.add(getString(R.string.menu_issue_02_weight_noselect));

				while (rs.next()) {
					mGroupCodes.add(rs.getString("SGIG_ID"));
					mGroupNames.add(rs.getString("SGIG_NAME"));
				}

				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (mPlaceCodes.size() == 0)
							mInfoStatus.setText(R.string.menu_issue_02_status_noplace);
						else {
							if (mPlaceCodes.size() == 1) {
								mVotePlace.setText(mPlaceNames.get(0));
								((TextView)mContentView.findViewById(R.id.infoVotePlaceCode)).setText(mPlaceCodes.get(0));
								mSelectedPlace = 0;
							}

							mVotePlace.setEnabled(mPlaceCodes.size() > 1);
							mVoteGroup.setEnabled(mGroupCodes.size() > 1);
							mContentView.findViewById(R.id.contentLayout).setVisibility(View.VISIBLE);
							mContentView.findViewById(R.id.statusLayout).setVisibility(View.GONE);
							mReady = true;
						}
					}
				});
				break;
		}

		return moreQuery;
    }

	@Override
    public boolean onResultSet(int rows, QueryID id) throws SQLException {
		if (id == QueryID.Q26 || id == QueryID.Q76) {
			if (rows == 1) {
				mFactory.transaction(ResourceFactory.TRANSACTION_COMMIT);

				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						View v = mContentView.findViewById(R.id.btnByeFragment);
						v.setTag(mDjno); // 추가 등재된 선거인을 조회하여 바로 발급 진행한다.
						v.performClick();
					}
				});
			}
			else {
				mFactory.transaction(ResourceFactory.TRANSACTION_ROLLBACK);
				showDefaultMessage(R.string.menu_issue_02_result_cant_add);
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mBtnOkay.setClickable(true);
					}
				});
			}
		}

		return true;
	}

	@Override
	public void onClick(View v) {
		v.setClickable(false);

		if (v.getId() == R.id.infoVotePlace) {
			if (mPlaceSelector == null) {
				mPlaceSelector = new SelectDialog(getActivity(), getString(R.string.menu_issue_02_voting_place), mPlaceNames.toArray(), mSelectedPlace);
				mPlaceSelector.setOnDismissListener(this);
			}

			mPlaceSelector.show();
		}
		else if(v.getId() == R.id.infoWeightGroup) {
			if (mGroupCodes.size() > 0) {
				if (mGroupSelector == null) {
					mGroupSelector = new SelectDialog(getActivity(), getString(R.string.menu_issue_02_weight_group), mGroupNames.toArray(), mSelectedGroup);
					mGroupSelector.setOnDismissListener(this);
				}

				mGroupSelector.show();
			}
		}
		else {
			mName = ((EditText)mContentView.findViewById(R.id.editElectorName)).getText().toString();
			mSocialNo = ((EditText)mContentView.findViewById(R.id.editElectorSocialNo)).getText().toString();
			mAddress = ((EditText)mContentView.findViewById(R.id.editElectorAddress)).getText().toString();
			mDesc = ((EditText)mContentView.findViewById(R.id.editElectorDesc)).getText().toString();
			Pattern ps = Pattern.compile("^[ㄱ-ㅎㅏ-ㅣ가-힣]+$");
			if (mName.isEmpty() || mSocialNo.isEmpty() || mAddress.isEmpty() || mDesc.isEmpty()){
				v.setClickable(true);
				showDefaultMessage(R.string.menu_issue_02_status_require);}
			else if(mName.getBytes().length > 18){
				v.setClickable(true);
				showDefaultMessage(getString(R.string.format_over_limit, getString(R.string.menu_issue_02_elector_name), 18/3));}
			else if(mSocialNo.length() > 13){
				v.setClickable(true);
				showDefaultMessage(getString(R.string.format_over_limit, getString(R.string.menu_issue_02_elector_socialno), 13));
				}
			else if(mSocialNo.length() < 6){
				v.setClickable(true);
				showDefaultMessage(R.string.menu_issue_02_status_ssn);}
			else if(mAddress.getBytes().length > 90){
				v.setClickable(true);
				showDefaultMessage(getString(R.string.format_over_limit, getString(R.string.menu_issue_01_address), 90/3));}
			else if(mDesc.getBytes().length >152){
				v.setClickable(true);
				showDefaultMessage(getString(R.string.format_over_limit, getString(R.string.menu_issue_01_desc), 152/3));}
			else if(Locale.getDefault().getDisplayLanguage().equals("ko")&&!ps.matcher(mName).matches()){
				Log.d("ADDFRAGMENT", "message : "+mName);
				v.setClickable(true);
				showDefaultMessage(R.string.format_other_char);
			}
			else if(mFactory.dbQuery(ResourceFactory.UI_TEST?QueryID.Q75:QueryID.Q25, ResourceFactory.UI_TEST?QueryID.Q74:QueryID.Q28));
				return;
		}

		v.setClickable(true);
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		if (dialog.equals(mPlaceSelector)) {
			if (mSelectedPlace != mPlaceSelector.getSelectedIndex()) {
				mSelectedPlace = mPlaceSelector.getSelectedIndex();

				mVotePlace.setText(mPlaceNames.get(mSelectedPlace));
				((TextView)mContentView.findViewById(R.id.infoVotePlaceCode)).setText(mPlaceCodes.get(mSelectedPlace));
			}
		}
		else if(mSelectedGroup != mGroupSelector.getSelectedIndex()) {
			mSelectedGroup = mGroupSelector.getSelectedIndex();

			mVoteGroup.setText(mGroupNames.get(mSelectedGroup));
			((TextView)mContentView.findViewById(R.id.infoWeightGroupCode)).setText(mGroupCodes.get(mSelectedGroup));
		}
	}
}