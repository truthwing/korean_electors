package com.mirusys.vt.fragment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.haesin.util.EditTextWithClearButton;
import com.haesin.util.SingleChoiceAdapter;
import com.haesinit.common.Base64Util;
import com.integratedbiometrics.ibscancommon.IBCommon.ImageDataExt;
import com.mirusys.electors.MainActivity;
import com.mirusys.electors.R;
import com.mirusys.electors.ResourceFactory;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.electors.ResourceFactory.QueryID;
import com.mirusys.sign.SeedCrypto;
import com.mirusys.sign.SimpleCrypto;
import com.mirusys.voting.MiruSmartCard;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import ksign.jce.util.Base64;

public class IssuedTicketFragment extends BasicFragment{
	private static final String TAG = "IssuedTicketFragment";
	private static final int SET_LIST = 0;
	private static final int SET_DETAIL = 1;
	private static final int SET_IMAGE = 2;
	private static final int SET_SEARCH = 3;
	private static final int SET_RELAYOUT = 4;
	private static final int SET_MATCH = 5;
	private static final int SET_CLEAR = 6;
	
	private View mContentView;
	private ListView mListView;
	private Dialog mPopupCheck = null;
	private Dialog mReqDialog = null;
	private Dialog mVerifyFingerDialog = null;
	private ArrayList<String> mPlaceCodes = new ArrayList<String>();
	private ArrayList<Object[]> mElectors;
	
	private EditText[] mSearchMethods = null;
	private ViewGroup mIssueDetail = null;
	private TextView mTvName = null;
	private TextView mTvBirthNo = null;
	private TextView mTvRegNo = null;
	private TextView mTvAddress = null;
	private TextView mTvDesc = null;
	private TextView mTvIssueDate = null;
	private TextView mTvIssuePlace = null;
	private Object[] mElector = new Object[15];
	private ImageView mIvPreviewFingerprint = null;
	private ImageView mIvIDCard = null;
	private int mPosition;
	private boolean mIsReissue = false;
	private GestureDetector mGestureItem;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		mContentView = inflater.inflate(R.layout.frag_vote_issued_ticket, container, false);
		mFactory.dbQuery(QueryID.Q19);
		
		mListView = (ListView)mContentView.findViewById(R.id.lvElectors);
		mGestureItem = new GestureDetector((MainActivity)getActivity(), new DoubleItemTap());
		setSearchMethod();
		btnSetEnable(false);
		return mContentView;
	}
	
	private void setSearchMethod(){
		if (mSearchMethods == null) {
			mSearchMethods = new EditText[3];
			mSearchMethods[0] = (EditText)((MainActivity)getActivity()).findViewById(R.id.editSearchByRegNo);
			mSearchMethods[1] = (EditText)((MainActivity)getActivity()).findViewById(R.id.editSearchByName);
			mSearchMethods[2] = (EditText)((MainActivity)getActivity()).findViewById(R.id.editSearchBySocialNo);
		}
	}
	
	private void btnSetEnable(boolean enable){
		mContentView.findViewById(R.id.btnIssueCancel).setEnabled(enable);
		mContentView.findViewById(R.id.btnReissue).setEnabled(enable);
		mContentView.findViewById(R.id.btnSnapshot).setEnabled(enable);
		mContentView.findViewById(R.id.btnVerify).setEnabled(enable&&mElector[9].toString().equals("F"));
	}
	
	@Override
	public Object[] getParameters(QueryID id) {
		// TODO Auto-generated method stub
		switch (id) {
		case Q32:
			return new Object[] {
					mFactory.getProperty(ResourceFactory.CONFIG_TPGID),
					mFactory.isJson()?new SimpleDateFormat("yy-MM-dd").format(new Date(mFactory.getVtDate())):new java.sql.Date(mFactory.getVtDate())
				};
		case Q70:
		case Q13:
			String[] searchData=((MainActivity)getActivity()).getSearchData();
			String recorgnizedNo = ((MainActivity)getActivity()).getRecorgnizedNo();
			return new Object[] {
					searchData[0],
					searchData[1],
					recorgnizedNo==null?searchData[2]:recorgnizedNo
			};
		case Q72:
			return 	new Object[] {
					mElector[0].toString()
			};
		case Q14:
			return mFactory.isJson()?
					new Object[] {
				mElector[0].toString(),
				mElector[12].toString()
			}
		:new Object[] {
				mElector[12].toString(),
				mElector[0].toString()
			};
		case Q17:
			return new Object[] {
					getResources().getString(R.string.popupReIssue).equals(
							((TextView)mReqDialog.findViewById(android.R.id.title)).getText().toString())
					?"A":"B",//발급취소
					mElector[5],
					mElector[0].toString(),
					mElector[12]
				};
		case Q77:
		case Q18:
			Log.d(TAG, "77 5:"+mElector[5]+",0:"+mElector[0]);
			return mFactory.isJson()&&!ResourceFactory.UI_TEST ? 
					new Object[]{
					mElector[5],
					mElector[0].toString(),
					mElector[12]
			}
			:new Object[] {
					mElector[5],
					mElector[0].toString()
				};
		default:
			break;
		}
		return null;
	}
	
	@Override
    public boolean onResultSet(int rows, QueryID id) throws SQLException {
		Log.d(TAG, "onResultSet for " + id + ", changed: " + rows);
		switch (id) {
		case Q18:
		case Q77:
			dismissedMessageBox();
			mFactory.transaction(ResourceFactory.TRANSACTION_COMMIT);
			mHandler.sendMessage(mHandler.obtainMessage(SET_SEARCH, mIsReissue?1:0, 0));
			mIsReissue = false;
			mHandler.sendEmptyMessage(SET_RELAYOUT);
			break;
		default:
			break;
		}
		return true;
    }
	@Override
	public boolean onResultSet(ResultSet rs, QueryID id) throws SQLException {
		// TODO Auto-generated method stub
		boolean isMoreQuery = true;
		switch (id) {
		case Q19:
			while (rs.next()) {
				mPlaceCodes.remove(rs.getString("TPG_ID"));
				mPlaceCodes.add(rs.getString("TPG_ID"));
			}
			break;
		case Q32:
			if (rs.next()
					&& ResourceFactory.VOTE_working.equals(rs
							.getString("STATUS"))){
//				mIssueDate = rs.getDate("NOW");
			}else {
				// 개시 상태가 아닌 경우 (DB기준) 오류 팝업
//				mIsEnter = true;
				showDefaultMessage(R.string.already_vote_finished);
				mElectors = new ArrayList<Object[]>();
				isMoreQuery = false;
			}
			break;
		case Q70:
		case Q13:
			String str = null;
			boolean isTpgId = false;
			boolean isTPGIDYn = false;
			mElectors = new ArrayList<Object[]>();
			while (rs.next()) {
				if(!ResourceFactory.UI_TEST)
				for (String i : mPlaceCodes) {
					if (i.equals(rs.getString("TPG_ID")))
						isTpgId = true;
				}
				Object[] elector = new Object[15];
				if ((ResourceFactory.UI_TEST&&rs.getString("VT_YN").equals("N"))||(isTpgId&& (mFactory.getProperty(ResourceFactory.CONFIG_TPGID).equals(
								rs.getString("TPG_ID_YN").equals("Y") ? rs.getString("VT_TPG_ID"): mFactory.getProperty(ResourceFactory.CONFIG_TPGID)))
								&& rs.getString("VT_YN").equals("N"))) 
				{
					if(rs.getString("VT_TPG_ID")==null)
						elector[13] = null;
					else
					elector[13] = rs.getString("VT_TPG_ID").equals("") ? ""
							: rs.getString("VT_TPG_ID");
					elector[0] = rs.getString("DJ_NO");
					elector[1] = rs.getString("ADDR");
					elector[2] = rs.getString("NAME");
					elector[6] = rs.getString("JUMIN_NO");
					elector[5] = rs.getString("ETC");
					elector[4] = rs.getString("VT_YN"); // 'Y'발급가능, 'N'발급불가
					elector[7] = new SimpleDateFormat("yyyy.MM.dd  ").format(rs.getDate("VT_DATE"))+new SimpleDateFormat("a hh:mm").format(rs.getTime("VT_DATE")); // 발급일자
					if(!ResourceFactory.UI_TEST){
						elector[8] = rs.getString("TPG_NAME"); // 발급장소
						elector[12] = rs.getString("TPG_ID");
						elector[3] = rs.getString("BIRTH_DATE");
						elector[14] = rs.getString("ISSUED_LOCATION");
					}else{
						elector[8] = mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME); // 발급장소
						elector[12] = mFactory.getProperty(ResourceFactory.CONFIG_TPGID);
					}
						
					elector[11] = true;
					mElectors.add(elector);
				}else if(!ResourceFactory.UI_TEST){
					isTPGIDYn=rs.getString("TPG_ID_YN").equals("Y");//지정여부
					str = rs.getString("TPG_NAME");
			}
			}
			if (mElectors.size() == 0) {
				if(isTPGIDYn)
					showDefaultMessage(getString(R.string.error_TPS_elector,str));//지정일경우에 해당 투표소 안내
				else
					showDefaultMessage(R.string.err_no_search_issued);
				((MainActivity)getActivity()).setIsEnter(true);
			}else
				mHandler.sendEmptyMessage(SET_LIST);
			break;
		case Q72:
		case Q14:
			if(rs.next()){
			if(mElector!=null){
				mElector[9]=rs.getString("SEAL_CD");
				byte[] image = null;
				try {
					image = ResourceFactory.UI_TEST?rs.getBytes("SEAL_IMG"):SimpleCrypto.Decrypt(rs.getBytes("SEAL_IMG"), mFactory.getTcardKey());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(mElector[9].toString().equals("F"))
					mElector[10]=mFactory.loadImageFromBuffer(image);
				else
					mElector[10]=BitmapFactory.decodeByteArray(image, 0, image.length);
				((MainActivity)getActivity()).setmElector(mElector);
			}
			mHandler.sendEmptyMessage(SET_IMAGE);
			}
		default:
			break;
		}
		return isMoreQuery;
	}
	
	@Override
	public void onEvent(int qn, String arg0) {
		// TODO Auto-generated method stub
		super.onEvent(qn, arg0);
		JSONArray arr;
		try {
			arr = new JSONArray(arg0);
			JSONObject rs;
				switch (qn) {
				case 19:
					for (int i = 0; i < arr.length(); i++) {
						rs = arr.getJSONObject(i);
					mPlaceCodes.remove(rs.getString("TPG_ID"));
					mPlaceCodes.add(rs.getString("TPG_ID"));
					}
					break;
				case 32:
					for (int i = 0; i < arr.length(); i++) {
						rs = arr.getJSONObject(i);} 
					break;
				case 70:
				case 13:
					String str = null;
					boolean isTpgId = false;
					boolean isTPGIDYn = false;
					mElectors = new ArrayList<Object[]>();
					for (int i = 0; i < arr.length(); i++) {
						rs = arr.getJSONObject(i);
						if(!ResourceFactory.UI_TEST)
							for (String place : mPlaceCodes) {
								if (place.equals(rs.getString("TPG_ID")))
									isTpgId = true;
							}
						Object[] elector = new Object[15];
						if ((ResourceFactory.UI_TEST &&rs.getString("VT_YN").equals("N")) || isTpgId&& (mFactory.getProperty(ResourceFactory.CONFIG_TPGID).equals(
										rs.getString("TPG_ID_YN").equals("Y") ? rs.getString("VT_TPG_ID"): mFactory.getProperty(ResourceFactory.CONFIG_TPGID)))
										&& rs.getString("VT_YN").equals("N")) 
						{
							elector[13] = rs.getString("VT_TPG_ID").equals("") ? ""
									: rs.getString("VT_TPG_ID");
							elector[0] = rs.getString("DJ_NO");
							elector[1] = rs.getString(ResourceFactory.UI_TEST?"REMARK":"ADDR");
							elector[2] = rs.getString("NAME");
							elector[6] = rs.getString("JUMIN_NO");
							elector[5] = rs.getString("ETC");
							elector[4] = rs.getString("VT_YN"); // 'Y'발급가능, 'N'발급불가
							try {
								elector[7] = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").parse(rs.getString("VT_DATE"));
							} catch (ParseException e) {
								Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
							} // 발급일자
							if(!ResourceFactory.UI_TEST){
								elector[8] = rs.getString("TPG_NAME"); // 발급장소
								elector[12] = rs.getString("TPG_ID");
								elector[3] = rs.getString("BIRTH_DATE");
								elector[14] = rs.getString("ISSUED_LOCATION");
							}else{
								elector[8] = mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME); // 발급장소
								elector[12] = mFactory.getProperty(ResourceFactory.CONFIG_TPGID);
							}
							elector[11] = true;
							mElectors.add(elector);
						}else if(!ResourceFactory.UI_TEST){
								isTPGIDYn=rs.getString("TPG_ID_YN").equals("Y");//지정여부
								str = rs.getString("TPG_NAME");
						}
					} 
					if (mElectors.size()==0) {
//						if(isTPGIDYn)
//							showDefaultMessage(getString(R.string.error_TPS_elector,str));//지정일경우에 해당 투표소 안내
//						else
							showDefaultMessage(R.string.err_no_search_issued);
						((MainActivity)getActivity()).setIsEnter(true);
					}else
						mHandler.sendEmptyMessage(SET_LIST);
					break;
				case 72:
				case 14:
					for (int i = 0; i < arr.length(); i++) {
						rs = arr.getJSONObject(i);
						if(rs.getString("SEAL_CD")!=null){
							mElector[9]=rs.getString("SEAL_CD");
							byte[] image;
							try {
								image = ResourceFactory.UI_TEST?Base64.decode(rs.getString("SEAL_IMG")):SimpleCrypto.Decrypt(Base64Util.decode(rs.getString("SEAL_IMG")), mFactory.getTcardKey());
								if(mElector[9].toString().equals("F"))
									mElector[10]=mFactory.loadImageFromBuffer(image);
								else
									mElector[10]=BitmapFactory.decodeByteArray(image, 0, image.length);
							} catch (IllegalArgumentException e) {
								Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
							} catch (Exception e) {
								Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
							}
							((MainActivity)getActivity()).setmElector(mElector);
							mHandler.sendEmptyMessage(SET_IMAGE);
						}
					}
					break;
				default:
					break;
				}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
		}
	}
	
	public void onPrepareUpdate(com.haesinit.ultralite.database.SQLNO qn, String[] jsonParam) {
		switch (qn) {
		case Q77:
		case Q18:
			dismissedMessageBox();
			mHandler.sendMessage(mHandler.obtainMessage(SET_SEARCH, mIsReissue?1:0, 0));
			mIsReissue = false;
		case Q17:
			mFactory.jsonCommit(qn, jsonParam);
			break;

		default:
			break;
		}
		mHandler.sendEmptyMessage(SET_RELAYOUT);
	};
	
	
	
	Handler mHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case SET_LIST:
				SingleChoiceAdapter adapter = new SingleChoiceAdapter((MainActivity)getActivity(), mElectors);
				if(mElectors.size()==1){
					mElector = mElectors.get(0);
					mPosition = 0;
					mHandler.sendEmptyMessage(SET_DETAIL);
					mFactory.dbQuery(ResourceFactory.UI_TEST?QueryID.Q72:QueryID.Q14);
				}
				adapter.setGestureItem(mGestureItem);
				mListView.setAdapter(adapter);
				break;
			case SET_DETAIL:
				selectedItem();
				break;
			case SET_IMAGE:
				setImage(mElector);
				break;
			case SET_SEARCH:
				if(msg.arg1==1)
				((EditTextWithClearButton)((MainActivity)getActivity()).findViewById(R.id.editSearchByRegNo)).setText(mElector[0].toString());
				else{
					((EditTextWithClearButton)((MainActivity)getActivity()).findViewById(R.id.editSearchByRegNo)).setText("");
					((EditTextWithClearButton)((MainActivity)getActivity()).findViewById(R.id.editSearchByName)).setText("");
					((EditTextWithClearButton)((MainActivity)getActivity()).findViewById(R.id.editSearchBySocialNo)).setText("");
					}
				break;
			case SET_RELAYOUT:
				((MainActivity)getActivity()).relayout(IssuedTicketFragment.this, null);
				break;
			case SET_CLEAR:
					SingleChoiceAdapter clear = (SingleChoiceAdapter)mListView.getAdapter();
					Object[] clearElector;
					if(clear!=null && 
							clear.getSelectedIndex() != -1){
						clearElector = clear.getItem(clear.getSelectedIndex())!=null?clear.getItem(clear.getSelectedIndex()):new Object[15];
						clearElector[ 4] = null; // VT_YN
						clearElector[ 5] = null; // ETC
						clearElector[ 7] = null; // VT_DATE
						clearElector[ 9] = null; // SEAL_CD
						clearElector[10] = null; // SEAL_IMG
					}else{
						 clearElector= null;
					}
			        mPosition = -1;
			        selectedItem();  //reselect and update detail
			        setImage(null);
			        if(clear!=null){
			        	clear.clear();
			        	clear.setNotifyOnChange(true);
			        }
			        mListView.setActivated(true);
				break;
			default:
				break;
			};
		}

	};
	
	
	private void selectedItem(){
		SingleChoiceAdapter adapter = (SingleChoiceAdapter) mListView
					.getAdapter();
		if(adapter != null){
			final Object[] elector = mPosition == -1 ? null : adapter.getItem(mPosition);
			final boolean showActionBar = elector != null
												&& "N".equals(elector[4]);
			displayDetail(elector, showActionBar);
			adapter.setSelectedIndex(mPosition);
			
			if (mIssueDetail != null
					|| (mIssueDetail = (ViewGroup) mContentView.findViewById(R.id.layoutElectorInfo)) != null) {
				mIssueDetail.setVisibility(elector != null ? View.VISIBLE: View.GONE);
			}
			mElector = elector;
			
			if (mSearchMethods != null)
				if (mPosition == -1) {
					for (EditText et : mSearchMethods)
						et.setText("");
				} else {
					mSearchMethods[0].setText((String)elector[0]);
					mSearchMethods[1].setText((String)elector[2]);
					mSearchMethods[2].setText((String)elector[6]);
				}
			
			
//		((TextView)mContentView.findViewById(R.id.infoElectorName)).setText(elector!=null?elector[2].toString():null);
//		try {
//			((TextView)mContentView.findViewById(R.id.infoElectorBirthday)).setText(elector!=null?
//					new SimpleDateFormat("yy.MM.dd").format(
//					new SimpleDateFormat("yyMMdd").parse(elector[6].toString().substring(0, 5))):null);
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
//		}
//		((TextView)mContentView.findViewById(R.id.infoElectorRegNo)).setText(elector!=null?elector[0].toString():null);
//		((TextView)mContentView.findViewById(R.id.infoElectorAddress)).setText(elector!=null?elector[1].toString():null);
//		((TextView)mContentView.findViewById(R.id.infoIssuedDate)).setText(elector!=null?
//				new SimpleDateFormat("yyyy.MM.dd  a hh:mm", getResources().getConfiguration().locale).format(
//				elector[7]):null);
//		((TextView)mContentView.findViewById(R.id.infoIssuedPlace)).setText(elector!=null?elector[8].toString():null);
//		((TextView)mContentView.findViewById(R.id.infoIssuedPlace)).setText(mElector[5].toString());
		}
	}

	private void displayDetail(Object[] elector, boolean showActionBar) {
		// TODO Auto-generated method stub
		String juminNo = elector != null? elector[6].toString() : null;
		if (mTvName != null || (mTvName = (TextView)mContentView.findViewById(R.id.infoElectorName)) != null) {
			mTvName.setText(elector != null? elector[2].toString() : null);
		}
		if (mTvBirthNo != null || (mTvBirthNo = (TextView)mContentView.findViewById(R.id.infoElectorBirthday)) != null) {
			mTvBirthNo.setText(juminNo == null || juminNo.length() != 13? juminNo : juminNo.substring(0, 6) + '-' + juminNo.substring(6));
		}
		if (mTvRegNo != null || (mTvRegNo = (TextView)mContentView.findViewById(R.id.infoElectorRegNo)) != null) {
			mTvRegNo.setText(elector != null? elector[0].toString() : null);
		}
		if (mTvAddress != null || (mTvAddress = (TextView)mContentView.findViewById(R.id.infoElectorAddress)) != null) {
			mTvAddress.setText(elector == null || elector[1] == null? null : elector[1].toString());
		}
		if (mTvDesc != null || (mTvDesc = (TextView)mContentView.findViewById(R.id.infoElectorDesc)) != null) {
			mTvDesc.setText(elector == null || elector[5] == null? null : elector[5].toString());
		}
		
	//		if (mIvPreviewFingerprint != null ||
	//				mLayoutElector != null && (mIvPreviewFingerprint = (ImageView)mLayoutElector.findViewById(R.id.imgFingerprint4Elector)) != null) {
	//				mIvPreviewFingerprint.setImageBitmap(null);
	//			}
	//
	//			if (mIvIDCard != null || (mIvIDCard = (ImageView)findViewById(R.id.imgIDCard)) != null) {
	//				if (mRecorgnizedBitmap != null && elector != null && ((mRecorgnizedNo != null && juminNo.equals(mRecorgnizedNo)) || (mRecorgnizedName != null && elector[2].equals(mRecorgnizedName))))
	//					mIvIDCard.setImageBitmap(mRecorgnizedBitmap);
	//				else
	//					mIvIDCard.setImageResource(R.drawable.img_idcard_default_01);
	//			}
		
		if (mTvIssueDate != null || (mTvIssueDate = (TextView)mContentView.findViewById(R.id.infoIssuedDate)) != null) {
//			Log.i(TAG, "issued Date : "+elector[7]);
			mTvIssueDate.setText(elector == null || elector[7] == null? null : mFactory.isJson()?
					new SimpleDateFormat("yyyy-MM-dd hh:mm:dd a").format((Date)elector[7]):(String)elector[7]);
		}
		if (mTvIssuePlace != null || (mTvIssuePlace = (TextView)mContentView.findViewById(R.id.infoIssuedPlace)) != null) {
			mTvIssuePlace.setText(elector == null || elector[8] == null? null : elector[8].toString());
		}
		
		if (elector != null) {
			setElectorInfo(getString(R.string.format_elector_info, elector[2], juminNo.substring(0, 2), juminNo.substring(2, 4), juminNo.substring(4, 6)),
					juminNo.length() > 6 ? ((juminNo.charAt(6)-'0')%2):2); // 0 - MALE, 1 - FEMALE
		}
		
	}
	
	private void setElectorInfo(String strInfo, int iSex){
		((ImageView)mContentView.findViewById(R.id.imgGender)).setImageDrawable(getResources().getDrawable(
				(iSex==1?R.drawable.ic_gender_w:
					(iSex==0?R.drawable.ic_gender_m:R.drawable.ic_gender_x))));
	}

	private void setImage(Object[] elector) {
		((ImageView)mContentView.findViewById(R.id.imgVerifyFingerprint)).setImageBitmap(elector==null?null:
				elector[9].toString().equals("F")?mFactory.convertImageToBitmap((ImageDataExt)elector[10]):null);
		((ImageView)mContentView.findViewById(R.id.imgVerifySignatureRead)).setImageBitmap(elector==null?null:
				elector[9].toString().equals("S")?
						(Bitmap)elector[10]:null);
		((MainActivity)getActivity()).findViewById(R.id.btnScreenClean).setEnabled(true);
		if(elector!=null)
			btnSetEnable(true);
		else
			btnSetEnable(false);
	}

	private class DoubleItemTap extends GestureDetector.SimpleOnGestureListener{
		@Override
		public boolean onDown(MotionEvent e) {
			// TODO Auto-generated method stub
			return true;
		}
		
		@Override
		public boolean onDoubleTap(MotionEvent e) {
			SingleChoiceAdapter adapter = (SingleChoiceAdapter) mListView.getAdapter();
			mPosition = 0;
			mElector= adapter.getItem(mPosition);
			mHandler.sendEmptyMessage(SET_DETAIL);
			mFactory.dbQuery(ResourceFactory.UI_TEST?QueryID.Q72:QueryID.Q14);
			return true;
		}
	}
	
	public void setPosition(int position){
		mPosition = position;
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		v.setClickable(false);
		switch (v.getId()) {
		case R.id.btnReissue:
			mIsReissue = true;
		case R.id.btnIssueCancel:
			mPopupCheck = new Dialog((MainActivity)getActivity());
			mPopupCheck.setContentView(R.layout.popup_issued_check_card);
			((TextView)mPopupCheck.findViewById(R.id.checkTitle)).setText(v.getId()==R.id.btnIssueCancel?R.string.popupIssuedCancel:R.string.popupReIssue);
			mPopupCheck.findViewById(R.id.btnCheckOk).setEnabled(false);
			mPopupCheck.findViewById(R.id.btnCheckOk).setOnClickListener(this);
			mPopupCheck.findViewById(R.id.btnCheckCancel).setOnClickListener(this);
			mPopupCheck.show();
			break;
		case R.id.btnSnapshot:
			((MainActivity)getActivity()).snapshot();
			break;
		case R.id.btnVerify:
			v.setTag(mElector);
			((MainActivity)getActivity()).onClick(v);
			break;
		case R.id.btnCheckOk:
			if(mPopupCheck!=null&&mPopupCheck.isShowing()){
				mReqDialog = new Dialog((MainActivity)getActivity());
				mReqDialog.setCancelable(false);
				mReqDialog.setContentView(R.layout.popup_issue_input);
				mReqDialog.findViewById(R.id.btnOkay).setOnClickListener(this);
				mReqDialog.findViewById(R.id.btnCancel).setOnClickListener(this);
				((TextView)mReqDialog.findViewById(R.id.dateVote)).setText((String)mElector[7]);//발급일시
				((TextView)mReqDialog.findViewById(R.id.localVote)).setText(new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(new Date()));//현재일시
				((TextView)mReqDialog.findViewById(R.id.ipVote)).setText((String)mElector[1]);//주소
				((TextView)mReqDialog.findViewById(R.id.djNoVote)).setText((String)mElector[0]);//등재번호
				((TextView)mReqDialog.findViewById(R.id.nameVote)).setText((String)mElector[2]);//이름
				((TextView)mReqDialog.findViewById(R.id.birthVote)).setText((String)mElector[6]);//주민번호
				((TextView)mReqDialog.findViewById(android.R.id.title)).setText(((TextView)mPopupCheck.findViewById(R.id.checkTitle)).getText());
			}
		case R.id.btnCheckCancel:
			if(mPopupCheck!=null&&mPopupCheck.isShowing())
				mPopupCheck.dismiss();
			break;
		case R.id.btnOkay:
			if(mReqDialog!=null&&mReqDialog.isShowing()){
				String str = ((EditTextWithClearButton)mReqDialog.findViewById(android.R.id.edit)).getText().toString();
				if(str.isEmpty()){
					dismissedMessageBox();
					showDefaultMessage(R.string.menu_issue_01_popup_issue_cancel_need);}
				else if(str.getBytes().length > 54*3){
					dismissedMessageBox();
					showDefaultMessage(getString(R.string.format_over_limit, getString(R.string.menu_issue_01_desc), 54));}
				else {
					mElector[5] = str;
					
					mFactory.dbQuery(
							//QueryID.Q17,오류로 임시 태그
							ResourceFactory.UI_TEST?QueryID.Q77:QueryID.Q18);
				}
			}
				
		case R.id.btnCancel:
			if(mReqDialog!=null&&mReqDialog.isShowing()){
				mReqDialog.dismiss();
				mReqDialog=null;
			}else if(mVerifyFingerDialog!=null&&mVerifyFingerDialog.isShowing()){
				mVerifyFingerDialog.dismiss();
				mVerifyFingerDialog = null;
			}
			break;
		case R.id.btnScreenClean:
				SingleChoiceAdapter adapter1 = (SingleChoiceAdapter)mListView.getAdapter();
				if(adapter1!=null&&mListView.getCount()>0){
					mHandler.sendEmptyMessage(SET_CLEAR);
			}
			break;
		default:
			break;
		}
		if(mReqDialog!=null&&!mReqDialog.isShowing())
			mReqDialog.show();
		v.setClickable(true);
	}

	@Override
	public void onAttached(MiruSmartCard card, byte type) {
		// TODO Auto-generated method stub
		super.onAttached(card, type);
		if(mPopupCheck!=null && 
				mPopupCheck.isShowing() && type=='T'){
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					mPopupCheck.findViewById(R.id.btnCheckOk).setEnabled(true);
				}
			});
		}
		
	}
	
	@Override
	public boolean onIdle(MiruSmartCard card) {
		// TODO Auto-generated method stub
		if(card!=null&&mFactory.isCardAttached())
			onAttached(card, card.CardGetType().getBytes()[0]);
		return super.onIdle(card);
	}
	
	@Override
	public void onDetached(MiruSmartCard card) {
		// TODO Auto-generated method stub
		super.onDetached(card);
		if(mPopupCheck!=null && mPopupCheck.isShowing())
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					mPopupCheck.findViewById(R.id.btnCheckOk).setEnabled(false);
				}
			});
	}
}
