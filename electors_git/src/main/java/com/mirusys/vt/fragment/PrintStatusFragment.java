package com.mirusys.vt.fragment;

import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.haesin.entities.VoteStatusObject;
import com.haesin.util.PdfExport;
import com.mirusys.electors.MainActivity;
import com.mirusys.electors.R;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.electors.ResourceFactory.QueryID;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class PrintStatusFragment extends BasicFragment implements OnClickListener {
	private final String TAG="PrintStatusFragment"; 
	
	
	private boolean mIsPreview=false;
    private View mContentView = null;
    private int[] mCnt;
    private int mTime = 120;
    
    
    private EditText mSelectTime, mIssCnt, mRate;
    private TextView mTimeChecker;
    private ListView mListView;
    private ArrayList<VoteStatusObject> mVoteStatuses = new ArrayList<VoteStatusObject>();
    private VoteStatusAdapter mVsAdapter=null;
    
    private Context mContext;
    
    private final int UI_SET_UPSIDE=0;
    private final int UI_DATA_CHANGE=1;
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.frag_print_status, container, false);
		mContext = getActivity().getApplicationContext();
		((EditText)mContentView.findViewById(R.id.et_vote_cnt)).setEnabled(false);
		(mIssCnt = (EditText)mContentView.findViewById(R.id.et_vote_isscnt)).setEnabled(false);
		(mRate = (EditText)mContentView.findViewById(R.id.et_vote_rate)).setEnabled(false);
		mTimeChecker = (TextView) mContentView.findViewById(R.id.time_timer);
		mSelectTime = (EditText)mContentView.findViewById(R.id.et_select_time);
		TextView tv = (TextView)mContentView.findViewById(R.id.tv_status_time);
		tv.setText(getString(R.string.print_status_time,mTime));
		mSelectTime.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (event.getAction() == KeyEvent.ACTION_DOWN)
					if (keyCode == KeyEvent.KEYCODE_ENTER) {
						if(mSelectTime.getText().toString().equals("0"))
							showDefaultMessage(R.string.error_search_0);
						else{
						mTime = Integer.parseInt(mSelectTime.getText().toString()
								.equals("")? mSelectTime.getHint().toString()
								: mSelectTime.getText().toString());
						TextView tv = (TextView)mContentView.findViewById(R.id.tv_status_time);
						tv.setText(getString(R.string.print_status_time,mTime));
						startTimer();
						}
						return true;
					}
				return false;
			}
		});
		mListView = (ListView) mContentView.findViewById(R.id.lv_vote_info);
		mCnt=new int[3];
		mFactory.dbQuery(QueryID.Q11, QueryID.Q12,QueryID.Q10);
		mVsAdapter = new VoteStatusAdapter();
		mListView.setAdapter(mVsAdapter);
		return mContentView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		mContentView.findViewById(R.id.btn_preview).setOnClickListener(this);
		startTimer();
	}
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (mPrintTimer != null) {
			mPrintTimer.cancel();
			mPrintTimer.purge();
			mPrintTimer = null;
		}
	}
	private Timer mPrintTimer = null;
	private class PrintTimerTask extends TimerTask{
		@Override
		public void run() {
			// TODO Auto-generated method stub
			mVoteStatuses.clear();
			mFactory.dbQuery(QueryID.Q11, QueryID.Q12,QueryID.Q10);
		}
	};
		
	private void startTimer() {
		if (mPrintTimer != null) {
			mPrintTimer.cancel();
			mPrintTimer.purge();
			mPrintTimer = null;
		}
		mPrintTimer = new Timer();
		mPrintTimer.scheduleAtFixedRate(new PrintTimerTask(), 0,
				mTime * 1000);
	}
	
	
	@SuppressLint("HandlerLeak")
	Handler mHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case UI_SET_UPSIDE:
				((EditText)mContentView.findViewById(R.id.et_vote_cnt)).setHint(Integer.toString(mCnt[0]));
				break;
			case UI_DATA_CHANGE:
				mVsAdapter.notifyDataSetChanged();
				mIssCnt.setHint(Integer.toString(mCnt[2]==0?0:mCnt[2]));
				mRate.setHint(new DecimalFormat("###,###.## %").format((float)mCnt[2]/(float)mCnt[0]));
				mTimeChecker.setText(getString(R.string.print_paper_information4)+new SimpleDateFormat(getString(R.string.print_paper_time)).format(new Date()));
				break;
			default:
				break;
			}
		};
	};
	
	public Object[] getParameters(QueryID id) {
		if(id == QueryID.Q10)
			return new Object[] {mFactory.isJson()?new SimpleDateFormat("yyyyMMdd").format(mFactory.getVtDate()):new java.sql.Date(mFactory.getVtDate())};
		else if(id == QueryID.Q12){
			return new Object[] {mFactory.isJson()?new SimpleDateFormat("yyyyMMdd").format(mFactory.getVtDate()):new java.sql.Date(mFactory.getVtDate())};
		}else
			return null;
			
	};
	
	@Override
	public boolean onResultSet(ResultSet rs, QueryID id) {
		// TODO Auto-generated method stub
		try{
			switch (id) {
				case Q11:
					if(rs.next()){
					mCnt[0] = rs.getInt("VTR_ALL");
					mHandler.sendEmptyMessage(UI_SET_UPSIDE);
					}else
						return false;
					break;
				case Q10:
					mCnt[2] = 0;
					while (rs.next()) {
						VoteStatusObject invso = new VoteStatusObject();
						invso.setTpgId(rs.getString("TPG_ID"));
						invso.setTpgName(rs.getString("TPG_NAME"));
						invso.setStatus(rs.getString("STATUS"));
						invso.setVtMcnt(rs.getInt("VT_MCNT"));
						invso.setVtCcnt(rs.getInt("VT_CCNT"));
						mCnt[2]+=rs.getInt("VT_CCNT");
						invso.setBkMcnt(rs.getInt("BK_MCNT"));
						mVoteStatuses.remove(invso);
						mVoteStatuses.add(invso);
					}
					mHandler.sendEmptyMessage(UI_DATA_CHANGE);
					if(mIsPreview){
						mIsPreview=false;
						mPrintTimer.cancel();
						mPrintTimer.purge();
						mPrintTimer=null;
						showWaitMessage(R.string.wating_pdf_3);
						Log.d(TAG, "pdf style 7 : "+PdfExport.createPdfStyle7("/sdcard/cache", "vto.pdf", "/sdcard/cache/font",mFactory.getProperty(mFactory.CONFIG_VTTITLE), mCnt[0], mVoteStatuses));
						dismissedMessageBox();
						((MainActivity) getActivity()).onMenu(mContentView.findViewById(R.id.btn_preview));
					}
					return false;
				case Q12:
					if(rs.next())
						mCnt[1] = rs.getInt("RATE");
					else
						return false;
					break;
				default:
					break;
			}
			return true;
		}catch (Exception e){
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return false;
		}
	}
	@Override
	public void onEvent(int qn, String arg0) {
		// TODO Auto-generated method stub
		super.onEvent(qn, arg0);
		JSONArray arr;
		JSONObject rs;
		try {
			arr = new JSONArray(arg0);
			switch (qn) {
			case 11:
				for (int i = 0; i < arr.length(); i++) {
					rs = arr.getJSONObject(i);
					if(rs.has("VTR_ALL")){
						mCnt[0] = rs.getInt("VTR_ALL");
						mHandler.sendEmptyMessage(UI_SET_UPSIDE);
					}
				}
				break;
			case 10:
				mCnt[2] = 0;
				for (int i = 0; i < arr.length(); i++) {
					rs = arr.getJSONObject(i);
					VoteStatusObject invso = new VoteStatusObject();
					invso.setTpgId(rs.getString("TPG_ID"));
					invso.setTpgName(rs.getString("TPG_NAME"));
					invso.setStatus(rs.getString("NAME"));
					invso.setVtMcnt(rs.getInt("VT_MCNT"));
					invso.setVtCcnt(rs.getInt("VT_CCNT"));
					mCnt[2]+=rs.getInt("VT_CCNT");
					invso.setBkMcnt(rs.getInt("BK_MCNT"));
					mVoteStatuses.remove(invso);
					mVoteStatuses.add(invso);
				}
				mHandler.sendEmptyMessage(UI_DATA_CHANGE);
				if(mIsPreview){
					mIsPreview=false;
					mPrintTimer.cancel();
					mPrintTimer.purge();
					mPrintTimer=null;
					showWaitMessage(R.string.wating_pdf_3);
					Log.d(TAG, "pdf style 7 : "+PdfExport.createPdfStyle7("/sdcard/cache", "vto.pdf", "/sdcard/cache/font",mFactory.getProperty(mFactory.CONFIG_VTTITLE), mCnt[0], mVoteStatuses));
					dismissedMessageBox();
					((MainActivity) getActivity()).onMenu(mContentView.findViewById(R.id.btn_preview));
				}
				break;
			case 12:
				for (int i = 0; i < arr.length(); i++) {
					rs = arr.getJSONObject(i);
					if(rs.has("CNT"))
					mCnt[1] = rs.getInt("CNT")-1;
				}
				break;
			default:
				break;
			}
		} catch (JSONException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			// TODO: handle exception
		}
	}
	private class VoteStatusAdapter extends BaseAdapter {
			@Override
			@SuppressLint("InflateParams")
			public View getView(int position, View convertView, ViewGroup parent) {
				// TODO Auto-generated method stub
				LayoutInflater li = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				if (convertView ==null) 
					convertView= li.inflate(R.layout.cell_local_voting, null);
					mVoteStatuses.trimToSize();
					if(mVoteStatuses.size()>0){
						TextView tvStatus = (TextView) convertView.findViewById(R.id.item_status);
						tvStatus.setText(mVoteStatuses.get(position).getStatus());
	
						TextView tvTps = (TextView) convertView.findViewById(R.id.item_tps);
						tvTps.setText(mVoteStatuses.get(position).getTpgName());
						TextView tvMcnt = (TextView) convertView.findViewById(R.id.item_mcnt);
						tvMcnt.setText(Integer.toString(mVoteStatuses.get(position).getBkMcnt()));
						TextView tvTcnt = (TextView) convertView.findViewById(R.id.item_tcnt);
						tvTcnt.setText(Integer.toString(mVoteStatuses.get(position).getVtMcnt()));
						TextView tvCcnt = (TextView) convertView.findViewById(R.id.item_ccnt);
						tvCcnt.setText(Integer.toString(mVoteStatuses.get(position).getVtCcnt()));
						TextView tvRate = (TextView)convertView.findViewById(R.id.item_tv_rate);
						float progress = ((mVoteStatuses.get(position).getVtCcnt()/(float)mCnt[1])*100);
						tvRate.setText(String.format("%.2f%%", progress));
						ProgressBar rate = (ProgressBar) convertView.findViewById(R.id.item_rate);
						int doInParams = Math.round(progress);
						rate.setProgress(doInParams);
					}else{
						showDefaultMessage(R.string.error_vote_info);
					}
					
				return convertView;
			}
			
			@Override
			public long getItemId(int position) {
				// TODO Auto-generated method stub
				return position;
			}
			
			@Override
			public Object getItem(int position) {
				// TODO Auto-generated method stub
				return mVoteStatuses.get(position);
			}
			
			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return mVoteStatuses.size();
			}
			
			
//			private class mAsyncTask extends AsyncTask<Integer, Void, Void>{
//				@Override
//				protected Void doInBackground(Integer[] params) {
//					for(int j=0;j<getCount();j++){
//						ProgressBar rate = ((ProgressBar) mContentView.findViewById(params[1]));
//						rate.setProgress(0);
//						for(int i =0;i<=params[0];i++)
//							rate.setProgress(i);
//					}
//					return null;
//				};
//			};
	}
	private class MakePdf extends AsyncTask<Void, Void, Boolean> {
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			mFactory.makeFont(mContext);
		}
		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			mIsPreview=true;
			return mFactory.dbQuery(QueryID.Q10);
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_preview:
			showWaitMessage(R.string.wating_pdf_1);
			new MakePdf().execute();
			break;
		default:
			break;
		}
	}
}