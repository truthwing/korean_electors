package com.mirusys.vt.fragment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.haesin.entities.PrintIntentObject;
import com.haesin.entities.SGGInfoObject;
import com.haesin.entities.TPGInfo;
import com.haesinit.common.Base64Util;
import com.haesinit.ultralite.database.SQLNO;
import com.mirusys.electors.R;
import com.mirusys.electors.ResourceFactory;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.electors.ResourceFactory.QueryID;
import com.mirusys.epson.BallotPrint;
import com.mirusys.epson.draw.DrawBallotManager;
import com.mirusys.epson.dto.Candidate;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

public class PrintTicketsFragment extends BasicFragment implements OnClickListener {
	private final String TAG="PrintTicketsFragment";
	private final String QUE="Question TAG";
	
    private View mContentView = null;
    private EditText mStartSerial, mEndSerial, mPaperCnt;
    private TextView mLastSerial;
    private LinearLayout mSelected;
    private ListView mSelectList;
    
    private boolean mIsItem=false;
    private boolean mIsPaperCnt=false;
    
    private final int SERIAL_NO = 0;
    private final int NOTIFY_DATA = 1;
    private int mAddSerial=0;
    private String mTPGID;
    private int mGetSGGNo=0;
    private ArrayList<String> mSggIDs = new ArrayList<String>();
    private ArrayList<SGGInfoObject> mSggInfo = new ArrayList<SGGInfoObject>();
    private ArrayList<Candidate> mCandidates = new ArrayList<Candidate>();
    private ArrayList<PrintIntentObject> mPio = new ArrayList<PrintIntentObject>();
    private ArrayList<TPGInfo> mTPGInfos = new ArrayList<TPGInfo>();
    private ArrayAdapter<TPGInfo> mAdapter;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.frag_print_tickets, container, false);
		String method = mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD);
		mContentView.findViewById(R.id.print_all).setVisibility(!method.equals(ResourceFactory.ISSUE_batch)?View.INVISIBLE:View.VISIBLE);
		((RadioButton)mContentView.findViewById(R.id.no_use_serial)).setChecked(method.equals(ResourceFactory.ISSUE_batch));
		
		mContentView.findViewById(R.id.print_select).setVisibility(!method.equals(ResourceFactory.ISSUE_paper)?View.INVISIBLE:View.VISIBLE);
		((RadioButton)mContentView.findViewById(R.id.use_serial)).setChecked(method.equals(ResourceFactory.ISSUE_paper));
		
		mContentView.findViewById(R.id.tv_print_paper_serial).setOnClickListener(this);
		mContentView.findViewById(R.id.tv_print_paper_serial_cancle).setOnClickListener(this);
		mContentView.findViewById(R.id.tv_print_paper_cnt).setOnClickListener(this);
		mContentView.findViewById(R.id.tv_print_paper_cnt_cancle).setOnClickListener(this);
		
		
		//카드일때 화면 여부 문의 후 작성
		
		//시리얼 번호
		mStartSerial = (EditText) mContentView.findViewById(R.id.et_serial_start);
		mEndSerial = (EditText) mContentView.findViewById(R.id.et_serial_end);
		mLastSerial = (TextView) mContentView.findViewById(R.id.last_serial);
		mContentView.findViewById(R.id.select_all).setOnClickListener(this);
		mContentView.findViewById(R.id.select_tpg).setOnClickListener(this);
		mContentView.findViewById(R.id.use_serial).setOnClickListener(this);
		mContentView.findViewById(R.id.no_use_serial).setOnClickListener(this);
		
		//사용 안함인 경우
		mPaperCnt = (EditText) mContentView.findViewById(R.id.et_paper_cnt);
		
		mSelected=(LinearLayout) mContentView.findViewById(R.id.selected);
		mSelected.setOnClickListener(this);
		mSelected.setEnabled(false);
		mSelectList = (ListView)mContentView.findViewById(R.id.select_list);
		mSelectList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				((TextView)mSelected.findViewById(R.id.selected_text)).setText(mTPGInfos.get(position).getmTPGName());
				mTPGID = mTPGInfos.get(position).getmTPGID().toString();
				mFactory.dbQuery(QueryID.Q62);
				mIsItem = true;
				mSelectList.setVisibility(View.GONE);
			}
		});
		mAdapter = new ArrayAdapter<TPGInfo>(mFactory.getApplicationContext(),R.layout.cell_select_list, mTPGInfos){
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				// TODO Auto-generated method stub
				LayoutInflater li = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				if(convertView ==null)
					convertView = li.inflate(R.layout.cell_select_list,parent, false);
				((TextView)convertView.findViewById(R.id.tpg_text)).setText(mTPGInfos.get(position).getmTPGName());
				return convertView; 
			}
		};
		mSelectList.setAdapter(mAdapter);
		mContentView.setOnClickListener(this);
		((RadioButton)mContentView.findViewById(R.id.select_all)).performClick();
		return mContentView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		mTPGInfos.clear();
		mFactory.dbQuery(QueryID.Q19);
	}
	
	@Override
	public Object[] getParameters(QueryID id) {
		switch (id) {
		case Q58:
			return new Object[]{mSggIDs.get(mGetSGGNo)};
		case Q61:
			return new Object[]{mTPGID};
		case Q62:
			return new Object[]{mTPGID};
		case Q60:
			return new Object[]{mSggInfo.get(mGetSGGNo).getSggId(), Integer.toString(mAddSerial)};
		}
		return null;
	}
	
	@Override
	public boolean onResultSet(ResultSet rs, QueryID id) throws SQLException {
		// TODO Auto-generated method stub
		switch (id) {
		case Q19:
			while(rs.next()){
				TPGInfo ti = new TPGInfo();
				ti.setmTPGID(rs.getString("TPG_ID"));
				ti.setmTPGName(rs.getString("TPG_NAME"));
				mTPGInfos.remove(ti);
				mTPGInfos.add(ti);
			}
			if(mTPGInfos.size()!=0){
				mTPGID = mTPGInfos.get(0).getmTPGID().toString();
				mHandler.sendEmptyMessage(NOTIFY_DATA);
				}
			break;
		case Q61:
			mCandidates.clear();
			while (rs.next()) {
				Candidate paper = new Candidate(rs.getString("HBJ_GIHO"),
						rs.getString("jd_name").equals("")?rs.getString("HBJ_NAME"):rs.getString("jd_name"),
						!rs.getString("HBJ_NAME").equals("")?rs.getString("HBJ_NAME"):rs.getString("jd_name"),
					"", "",rs.getString("SGG_ID"));
				mCandidates.add(paper);
			}
			for (int j = 0; j < mPio.size(); j++) {
				ArrayList<Candidate> candidates = new ArrayList<Candidate>();
				for (int i = 0; i < mCandidates.size(); i++) {
					Candidate paper = mCandidates.get(i);
					if(mSggInfo.get(j).getSggId().equals(paper.getId())){
						if (!paper.getHbjName().equals("")
								&& !paper.getJdName().equals("")
								&& paper.getJdName().equals(paper.getHbjName())
								)
							mPio.get(j).setBallotType(ResourceFactory.BALLOT_TYPE1);
						else if (!paper.getHbjName().equals("")
								&& !paper.getJdName().equals("")
								&& !paper.getJdName().equals(paper.getHbjName()))
							mPio.get(j).setBallotType(ResourceFactory.BALLOT_TYPE0);
						else
							mPio.get(j).setBallotType(ResourceFactory.BALLOT_TYPE4);
						candidates.add(paper);
					}
					mPio.get(j).setCandidate(candidates);
					mPio.get(j).setCandidateNum(candidates.size());
				}
			}
			if(mIsItem)
				mIsItem = false;
			else if(mIsPaperCnt)
				mIsPaperCnt = false;
			break;
		case Q62:
			int i=0;
			mSggIDs.clear();mSggInfo.clear();mPio.clear();
			while(rs.next()) {
				mSggIDs.add(rs.getString("SGG_ID"));
				SGGInfoObject sggInfo = new SGGInfoObject();
				sggInfo.setSggId(rs.getString("SGG_ID"));
				sggInfo.setSggName(rs.getString("SGG_NAME"));
				sggInfo.setSggBpsName(rs.getString("SGG_SNAME"));
				sggInfo.setSggBpsStamp(rs.getBytes("SGG_BPS_STAMP"));
				PrintIntentObject pio = new PrintIntentObject();
				pio.setTitleName(rs.getString("SGG_NAME"));
				pio.setSubTitleName(rs.getString("SGG_BPS_NAME"));
				pio.setBallotColor(ResourceFactory.BOX_COLOR2);
				pio.setFillType(ResourceFactory.FILL_FULL);
				if(rs.getString("SGG_BPS_STAMP").length()>0)
					try {
						pio.setStamp(rs.getBytes("SGG_BPS_STAMP"));
					} catch (Exception e) {
						Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
					}
				else
					pio.setStamp(null);
				if(mFactory.getAdminStamp()==null || mFactory.getAdminStamp().length>0)
					pio.setAdminstamp(mFactory.getAdminStamp());
				else
					pio.setAdminstamp(null);
				mSggInfo.add(i, sggInfo);
				mPio.add(i++, pio);
			}
			if(mIsItem)
				mIsItem = mFactory.dbQuery(QueryID.Q58);
			else if(mIsPaperCnt)
				mIsPaperCnt=mFactory.dbQuery(QueryID.Q61);
			break;
		case Q58:
			if(rs.next())
				mHandler.sendMessage(mHandler.obtainMessage(SERIAL_NO, rs.getInt("PRT_SN"),0));
			if(mIsItem)
				mIsItem = mFactory.dbQuery(QueryID.Q61);
			break;
		case Q69:
			if (rs.next()) {
				if(mFactory.getAdminStamp() == null || mFactory.getAdminStamp().length <= 0)
					mFactory.setAdminStamp(rs.getBytes("IMG_MGR_STAMP"));
			}
		default:
			break;
		}
		return true;
	}
	
	@Override
	public boolean onResultSet(int rows, QueryID id) throws SQLException {
		// TODO Auto-generated method stub
		switch (id) {
		case Q60:
			mGetSGGNo++;
			mFactory.transaction(ResourceFactory.TRANSACTION_COMMIT);
			if(mGetSGGNo>=mPio.size()){
				mFactory.dbQuery(QueryID.Q58);
				mGetSGGNo=0;
			}else{
				mFactory.dbQuery(QueryID.Q60);
			}
			return true;

		default:
			break;
		}
		return false;
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		mSelectList.setVisibility(View.GONE);
		switch (v.getId()) {
		case R.id.selected:
			if(((RadioButton) mContentView.findViewById(R.id.select_tpg)).isChecked())
				mSelectList.setVisibility(mSelectList.getVisibility()==View.VISIBLE?View.GONE:View.VISIBLE);
			break;
		case R.id.select_all:
			((RadioButton) mContentView.findViewById(R.id.select_tpg)).setChecked(false);
			((RadioButton) v).setChecked(true);
			((RadioButton) mContentView.findViewById(R.id.no_use_serial)).performClick();
			((RadioButton) mContentView.findViewById(R.id.use_serial)).setChecked(false);
			((RadioButton) mContentView.findViewById(R.id.use_serial)).setEnabled(false);
				mFactory.dbQuery(QueryID.Q62);//선거구 정보 조회.
			break;
		case R.id.use_serial:
			if(((RadioButton) mContentView.findViewById(R.id.select_all)).isChecked()){
			((RadioButton) v).setChecked(true);
			((RadioButton) mContentView.findViewById(R.id.use_serial)).setChecked(false);
			}else{
				((RadioButton) mContentView.findViewById(R.id.no_use_serial)).setChecked(false);
				((RadioButton) mContentView.findViewById(R.id.use_serial)).setChecked(true);
				if(mContentView.findViewById(R.id.print_select).getVisibility()!=View.VISIBLE){
					mContentView.findViewById(R.id.print_select).setVisibility(View.VISIBLE);
					mContentView.findViewById(R.id.print_all).setVisibility(View.INVISIBLE);}
				}
			
			break;
		case R.id.select_tpg://투표구 라디오버튼 체크시 자동으로 일련번호 체크.
			((RadioButton) v).setChecked(true);//투표구 사용 체크
			((RadioButton) mContentView.findViewById(R.id.use_serial)).performClick();//일련번호 선택
			((RadioButton) mContentView.findViewById(R.id.select_all)).setChecked(false);//전체 선택 해제
			mSelected.setEnabled(true);//리스트뷰 클릭 활성화.
			mSelectList.performItemClick(mSelectList.getChildAt(0), 0, mAdapter.getItemId(0));//첫번째 값 선택으로 자동 설정
			((RadioButton) mContentView.findViewById(R.id.use_serial)).setEnabled(true);//투표구 검색일때는 활성화.
			break;
		case R.id.no_use_serial:
			((RadioButton) v).setChecked(true);
			((RadioButton) mContentView.findViewById(R.id.use_serial)).setChecked(false);
			if(mContentView.findViewById(R.id.print_all).getVisibility()!=View.VISIBLE){
			mContentView.findViewById(R.id.print_all).setVisibility(View.VISIBLE);
			mContentView.findViewById(R.id.print_select).setVisibility(View.INVISIBLE);}
			break;
		case R.id.tv_print_paper_serial:
			int startNo, endNo;
			if(mLastSerial.getText().toString().trim().equals(""))
				showDefaultMessage(R.string.error_start_serial);
			if(mStartSerial.getText().toString().trim().equals(""))
				showDefaultMessage(R.string.error_empty_start_serial);
			else if((startNo=Integer.parseInt(mStartSerial.getText().toString().trim()))
					<Integer.parseInt(mLastSerial.getText().toString().trim()))
				showQuestionMessage(R.string.error_start_serial, TAG);
			else if(mEndSerial.getText().toString().trim().equals(""))
				showDefaultMessage(R.string.error_empty_end_serial);
			else if((endNo=Integer.parseInt(mEndSerial.getText().toString().trim()))<startNo)
				showDefaultMessage(R.string.error_end_serial);
			else{
				printMethod(startNo, endNo, true);
			}
			break;
		case R.id.tv_print_paper_cnt:
			if(mPaperCnt.getText().toString().equals("")||mPaperCnt.getText().toString().isEmpty())
				showDefaultMessage(R.string.error_cnt);
			else{
				mIsPaperCnt = true;
				mFactory.dbQuery(QueryID.Q62);
				try {Thread.sleep(500);} catch (InterruptedException e) {Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();}
				if(!mPaperCnt.getText().toString().isEmpty())
				printMethod(1, Integer.parseInt(mPaperCnt.getText().toString().trim()), false);
				else;
			}
			break;
		case R.id.tv_print_paper_serial_cancle:
		case R.id.tv_print_paper_cnt_cancle:
			cancelPrint();
			break;
		default:
			break;
		}
	}
	
	@SuppressLint("HandlerLeak")
	Handler mHandler = new Handler(){
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case SERIAL_NO:
				mLastSerial.setText(Integer.toString(msg.arg1));
				break;
			case NOTIFY_DATA:
				mAdapter.notifyDataSetChanged();
				break;
			default:
				break;
			}
		};
	};
	
	private Timer mTimer = null;
	private void printMethod(int startNo,int endNo, boolean isSerial){
		for (int i = 0; i < mPio.size(); i++) {
			mPio.get(i).setSerial(isSerial);
			mPio.get(i).setAdminstamp(mFactory.getAdminStamp());
			mPio.get(i).setStartSerialNum(startNo);
			mPio.get(i).setEndSerialNum(endNo);
		}
		//임시 소스. Q69가 되면 mStamp로 변경.
 		if(mFactory.getmBallotPrint()==null)
			mFactory.setmBallotPrint(new BallotPrint(mFactory));
 		if( mFactory.getmBallotPrint().getPrintStatus() == ResourceFactory.PRINT_READY)
			mFactory.getmBallotPrint().printBallot(mPio);
 		else
			showDefaultMessage(R.string.printBusy);
 		
 		if(isSerial){
 			mTimer = new Timer();
 			mTimer.schedule(new PrintCloseCheckTask(), 3300, 300);
 		}
	}
	private class PrintCloseCheckTask extends TimerTask{

		@Override
		public void run() {
			Log.d(TAG, "close print check...");
			if(!mFactory.getmBallotPrint().isOpen()){
				Log.d(TAG, "close print check in...");
				if(mAddSerial<DrawBallotManager.serialNumber){
					mAddSerial = DrawBallotManager.serialNumber;
					mGetSGGNo = 0;}
					if(!ResourceFactory.UI_TEST)
						mFactory.dbQuery(QueryID.Q60);
				closeTimer();
			}
		}
	}
	private void closeTimer() {
		if(mTimer!=null){
			mTimer.cancel();
			mTimer.purge();
			mTimer = null;
		}
	}
	
	private void cancelPrint(){
		showQuestionMessage(R.string.print_paper_cancel_question, QUE);
	}
	
	@Override
	public void onMessageResult(View v, Object tag) {
    	// TODO Auto-generated method stub
		int startNo, endNo;
    	if(v.getId()==android.R.id.button1&&tag==TAG){
    		startNo=Integer.parseInt(mStartSerial.getText().toString().trim());
    		endNo=Integer.parseInt(mEndSerial.getText().toString().trim());
    		printMethod(startNo, endNo, true);
    	}else if(v.getId()==android.R.id.button1&&tag==QUE){
    		if(mFactory.getmBallotPrint()==null){
    			mFactory.setmBallotPrint(new BallotPrint(mFactory));
    		}
    		mFactory.getmBallotPrint().doStop();
    	}
    	super.onMessageResult(v, tag);
    }
	@Override
	public void onEvent(int qn, int updateCount) {
		// TODO Auto-generated method stub
		super.onEvent(qn, updateCount);
		Log.d(TAG, "qn : "+qn+" update count : "+updateCount);
		switch (qn) {
		case 60:
			mGetSGGNo++;
			if(mGetSGGNo>=mPio.size()){
				mFactory.dbQuery(QueryID.Q58);
				mGetSGGNo=0;
			}else{
				mFactory.dbQuery(QueryID.Q60);
			}
			break;
		default:
			break;
		}
	}
	@Override
	public void onEvent(int qn, String arg0) {
		// TODO Auto-generated method stub
		JSONArray arr;
		try {
			arr = new JSONArray(arg0);
			super.onEvent(qn, arg0);
			switch (qn) {
			case 19:
				for (int i = 0; i < arr.length(); i++) {
					JSONObject res = arr.getJSONObject(i);
					TPGInfo ti = new TPGInfo();
					ti.setmTPGID(res.getString("TPG_ID"));
					ti.setmTPGName(res.getString("TPG_NAME"));
					mTPGInfos.remove(ti);
					mTPGInfos.add(ti);
				}
				if (mTPGInfos.size() != 0) {
					mTPGID = mTPGInfos.get(0).getmTPGID().toString();
					mAdapter.notifyDataSetChanged();
				}
				break;
			case 62:
				mSggIDs.clear();mSggInfo.clear();mPio.clear();
				for (int i = 0; i < arr.length(); i++) {
					JSONObject res = arr.getJSONObject(i);
					mSggIDs.add(res.getString("SGG_ID"));
					SGGInfoObject sggInfo = new SGGInfoObject();
					sggInfo.setSggId(res.getString("SGG_ID"));
					sggInfo.setSggName(res.getString("SGG_NAME"));
					sggInfo.setSggBpsName(res.getString("SGG_SNAME"));
					try {
						sggInfo.setSggBpsStamp(Base64Util.decode(res.getString("SGG_BPS_STAMP")));
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					PrintIntentObject pio = new PrintIntentObject();
					pio.setTitleName(res.getString("SGG_NAME"));
					pio.setSubTitleName(res.getString("SGG_BPS_NAME"));
					pio.setBallotColor(ResourceFactory.BOX_COLOR2);
					pio.setFillType(ResourceFactory.FILL_FULL);
					if(res.getString("SGG_BPS_STAMP").length()>0)
						try {
							pio.setStamp(sggInfo.getSggBpsStamp());
						} catch (Exception e) {
							Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
						}
					else
						pio.setStamp(null);
					if(mFactory.getAdminStamp()==null || mFactory.getAdminStamp().length>0)
						pio.setAdminstamp(mFactory.getAdminStamp());
					else
						pio.setAdminstamp(null);
					mSggInfo.add(i, sggInfo);
					mPio.add(i, pio);
				}
				if(mIsItem)
					mIsItem = mFactory.dbQuery(QueryID.Q58);
				else if(mIsPaperCnt)
					mIsPaperCnt=mFactory.dbQuery(QueryID.Q61);
				break;
			case 58:
				for (int i = 0; i < arr.length(); i++) {
					JSONObject res = arr.getJSONObject(i);
				mHandler.sendMessage(mHandler.obtainMessage(SERIAL_NO, res.getInt("PRT_SN"),0));
				}
				if(mIsItem)
					mIsItem = mFactory.dbQuery(QueryID.Q61);
				break;
			case 69:
					try {
						arr = new JSONArray(arg0);
					for (int i = 0; i < arr.length(); i++) {
						JSONObject res = arr.getJSONObject(i);
						if(mFactory.getAdminStamp() == null || mFactory.getAdminStamp().length <= 0)
							mFactory.setAdminStamp(Base64Util.decode(res.getString("IMG_MGR_STAMP")));
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
					}
			case 61:
				mCandidates.clear();
				for (int i = 0; i < arr.length(); i++) {
					JSONObject res = arr.getJSONObject(i);
					Candidate paper = new Candidate(res.getString("HBJ_GIHO"),
							res.getString("jd_name").equals("") ? res
									.getString("HBJ_NAME") : res
									.getString("jd_name"),
							!res.getString("HBJ_NAME").equals("") ? res
									.getString("HBJ_NAME") : res
									.getString("jd_name"), "", "", res
									.getString("SGG_ID"));
					mCandidates.add(paper);
				}
				for (int j = 0; j < mPio.size(); j++) {
					ArrayList<Candidate> candidates = new ArrayList<Candidate>();
					for (int i = 0; i < mCandidates.size(); i++) {
						Candidate paper = mCandidates.get(i);
						if(mSggInfo.get(j).getSggId().equals(paper.getId())){
							if (!paper.getHbjName().equals("")
									&& !paper.getJdName().equals("")
									&& paper.getJdName().equals(paper.getHbjName())
									)
								mPio.get(j).setBallotType(ResourceFactory.BALLOT_TYPE1);
							else if (!paper.getHbjName().equals("")
									&& !paper.getJdName().equals("")
									&& !paper.getJdName().equals(paper.getHbjName()))
								mPio.get(j).setBallotType(ResourceFactory.BALLOT_TYPE0);
							else
								mPio.get(j).setBallotType(ResourceFactory.BALLOT_TYPE4);
							candidates.add(paper);
						}
						mPio.get(j).setCandidate(candidates);
						mPio.get(j).setCandidateNum(candidates.size());
					}
				}
				if(mIsItem)
					mIsItem = false;
				else if(mIsPaperCnt)
					mIsPaperCnt = false;
				break;
			default:
				break;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			mIsPaperCnt = false;
			mIsItem=false;
		}
	}
	@Override
	public void onPrepareUpdate(SQLNO qn, String[] jsonParam) {
		super.onPrepareUpdate(qn, jsonParam);
		switch (qn) {
		case Q60:
			mFactory.jsonCommit(qn, jsonParam);
			break;

		default:
			break;
		}
	}
}