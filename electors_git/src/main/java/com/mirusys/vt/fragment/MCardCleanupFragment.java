package com.mirusys.vt.fragment;

import java.sql.SQLException;
import java.text.SimpleDateFormat;

import com.haesinit.ultralite.database.SQLNO;
import com.mirusys.electors.BasicActivity;
import com.mirusys.electors.R;
import com.mirusys.electors.ResourceFactory;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.electors.ResourceFactory.QueryID;
import com.mirusys.voting.MiruSmartCard;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MCardCleanupFragment extends BasicFragment implements OnClickListener{
	public final static String TAG = "MCardCleanupFragment";
	private View mContentView = null;
    private Button mBtnSearch, mBtnClear;
    private EditText mSecure;
    
    private boolean mIsUpdated = false;
    private boolean mIsQ8 = false;
    
    private int[] mTPGCnt={R.id.img01, R.id.img02, R.id.img03, R.id.img04, R.id.img05
    		, R.id.img06, R.id.img07, R.id.img08, R.id.img09, R.id.img10
    		, R.id.img11, R.id.img12, R.id.img13, R.id.img14, R.id.img15
    		, R.id.img16, R.id.img17, R.id.img18, R.id.img19, R.id.img20};
    private TextView mTPGView[] = new TextView[20];
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.frag_mcard_cleanup, container, false);
		mBtnSearch=(Button) mContentView.findViewById(R.id.btnLogin);
		mBtnSearch.setOnClickListener(this);
		mBtnSearch.performClick();
		mBtnClear=(Button) mContentView.findViewById(R.id.btnClear);
		mBtnClear.setOnClickListener(this);
		mSecure = (EditText) mContentView.findViewById(R.id.editSecureCode);
		mSecure.setText(mFactory.getManagerKey());
		for (int i = 0; i < mTPGCnt.length; i++) {
			mTPGView[i] = (TextView) mContentView.findViewById(mTPGCnt[i]);
		}
		return mContentView;
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnLogin:
			showWaitMessage(R.string.menu_issue_01_popup_closing_retrieve);
			new Thread(){
				@Override
				public void run() {
					super.run();
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
					}
					byte[] outDate = new byte[51];
					if(mSecure.getText().toString().equals("")){
						dismissedMessageBox();
						showDefaultMessage(R.string.error_password_empty);}
					else if(!ResourceFactory.UI_TEST&&!mFactory.getSmartCard(0).CardGetType().equals("T")){
						dismissedMessageBox();
						showDefaultMessage(R.string.error_password_nomanager);}
					else if(!ResourceFactory.UI_TEST&&
							mSecure.getText().toString().length()!=8||
							mFactory.getSmartCard(0).CardVerify(mSecure.getText().toString())!=MiruSmartCard.SUCCESS){
						dismissedMessageBox();
						showDefaultMessage(R.string.error_password_wrong_normal);}
					else if(!ResourceFactory.UI_TEST&&mFactory.getSmartCard(0).TCardReadBSection(outDate)!=MiruSmartCard.SUCCESS){
						dismissedMessageBox();
						showDefaultMessage(R.string.error_password_elector);}
					else {
						int ret = 0;
					
						for (int i = 1; i <= 20; i++) {
							byte cData[] = new byte[24];
							if(!ResourceFactory.UI_TEST)
							ret = mFactory.getSmartCard(0).TCardReadCSectionRow(new byte[3], (byte)(0x00+i), cData);
							Log.i(TAG, "C section ret = "+ret);
							if(!ResourceFactory.UI_TEST&&ret!=MiruSmartCard.SUCCESS){
								dismissedMessageBox();
								showDefaultMessage(R.string.mcard_error_info);
								break;
							}else{
								char a= new String(cData).charAt(18);
								a=a==0?'A':a;
								a=a=='B'||a=='C'||a=='A'?a:' ';
								mHandler.sendMessage(mHandler.obtainMessage(i-1, a));
								Log.i(TAG, "now status 24 : "+new String(cData));
							}
						}
						dismissedMessageBox();
						Log.d(TAG, "Csection Read ret = "+ret);				
						mHandler.sendMessage(mHandler.obtainMessage(21, outDate));
					}
				}
			}.start();
			break;
		case R.id.btnClear:
			final byte[] inData = new byte[13];
			inData[0]=(byte)ResourceFactory.VOTE_ready.charAt(0);
			if(mSecure.getText().toString().equals(""))
				showDefaultMessage(R.string.error_password_empty);
			else if(!ResourceFactory.UI_TEST&&!mFactory.getSmartCard(0).CardGetType().equals("T"))
				showDefaultMessage(R.string.error_password_nomanager);
			else if(!ResourceFactory.UI_TEST&&
					mSecure.getText().toString().length()!=8||
					mFactory.getSmartCard(0).CardVerify(mSecure.getText().toString())!=MiruSmartCard.SUCCESS)
				showDefaultMessage(R.string.error_password_wrong_normal);
			else{
				final byte cData[] = new byte[24];
				final byte[] random = new byte[10];
				final Dialog dialog = new Dialog(((BasicActivity)getActivity()));
				dialog.setContentView(R.layout.popup_message);
				((TextView) dialog.findViewById(android.R.id.message)).setText(getResources().getString(R.string.mcard_issue_question));
				dialog.findViewById(android.R.id.button1).setOnClickListener(
						new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								dialog.dismiss();
								new Thread() {
									@Override
									public void run() {
										// TODO Auto-generated method stub
										super.run();
										int msg = 0;
										if (!ResourceFactory.UI_TEST
												&& mFactory.getSmartCard(0).TCardWriteTPGData(inData) != MiruSmartCard.SUCCESS
												&& mFactory.getSmartCard(0).TCardWriteVoteRightRandom(random) != MiruSmartCard.SUCCESS)
											msg = R.string.error_write_card;
										else {
											for (int i = 1; i <= 20; i++) {
												int ret = mFactory.getSmartCard(0).TCardWriteCSectionRow(cData,(byte) (0x00 + i));
												if (!ResourceFactory.UI_TEST && ret == MiruSmartCard.SUCCESS) {
													msg = R.string.mcard_init_complete;
												} else {
													msg = R.string.mcard_error_issue;
													break;
												}
											}
											if (msg == R.string.mcard_init_complete) {
												if(mFactory.dbQuery(QueryID.Q8, QueryID.Q9))
													mHandler.sendEmptyMessage(22);
											}else
												showDefaultMessage(msg);
										}
									}
								}.start();
							}
						});
				dialog.findViewById(android.R.id.button2).setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});
				dialog.findViewById(android.R.id.button2).setVisibility(View.VISIBLE);
				((Button)dialog.findViewById(android.R.id.button1)).setText(getResources().getString(android.R.string.yes));
				dialog.show();
			}
			break;
		default:
			break;
		}
	}
	
	public Object[] getParameters(QueryID id) {
		switch (id) {
		case Q8:
			return new Object[]{
					((TextView) mContentView.findViewById(R.id.infoVotePlaceCode)).getText().toString().equals("")?
							mFactory.getProperty(ResourceFactory.CONFIG_TPGID):
								((TextView) mContentView.findViewById(R.id.infoVotePlaceCode)).getText().toString()};
		case Q9:
			return new Object[]{
					((TextView) mContentView.findViewById(R.id.infoVotePlaceCode)).getText().toString().equals("")?
							mFactory.getProperty(ResourceFactory.CONFIG_TPGID):
								((TextView) mContentView.findViewById(R.id.infoVotePlaceCode)).getText().toString()};
		default:
			break;
		}
		return null;
	};
	
	public boolean onResultSet(int rows, QueryID id) throws SQLException{
		switch (id) {
		case Q8:
			mIsQ8=rows>0;
			if(rows>0)
				mFactory.transaction(ResourceFactory.TRANSACTION_COMMIT);
			else
				mFactory.transaction(ResourceFactory.TRANSACTION_ROLLBACK);
			break;
		case Q9:
			if(rows>0&&mIsQ8){
				mFactory.transaction(ResourceFactory.TRANSACTION_COMMIT);
				mIsUpdated = true;

				mContentView.findViewById(R.id.btnLogin).performClick();
			}else{
				mFactory.transaction(ResourceFactory.TRANSACTION_ROLLBACK);
				}
			break;

		default:
			break;
		}
		return true;
	};
	
	public void onPrepareUpdate(SQLNO qn, String[] jsonParam) {
		mFactory.jsonCommit(qn, jsonParam);
	};
	
	public void onEvent(int qn, int updateCount) {
		switch (qn) {
		case 9:
			mIsUpdated = true;
			break;
		default:
			break;
		}
	};
	
	Handler mHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			if(msg.what<20)
			mTPGView[msg.what].setText(String.valueOf((Character)msg.obj));
			switch (msg.what) {
			case 21:
				byte[] outDate=(byte[])msg.obj;
				TextView infoVoteDate = (TextView) mContentView
						.findViewById(R.id.infoVoteDate);
				infoVoteDate.setVisibility(View.VISIBLE);
				TextView infoVotePlaceCode = (TextView) mContentView
						.findViewById(R.id.infoVotePlaceCode);
				infoVotePlaceCode.setVisibility(View.VISIBLE);
				TextView infoVoteStatus = (TextView) mContentView
						.findViewById(R.id.infoVoteStatus);
				infoVoteStatus.setVisibility(View.VISIBLE);
				if (!ResourceFactory.UI_TEST) {
					String str = null;
					try {
						str = new SimpleDateFormat("yyyy. MM. dd. ").format(new SimpleDateFormat("yyyyMMdd").parse(new String(outDate, 10, 8)))
								+ new SimpleDateFormat(" a hh : mm : ss").format(new SimpleDateFormat("HHmmss").parse(new String(outDate, 19, 6, "MS949")));
					} catch (Exception e) {
						Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
					}
					infoVoteDate.setText(str);
					infoVotePlaceCode.setText(new String(outDate, 0, 10));
					infoVoteStatus.setText(new String(outDate, 18, 1));
					if(mIsUpdated)
					showDefaultMessage(R.string.mcard_init_complete);
				} else {

					infoVoteDate.setText(new String().format("%s. %s. %s, ","2015", "04", "07")+ new String().format("%s : %s : %s", "08", "14","21"));
					infoVotePlaceCode.setText(new String(mFactory
							.getProperty(ResourceFactory.CONFIG_TPGID)));
					infoVoteStatus.setText(new String("B".getBytes()));
					if(mIsUpdated)
						showDefaultMessage(R.string.mcard_init_complete);
				}
				mIsUpdated = false;
				break;
			case 22:
				mBtnSearch.performClick();
				break;

			default:
				break;
			}

		};
	};
}