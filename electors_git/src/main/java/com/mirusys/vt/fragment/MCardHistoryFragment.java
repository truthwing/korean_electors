package com.mirusys.vt.fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.haesin.entities.CardBackupObject;
import com.mirusys.electors.MainActivity;
import com.mirusys.electors.R;
import com.mirusys.electors.ResourceFactory;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.voting.MiruSmartCard;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class MCardHistoryFragment extends BasicFragment implements View.OnClickListener {
	private static final String TAG="MCardHistoryFragment";
    private View mContentView;
    private LayoutInflater mInflater;

    private ListView mListView;
    private Dialog mDialog;
    private String mPassword;
    private boolean mCardCheckable = false;
    private boolean mCardAttachable = false;

    private List<CardBackupObject> mCardbackups = new ArrayList<CardBackupObject>();
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mInflater = inflater;
		mContentView = inflater.inflate(R.layout.frag_mcard_history, container, false);
		mCardbackups = ((MainActivity)getActivity()).CARDSQLITE.select();
		mContentView.findViewById(R.id.btnClear).setOnClickListener(this);
		mListView = (ListView)mContentView.findViewById(R.id.lvCards);
		mListView.setAdapter(mlvAdapter);
		return mContentView;
	}

	@Override
	public void onResume() {
		super.onResume();

		mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER); // 관리자카드용 스마트카드 연결
	}

	@Override
	public void onPause() {
		mCardCheckable = false;
		mCardAttachable = false;

		dismissedMessageBox();

		if (mDialog != null && mDialog.isShowing()) {
			mDialog.dismiss();
		}

		super.onPause();
	}

	@Override
    public void onAttached(MiruSmartCard card, byte type) {
		if (mCardCheckable) {
			mCardCheckable = false;
			dismissedMessageBox();

			if (ResourceFactory.UI_TEST == false && !card.CardGetType().equals("B")) {
				mCardAttachable = true;
				showDefaultMessage(R.string.error_password_nomaster);
			}
			else if(ResourceFactory.UI_TEST || 
					mPassword.length()==8||
					card.CardVerify(mPassword) == MiruSmartCard.SUCCESS) {
				mDialog.dismiss();

				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						/* 백업자료 초기화 */
						((MainActivity)getActivity()).CARDSQLITE.delete();
						mCardbackups = ((MainActivity)getActivity()).CARDSQLITE.select();
						mlvAdapter.notifyDataSetChanged();
//						((MainActivity)getActivity()).relayout(MCardHistoryFragment.this, null);
					}
				});
			}
			else {
				showDefaultMessage(R.string.error_password_wrong_normal);
				mDialog.findViewById(R.id.btnOkay).setClickable(true);
			}
		}
    }

	public void onDetached(MiruSmartCard card) {
		if (mCardAttachable) {
			mCardCheckable = true;
			mCardAttachable = false;
		}
    }

	@Override
    public boolean onIdle(MiruSmartCard card) {
    	return mCardCheckable && mFactory.isCardAttached();
    }

	@Override
	public void onClick(View v) {
		v.setClickable(false);

		if (v.getId() == R.id.btnClear) {
			if (mDialog == null) {
				mDialog = new Dialog(getActivity());
				mDialog.setContentView(R.layout.popup_cleanup_history_confirm);
				mDialog.findViewById(R.id.btnCancel).setOnClickListener(this);
				mDialog.findViewById(R.id.btnOkay).setOnClickListener(this);
			}

			if (mDialog.isShowing() == false) {
				mDialog.findViewById(R.id.btnOkay).setClickable(true);
				mDialog.show();
			}
		}
		else if(mDialog != null && mDialog.isShowing()) {
			if (v.getId() == R.id.btnCancel)
				mDialog.dismiss();
			else if(v.getId()==R.id.btnOkay){
				EditText et = (EditText)mDialog.findViewById(R.id.et_password);
				mPassword = et.getText().toString();

				if (mPassword.length() == 0)
					showDefaultMessage(R.string.error_password_empty);
				else if (mPassword.length()> 8)
					showDefaultMessage(R.string.error_password_wrong_normal);
				else {
					mCardCheckable = true;
					if (mFactory.isCardAttached() == false) {
						showDefaultMessage(R.string.error_password_nomaster);
					}
					return; // not clickable 'okay' button
				}
			}
		}

		v.setClickable(true);
	}

	private BaseAdapter mlvAdapter = new BaseAdapter() {
		private int[] mTPGid={R.id.c_img01, R.id.c_img02, R.id.c_img03, R.id.c_img04, R.id.c_img05
	    		, R.id.c_img06, R.id.c_img07, R.id.c_img08, R.id.c_img09, R.id.c_img10
	    		, R.id.c_img11, R.id.c_img12, R.id.c_img13, R.id.c_img14, R.id.c_img15
	    		, R.id.c_img16, R.id.c_img17, R.id.c_img18, R.id.c_img19, R.id.c_img20};
		@Override
		@SuppressLint("InflateParams")
		public View getView(int position, View convertView, ViewGroup parent) {
			if(mCardbackups.get(position)!=null){
				CardBackupObject cbo = mCardbackups.get(position);
				if (convertView == null) {
					convertView = mInflater.inflate(R.layout.row_mcard_history, null);
				}
				
				//백업된 데이터를 중에서 투표기 개시 상태 표시
				if(mCardbackups.size()>0){
					for (int i = 0; i < mTPGid.length; i++) {
						if(cbo!=null && cbo.gettPGStatus() != null && cbo.gettPGStatus()[i]!=null){
							String str = String.valueOf(cbo.gettPGStatus()[i].charAt(18));
						((TextView) convertView.
								findViewById(mTPGid[i])).setText(str.equals("B")||str.equals("C")?str:"");}
						}
					try {
						if(!cbo.getTime().isEmpty())
						((TextView) convertView.findViewById(R.id.textview01)).setText(
								new SimpleDateFormat("yyyy MM/dd").format(
								new SimpleDateFormat("yyyyMMdd").parse(cbo.getDate()))+
								new SimpleDateFormat(" a hh:mm:ss").format(
								new SimpleDateFormat("HHmmss").parse(cbo.getTime())));
						else
							((TextView) convertView.findViewById(R.id.textview01)).setText(
									new SimpleDateFormat("yyyy MM/dd").format(
											new SimpleDateFormat("yyyyMMdd").parse(cbo.getDate()))+
											new SimpleDateFormat(" a hh:mm:ss").format(
													new SimpleDateFormat("HHmmdd").parse("000000")));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
					}
					((TextView) convertView.findViewById(R.id.textview02)).setText(cbo.gettPGID());
					((TextView) convertView.findViewById(R.id.textview03)).setText(cbo.getStatus());
					}
			}
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public Object getItem(int position) {
			return mCardbackups.get(position);
		}

		@Override
		public int getCount() {
			return mCardbackups.size();
		}
	};
}