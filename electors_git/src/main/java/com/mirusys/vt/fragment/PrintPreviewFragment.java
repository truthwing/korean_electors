package com.mirusys.vt.fragment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.artifex.mupdfdemo.MuPDFCore;
import com.mirusys.electors.BasicActivity;
import com.mirusys.electors.MainActivity;
import com.mirusys.electors.R;
import com.mirusys.electors.ResourceFactory.Log;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class PrintPreviewFragment extends BasicFragment implements OnClickListener{
	private View mPreView;
	private ImageView mPDFView;
	private final String TAG = "PDFPreView";
	private MuPDFCore mCore;
	private final String VTOPDF = "/sdcard/cache/vto.pdf";
	private int pages;
	private Bitmap bm=null;
	private MuPDFCore.Cookie cookie;
	private View[] tv_btn = new TextView[5];
	private int nowPage;
	private int btn_pages[] = {R.id.btn_page_1, R.id.btn_page_2, R.id.btn_page_3, R.id.btn_page_4, R.id.btn_page_5};
	private int btn_move_page[] = {R.id.btn_backward_to_first,R.id.btn_backward_to_one,R.id.btn_forward_to_one, R.id.btn_forward_to_last};
	private float mSize = 100.0f;
	private float mHeight, mWidth;
	private TextView mPercent;
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		try {
			mCore = new MuPDFCore(getActivity(), VTOPDF);
			cookie = mCore.new Cookie();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
		}
		pages = mCore.countPages();
		nowPage = 0;
	}
	private void calcPage() {
		// TODO Auto-generated method stub
		int totPagesNo = pages / 5;
		int remainPages = pages % 5;
		int nowPageNo = nowPage / 5;
		if(totPagesNo==nowPageNo){
			for (int i = 0; i <= nowPageNo; i++) {
				for (int j = 0; j < 5; j++) {
					if (remainPages > j) 
						((TextView)tv_btn[j]).setText(Integer.toString((i * 5) + (j + 1)));
					else{
						((TextView)tv_btn[j]).setText("");
						((TextView)tv_btn[j]).setClickable(false);
						}
				}
			}
		}else if(totPagesNo>nowPageNo){
			for (int i = 0; i <= nowPageNo; i++) 
				for (int j = 0; j < 5; j++) {
						((TextView)tv_btn[j]).setText(Integer.toString((i * 5) + (j + 1)));
						tv_btn[j].setClickable(true);
				}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mPreView = inflater.inflate(R.layout.frag_print_view, container, false);
		for (int i=0;i< btn_pages.length;i++) {
			tv_btn[i] = mPreView.findViewById(btn_pages[i]);
			tv_btn[i].setOnClickListener(this);
		}
		for (int i=0;i< btn_move_page.length;i++) {
			((ImageView)mPreView.findViewById(btn_move_page[i])).setOnClickListener(this);;
		}
		mPDFView = (ImageView) mPreView.findViewById(R.id.pdf_preview);
		mPreView.findViewById(R.id.btn_zoomin).setOnClickListener(this);
		mPreView.findViewById(R.id.btn_zoomout).setOnClickListener(this);
		mPreView.findViewById(R.id.btn_print_save).setOnClickListener(this);
		mPercent = (TextView) mPreView.findViewById(R.id.tv_percent);
		tv_btn[0].performClick();
		calcPage();
		makeBitmap(nowPage);
		mWidth = mPDFView.getScaleX();
		mHeight = mPDFView.getScaleY();
		mPreView.findViewById(R.id.btnStopFragment).setOnClickListener(this);
		return mPreView;
	}
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	private void makeBitmap(int page) {
		PointF size = mCore.getPageSize(page);
		int x_thumb = (int) (480*(mSize/100));
		int y_thumb = (int) (x_thumb * size.y / size.x);
		if(bm!=null){
		bm.recycle();
		bm=null;}
		bm = Bitmap.createBitmap(x_thumb, y_thumb, Bitmap.Config.ARGB_8888);
		mCore.drawPage(bm, page, x_thumb, y_thumb, 0, 0, x_thumb, y_thumb,
				cookie);
		mPDFView.setImageBitmap(bm);
		mPercent.setText(mSize+"%");
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		for (int i = 0; i < btn_pages.length; i++) {
			if(v.getId()==btn_pages[i]){
				nowPage=Integer.parseInt(((TextView) v).getText().toString())-1;
				makeBitmap(nowPage);
				break;
			}
		}
		if(v.getId()==btn_move_page[0]){
			nowPage = 0;
		makeBitmap(nowPage);}
		else if(v.getId()==btn_move_page[1]){
			nowPage = nowPage+(nowPage==0?0:-1);
		makeBitmap(nowPage);}
		else if(v.getId()==btn_move_page[2]){
			nowPage = nowPage+(nowPage==pages-1?0:1);
		makeBitmap(nowPage);}
		else if(v.getId()==btn_move_page[3]){
			nowPage=pages-1;
		makeBitmap(nowPage);}
		else if(v.getId()==R.id.btn_zoomin){
			if(mSize<200)
				mSize+=25;
		makeBitmap(nowPage);}
		else if(v.getId()==R.id.btn_zoomout){
			if(mSize>100)
				mSize-=25;
		makeBitmap(nowPage);}
		else if(v.getId()==R.id.btn_print_save){
			new SendToUSB().execute();
			}
		else if (v.getId()==R.id.btnStopFragment) {
			((MainActivity)getActivity()).relayout(this, null);
			return;
		}
//		makeBitmap(nowPage); 통합메소드로 사용시 zoomout&btnStopFragment onclick 미작동.
		calcPage();
		
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		bm.recycle();
	}
	
	public class SendToUSB extends AsyncTask<Void, Void, Void>{
		private boolean noUSB=false;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		String SAVE_USB_PATH;
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if(((BasicActivity)getActivity()).usbChecker()){
				SAVE_USB_PATH = ((BasicActivity)getActivity()).getUsbDirectory()+"/"+new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss").format(new Date())+".pdf";
				try {
					File f = new File(SAVE_USB_PATH);
					FileInputStream is = new FileInputStream(VTOPDF);
					FileOutputStream os = new FileOutputStream(f);
					f.createNewFile();
					byte[] b = new byte[is.available()];
					is.read(b);
					os.write(b);
					os.flush();
					is.close();
					os.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				}
				
			}else{
				noUSB = true;
				}
			return null;
		}
		protected void onPostExecute(Void result) {
			if(noUSB)
				showDefaultMessage(R.string.err_nodev_storage);
			else
				showDefaultMessage(getString(R.string.okay_storage_with_path,SAVE_USB_PATH));
			
		};
		
	};
}
