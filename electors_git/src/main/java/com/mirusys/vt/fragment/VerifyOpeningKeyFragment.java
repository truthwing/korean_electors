package com.mirusys.vt.fragment;

import com.mirusys.electors.R;
import com.mirusys.electors.ResourceFactory;
import com.mirusys.voting.MiruSmartCard;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class VerifyOpeningKeyFragment extends BasicFragment {
    private View mContentView = null;

    private View mImgWaiting = null;
    private TextView mTextStatus = null;

    private String mVtOpeningValue;
    private boolean mCardCheckable = false;
    private boolean mCardAttachable = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.frag_verify_opening_key, container, false);

		mImgWaiting = mContentView.findViewById(R.id.imgLogo);
		mTextStatus = (TextView)mContentView.findViewById(R.id.infoStatus);
		return mContentView;
	}

	@Override
	public void onStart() {
		super.onStart();

		mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER); // 관리자카드용 스마트카드 연결
		mVtOpeningValue = mFactory.getVtOpeningValue();

		if (mVtOpeningValue == null || mVtOpeningValue.length() == 0)
			mTextStatus.setText(R.string.menu_issue_07_result_1);
		else {
			((TextView)mContentView.findViewById(R.id.infoKeyInSystem)).setText(mVtOpeningValue);

			if (mVtOpeningValue.length() != 10)
				mTextStatus.setText(R.string.menu_issue_07_result_2);
			else {
				if (ResourceFactory.UI_TEST == false)
					mCardCheckable = true;
				else {
					new Thread(new Runnable() {
						@Override
						public void run() {
							try { Thread.sleep(2000); } catch(InterruptedException e) {}
							mCardCheckable = true;
						}
					}).start();
				}

				mImgWaiting.setVisibility(View.VISIBLE);
				((AnimationDrawable)mImgWaiting.getBackground()).start();
				mTextStatus.setText(mFactory.isCardAttached()? R.string.menu_issue_07_status_1 : R.string.menu_issue_07_status_0);
			}
		}
	}

	@Override
    public void onAttached(MiruSmartCard card, byte type) {
		if (mCardCheckable) {
			byte[] bytesBSection = new byte[51];

			mCardCheckable = false;

			if (ResourceFactory.UI_TEST || (
				type == 'T' &&
				card.CardVerify(mFactory.getManagerKey()) == MiruSmartCard.SUCCESS &&
				card.TCardReadBSection(bytesBSection) == MiruSmartCard.SUCCESS))
			{
				final String vtOpeningValue = new String(bytesBSection, bytesBSection.length - 10, 10); // 개시값

				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mImgWaiting.setVisibility(View.GONE);
						((AnimationDrawable)mImgWaiting.getBackground()).stop();

						if (vtOpeningValue == null || vtOpeningValue.length() == 0)
							mTextStatus.setText(R.string.menu_issue_07_result_3);
						else {
							((TextView)mContentView.findViewById(R.id.infoKeyInCard)).setText(vtOpeningValue);

							if (vtOpeningValue.length() != 10)
								mTextStatus.setText(R.string.menu_issue_07_result_4);
							else
								mTextStatus.setText(mVtOpeningValue.equals(vtOpeningValue)? R.string.menu_issue_07_result_0 : R.string.menu_issue_07_result_5);
						}
					}
				});
			}
			else {
				mCardAttachable = true;

				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mTextStatus.setText(R.string.menu_issue_07_status_0);
					}
				});
			}
		}
    }

	@Override
	public void onDetached(MiruSmartCard card) {
		if (mCardAttachable) {
			mCardCheckable = true;
			mCardAttachable = false;

			dismissedMessageBox();
		}
    }

	@Override
    public boolean onIdle(MiruSmartCard card) {
    	return mCardCheckable && mFactory.isCardAttached();
    }

	@Override
	public void onPause() {
		mCardCheckable = false;
		mCardAttachable = false;

		dismissedMessageBox();

		super.onPause();
	}
}