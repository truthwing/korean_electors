package com.mirusys.vt.fragment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.haesinit.ultralite.database.SQLNO;
import com.mirusys.electors.MainActivity;
import com.mirusys.electors.R;
import com.mirusys.electors.ResourceFactory;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.electors.ResourceFactory.QueryID;
import com.mirusys.electors.ResourceFactory.SQLState;
import com.mirusys.voting.MiruSmartCard;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class OpeningFragment extends BasicFragment implements View.OnClickListener {
	private String TAG = "OpeningFragment";

	private View mContentView = null;
	private Button mButton = null;

	private boolean mCardCheckable = false;
	private boolean mCardAttachable = false;

    private int mOpeningStep = STEP_NONE;
    private Dialog mConfirmDialog = null;
    private int mIssuedCount, mBkCount, mVtCount;
    private long mOpeningDate;

    private final static int STEP_NONE = 0;
    private final static int STEP_WAIT_FOR_ATTACH = 1;
    private final static int STEP_STATUS_CHECKING = 2;
    private final static int STEP_MANAGER_CONFIRM = 3;
    private final static int STEP_FINISHING = 4;

    private AnimationDrawable mOpeningAnimation;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.frag_vote_opening, container, false);
		mButton = (Button)mContentView.findViewById(R.id.btnOpening);
		mButton.setOnClickListener(this);
		return mContentView;
	}

	@Override
	public void onResume() {
		super.onResume();
//		mButton.setBackground(getResources().getDrawable(R.drawable.img_play_animation_01));
		mOpeningAnimation=(AnimationDrawable)((StateListDrawable) mButton.getBackground()).getCurrent();
		mOpeningAnimation.setVisible(true, true);
		mOpeningAnimation.start();
//		((AnimationDrawable)((AlphaButton.AlphaBackgroundDrawable)mButton.getBackground()).getDrawable(0)).start();
		mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER); // 관리자카드용 스마트카드 연결
	}

	@Override
	public void onPause() {
		mCardCheckable = false;
		mCardAttachable = false;
		mOpeningStep = STEP_NONE;

		dismissedMessageBox();

		if (mConfirmDialog != null && mConfirmDialog.isShowing()) {
			mConfirmDialog.dismiss();
		}
		mOpeningAnimation.stop();
//		((AnimationDrawable)((AlphaButton.AlphaBackgroundDrawable)mButton.getBackground()).getDrawable(0)).stop();
		super.onPause();
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		mButton.setBackground(null);
//		System.gc();
		super.onDestroy();
	}

	@Override
    public void onSqlStatus(ResourceFactory.SQLState state) {
		if (state != ResourceFactory.SQLState.SQL_CONNECTED) {
			Log.e(TAG, "onSqlStatus " + state);
			showDefaultMessage(R.string.error_database_failed_connection);

			if (mOpeningStep != STEP_NONE) {
	    		// commit중 (네트워크) 오류가 발생한 경우 갱신된 카드 정보를 초기화한다.
				if (mOpeningStep == STEP_FINISHING) {
					MiruSmartCard card = mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER);

					synchronized(card) {
						// 관리카드의 개시 정보를 원복하고
						byte[] bytesReadyState = new byte[13];
						bytesReadyState[0] = (byte)ResourceFactory.VOTE_ready.charAt(0);
						card.TCardWriteTPGData(bytesReadyState);
					}
				}

				mOpeningStep = STEP_NONE;
	    		// 개시 정보 업데이트 (개시 팝업) 중 데이터베이스 오류가 발생한 경우 팝업 종료
	    		if (mConfirmDialog != null && mConfirmDialog.isShowing()) {
	    			mConfirmDialog.dismiss();
	    		}
	    	}
		}
    }

	@Override
	@SuppressWarnings("incomplete-switch")
    public Object[] getParameters(QueryID id) {
		String strTpgId = mFactory.getProperty(ResourceFactory.CONFIG_TPGID);

		switch (id) {
			case Q32:
				return new Object[] {
					strTpgId,
					mFactory.isJson()?new SimpleDateFormat("yy-MM-dd").format(new Date(mFactory.getVtDate())):new java.sql.Date(mFactory.getVtDate())
				};
			case Q53:
				return new Object[] {
					strTpgId	
				};
			case Q54:
				return new Object[] {
					mFactory.isJson()?new SimpleDateFormat("yy-MM-dd").format(new Date(mOpeningDate)):new java.sql.Date(mOpeningDate),
					mFactory.isJson()?Integer.toString(mBkCount):mBkCount,
					mFactory.isJson()?Integer.toString(mVtCount):mVtCount,
					strTpgId,
					mFactory.isJson()?new SimpleDateFormat("yy-MM-dd").format(new Date(mFactory.getVtDate())):new java.sql.Date(mFactory.getVtDate())
				};
		}

		return null; // Query에 요구되는 파라메터가 없을 경우 null을 반환한다.
    }

	@Override
	@SuppressWarnings("incomplete-switch")
    public boolean onResultSet(ResultSet rs, QueryID id) throws SQLException {
		boolean moreQuery = true;

		if (rs.next()) {
			switch(id) {
				case Q32:
					if (mOpeningStep == STEP_STATUS_CHECKING) {
						String status = rs.getString("STATUS");
						if (ResourceFactory.VOTE_ready.equals(status) == false) {
							// 이미 개시/마감 상태인 경우 (DB기준) 오류 팝업
							if(!ResourceFactory.VOTE_ready.equals(status))
								showDefaultMessage(R.string.already_vote_opened);
							mOpeningStep = STEP_NONE; // 팝업 전이므로 개시 진행 속성만 초기화
							moreQuery = false;
						}
					}
					break;

				case Q53:
					mOpeningDate = rs.getDate("NOW").getTime();
					mIssuedCount = rs.getInt("VT_CCNT");

					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							mOpeningStep = STEP_MANAGER_CONFIRM;

							if (mConfirmDialog != null)
								mConfirmDialog.findViewById(R.id.btnOkay).setClickable(true);
							else {
							    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd  a hh:mm", getResources().getConfiguration().locale);

							    mConfirmDialog = new Dialog(getActivity());
								mConfirmDialog.setContentView(R.layout.popup_opening_confirm);
								mConfirmDialog.findViewById(R.id.btnOkay).setOnClickListener(OpeningFragment.this);
								mConfirmDialog.findViewById(R.id.btnCancel).setOnClickListener(OpeningFragment.this);

								((TextView)mConfirmDialog.findViewById(R.id.textTitleVote)).setText(mFactory.getProperty(ResourceFactory.CONFIG_VTTITLE));
								((TextView)mConfirmDialog.findViewById(R.id.textTitlePolls)).setText(mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME));
								((TextView)mConfirmDialog.findViewById(R.id.infoVoteDate)).setText(dateFormat.format(new java.util.Date(mOpeningDate)));
							}

							((TextView)mConfirmDialog.findViewById(R.id.infoVoteElectors)).setText(getString(R.string.format_issued_count, mIssuedCount));

							mConfirmDialog.show();
						}
					});
					break;
			}
		}
		// no data
		else if(mOpeningStep != STEP_NONE) {
			if (ResourceFactory.UI_TEST)
				Log.d(TAG, "onResultSet (empty) " + id);
			else {
				showDefaultMessage(R.string.error_database_failed_connection);
	    		mOpeningStep = STEP_NONE; // 팝업 전이므로 개시 진행 속성만 초기화
	    		moreQuery = false;
			}
		}

		return moreQuery;
    }

	@Override
	@SuppressLint({ "SimpleDateFormat", "DefaultLocale" })
    public boolean onResultSet(int rows, QueryID id) throws SQLException {
		if (id == QueryID.Q54) {
			if (rows == 0 && ResourceFactory.UI_TEST == false) // no updated record(s)
				showDefaultMessage(R.string.error_database_failed_connection);
			else if(mOpeningStep != STEP_NONE) {
				mOpeningStep = STEP_FINISHING; // 개시 정보 업데이트 중에는 팝업을 비활성화하지 못하도록 제한한다.

				MiruSmartCard card = mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER);

				synchronized(ResourceFactory.UI_TEST? mFactory : card) {
					// 관리카드의 개시 정보를 업데이트하고
					byte[] bytesOpeningState = new byte[13];
					String strOpeningValue = String.format("%010d", (long)(Math.random() * 9999999999L));
					String strOpeningTime = new SimpleDateFormat("hhmmss").format(new java.util.Date(mOpeningDate));
					Log.d(TAG,"Opening Time : "+strOpeningTime);
					bytesOpeningState[0] = (byte)ResourceFactory.VOTE_working.charAt(0);
					System.arraycopy(strOpeningTime.getBytes(), 0, bytesOpeningState, 1, 6);

					if (ResourceFactory.UI_TEST || (
						card.TCardWriteVoteRightRandom(strOpeningValue.getBytes()) == MiruSmartCard.SUCCESS &&
						card.TCardWriteTPGData(bytesOpeningState) == MiruSmartCard.SUCCESS)) {
						// auto-commit 모드가 아니기 때문에 갱신된 데이터를 물리(physical)테이블에 적용하기 위해 commit한다.
						mFactory.transaction(ResourceFactory.TRANSACTION_COMMIT);

						mFactory.setVtStatus(ResourceFactory.VOTE_working);
						mFactory.setVtOpeningValue(strOpeningValue);
						mOpeningStep = STEP_NONE;

						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								((MainActivity)getActivity()).openVoting();

								if (mConfirmDialog.isShowing()) {
									mConfirmDialog.dismiss();
								}
							}
						});

						return true;
					}
				}

				// 카드에 개시 정보 갱신이 안된 경우
				showDefaultMessage(getString(R.string.error_write_card) + '\n' + getString(R.string.error_vote_opening));
			}
			//else {
			// 팝업 상태에서 개시 정보 갱신 중 관리자 카드가 탈거되었거나 사용자가 팝업의 취소 버튼을 누른 경우
			//}

			mOpeningStep = STEP_NONE;
			// 	1. 갱신된 레코드를 원복하고
			mFactory.transaction(ResourceFactory.TRANSACTION_ROLLBACK);
			// 	2. 개시 팝업을 종료한다.
    		if (mConfirmDialog != null && mConfirmDialog.isShowing()) {
    			mConfirmDialog.dismiss();
    		}
		}

		return true;
    }

	@Override
    public void onAttached(MiruSmartCard card, byte type) {
		if (mCardCheckable) {
			mCardCheckable = false;
			dismissedMessageBox();

			byte[] bytesBSection = new byte[51];

			if (ResourceFactory.UI_TEST == false && (
				type != 'T' ||
				card.CardVerify(mFactory.getManagerKey()) != MiruSmartCard.SUCCESS ||
				card.TCardReadBSection(bytesBSection) != MiruSmartCard.SUCCESS ||
				mFactory.getProperty(ResourceFactory.CONFIG_TPGID).equals(ResourceFactory.bytesToString(bytesBSection, 0, 10)) == false))
			{
				mCardAttachable = true;
				showDefaultMessage(getString(R.string.error_password_miscard, mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME)));
			}
			else if(ResourceFactory.UI_TEST == false && (char)bytesBSection[18] != ResourceFactory.VOTE_ready.charAt(0) && (char)bytesBSection[18] != 0) {
				// 이미 개시/마감 상태인 경우 (Card기준) 오류 팝업
				showDefaultMessage((char)bytesBSection[18] == ResourceFactory.VOTE_working.charAt(0)? R.string.already_vote_opened : R.string.already_vote_finished);
				mOpeningStep = STEP_NONE; // 팝업 전이므로 개시 진행 속성만 초기화
			}
			else if(mFactory.dbQuery(QueryID.Q32, QueryID.Q53)) {
				mOpeningStep = STEP_STATUS_CHECKING;
			}
		}
    }

	@Override
	public void onDetached(MiruSmartCard card) {
		if (mCardAttachable) {
			mCardAttachable = false;
			mCardCheckable = (mOpeningStep == STEP_WAIT_FOR_ATTACH);
		}
		else if(mOpeningStep != STEP_NONE) {
    		mOpeningStep = STEP_NONE;
    		showDefaultMessage(getString(R.string.error_detached_scard) + '\n' + getString(R.string.error_vote_opening));
    		if (mConfirmDialog != null && mConfirmDialog.isShowing()) {
    			mConfirmDialog.dismiss();
    		}
    	}
    }

	@Override
    public boolean onIdle(MiruSmartCard card) {
    	return mCardCheckable && mFactory.isCardAttached();
    }

	@Override
	public void onMessageResult(View v, Object tag) {
		if (tag != null) {
			if (v.getId() == android.R.id.button1) { // yes
				mIssuedCount = 0;
				onClick((View)tag);
			}
		}
	}

	@Override
	public void onClick(View v) {
		boolean okay = false;

		v.setClickable(false);

		switch (v.getId()) {
			case R.id.btnOpening:
				if (mOpeningStep == STEP_NONE) {
					if (ResourceFactory.VOTE_finished.equals(mFactory.getVtStatus()))
						showDefaultMessage(R.string.already_vote_finished);
					else {
						mCardCheckable = true;
						mOpeningStep = STEP_WAIT_FOR_ATTACH;

						if (mFactory.isCardAttached() == false) {
							showDefaultMessage(R.string.error_password_nomanager);
						}
					}
				}
				v.setClickable(true);
				break;

			case R.id.btnOkay: okay = true;
			case R.id.btnCancel:
				if (mConfirmDialog != null && mConfirmDialog.isShowing()) {
					if (okay == false) {
						if (mOpeningStep != STEP_FINISHING) {
							mOpeningStep = STEP_NONE;
							mConfirmDialog.dismiss();
						}
					}
					else if(((EditText)mConfirmDialog.findViewById(R.id.editElectorDeivceCount)).getText().length() == 0)
						showDefaultMessage(R.string.error_empty_bkcount);
					else if(((EditText)mConfirmDialog.findViewById(R.id.editVotingDeivceCount)).getText().length() == 0)
						showDefaultMessage(R.string.error_empty_vtcount);
					else if(mIssuedCount != 0)
						showQuestionMessage(R.string.ask_forcely_opening, v);
					else {
						mBkCount = Integer.parseInt(((EditText)mConfirmDialog.findViewById(R.id.editElectorDeivceCount)).getText().toString());
						mVtCount = Integer.parseInt(((EditText)mConfirmDialog.findViewById(R.id.editVotingDeivceCount)).getText().toString());
						if (mFactory.dbQuery(QueryID.Q54)) return; // 확인 버튼 비활성화(not clickable)
					}
				}
				break;
		}

		v.setClickable(true);
	}
	@Override
	public void onEvent(int qn, String arg0) {
		// TODO Auto-generated method stub
		JSONObject res;
		JSONArray arr;
		try {
			arr = new JSONArray(arg0);
			for (int i = 0; i < arr.length(); i++) {
			res = arr.getJSONObject(i);
			if (qn==32) {
				// Q32
				if (mOpeningStep == STEP_STATUS_CHECKING) {
					String status = res.getString("STATUS");

					if (ResourceFactory.VOTE_ready.equals(status) == false) {
						// 이미 개시/마감 상태인 경우 (DB기준) 오류 팝업
						showDefaultMessage(ResourceFactory.VOTE_working
								.equals(status) ? R.string.already_vote_opened
								: R.string.already_vote_finished);
						mOpeningStep = STEP_NONE; // 팝업 전이므로 개시 진행 속성만 초기화
					}
				}
			} else if (qn==53) {
				// Q53
				String openDate = res.getString("NOW");
				try {
					mOpeningDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
							.parse(openDate).getTime();
				} catch (ParseException e) {
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				}
				mIssuedCount = res.getInt("VT_CCNT");

				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mOpeningStep = STEP_MANAGER_CONFIRM;

						if (mConfirmDialog != null)
							mConfirmDialog.findViewById(R.id.btnOkay)
									.setClickable(true);
						else {
							SimpleDateFormat dateFormat = new SimpleDateFormat(
									"yyyy.MM.dd  a hh:mm", getResources().getConfiguration().locale);

							mConfirmDialog = new Dialog(getActivity());
							mConfirmDialog
									.setContentView(R.layout.popup_opening_confirm);
							mConfirmDialog.findViewById(R.id.btnOkay)
									.setOnClickListener(OpeningFragment.this);
							mConfirmDialog.findViewById(R.id.btnCancel)
									.setOnClickListener(OpeningFragment.this);

							((TextView) mConfirmDialog
									.findViewById(R.id.textTitleVote)).setText(mFactory
									.getProperty(ResourceFactory.CONFIG_VTTITLE));
							((TextView) mConfirmDialog
									.findViewById(R.id.textTitlePolls)).setText(mFactory
									.getProperty(ResourceFactory.CONFIG_TPGNAME));
							((TextView) mConfirmDialog
									.findViewById(R.id.infoVoteDate))
									.setText(dateFormat
											.format(new java.util.Date(
													mOpeningDate)));
						}

						((TextView) mConfirmDialog
								.findViewById(R.id.infoVoteElectors))
								.setText(getString(
										R.string.format_issued_count,
										mIssuedCount));

						mConfirmDialog.show();
					}
				});
			}}
		} catch (JSONException e) {
			mOpeningStep = STEP_NONE;
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
		}		
	}
	@Override
	public void onEvent(int qn, int updateCount) {
		// TODO Auto-generated method stub
		if(qn==54){
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					((MainActivity)getActivity()).openVoting();

					if (mConfirmDialog.isShowing()) {
						mConfirmDialog.dismiss();
					}
				}
			});
		}
	}
	
	@Override
	public void onPrepareUpdate(SQLNO qn, String[] jsonParam) {
		// TODO Auto-generated method stub
		if (qn.equals(SQLNO.Q54)) {
			if (mOpeningStep != STEP_NONE) {
				mOpeningStep = STEP_FINISHING; // 개시 정보 업데이트 중에는 팝업을 비활성화하지 못하도록 제한한다.
				MiruSmartCard card = mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER);
				synchronized (ResourceFactory.UI_TEST ? mFactory : card) {
					// 관리카드의 개시 정보를 업데이트하고
					byte[] bytesOpeningState = new byte[13];
					String strOpeningValue = String.format("%010d",(long) (Math.random() * 9999999999L));
					String strOpeningTime = new SimpleDateFormat("HHmmss").format(new java.util.Date(mOpeningDate));
					bytesOpeningState[0] = (byte) ResourceFactory.VOTE_working.charAt(0);
					System.arraycopy(strOpeningTime.getBytes(), 0, bytesOpeningState, 1, 6);

					if (ResourceFactory.UI_TEST || 
							(card.TCardWriteVoteRightRandom(strOpeningValue.getBytes()) == MiruSmartCard.SUCCESS 
							&& card.TCardWriteTPGData(bytesOpeningState) == MiruSmartCard.SUCCESS)) {
						mFactory.setVtStatus(ResourceFactory.VOTE_working);
						mFactory.setVtOpeningValue(strOpeningValue);
						mOpeningStep = STEP_NONE;
						mFactory.jsonCommit(qn, jsonParam);
					}else{
						// 오류나올 경우 처리문.
						onSqlStatus(SQLState.SQL_UNKNOWN);
					}
				}
			} else {
				// 오류나올 경우 처리문.
				
			}
		}
	}
}