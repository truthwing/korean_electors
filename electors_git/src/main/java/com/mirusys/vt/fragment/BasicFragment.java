package com.mirusys.vt.fragment;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.haesinit.ultralite.database.SQLNO;
import com.mirusys.electors.BasicActivity;
import com.mirusys.electors.ResourceFactory;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.electors.ResourceFactory.QueryID;
import com.mirusys.voting.MiruSmartCard;

import android.app.Activity;
import android.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;

public class BasicFragment extends Fragment implements ResourceFactory.OnSQListener, OnClickListener{
	protected ResourceFactory mFactory;
	final static int MSG_NODELAY = 5;
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mFactory = ResourceFactory.getInstance();
	}
	@Override
    public void onSqlStatus(ResourceFactory.SQLState state) {
    }

	@Override
    public Object[] getParameters(QueryID id) {
    	return null; // Query에 요구되는 파라메터가 없을 경우 null을 반환한다.
    }

	@Override
    public boolean onResultSet(ResultSet rs, QueryID id) throws SQLException {
		return true;
    }

	@Override
    public boolean onResultSet(int rows, QueryID id) throws SQLException {
		return true;
    }

    public void onAttached(MiruSmartCard card, byte type) {
    }

	public void onDetached(MiruSmartCard card) {
    }

    public boolean onIdle(MiruSmartCard card) {
    	return false;
    }

    void dismissedMessageBox(){
    	((BasicActivity)getActivity()).dismissedMessageBox();
    }
    
    void disposeMessageDelayed(int delayMillis) {
    	((BasicActivity)getActivity()).disposeMessageDelayed(delayMillis);
    }

    void showToastMessage(int msgId, int timeout) {
		showToastMessage(getString(msgId), timeout);
	}

    void showToastMessage(String message, int timeout) {
    	((BasicActivity)getActivity()).showToastMessage(message, timeout);
	}

    void showDefaultMessage(int msgId) {
		showQuestionMessage(getString(msgId), null);
	}

    void showDefaultMessage(String message) {
		showQuestionMessage(message, null);
	}

    void showQuestionMessage(int msgId, Object tag) {
		showQuestionMessage(getString(msgId), tag);
	}

    void showQuestionMessage(String message, Object tag) {
    	((BasicActivity)getActivity()).showQuestionMessage(message, tag);
	}

    void showWaitMessage(int msgId) {
		showWaitMessage(getString(msgId));
	}

    void showWaitMessage(String message) {
    	((BasicActivity)getActivity()).showWaitMessage(message);
	}

	public void onMessageResult(View v, Object tag) {
	}

	@Override
	public void onEvent(int qn, String arg0) {
		// TODO Auto-generated method stub
		Log.i("Fragment", arg0);
	}

	@Override
	public void onEvent(int qn, int updateCount) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPrepareUpdate(SQLNO qn, String[] jsonParam) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	
}