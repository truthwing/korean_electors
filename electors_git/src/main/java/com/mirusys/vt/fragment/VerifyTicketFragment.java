package com.mirusys.vt.fragment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mirusys.electors.MainActivity;
import com.mirusys.electors.R;
import com.mirusys.electors.ResourceFactory;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.electors.ResourceFactory.QueryID;
import com.mirusys.voting.MiruSmartCard;

import android.annotation.SuppressLint;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class VerifyTicketFragment extends BasicFragment {
	private final String TAG = "VerifyTicketFragment";
    private View mContentView = null;

    private View mImgWaiting = null;
    private TextView mTextStatus = null;

	private byte[] mCardData = null;
    private boolean mCardCheckable = false;
	private String[] mTpgName = new String[2];
	private int mQueryIndex = 0;

	private final int MSG_STATUS = 1;
	private final int MSG_RESULT = 2;
	private final int MSG_ANIMATION = 3;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.frag_verify_ticket, container, false);

		mImgWaiting = mContentView.findViewById(R.id.imgLogo);
		mTextStatus = (TextView)mContentView.findViewById(R.id.infoStatus);
		return mContentView;
	}

	@SuppressLint("StringFormatInvalid")
	@Override
	public void onStart() {
		super.onStart();

		if (mFactory.getSmartCard(0) == null && ResourceFactory.UI_TEST == false) // 투표권카드용 스마트카드 연결
			mTextStatus.setText(getString(R.string.err_nodev_writer, 1));
		else {
//			((MainActivity)getActivity()).scannerForward();

			if (ResourceFactory.UI_TEST == false)
				mCardCheckable = true;
			else {
				new Thread(new Runnable() {
					@Override
					public void run() {
						try { Thread.sleep(2000); } catch(InterruptedException e) {}
						mCardCheckable = true;
					}
				}).start();
			}

			mImgWaiting.setVisibility(View.VISIBLE);
			((AnimationDrawable)mImgWaiting.getBackground()).start();
			mTextStatus.setText(R.string.menu_issue_03_status_0);
		}
	}
	@Override
    public void onAttached(MiruSmartCard card, byte type) {
		Log.i(TAG, "attached");
		if (mCardCheckable) {
			mHandler.sendEmptyMessage(MSG_ANIMATION);
			mCardCheckable = false;

			mHandler.sendMessage(mHandler.obtainMessage(MSG_STATUS, R.string.menu_issue_03_status_1, 1));
			int ret = 0;
			if((mCardData = new byte[10+10+14+2+344+3+14+344]) != null &&
				ResourceFactory.UI_TEST || (
				type == 'V' && 
				(ret=card.CardAuthentication()) == MiruSmartCard.SUCCESS &&
				(ret=card.VCardReadBSection(mCardData)) == MiruSmartCard.SUCCESS &&
				ResourceFactory.bytesToString(mCardData).length() > 0))
			{
				if (ResourceFactory.UI_TEST) {
					System.arraycopy(mFactory.getProperty(ResourceFactory.CONFIG_TPGID).getBytes(), 0, mCardData, 0, 10);
					System.arraycopy(mCardData, 0, mCardData, 10, 10);
					System.arraycopy("20150203000000".getBytes(), 0, mCardData, 20, 14);
					//System.arraycopy(mCardData, 20, mCardData, 383, 14);
				}
				
				if(!mFactory.isJson())
				mFactory.dbQuery(QueryID.Q46, QueryID.Q46); // 투표구, 투표소 조회
				else
					mFactory.dbQuery(QueryID.Q46); // 투표구, 투표소 조회
					
			}
			else {
				mHandler.sendMessage(mHandler.obtainMessage(MSG_STATUS, type != 'V'? R.string.menu_issue_03_result_1 : R.string.menu_issue_03_result_2, 0));
			}
			Log.d(TAG, "ret : "+ret);
		}
    }

	@Override
    public boolean onIdle(MiruSmartCard card) {
    	return mCardCheckable && mFactory.isCardAttached();
    }

	
	@Override
	public void onDetached(MiruSmartCard card) {
		// TODO Auto-generated method stub
		super.onDetached(card);
		Log.i(TAG,"detached");
		mCardCheckable=true;
		mQueryIndex=0;
		mCardData = new byte[10+10+14+2+344+3+14+344];
		mTpgName = new String[2];
		mHandler.sendMessage(mHandler.obtainMessage(MSG_RESULT, R.string.menu_issue_03_status_0, 0));
		mHandler.sendMessage(mHandler.obtainMessage(MSG_STATUS, R.string.menu_issue_03_status_0, 0));
	}
	@Override
    public void onSqlStatus(ResourceFactory.SQLState state) {
		if (state != ResourceFactory.SQLState.SQL_CONNECTED) {
			mHandler.sendMessage(mHandler.obtainMessage(MSG_STATUS, R.string.error_database_failed_connection, 0));
		}
    }

	@Override
    public Object[] getParameters(QueryID id) {
		if (id == QueryID.Q46)
			return new Object[] {
				new String(mCardData, mQueryIndex * 10, 10) // 투표구, 투표소
			};

    	return null; // Query에 요구되는 파라메터가 없을 경우 null을 반환한다.
    }

	
	@Override
	public void onEvent(int qn, String arg0) {
		// TODO Auto-generated method stub
		super.onEvent(qn, arg0);
		try {
			JSONArray arr = new JSONArray(arg0);
			JSONObject res;
			switch (qn) {
			case 46:
				for (int i = 0; i < arr.length(); i++) {
					res = arr.getJSONObject(i);
					mTpgName[mQueryIndex++]=res.has("TPG_NAME")?res.getString("TPG_NAME"):null;
					if (mQueryIndex >= mTpgName.length) {
						mHandler.sendMessage(mHandler.obtainMessage(MSG_RESULT, R.string.menu_issue_03_result_0, 0));
					}
				}
				if (arr.length() == 0) {
						mQueryIndex++;
					mHandler.sendMessage(mHandler.obtainMessage(MSG_RESULT,
							R.string.menu_issue_03_result_4, 0));
				}
				if (mQueryIndex < mTpgName.length) 
					mFactory.dbQuery(QueryID.Q46);
				break;
	
			default:
				break;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
		}
	}
	
	@Override
    public boolean onResultSet(ResultSet rs, QueryID id) throws SQLException {
		if (id == QueryID.Q46) {
			mTpgName[mQueryIndex++] = rs.next()? rs.getString("TPG_NAME") : null;

			if (mQueryIndex >= mTpgName.length) {
				mHandler.sendMessage(mHandler.obtainMessage(MSG_RESULT, R.string.menu_issue_03_result_0, 0));
			}
		}

		return true;
    }

	@SuppressLint("HandlerLeak")
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case MSG_RESULT: {
					Date date;
					SimpleDateFormat parseFormat = new SimpleDateFormat("yyyyMMddHHmmss", getResources().getConfiguration().locale);
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy. MM. dd.  a hh : mm", getResources().getConfiguration().locale);

					mImgWaiting.setVisibility(View.GONE);
					((AnimationDrawable)mImgWaiting.getBackground()).stop();

					((TextView)mContentView.findViewById(R.id.infoIssuedPlace)).setText(mTpgName[0]); // 투표구

					try {
						date = parseFormat.parse(ResourceFactory.bytesToString(mCardData, 20, 14));
					} catch(ParseException e) {
						date = null;
					}
					((TextView)mContentView.findViewById(R.id.infoIssuedDate)).setText(date != null? dateFormat.format(date) : null); // 발급일시

					((TextView)mContentView.findViewById(R.id.infoVotingPlace)).setText(mTpgName[1]); // 투표소

					try {
						date = parseFormat.parse(ResourceFactory.bytesToString(mCardData, 383, 14));
					} catch(ParseException e) {
						date = null;
					}
					((TextView)mContentView.findViewById(R.id.infoVotingDate)).setText(date != null? dateFormat.format(date) : null); // 투표일시
					((TextView)mContentView.findViewById(R.id.infoVotingResult)).setText(
							((TextView)mContentView.findViewById(R.id.infoVotingDate)).getText().toString().isEmpty()?
									R.string.menu_issue_03_not_voted : R.string.menu_issue_03_voted); // 투표여부
				}
				mTextStatus.setText(msg.arg1);
				break;
				case MSG_STATUS:
//					if (msg.arg2 == 0) {
//						((MainActivity)getActivity()).scannerBackward();
//					}
					mTextStatus.setText(msg.arg1);
					((TextView)mContentView.findViewById(R.id.infoVotingResult)).setText("");
					break;
				case MSG_ANIMATION:
					mImgWaiting.setVisibility(View.VISIBLE);
					((AnimationDrawable)mImgWaiting.getBackground()).start();
					break;
			}
		}
	};
}