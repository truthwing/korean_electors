package com.mirusys.vt.fragment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.haesin.entities.PDFTPGGubunObject;
import com.haesin.entities.PDFVTPeopleObj;
import com.haesin.util.EditTextWithClearButton;
import com.haesin.util.PdfExport;
import com.mirusys.electors.MainActivity;
import com.mirusys.electors.R;
import com.mirusys.electors.ResourceFactory;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.electors.ResourceFactory.QueryID;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

@SuppressLint("SdCardPath")
public class PrintStatisticsFragment extends BasicFragment implements
		OnClickListener {
	private Context mContext;
	private int startDay = -1, endDay = -1;
	public final String TAG = "PrintStatistics";
	private View mContentView = null;
	private EditTextWithClearButton mSerialStart, mSerialEnd;
	
	private boolean mIsIssueInfo=false;
	private List<PDFTPGGubunObject> mGenderVtPeople, mAgeVtPeople, mTimeVtPeople;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.frag_print_statistics, container, false);
		mSerialStart = (EditTextWithClearButton) mContentView
				.findViewById(R.id.et_serial_start);
		mSerialEnd = (EditTextWithClearButton) mContentView
				.findViewById(R.id.et_serial_end);
		mContext = getActivity().getApplicationContext();
		return mContentView;
	}

	@Override
	public void onResume() {
		super.onResume();
		mContentView.findViewById(R.id.btnPrintElectors).setOnClickListener(this);
		mContentView.findViewById(R.id.btn_issue_info).setOnClickListener(this);
		mFactory.dbQuery(QueryID.Q42);
		String bundle = null;
		if(((MainActivity)getActivity()).getIntent().getExtras()!=null&&
				(bundle = ((MainActivity)getActivity()).getIntent().getExtras().getString("tag"))!=null&&
				bundle.equals(MainActivity.PREVIEW)
				)
		mContentView.findViewById(R.id.btn_issue_info).performClick();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnPrintElectors:
			if (mSerialStart.getText().toString().equals("")) {
				showDefaultMessage(R.string.preview_start_empty);
			} else if (mSerialEnd.getText().toString().equals("")) {
				showDefaultMessage(R.string.preview_end_empty);
			} else if (Integer.parseInt(mSerialStart.getText().toString()) > Integer
					.parseInt(mSerialEnd.getText().toString())) {
				showDefaultMessage(R.string.preview_smaller_no);
			} else {
				startDay = Integer.parseInt(mSerialStart.getText().toString());
				endDay = Integer.parseInt(mSerialEnd.getText().toString());
				// mFactory.dbQuery(QueryID.Q41);
				showWaitMessage(R.string.wating_pdf_1);
				new MakePdf().execute();
			}
			break;
		case R.id.btn_issue_info:
			mIsIssueInfo = true;
			showWaitMessage(R.string.wating_pdf_1);
			new MakePdf().execute();
			break;
		default:
			break;
		}
	}

	@Override
	public boolean onResultSet(final ResultSet rs, QueryID id) throws SQLException {
		// TODO Auto-generated method stub
		switch (id) {
		case Q41:
			List<PDFVTPeopleObj> pvos = new ArrayList<PDFVTPeopleObj>();
			while (rs.next()) {
				PDFVTPeopleObj pvo = new PDFVTPeopleObj();
				pvo.setNo(Integer.parseInt(rs.getString("DJ_NO")));
				pvo.setAddress(rs.getString("ADDR"));
				pvo.setName(rs.getString("NAME"));
				pvo.setBirth(rs.getString("BIRTH_DATE"));
				String vt_yn = rs.getString("VT_YN");
				pvo.setmDate(!vt_yn.equals("") ? 
						vt_yn : "");
				pvos.add(pvo);
				Log.i(TAG, pvo.toString());
			}
			showWaitMessage(R.string.wating_pdf_2);
			PdfExport.createPdfStyle1("/sdcard/cache", "vto.pdf",
					"/sdcard/cache/font",
					mFactory.getProperty(mFactory.CONFIG_VTTITLE), pvos);
			dismissedMessageBox();
			((MainActivity) getActivity()).onMenu(mContentView.findViewById(R.id.btnPrintElectors));
			return false;
		case Q42:
			if (rs.next()) { // only 1
				final int[] elector = new int[3];

				elector[0] = rs.getInt("TOTAL_SGI");
				elector[1] = rs.getInt("TOTAL_ISSUE");
				elector[2] = rs.getInt("TPG_VCARD_ISSUE") + rs.getInt("TPG_VPAPER_ISSUE") + rs.getInt("TPG_ETC_ISSUE");

				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						((TextView) mContentView.findViewById(R.id.tv_vote_rating)).setText(new DecimalFormat("###,###.## %").format((float)elector[1]/(float)elector[0]));
						((TextView) mContentView.findViewById(R.id.tv_vote_total_cnt)).setText(String.valueOf(elector[0]));
						((TextView) mContentView.findViewById(R.id.tv_vote_cnt)).setText(String.valueOf(elector[1]));
						((TextView) mContentView.findViewById(R.id.tv_tpg_vote_cnt)).setText(String.valueOf(elector[2]));
					}
				});
			}
			return false;
		case Q43:
			mGenderVtPeople= new ArrayList<PDFTPGGubunObject>();
			while(rs.next()) {
				PDFTPGGubunObject ptgo = new PDFTPGGubunObject();
				ptgo.setGubun(rs.getString("GUBUN"));
				ptgo.setGubunIssCnt(rs.getInt("GUBUN_ISS_CNT"));
				ptgo.setGubunSgiCnt(rs.getInt("GUBUN_SGI_CNT"));
				ptgo.setGubunIssRating(rs.getString("GUBUN_ISS_RATIO"));
				ptgo.setTpgName(rs.getString("TPG_NAME"));
				mGenderVtPeople.remove(ptgo);
				mGenderVtPeople.add(ptgo);
			}
			return true;
		case Q44:
			mAgeVtPeople= new ArrayList<PDFTPGGubunObject>();
			while(rs.next()) {
				PDFTPGGubunObject ptgo = new PDFTPGGubunObject();
				ptgo.setGubun(rs.getString("GUBUN"));
				ptgo.setGubunIssCnt(rs.getInt("GUBUN_ISS_CNT"));
				ptgo.setGubunSgiCnt(rs.getInt("GUBUN_SGI_CNT"));
				ptgo.setGubunIssRating(rs.getString("GUBUN_ISS_RATIO"));
				ptgo.setTpgName(rs.getString("TPG_NAME"));
				mAgeVtPeople.remove(ptgo);
				mAgeVtPeople.add(ptgo);
			}
			return true;
		case Q45:
			mTimeVtPeople= new ArrayList<PDFTPGGubunObject>();
			while(rs.next()) {
				PDFTPGGubunObject ptgo = new PDFTPGGubunObject();
				ptgo.setGubun(rs.getString("GUBUN"));
				ptgo.setGubunIssCnt(rs.getInt("GUBUN_ISS_CNT"));
				ptgo.setGubunSgiCnt(rs.getInt("SGI_TOTAL"));
				ptgo.setGubunIssRating(rs.getString("GUBUN_ISS_RATIO"));
				ptgo.setTpgName(rs.getString("TPG_NAME"));
				mTimeVtPeople.remove(ptgo);
				mTimeVtPeople.add(ptgo);
			}
			if(mTimeVtPeople!=null&&mTimeVtPeople.size()>0){
				showWaitMessage(R.string.wating_pdf_2);
				mFactory.makeFont(mContext);
				PdfExport.createPdfStyle8("/sdcard/cache", "vto.pdf",
					"/sdcard/cache/font", mFactory.getProperty(ResourceFactory.CONFIG_VTTITLE),
					Integer.parseInt(((TextView) mContentView.findViewById(R.id.tv_vote_total_cnt)).getText().toString()),
					Integer.parseInt(((TextView) mContentView.findViewById(R.id.tv_vote_cnt)).getText().toString()),
					((TextView)mContentView.findViewById(R.id.tv_vote_rating)).getText().toString(),
					mGenderVtPeople, mAgeVtPeople, mTimeVtPeople);
				dismissedMessageBox();
				((MainActivity) getActivity()).onMenu(mContentView.findViewById(R.id.btn_issue_info));
			}
			else{
				dismissedMessageBox();
				showDefaultMessage(R.string.error_empty_sgi_0);
			}
			return false;
		default:
			break;
		}

		return true;
	}

	private class MakePdf extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			mFactory.makeFont(mContext);
		}
		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return mIsIssueInfo?mFactory.dbQuery(QueryID.Q43,QueryID.Q44,QueryID.Q45):mFactory.dbQuery(QueryID.Q41);
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			mIsIssueInfo = false;
		}
	}

	@Override
	public boolean onResultSet(int rows, QueryID id) throws SQLException {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public Object[] getParameters(QueryID id) {
		// TODO Auto-generated method stub
		if (id == QueryID.Q41) {
			return mFactory.isJson()?new Object[]{Integer.toString(startDay),Integer.toString(endDay)}:
				new Object[] { startDay, endDay };
		}
		else if (id == QueryID.Q42) {
			return new Object[] { mFactory.getProperty(ResourceFactory.CONFIG_TPGID), 
					mFactory.getProperty(ResourceFactory.CONFIG_TPGID), 
					mFactory.getProperty(ResourceFactory.CONFIG_TPGID), 
					mFactory.getProperty(ResourceFactory.CONFIG_TPGID) };
		}
		else if (id == QueryID.Q43) {
			return new Object[] {mFactory.getProperty(ResourceFactory.CONFIG_TPGID)};}
		else if (!mFactory.isJson()&&id == QueryID.Q44) {
			return new Object[] {mFactory.getProperty(ResourceFactory.CONFIG_TPGID)};}
		else if (!mFactory.isJson() && id == QueryID.Q45){ 
			return new Object[] {mFactory.getProperty(ResourceFactory.CONFIG_TPGID)};}
		return null;
	}
	@Override
	public void onEvent(int qn, String arg0) {
		int jijon=0;
		PDFTPGGubunObject[] agePeople;
		super.onEvent(qn, arg0);
		// TODO Auto-generated method stub
		JSONArray arr;
		JSONObject rs;
		try {
			arr = new JSONArray(arg0);
		switch (qn) {
		case 41:
			List<PDFVTPeopleObj> pvos = new ArrayList<PDFVTPeopleObj>();
			for (int i = 0; i < arr.length(); i++) {
				rs =arr.getJSONObject(i);
				PDFVTPeopleObj pvo = new PDFVTPeopleObj();
				pvo.setNo(Integer.parseInt(rs.getString("DJ_NO")));
				pvo.setAddress(rs.getString("ADDR"));
				pvo.setName(rs.getString("NAME"));
				pvo.setBirth(rs.getString("BIRTH_DATE"));
				String vt_yn = rs.getString("VT_YN");
				pvo.setmDate(!vt_yn.equals("") ? 
						vt_yn : "");
				pvos.add(pvo);
				Log.i(TAG, pvo.toString());
			}
			showWaitMessage(R.string.wating_pdf_2);
			PdfExport.createPdfStyle1("/sdcard/cache", "vto.pdf",
					"/sdcard/cache/font",
					mFactory.getProperty(mFactory.CONFIG_VTTITLE), pvos);
			dismissedMessageBox();
			((MainActivity) getActivity()).onMenu(mContentView.findViewById(R.id.btnPrintElectors));
			break;
		case 42:
			for (int i = 0; i < arr.length(); i++) {
				rs = arr.getJSONObject(i);
				final int[] elector = new int[3];
				elector[0] = rs.getInt("TOTAL_SGI");
				elector[1] = rs.getInt("TOTAL_ISSUE");
				elector[2] = rs.getInt("TPG_VCARD_ISSUE") + rs.getInt("TPG_VPAPER_ISSUE") + rs.getInt("TPG_ETC_ISSUE");
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						((TextView) mContentView.findViewById(R.id.tv_vote_rating)).setText(new DecimalFormat("###,###.## %").format((float)elector[1]/(float)elector[0]));
						((TextView) mContentView.findViewById(R.id.tv_vote_total_cnt)).setText(String.valueOf(elector[0]));
						((TextView) mContentView.findViewById(R.id.tv_vote_cnt)).setText(String.valueOf(elector[1]));
						((TextView) mContentView.findViewById(R.id.tv_tpg_vote_cnt)).setText(String.valueOf(elector[2]));
					}
				});
			}
			break;
		case 43:
			mGenderVtPeople= new ArrayList<PDFTPGGubunObject>();
			for (int i = 0; i < arr.length(); i++) {
				rs = arr.getJSONObject(i);
				PDFTPGGubunObject ptgo = new PDFTPGGubunObject();
				ptgo.setGubun(rs.getString("GUBUN"));
				ptgo.setGubunIssCnt(rs.getInt("GUBUN_ISS_CNT"));
				ptgo.setGubunSgiCnt(rs.getInt("GUBUN_SGI_CNT"));
				ptgo.setGubunIssRating(rs.getString("GUBUN_ISS_RATIO"));
				ptgo.setTpgName(mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME));
				mGenderVtPeople.remove(ptgo);
				mGenderVtPeople.add(ptgo);
			}
			break;
		case 44:
			jijon = 0;
			mAgeVtPeople= new ArrayList<PDFTPGGubunObject>();
			agePeople = new PDFTPGGubunObject[7];
			for (int i = 0; i < arr.length(); i++){
				
				rs = arr.getJSONObject(i);
				Log.d(TAG, rs.toString());
				Log.d(TAG, mFactory.getProperty(ResourceFactory.CONFIG_TPGID));
				int gubun_ord=0;
				try{
					gubun_ord = Integer.parseInt(rs.getString("GUBUN_ORDER"))-1;
				}catch(Exception e){
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				}
				if(agePeople[gubun_ord]==null){
					agePeople[gubun_ord] = new PDFTPGGubunObject();
					agePeople[gubun_ord].setVtTpgId(mFactory.getProperty(ResourceFactory.CONFIG_TPGID));
					agePeople[gubun_ord].setTpgName(mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME));
					agePeople[gubun_ord].setGubun(rs.getString("GUBUN"));
				}
				String tpg_id = rs.getString("VT_TPG_ID");
				if(tpg_id == null || tpg_id.length()==0){
					if(jijon!=1){
						jijon=1;
						agePeople[gubun_ord].addGubunSgiCnt(agePeople[gubun_ord].getGubunOtherSgiCnt());
					}
				}
				else{
					if(jijon==0)
						jijon=2;
				}
				
				if(jijon==1){
					agePeople[gubun_ord].addGubunSgiCnt(1);
				}else{
					if(tpg_id.equals(mFactory.getProperty(ResourceFactory.CONFIG_TPGID))){
						agePeople[gubun_ord].addGubunSgiCnt(1);
					}else
						agePeople[gubun_ord].addGubunOtherSgiCnt(1);
				}
				if(tpg_id.equals(mFactory.getProperty(ResourceFactory.CONFIG_TPGID)))
					if(rs.getString("VT_YN").equals("N")){
						agePeople[gubun_ord].addGubunIssCnt(1);
					}
				
			}
			for (int i = 0; i < 7; i++) {
				if(agePeople[i]!=null){
					agePeople[i].setGubunIssRating();
					mAgeVtPeople.add(agePeople[i]);
				}
			}
//			for (int i = 0; i < arr.length(); i++) {
//				rs = arr.getJSONObject(i);
//				PDFTPGGubunObject ptgo = new PDFTPGGubunObject();
//				ptgo.setGubun(rs.getString("GUBUN"));
//				ptgo.setGubunIssCnt(rs.getInt("GUBUN_ISS_CNT"));
//				ptgo.setGubunSgiCnt(rs.getInt("GUBUN_SGI_CNT"));
//				ptgo.setGubunIssRating(rs.getString("GUBUN_ISS_RATIO"));
//				ptgo.setTpgName(rs.getString("TPG_NAME"));
//				mAgeVtPeople.remove(ptgo);
//				mAgeVtPeople.add(ptgo);
//			}
			break;
		case 45:
			jijon = 0;
			mTimeVtPeople= new ArrayList<PDFTPGGubunObject>();
			agePeople = new PDFTPGGubunObject[9];
			for (int i = 0; i < arr.length(); i++){
				rs = arr.getJSONObject(i);
				int gubun_ord = 0;
				try{
					gubun_ord = Integer.parseInt(rs.getString("GUBUN_ORDER"))-1;
				}catch(Exception e){
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				}

				if(agePeople[0]==null){
					agePeople[0] = new PDFTPGGubunObject();
					agePeople[0].setTpgName(mFactory.getProperty(ResourceFactory.CONFIG_TPGID));
					agePeople[0].setTpgName(mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME));
				}
				String tpg_id = rs.getString("VT_TPG_ID");
				if(tpg_id == null|| tpg_id.length()==0){
					if(jijon!=1){
						agePeople[0].addGubunSgiCnt(agePeople[0].getGubunOtherSgiCnt());
						jijon=1;
					}
				}
				else{
					if(jijon==0)
						jijon=2;
				}
				
				if(jijon==1 ){
					agePeople[0].addGubunSgiCnt(1);
				}else{
					if(tpg_id.equals(mFactory.getProperty(ResourceFactory.CONFIG_TPGID))){
						agePeople[0].addGubunSgiCnt(1);
					}else
						agePeople[0].addGubunOtherSgiCnt(1);
				}
				if(tpg_id.equals(mFactory.getProperty(ResourceFactory.CONFIG_TPGID))){
					if(rs.getString("VT_YN").equals("N")){
						if(agePeople[gubun_ord]==null){
							agePeople[gubun_ord] = new PDFTPGGubunObject();
							agePeople[gubun_ord].setVtTpgId(mFactory.getProperty(ResourceFactory.CONFIG_TPGID));
							agePeople[gubun_ord].setTpgName(mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME));
						}
						agePeople[gubun_ord].addGubunIssCnt(1);
						agePeople[gubun_ord].setGubun(rs.getString("GUBUN"));
					}
				}
				
			}
			for (int i = 0; i < 9; i++) {
				if(agePeople[i]!=null){
				if(i!=0)
					agePeople[i].setGubunSgiCnt(agePeople[0].getGubunSgiCnt());
				if(i!=0 || agePeople[0].getGubunIssCnt()>0){
					agePeople[i].setGubunIssRating();
					mTimeVtPeople.add(agePeople[i]);
				}
				}
			}
//			for (int i = 0; i < arr.length(); i++) {
//				rs = arr.getJSONObject(i);
//				PDFTPGGubunObject ptgo = new PDFTPGGubunObject();
//				ptgo.setGubun(rs.getString("GUBUN"));
//				ptgo.setGubunIssCnt(rs.getInt("GUBUN_ISS_CNT"));
//				ptgo.setGubunSgiCnt(rs.getInt("SGI_TOTAL"));
//				ptgo.setGubunIssRating(rs.getString("GUBUN_ISS_RATIO"));
//				ptgo.setTpgName(rs.getString("TPG_NAME"));
//				mTimeVtPeople.remove(ptgo);
//				mTimeVtPeople.add(ptgo);
//			}
			if(mTimeVtPeople!=null&&mTimeVtPeople.size()>0){
				showWaitMessage(R.string.wating_pdf_2);
				mFactory.makeFont(mContext);
				PdfExport.createPdfStyle8("/sdcard/cache", "vto.pdf",
					"/sdcard/cache/font", mFactory.getProperty(ResourceFactory.CONFIG_VTTITLE),
					Integer.parseInt(((TextView) mContentView.findViewById(R.id.tv_vote_total_cnt)).getText().toString()),
					Integer.parseInt(((TextView) mContentView.findViewById(R.id.tv_vote_cnt)).getText().toString()),
					((TextView)mContentView.findViewById(R.id.tv_vote_rating)).getText().toString(),
					mGenderVtPeople, mAgeVtPeople, mTimeVtPeople);
				dismissedMessageBox();
				((MainActivity) getActivity()).onMenu(mContentView.findViewById(R.id.btn_issue_info));
			}else{
				dismissedMessageBox();
				showDefaultMessage(R.string.error_empty_sgi_0);
			}
			break;
		}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
		}
		
	}
}