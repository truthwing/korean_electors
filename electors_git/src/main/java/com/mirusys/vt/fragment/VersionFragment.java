package com.mirusys.vt.fragment;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipFile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mirusys.electors.R;
import com.mirusys.electors.ResourceFactory;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.electors.ResourceFactory.QueryID;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class VersionFragment extends BasicFragment {
	private static final String TAG="VersionFragment";
	private View mContentView = null;

	private SimpleDateFormat mDateFormat = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return (mContentView = inflater.inflate(R.layout.frag_version_info, container, false));
	}

	@Override
	public void onStart() {
		super.onStart();

		mFactory.dbQuery(QueryID.Q3, QueryID.Q4);
		mDateFormat = new SimpleDateFormat("yyyy.MM.dd.  a hh : mm", getResources().getConfiguration().locale);
		String strApkInfo;

		try {
			PackageInfo pi = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
			strApkInfo = String.valueOf(pi.versionCode) + "." + pi.versionName;
		} catch(NameNotFoundException e) {
			strApkInfo = getString(R.string.menu_issue_08_version_unknown);
		}

		((TextView)mContentView.findViewById(R.id.textApkVer)).setText("BK " + strApkInfo);

		ZipFile zf = null;

		try {
			ApplicationInfo ai = getActivity().getPackageManager().getApplicationInfo(getActivity().getPackageName(), 0);

			zf = new ZipFile(ai.sourceDir);
			strApkInfo = mDateFormat.format(new Date(zf.getEntry("META-INF/MANIFEST.MF").getTime()));
		} catch(Exception e) {
			strApkInfo = getString(R.string.menu_issue_08_version_unknown);
		} finally {
			try { zf.close(); } catch(IOException ie) {}
		}

		((TextView)mContentView.findViewById(R.id.infoBuildate)).setText(strApkInfo);
	}

	@Override
    public void onSqlStatus(ResourceFactory.SQLState state) {
		if (state != ResourceFactory.SQLState.SQL_CONNECTED) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					((TextView)mContentView.findViewById(R.id.infoDatabase)).setText(R.string.menu_issue_08_version_unknown);
					((TextView)mContentView.findViewById(R.id.infoVerifiedate)).setText(R.string.menu_issue_08_version_unknown);
				}
			});
		}
    }

	@Override
	@SuppressWarnings("incomplete-switch")
    public boolean onResultSet(ResultSet rs, QueryID id) throws SQLException {
		final java.sql.Date date = rs.next()? rs.getDate(1) : null;

		switch(id) {
			case Q3:
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						View v = mContentView.findViewById(R.id.infoDatabase);
						String string;

						if (date == null)
							string = getString(R.string.menu_issue_08_version_unknown);
						else
							string = mDateFormat.format(date);

						((TextView)v).setText(string + "\n(" + mFactory.getDatabaseVersion() + ')');
					}
				});
				break;

			case Q4:
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						View v = mContentView.findViewById(R.id.infoVerifiedate);

						if (date == null)
							((TextView)v).setText(R.string.menu_issue_08_version_unknown);
						else
							((TextView)v).setText(mDateFormat.format(date));
					}
				});
				break;
		}

		return true;
    }
	@Override
	public void onEvent(int qn, String arg0) {
		// TODO Auto-generated method stub
		super.onEvent(qn, arg0);
		
		
		switch(qn) {
		case 3:
			try {
				JSONArray arr = new JSONArray(arg0);
				for(int i=0;i<arr.length();i++){
					JSONObject obj = arr.getJSONObject(i);
					try {
						final Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm:sss").parse(obj.getString("VER_DATE"));
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								View v = mContentView.findViewById(R.id.infoDatabase);
								String string;

								if (date == null)
									string = getString(R.string.menu_issue_08_version_unknown);
								else
									string = mDateFormat.format(date);

								((TextView)v).setText(string + "\n(" + mFactory.getDatabaseVersion() + ')');
							}
						});
						break;
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}
			break;

		case 4:
			try {
				JSONArray arr = new JSONArray(arg0);
				for(int i=0;i<arr.length();i++){
					JSONObject obj = arr.getJSONObject(i);
					try {
						final Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").parse(obj.getString("DATACHECK_DATE"));
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								View v = mContentView.findViewById(R.id.infoVerifiedate);

								if (date == null)
									((TextView)v).setText(R.string.menu_issue_08_version_unknown);
								else
									((TextView)v).setText(mDateFormat.format(date));
							}
						});
					} catch (ParseException e) {
						Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								View v = mContentView.findViewById(R.id.infoVerifiedate);
								((TextView)v).setText(R.string.menu_issue_08_version_unknown);
							}
						});
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}
			
			break;
	}
	}
}