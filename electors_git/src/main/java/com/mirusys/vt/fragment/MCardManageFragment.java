package com.mirusys.vt.fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.haesin.entities.CardBackupObject;
import com.mirusys.electors.MainActivity;
import com.mirusys.electors.R;
import com.mirusys.electors.ResourceFactory;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.voting.MiruSmartCard;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class MCardManageFragment extends BasicFragment implements OnClickListener, OnItemClickListener {
	public final static String TAG = "MCardManageFragment";
	private View mContentView;
    private LayoutInflater mInflater;
    private EditText mSecureCode;
    private Button mBtnQuery, mBtnBackup, mBtnRestore;
    private ListView mListView = null;
    private List<CardBackupObject> mCardbackups = new ArrayList<CardBackupObject>();
    private CardBackupObject mCbo = null;
    private int[] mTPGCnt={R.id.img01, R.id.img02, R.id.img03, R.id.img04, R.id.img05
    		, R.id.img06, R.id.img07, R.id.img08, R.id.img09, R.id.img10
    		, R.id.img11, R.id.img12, R.id.img13, R.id.img14, R.id.img15
    		, R.id.img16, R.id.img17, R.id.img18, R.id.img19, R.id.img20};
    
    private int mPosition=-1;
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mInflater = inflater;
		mContentView = inflater.inflate(R.layout.frag_mcard_manage, container, false);
		mSecureCode = (EditText)mContentView.findViewById(R.id.editSecureCode);
		mSecureCode.setText(mFactory.getManagerKey());
		mBtnQuery = (Button)mContentView.findViewById(R.id.btnLogin);
		mBtnQuery.setOnClickListener(this);
		mBtnBackup = (Button)mContentView.findViewById(R.id.btnBackup);
		mBtnBackup.setOnClickListener(this);
		mBtnRestore = (Button)mContentView.findViewById(R.id.btnRestore);
		mBtnRestore.setOnClickListener(this);
		mCardbackups = ((MainActivity)getActivity()).CARDSQLITE.select();
		mListView = (ListView)mContentView.findViewById(R.id.lvCards);
		mListView.setAdapter(mlvAdapter);
		mListView.setOnItemClickListener(this);
		return mContentView;
	}

	@Override
	public void onResume() {
		super.onResume();
		mContentView.findViewById(R.id.btnLogin).performClick();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	private BaseAdapter mlvAdapter = new BaseAdapter() {
		private int[] mTPGid={R.id.c_img01, R.id.c_img02, R.id.c_img03, R.id.c_img04, R.id.c_img05
	    		, R.id.c_img06, R.id.c_img07, R.id.c_img08, R.id.c_img09, R.id.c_img10
	    		, R.id.c_img11, R.id.c_img12, R.id.c_img13, R.id.c_img14, R.id.c_img15
	    		, R.id.c_img16, R.id.c_img17, R.id.c_img18, R.id.c_img19, R.id.c_img20};
		MyHolder holder;
		TextView textView1,textView2,textView3;
		TextView[] textStatuses = new TextView[20];
		@Override
		@SuppressLint("InflateParams")
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.row_mcard_manage, null);
				holder = new MyHolder();
				textView1=((TextView) convertView.findViewById(R.id.textview01));
				textView2=((TextView) convertView.findViewById(R.id.textview02));
				textView3=((TextView) convertView.findViewById(R.id.textview03));
				holder.textView1 = textView1;
				holder.textView2 = textView2;
				holder.textView3 = textView3;
				for (int i = 0; i < mTPGid.length; i++) {
					textStatuses[i]=
							((TextView) convertView.
							findViewById(mTPGid[i]));
				}
				holder.textArr = textStatuses;
				convertView.setTag(holder);
			}else{
				holder = (MyHolder) convertView.getTag();
				textView1=holder.textView1;
				textView2=holder.textView2;
				textView3=holder.textView3;
				textStatuses=holder.textArr;
			}
			if(mCardbackups.get(position)!=null){
			CardBackupObject cbo = mCardbackups.get(position);
			
			//리스트 뷰의 내용에서 투표기 개시 상태 표시
				if(mCardbackups.size()>0){
				for (int i = 0; i < mTPGid.length; i++) {
					if(cbo!=null && cbo.gettPGStatus() != null && cbo.gettPGStatus()[i]!=null){
						String statusTpg= String.valueOf(cbo.gettPGStatus()[i].charAt(18));
					textStatuses[i].setText(statusTpg.equals("B")||statusTpg.equals("C")||statusTpg.equals("A")?statusTpg:"");}
					}
				try {
					if(!cbo.getTime().isEmpty())
						textView1.setText(
							new SimpleDateFormat("yyyy MM/dd").format(
							new SimpleDateFormat("yyyyMMdd").parse(cbo.getDate()))+
							new SimpleDateFormat(" a hh:mm:ss").format(
							new SimpleDateFormat("HHmmss").parse(cbo.getTime())));
					else
						textView1.setText(
								new SimpleDateFormat("yyyy MM/dd").format(
										new SimpleDateFormat("yyyyMMdd").parse(cbo.getDate()))+
										new SimpleDateFormat(" a hh:mm:ss").format(
												new SimpleDateFormat("hhmmss").parse("000000")));
				} catch (ParseException e) {
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				}
				textView2.setText(cbo.gettPGID());
				textView3.setText(cbo.getStatus());
				}
			}
			if(mPosition==position)
				convertView.setBackgroundColor(getResources().getColor(R.color.article_active));
			else
				convertView.setBackground(null);
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public CardBackupObject getItem(int position) {
			return mCardbackups.get(position);
		}

		@Override
		public int getCount() {
			return mCardbackups.size();
		}
	};
	
	private class MyHolder{
		private TextView[] textArr = new TextView[20];
		private TextView textView1, textView2,textView3;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnLogin:
			showWaitMessage(R.string.menu_issue_01_popup_closing_retrieve);
			new Thread(){
				@Override
				public void run() {
					// TODO Auto-generated method stub
					super.run();
					byte []outData = new byte[51];
					CardBackupObject card = new CardBackupObject();
					int ret = 0;
					if(!ResourceFactory.UI_TEST&&
							!mFactory.getSmartCard(0).CardGetType().equals("T")){
						dismissedMessageBox();
						showDefaultMessage(R.string.error_password_nomanager);
						}
					else if(!ResourceFactory.UI_TEST&&
							mSecureCode.getText().toString().length()!=8||
							(ret=mFactory.getSmartCard(0).CardVerify(mSecureCode.getText().toString()))!=MiruSmartCard.SUCCESS){
						dismissedMessageBox();
						showDefaultMessage(R.string.error_password_wrong_normal);}
					else if(!ResourceFactory.UI_TEST&&
							(ret=mFactory.getSmartCard(0).TCardReadBSection(outData))!=MiruSmartCard.SUCCESS){
						dismissedMessageBox();
						showDefaultMessage(R.string.mcard_error_info);}
					else {
						String[] statuses = new String[20];
						for (int i = 1; i <= 20; i++) {
							byte cData[] = new byte[24];
							if(!ResourceFactory.UI_TEST&&
									(ret=mFactory.getSmartCard(0).TCardReadCSectionRow(new byte[3], (byte)(0x00+i), cData))!=MiruSmartCard.SUCCESS){
								showDefaultMessage(R.string.mcard_error_info);
								break;
							}else{
								statuses[i-1]=new String(cData);
							}
						}
						card.settPGStatus(statuses);
						card.setDate(ResourceFactory.bytesToString(outData, 10, 8));
						card.settPGID(ResourceFactory.bytesToString(outData, 0, 10));
						card.setStatus(ResourceFactory.bytesToString(outData, 18, 1));
						card.setTime(ResourceFactory.bytesToString(outData, 19, 6));
						dismissedMessageBox();
						mHandler.sendMessage(mHandler.obtainMessage(1, card));
					}
					Log.d(TAG,"ret issue = "+ret+" select data : "+card.toString());
				}
			}.start();
			break;
		case R.id.btnBackup:
			showWaitMessage(R.string.menu_issue_01_popup_closing_retrieve);
			mPosition  = -1;
			mCbo = null;
			new Thread(){
				public void run() {
					byte []outData1 = new byte[51];
					CardBackupObject card1 = new CardBackupObject();
					int ret2 = 0;
					if(!ResourceFactory.UI_TEST&&
							!mFactory.getSmartCard(0).CardGetType().equals("T")){
						dismissedMessageBox();
						showDefaultMessage(R.string.error_password_nomanager);}
					else if(!ResourceFactory.UI_TEST&&
							mSecureCode.getText().toString().length()!=8||
							(ret2=mFactory.getSmartCard(0).CardVerify(mSecureCode.getText().toString()))!=MiruSmartCard.SUCCESS){
						dismissedMessageBox();
						showDefaultMessage(R.string.error_password_wrong_normal);}
					else if(!ResourceFactory.UI_TEST&&
							(ret2=mFactory.getSmartCard(0).TCardReadBSection(outData1))!=MiruSmartCard.SUCCESS){
						dismissedMessageBox();
						showDefaultMessage(R.string.mcard_error_info);}
					else {
						String[] statuses = new String[20];
						for (int i = 1; i <= 20; i++) {
							byte cData[] = new byte[24];
							if(!ResourceFactory.UI_TEST&&
									(ret2=mFactory.getSmartCard(0).TCardReadCSectionRow(new byte[3], (byte)(0x00+i), cData))!=MiruSmartCard.SUCCESS){
								showDefaultMessage(R.string.mcard_error_info);
								break;
							}else{
								statuses[i-1]=new String(cData);
							}
						}
						card1.settPGStatus(statuses);
						card1.setDate(ResourceFactory.bytesToString(outData1, 10, 8));
						card1.settPGID(ResourceFactory.bytesToString(outData1, 0, 10));
						card1.setStatus(ResourceFactory.bytesToString(outData1, 18, 1));
						card1.setTime(ResourceFactory.bytesToString(outData1, 19, 6));
						((MainActivity)getActivity()).CARDSQLITE.insert(card1);
						dismissedMessageBox();
						mHandler.sendEmptyMessage(2);
					}
					Log.d(TAG,"ret issue = "+ret2+" select data : "+card1.toString());
				};
			}.start();
			break;
		case R.id.btnRestore:
			showQuestionMessage(R.string.menu_admin_question, TAG);
			break;
		default:
			break;
		}
	}

@Override
public void onMessageResult(View v, Object tag) {
	// TODO Auto-generated method stub
	super.onMessageResult(v, tag);
	if(tag==TAG){
			showWaitMessage(R.string.menu_issue_01_popup_closing_retrieve);
			new Thread() {
				public void run() {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
					}
					boolean mIsRestore = false;
					int ret1 = 0;
					byte[] inData = new byte[13];
					if (mCbo != null) {
						inData[0] = (byte) mCbo.getStatus().charAt(0);
						if (!mCbo.getTime().isEmpty())
							System.arraycopy(mCbo.getTime().getBytes(), 0, inData, 1, 6);
						else
							System.arraycopy("000000".getBytes(), 0, inData, 1, 6);
						if (!ResourceFactory.UI_TEST && !mFactory.getSmartCard(0).CardGetType().equals("T")) {
							dismissedMessageBox();
							showDefaultMessage(R.string.error_password_nomanager);
						} else if (!ResourceFactory.UI_TEST && mSecureCode.getText().toString().length() != 8
								|| (ret1 = mFactory.getSmartCard(0)
										.CardVerify(mSecureCode.getText().toString())) != MiruSmartCard.SUCCESS) {
							dismissedMessageBox();
							showDefaultMessage(R.string.error_password_wrong_normal);
						} else if (!ResourceFactory.UI_TEST && (ret1 = mFactory.getSmartCard(0)
								.TCardWriteTPGData(inData)) != MiruSmartCard.SUCCESS) {
							dismissedMessageBox();
							showDefaultMessage(R.string.mcard_error_info);
						} else {
							for (int i = 1; i <= 20; i++) {
								String[] statuses = new String[20];
								statuses[i - 1] = mCbo.gettPGStatus()[i - 1];
								if (statuses[i - 1] == null)
									statuses[i - 1] = "";
								if (!ResourceFactory.UI_TEST && (ret1 = mFactory.getSmartCard(0).TCardWriteCSectionRow(
										statuses[i - 1].getBytes(), (byte) (0x00 + i))) != MiruSmartCard.SUCCESS) {
									dismissedMessageBox();
									showDefaultMessage(R.string.mcard_error_info);
									mIsRestore = false;
									break;
								} else {
									mIsRestore = true;
								}
							}
							dismissedMessageBox();
						}
						Log.d(TAG, "ret = " + ret1);
						if (mIsRestore) {
							mHandler.sendEmptyMessage(3);
							mCbo = null;
						}
					} else {
						dismissedMessageBox();
						showDefaultMessage(R.string.mcard_error_select);
					}
				};
			}.start();
		}
}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		mPosition = position;
		for(int i = 0 ; i < parent.getChildCount();i++){
			parent.getChildAt(i).setBackground(null);
		}
		view.setBackgroundColor(getResources().getColor(R.color.article_active));
		mCbo = new CardBackupObject();
		mCbo = mCardbackups.get(position);
		Log.d(TAG, "CardBackup = "+mCbo.toString());
	}
	
	Handler mHandler = new  Handler(){
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 1:
				CardBackupObject card = (CardBackupObject) msg.obj;
				String[] statuses=  card.gettPGStatus();
				((TextView)mContentView.findViewById(R.id.infoVoteStatus)).setText(card.getStatus());
				((TextView)mContentView.findViewById(R.id.infoVotePlaceCode)).setText(card.gettPGID());
				try {
					if(!card.getTime().isEmpty())
					((TextView)mContentView.findViewById(R.id.infoVoteDate)).setText(
							new SimpleDateFormat("yyyy MM/dd").format(
									new SimpleDateFormat("yyyyMMdd").parse(card.getDate()))+
									new SimpleDateFormat(" a hh:mm:ss").format(
											new SimpleDateFormat("HHmmss").parse(card.getTime()))
							);
					else
					((TextView)mContentView.findViewById(R.id.infoVoteDate)).setText(
							new SimpleDateFormat("yyyy MM/dd").format(
									new SimpleDateFormat("yyyyMMdd").parse(card.getDate()))+
									new SimpleDateFormat(" a hh:mm:ss").format(
											new SimpleDateFormat("HHmmss").parse("000000"))
							);
				} catch (ParseException e) {
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				}
				//상위에 조회 내용 중 투표기 개시 상태 표시
				for (int i = 0; i < statuses.length; i++) {
					if(statuses[i]==null){
						((TextView)mContentView.findViewById(mTPGCnt[i])).setText("");
					}else{
						if(statuses[i].charAt(18)=='B'||statuses[i].charAt(18)=='C')
							((TextView)mContentView.findViewById(mTPGCnt[i])).setText(String.valueOf(statuses[i].charAt(18)));
						else
							((TextView)mContentView.findViewById(mTPGCnt[i])).setText("");
						}
				}
				break;
			case 2:
				mCardbackups = ((MainActivity)getActivity()).CARDSQLITE.select();
				mlvAdapter.notifyDataSetChanged();
				break;
			case 3:
				mContentView.findViewById(R.id.btnLogin).performClick();
				mPosition = -1;
				for (int i = 0; i < mListView.getChildCount(); i++) {
					mListView.getChildAt(i).setBackground(null);
				}
				break;
			default:
				break;
			}
			
		};
	};
}