package com.mirusys.voting;

public class MiruScanner {
	// 지정된 scanner(ex. /dev/miru0)를 open한다. 
	public native boolean Open(String devpath);
	public native void Close();
	public native boolean IsClosed();

	// Command가 정상 전송된 경우, (파라메터로 지정한 length만큼 전송)
	// Scanner로부터 수신된 응답을 반환한다. (수신된 byte수를 반환)
	// buffer는 최소 512 bytes여야 한다.
	// 오류 발생 시 POSIX 형식의 오류 코드를 반환한다.
	public native int SendCommand(byte[] buffer, int length);
	// Scanner로부터 bytes 크기만큼 RAW 데이터를 수신한다.
	// 반드시, STxx 응답에 담긴 width*height만큼 수신해야 한다.
	// 오류 발생 시 POSIX 형식의 오류 코드를 반환한다.
	public native int ReadImage(byte[] buffer);

	static {
		System.loadLibrary("miruscan");
	}
}
