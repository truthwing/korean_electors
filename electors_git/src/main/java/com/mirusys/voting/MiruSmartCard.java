package com.mirusys.voting;

public class MiruSmartCard {
//빈 인자값을 던져주고 넣어서 종료되는 메소드
	//Error Code Define=====================================================================================
	public static final int SUCCESS = 0;                            // 정상완료
	public static final int ERR_CARDCMD = -1;			    // 카드 명령어 에러(정상적인 경우 발생하면 카드 broken가능)
	public static final int ERR_PCSC = -2;				            // PCSC 명령어 에러
	public static final int ERR_SCREADER_NONE = -3;		// 리더기 선택 에러
	public static final int ERR_CARDCONNECT = -4;		    // 카드 연결 에러
	public static final int ERR_CARDDISCONNECT = -5;	// 카드 종료 에러
	public static final int ERR_INVALID_CARD = -6;		    // 전자투표용 카드가 아님
	public static final int ERR_AUTHCARD = -7;			    // 카드 인증 실패
	public static final int ERR_VERHASH = -8;			        // 카드 Hash 확인 에러
	public static final int ERR_INVALID_CARDTYPE = -9;	// 카드 타입 에러
	public static final int ERR_SC_PIN_LOCK = -10;		    // 관리자 PIN LOCK
	public static final int ERR_SC_PIN_ERR = -11;		        // 관리자 PIN 실패(LOCK은 아님)
	public static final int ERR_SC_GET_KEY = -12;		    // 관리자카드 읽기 에러
	public static final int ERR_ROW_KEY = -13;			    // 입력 IndexKey에러
	public static final int ERR_APIPARA = -14;			        // 입력 parameter 에러
	public static final int ERR_NOGENKEY = -15;			    // 키 없음(관리자카드 인증 안함)
	public static final int ERR_INITFAIL = -16;			        // 스마트카드리더 초기화실
	public static final int ERR_NOCARD = -17;                 // 스마트가드가 없음
	public static final int ERR_RESETFAIL = -18;
	public static final int ERR_ATR = -19;

	//Basic Functions=======================================================================================
	public native int CardSelectActivePort(int port_no);//USB포트 번호 or 내부에 연결된 스마트 카드 리더기 포트 번호
	public native int CardOpenSession(int port_no);//USB포트 번호 or 내부에 연결된 스마트 카드 리더기 포트 번호
	public native int CardCloseSession(int port_no);//USB포트 번호 or 내부에 연결된 스마트 카드 리더기 포트 번호
	public native int CheckCardInOut();//카드 여부

	//SCXPDLL or SCCEDLL====================================================================================
	//명부기에서 사용하지 않는 기능
	//A:마스터카드, B:구시군카드, T:투표소관리카드, G:개표소관리카드, V:투표자카드 
	public native String CardGetType();
	public native int CardVerify(String passwd);//카드에 대한 비밀번호 인증
	public native int CardAuthentication();//인증기능
	public native int CardChangePIN(String oldPinCode, String newPinCode);//
	
	//TCARD Function
	/**
	 * @param InData 투표소상태(A:준비,B:개시,C:마감)[1], 개시시각[6], 마감시각[6]
	 * @return 성공(SUCCESS), 그 외 (ERR_)
	 */
	public native int TCardWriteTPGData(byte[] InData);
	public native int TCardWriteCSectionRow(byte[] InData, byte IndexKey);//(투표기용 일련번호[3], mac address[15], 투표기상태[1], 전자투표자수[5]),indexkey(투표기 갯수 20)
	public native int TCardInitCSectionRow(byte[] RowKey, byte IndexKey);//사용하지 않는 기능
	public native int TCardInitCSectionAll();//스마트 카드의 C섹션 데이터 완전 삭제로 테스팅 후에 데이터 삭제를 위한 기능(초기화 작업할 때 TCardWriteTPGData를 불러 준비상태로 변경)
	/**
	 * @param OutData 투표ID[10], 투표일자[8], 투표소상태(A:준비,B:개시,C:마감)[1], 개시시각[6], 마감시각[6], 실행프로그램용 난수[10](빈칸으로 두기)
	 * @return 성공(SUCCESS), 그 외 (ERR_)
	 */
	public native int TCardReadBSection(byte[] OutData);
	public native int TCardReadCSectionRow(byte[] RowKey, byte IndexKey, byte[] OutData);//null[3], indexkey(투표기 갯수 20), (투표기용 일련번호[3], mac address[15], 투표기상태[1], 전자투표자수[5])
	public native int TCardReadDNData(byte[] oDNCERT);//사용하지 않는 기능
	public native int TCardReadPKIDatainVT(byte[] TOSHASH, byte[] TEXEHASH, byte ReadType, byte[] TMC_PKI);//사용하지 않는 기능
	/**
	 * 해시검사 및 요청한 인증서 데이터 반환
	 * 
	 * @param MEXEHASH 실행파일 해시코드 (입력)
	 * @param ReadType 인증서 타입 (입력: 서명용인증서=1,서명용키=2,중앙선관위인증서=3,구시군선관위 인증서=4)
	 * @param TMC_PKI 인증서 (출력, 최대 1302bytes)
	 * @return 성공(SUCCESS), 그 외 (ERR_)
	 */
	public native int TCardReadPKIDatainBK(byte[] MEXEHASH, byte ReadType, byte[] TMC_PKI);
	/**
	 * 데이터베이스 접속을 위한 계정 정보 반환
	 * @param PKey 개인 키 (? 최대 16bytes)
	 * @param DBID 계정 ID (최대 50bytes)
	 * @param DBPWD 계정 비밀번호 (최대 50bytes)
	 * @return 성공(SUCCESS), 그 외 (ERR_)
	 */
	public native int TCardReadKeyDBData(byte[] PKey, byte[] DBID, byte[] DBPWD);
	public native int TCardWriteVoteRightRandom(byte[] voteRightRandom);//실행프로그램용 난수[10] (TCardWriteTPGData에서 정의된 내용을 여기서 사용하기에 위에서는 사용하지 않아도 됨.)
	//GCARD Function (사용하지 않는 기능)
	public native int GCardReadBSection(byte[] OutData);//
	public native int GCardReadDNData(byte[] oDNCERT);//
	public native int GCardReadPKIDatainCT(byte[] CEXEHASH, byte ReadType, byte[] CMC_PKI);//
	public native int GCardReadPKIDatainVF(byte[] VEXEHASH, byte ReadType, byte[] CMC_PKI);//
	public native int GCardReadDBData(byte[] DBID, byte[] DBPWD);//
	//VCARD Function 
	/**
	 * @param InData 투표구ID[10], 투표소ID[10], 발급일자[14], 선거인그룹[2](가중치), 전자서명[344](명부기 인증서 데이터),
					(투표기에서 작성하는 사항이고 발급기에서 null이 아닌 경우 발급한 것으로 간주함)투표기번호[3], 투표일시[14], 전자서명[344](투표기 인증서)
	 * @return 성공(SUCCESS), 그 외 (ERR_)
	 */
	public native int VCardWriteBSection(byte[] InData);
	/**
	 * 발급할 때 초기화 작업
	 * @return 성공(SUCCESS), 그 외 (ERR_)
	 */
	public native int VCardInitBSection();
	/**
	 * @param OutData 투표구ID[10], 투표소ID[10], 발급일자[14], 선거인그룹[2](가중치), 전자서명[344](명부기 인증서 데이터),
	 * 				 (투표기에서 작성하는 사항이고 발급기에서 null이 아닌 경우 발급한 것으로 간주함)투표기번호[3], 투표일시[14], 전자서명[344](투표기 인증서)
	 * @return 성공(SUCCESS), 그 외 (ERR_)
	 */
	public native int VCardReadBSection(byte[] OutData);
	public native int VCardReadCertDNData(byte[] NECCERT, byte[] oDNCERT);//명부기에서 사용안함
	public native int VCardReadDNData(byte[] oDNCERT);//전자서명[344](투표자 카드 인증)
	public native int VCardReadVoteCert(byte[] oVoteName, byte[] oVoteNameCert);//명부기에서 사용안함
//하위내용은 스마트카드 발급 API 문서에 지정되어 있음.
	//SCIssDLL===============================================================================================
	public native int ProcWritePIN(String PinCode);//
	public native int ProcChangePIN(String oldPinCode, String newPinCode);//
	/**
	 * 삽입된 카드 타입을 반환한다.
	 * 첫번째 바이트에 카드 타입이 기록되어 반환된다. (A:마스터카드, B:구시군카드, T:투표소관리카드, G:개표소관리카드, V:투표자카드)
	 * 
	 * @param cType [out] 인식된 카드 타입을 반환 받기 위한 버퍼 (2바이트 이상이어야 함)
	 * @return 성공(SUCCESS), 그 외 (ERR_)
	 */
	public native int ProcGetCardType(byte[] cType);
	public native int ProcGenerateKeyMAP(byte[] seed1, byte[] seed2, byte[] seed3);//명부기에서 사용하지 않는 정의

	public native int ProcIssNECIntData(byte[] NEC_PIN, byte[] NEC_CSN);//명부기에서 사용하지 않는 정의
	public native int ProcIssNECPKIData(byte IssType, byte[] NEC_PKI);//명부기에서 사용하지 않는 정의
	public native int ProcIssNECDBData(byte[] DBID, byte[] DBPWD);//명부기에서 사용하지 않는 정의

	public native int ProcReadNECIntData(byte[] NEC_PIN);//명부기에서 사용하지 않는 정의?? 관리자카드 인증(비밀번호[8]) (check)
	public native int ProcReadNECPKIData(byte ReadType, byte[] NEC_PKI);//명부기에서 사용하지 않는 정의
	public native int ProcReadNECDBData(byte[] DBID, byte[] DBPWD);//명부기에서 사용하지 않는 정의

	public native int ProcIssLECIntData(byte[] LEC_PIN, byte[] LEC_CSN);//명부기에서 사용하지 않는 정의
	public native int ProcIssLECPKIData(byte IssType, byte[] LEC_PKI);//명부기에서 사용하지 않는 정의
	public native int ProcIssLECDBData(byte[] DBID, byte[] DBPWD);//명부기에서 사용하지 않는 정의
	public native int ProcIssLECCommitteeData(byte[] COID);//명부기에서 사용하지 않는 정의
//구시군 읽기전용
	//ProcReadLECIntData 비밀번호 인증을 해야지만 아래의 다른 메소드 사용 가능.
	//인코딩되는 값 알아야함. ms949? 모든 String이 ms949로 되어 있을 경우 인코딩 작업을 매번 해줘야함.
	public native int ProcReadLECIntData(byte[] LEC_PIN);//구시군카드 인증(암호) ms949형식으로 인코딩해야함. return 0 or 1 or -1
	public native int ProcReadLECPKIData(byte ReadType, byte[] LEC_PKI);//중앙선관위카드의 각 키에 해당하는 인증서 가져오는 작업(키, 인증서(빈 값을 넣으면 채워져서 나옴)[1302])
	public native int ProcReadLECDBData(byte[] DBID, byte[] DBPWD);//DB ID값과 pw를 가지고 올 수 있음.(DBID[50]빈 값을 넣으면 채워져서 나옴, DBPWD[50]도 동일)orcle? sybase?
	public native int ProcReadLECCommitteeData(byte[] COID);//빈값을 넣으면 output 10byte
//관리자 카드 인증작업.
	//ProcReadNECIntData 관리자 카드 인증 작업을 해야만 다른 작업을 할 수 있음.
	//인코딩에 대한 명확한 정리가 필요.(MS949 코드로 전부 인코딩 해야함 카드에 적힌 모든 내용이 MS949.)
	public native int ProcIssTMCIntDatainNEC(byte[] TM_PIN, byte[] TM_CSN);//관리자카드 인증작업(비밀번호[8], 시리얼번호 [8]) 시리얼 번호 발급? API상 output이 없는 것으로 적혀있음.  
	public native int ProcIssTMCPKIDatainNEC(byte[] NECCERT, byte[] DNCert);//인증서로 암호화된 DN서명값 발급(API문서와 다른 내용으로 확인 불가)(check)(소스상 정의만하고 사용안함)
	public native int ProcIssTMCHashDatainNEC(byte HashType, byte[] HashData);//해쉬 발급(해쉬 종류[1](투표기 os hash =1, 투표기 실행파일 hash =2, 명부기 실행파일 hash =3), hashdata[44])(소스상 사용하지 않음)
	public native int ProcIssTMCPKIDatainLEC(byte IssType, byte[] TMC_PKI);//구시군 선관위 인증키(해쉬 종류[1], 인증서[1302] 바이트 배열)(소스상 정의만하고 사용안함)
	public native int ProcIssTMCKeyDatainLEC(byte[] PrivateKEY);//키 발급(빈값 넣어서 가져오는 parameter)(소스상 정의만하고 사용안함)
	public native int ProcIssTMCPollsDatainLEC(byte[] PollsID, byte[] VoteDate);//ID[10] 날짜[8] (소스상 정의만하고 사용안함)

	public native int ProcIssTMCRandominLEC(byte[] TMGR_PIN, byte[] RandomData);//비밀번호를 통한 난수 발급? api상 둘다 input으로 되어 있음.발급은 없는 것으로 확인(check)
	public native int ProcReadTMCRandom(byte[] TMGR_PIN, byte[] RandomData);//난수 읽기 비밀번호 입력[8], 난수에 빈값[10] 
	public native int ProcReadTMCVoteRightRandom(byte[] TMGR_PIN, byte[] RandomData);//API상 사용하지 않음.(소스상 정의만하고 사용안함)
	public native int ProcIssTMCVoteRightRandom(byte[] TMGR_PIN, byte[] RandomData);//API상 사용하지 않음.(소스상 정의만하고 사용안함)
	public native int ProcReadTMCPollsData(byte[] TMGR_PIN, byte[] oPollsID, byte[] oVoteDate);//비밀번호[8]을 입력하면 투표소 ID와 날짜를 반환.
//개표기 관련
	public native int ProcIssCMCIntDatainNEC(byte[] CM_PIN, byte[] CM_CSN);//개표상 인증 발급?api상 둘다 input으로 되어 있음.발급은 없는 것으로 확인(소스상 정의만하고 사용안함)
	public native int ProcIssCMCPKIDatainNEC(byte[] NECCERT, byte[] DNCert);//서명값 발급 parameter가 다름 (소스상 정의만하고 사용안함)
	public native int ProcIssCMCHashDatainNEC(byte HashType, byte[] HashData);//개표기 관련 함수(소스상 정의만하고 사용안함)
	public native int ProcIssCMCDBDatainNEC(byte[] DBID, byte[] DBPWD);//(소스상 정의만하고 사용안함)

	public native int ProcIssCMCPKIDatainLEC(byte IssType, byte[] CMC_PKI);//(소스상 정의만하고 사용안함)
	public native int ProcIssCMCCountsDatainLEC(byte[] countsData1, byte[] countsData2);//(소스상 정의만하고 사용안함)
	public native int ProcReadCMCCountsData(byte[] CMGR_PIN, byte[] outBuf1, byte[] outBuf2);//개표소 카드인지 확인하는 작업할때 사용함.(불필요한 작업으로 예상됨)

	public native int ProcIssCMCRandominLEC(byte[] CMGR_PIN, byte[] RandomData);//난수 발급하는 작업.(불필요한 작업으로 예상)
	public native int ProcReadCMCRandom(byte[] CMGR_PIN, byte[] RandomData);//난수 가져오는 작업.(불필요한 작업으로 예상)
	
//유권자 카드
	public native int ProcIssVTCIntDatainNEC(byte[] VTC_CSN);//ProcReadNECIntData선 실행 후 인증 된 경우만 진행. 시리얼 번호 발급[8](소스상 정의만하고 사용안함)

	public native int ProcIssVTCPKIDatainNEC(byte[] NECCERT, byte[] DNCert);//ProcIssVTCIntDatainNEC 선 실행 후 인증된 경우만 진행. 전자서명값 발급(소스상 정의만하고 사용안함)
	public native int ProcIssVTCPKIDatainLEC(byte[] VoteName, byte[] VoteNameCert);//(소스상 정의만하고 사용안함)
	public native int ProcReadTMCDNData(byte[] TMGR_PIN, byte[] oDNCERT);//관리자카드 input : 비밀번호[8], output :  전자서명값[346](소스상 사용하지 않음)

	public native int ProcReadTMCPKIDatainVK(byte[] TMGR_PIN, byte[] TOSHASH, byte[] TEXEHASH, byte ReadType, byte[] TMC_PKI);//hash코드를 이용한 인증서 읽기 (비밀번호[1]?,OS hash [44], 투표기 실행파일[44], 종류[1](서명용인증서=1,서명용키=2,중앙선관위인증서=3,구시군선관위 인증서=4),output=인증서[1302])(소스상 사용하지 않음)
	public native int ProcReadTMCPKIDatainBK(byte[] TMGR_PIN, byte[] MEXEHASH, byte ReadType, byte[] TMC_PKI);//관리자카드 (비밀번호[1]?보통 8로 되어 있음, 명부기 hash[44], 종류[1](서명용인증서=1,서명용키=2,중앙선관위인증서=3,구시군선관위 인증서=4),output=인증서[1302])(check)
	public native int ProcReadTMCKeyDBData(byte[] TMGR_PIN, byte[] PKey,  byte[] DBID,  byte[] DBPWD);//관리자카드 비밀번호를 통한 output = (privatekey[16]?, DB_ID[50], DB_pw[50])(check)
	public native int ProcReadCMCDNData(byte[] CMGR_PIN, byte[] oDNCERT);//(소스상 사용하지 않음)

	public native int ProcReadCMCPKIDatainCT(byte[] TMGR_PIN, byte[] CEXEHASH, byte pkiType, byte[] CMC_PKI);//개표기 카드 인식(불필요한 소스로 예상)
	public native int ProcReadCMCPKIDatainVF(byte[] CMGR_PIN, byte[] VEXEHASH, byte pkiType, byte[] CMC_PKI);//개표기 카드 인식(소스상 정의만하고 사용안함)
	public native int ProcReadCMCKeyDBData(byte[] CMGR_PIN, byte[] DBID, byte[] DBPWD);//개표기 카드 인식(불필요한 소스로 예상)

	public native int ProcReadVTCDNData(byte[] NECCERT, byte[] oDNCert);//ProcReadTMCDNData 선 실행 후 선거인카드 DN 전자 서명값을 받는다 사용여부 확인? output=(인증서[1302], 전자서명값[346])(소스상 사용하지 않음)(check)
	public native int ProcReadVTCVoteCert(byte[] VoteName, byte[] VoteNameCert);//ProcReadTMCDNData 선 실행 후 선거인카드의 선거이름과 전자서명값을 가져온다 사용여부 확인? output=(선거인이름? 선거이름?[10], 전자서명값[346])(check)

	static {
		System.loadLibrary("miruscard");
	}
}
/**
 * public native int CardOpenSession(int port_no);//USB포트 번호 or 내부에 연결된 스마트 카드 리더기 포트 번호
 * 항상 카드 사용전에 사용해야할 함수.
**/
/**
 * 유권자카드 발급
	public native String CardGetType();카드타입 가져오기 'V'
	public native int CardVerify(String passwd); 카드에 대한 비밀번호 인증
	
	확인하는 작업을 할때 투표일자가 있는지 여부로 투표 여부를 판단.
	투표일자가 있을 경우 투표한 것으로 판단.
	public native int VCardReadBSection(byte[] OutData);//투표구ID[10], 투표소ID[10], 발급일자[14], 선거인그룹[2](가중치), 전자서명[344](명부기 인증서 데이터), 
	 (투표기에서 작성하는 사항이고 발급기에서 null이 아닌 경우 발급한 것으로 간주함)투표기번호[3], 투표일시[14], 전자서명[344](투표기 인증서)
	 *  public native int CardAuthentication();//인증기능 이 작업은 VCard 여부를 확인하는 작업.
	 * 
	 *  투표하지 않은 경우 초기화 작업 한번 진행.
	 *  public native int VCardInitBSection();//발급할 때 초기화 작업. 위의 작업을 실행하지 않으면 가져올 수 없음.
	 *  
	 *  초기화 작업 후 카드 작성.
	 *	관리자 카드 인증서 가지고 있어서 인증서로 암호화 작업 후 전자서명값을 넣음.
	public native int VCardWriteBSection(byte[] InData);//투표구ID[10], 투표소ID[10], 발급일자[14], 선거인그룹[2](가중치), 전자서명[344](명부기 인증서 데이터), 
 	(투표기에서 작성하는 사항이고 발급기에서 null이 아닌 경우 발급한 것으로 간주함)투표기번호[3], 투표일시[14], 전자서명[344](투표기 인증서)
 **/
/**
 * 관리자카드 로그인
 * 	public native String CardGetType(); 카드 타입 가져오기 'T'
 * 	public native int CardVerify(String passwd);
 * 카드에 대한 비밀번호 인증(5회 비밀번호 오류시 카드 사용 불가)
 * 
 * 현재 상태 확인
 * public native int TCardReadBSection(byte[] OutData);
 * 투표ID[10]String, 투표일자[8]"YYYYMMDD", 투표소상태(A:준비,B:개시,C:마감)[1], 개시시각[6]"HHmmSS", 마감시각[6]"HHmmSS", 실행프로그램용 난수[10](빈칸으로 두기)
 * 전부다 빈 값으로 넣어서 투표소 상태와 DB의 투표소 상태를 비교 해서 정상작동인지 여부 확인.
 * 난수값 = 개시값. 
 * 
 * 인증서 가져오는 메소드 실행.
 * public native int ProcReadTMCPKIDatainBK(byte[] TMGR_PIN, byte[] MEXEHASH, byte ReadType, byte[] TMC_PKI); 
 * 관리자카드 (비밀번호[8], 명부기 hash[44], 종류[1](서명용인증서=1,서명용키=2,중앙선관위인증서=3,구시군선관위 인증서=4),output=인증서[1302])
 * 중앙선관위 인증서 = 3을 가져와야 함. 인증서를 가지고 암호화 작동해야함.
 * 
 * DB암호 읽기
 * public native int ProcReadTMCKeyDBData(byte[] TMGR_PIN, byte[] PKey,  byte[] DBID,  byte[] DBPWD);
 * 관리자카드 비밀번호[8]를 통한 output = (privatekey 지문과 서명 암호키[16], oracle PW[50], sybase PW[50])(check)
 * 
 **/
/**
 * 
 * 투표소 ID와 투표일자 가져오는 함수
 * public native int ProcReadTMCPollsData(byte[] TMGR_PIN, byte[] oPollsID, byte[] oVoteDate);
 * 비밀번호[8]을 입력하면 투표소 ID와 날짜를 반환.
 * 
 **/
/**
 * 
 * public native int TCardWriteTPGData(byte[] InData);
 * 투표소상태(A:준비,B:개시,C:마감)[1], 개시시각[6], 마감시각[6] 작성.
 * 
 **/
/**
 * 개시
 * 개시 버튼 클릭
 * 
 * 카드 여부 확인 후 팝업 창 띄움(관리자카드 삽입 확인, 취소)
 * DB에서 개시 상태 검사(Q52)
 * 투표소 값을 A인 경우에만 정상 작동. 만약 B인 상태면 DB와 맞지 않는 상태이고 이미 개시한 상태라는 팝업을 띄움 C인 상태는 마감상태로 이미 투표가 마감되었다는 팝업 띄움
 * 
 * 화면 UI 흐름창 표시
 * 
 * OpenCardSession(포트번호) 이 부분은 모든 카드 작성, 읽기에서 사용하는 함수로 카드 읽기전 선 실행되어야 함.
 * 
 * CardGetType()이 T카드가 아니거나 VerifyCard(mvTPassword)가 false일 경우 개시 불가 관리자카드 필요 팝업 띄움
 * 
 * 로그인시에 넣은 관리자카드의 투표소 ID(변수로 가지고 있음)와 현재 들어간 관리자카드의 투표소 ID 값 비교.TCardReadBSection(outbyte[51])
 * ID값이 다르면 다른 투표소의 관리자 카드일 수 있음으로 안내 팝업 띄움
 * 카드의 상태 값 조회해서 A이거나 빈 값이면 정상으로 진행하고 B나 C인 경우 DB에서 상태 검사한 것과 동일한 값을 띄움. 
 *  
 * DB입력 하기전 발급자수가 있을 경우 강제 실행 여부 확인 해야함.(Q53)
 * 
 * 입력 팝업창 띄움
 * 
 * 입력받은 투표기 또는 명부기 댓수가 0일경우 개시를 시작할 수 없기 때문에 오류 팝업창 띄움
 * DB반영 작업 (Q54)
 * 
 * 난수값 생성 개시값이 다른 경우 
 * Dim rndValue As String = ""
                rndValue += fGetRandomValue(999).ToString().PadLeft(3, "0"c)
                rndValue += fGetRandomValue(999).ToString().PadLeft(3, "0"c)
                rndValue += fGetRandomValue(999).ToString().PadLeft(3, "0"c)
                rndValue += fGetRandomValue(9).ToString()
                10자리 String으로 된 숫자 난수 생성 함수.
 * 
 * TCardWriteVoteRightRandom(위에서 생성한 10자리 char 10byte) 난수 입력 작업.
 * 입력완료 되면 난수값 변수로 보관(개시값)
 * TCardWriteTPGData(byte[13] InData)
 * 투표소상태(A:준비,B:개시,C:마감)[1], 개시시각[6], 마감시각[6] 작성.
 * 투표소 상태를 B로 변경하고 개시시각만 HHMMSS형식으로 6바이트만 넣어서 작성. 마감시각은 마감시에.
 * 
 * DB commit.
**/