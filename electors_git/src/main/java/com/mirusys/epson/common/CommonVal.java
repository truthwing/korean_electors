package com.mirusys.epson.common;

import com.mirusys.epson.common.type.BallotType;
import com.mirusys.epson.common.type.ColorType;
import com.mirusys.epson.dto.Candidate;

import android.graphics.Typeface;

public class CommonVal {
	public static boolean doPrint = false;
	public static final int BALLOT_TYPE0 = 0;
	public static final int BALLOT_TYPE1 = 1;
	public static final int BALLOT_TYPE2 = 2;
	public static final int BALLOT_TYPE3 = 3;
	public static final int BALLOT_TYPE4 = 4;
	public static final int BALLOT_TYPE5 = 5;
	
	public static final long[] lBallotLength = new long[]
		{
			0,		155,	155,	155,	155,	180,	205,	216,	239,	
			262,	285,  	308,	240,	255,	270,	285,	300,	315,	
			296,	309,	322,	335,	348,	361,	374,	387,	400,	
			413,	426,	439,	452
		};
	
	public static final long[] Cut50TextPosX = { 4, 16, 28 };
	public static final long[] Cut50TextPosY = { 18, 13, 8 };
	public static final long[] Cut55TextPosX = { 4, 16, 28 };
	public static final long[] Cut55TextPosY = { 18, 13, 9 };

	public static final long[] CutEduTextPosX = { 16, 28, 40 };
	public static final long[] CutEduTextPosY = { 18, 13, 9 };
	
	public static final long[] CutH30TextPosX = { 4, 16, 28 };
	public static final long[] CutH30TextPosY = { 27, 20, 13 };
	
	
	
	
	
	public static final String[] titlesForDemo = {
												"국회의원선거투표", "비례대표국회의원선거투표", "비례대표시도의원선거" ,
												"지역구시도의원선거", "구시군의장선거", "비례대표구시군의원선거", 
												"지역구구시군의원선거"
											};
	public static final String[] subTitlesForDemo = {
		"(ㅇㅇㅇ선거구)", "", "" ,
		"", "", "", 
		""
	};
	public static final int[] ballotColorForDemo = {
			ColorType.BOX_COLOR1, ColorType.BOX_COLOR2, ColorType.BOX_COLOR3, 
			ColorType.BOX_COLOR4, ColorType.BOX_COLOR5, ColorType.BOX_COLOR6, ColorType.BOX_COLOR1,
			};
	
	public static final int[] ballotTypeForDemo = {
					BallotType.BALLOT_TYPE0, BallotType.BALLOT_TYPE1, 
					BallotType.BALLOT_TYPE4, BallotType.BALLOT_TYPE12,
					BallotType.BALLOT_TYPE0, BallotType.BALLOT_TYPE1,
					BallotType.BALLOT_TYPE0,
					};
	
	public static final Candidate[] yesNoForTest = new Candidate[]{ 
		new Candidate("", "", "찬 성", "", "", ""),
		new Candidate("", "", "반 대", "", "", ""),
		};
	
	public static final Candidate[] candidateForTest = new Candidate[]{ 
		new Candidate("1", "갑당", "백두산", "", "", ""),
		new Candidate("2", "을당", "한라산", "", "", ""),
		new Candidate("3", "병당", "금강산", "", "", ""),
		new Candidate("4", "정당", "단군", "", "", ""),
		new Candidate("5", "자축당", "김유신", "", "", ""),
		new Candidate("6", "인묘당", "이인로", "", "", ""),
		new Candidate("7", "진사당", "김부식", "", "", ""),
		new Candidate("8", "오미당", "정몽주", "", "", ""),
		new Candidate("9", "신유당", "황희", "", "", ""),
		new Candidate("10", "술해당", "신사임당", "", "", ""),
		new Candidate("11", "백호당", "이순신", "", "", ""),
		new Candidate("12", "하나당", "세종대왕", "", "", ""),
		new Candidate("13", "둘리당", "홍길동", "", "", ""),
		new Candidate("14", "셋삼당", "임꺽정", "", "", ""),
		new Candidate("15", "넷사당", "한석봉", "", "", ""),
		new Candidate("16", "다섯당", "전봉준", "", "", ""),
		new Candidate("17", "여섯당", "김홍도", "", "", ""),
		new Candidate("18", "일곱당", "정약용", "", "", ""),
		new Candidate("19", "여덜당", "윤동주", "", "", ""),
		new Candidate("20", "아홉당", "안중근", "", "", "")
		};
	public static Typeface typefaceBatang;
	public static Typeface typefaceMalgun;
	public static Typeface typefaceH2HDRM;
	
}
