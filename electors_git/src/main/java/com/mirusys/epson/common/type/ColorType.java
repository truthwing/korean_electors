package com.mirusys.epson.common.type;

import android.graphics.Color;

public final class ColorType {
	public static final int BOX_COLOR1 = Color.rgb(255, 255, 255);	//흰색
	public static final int BOX_COLOR2 = Color.rgb(230, 250, 230);	//연두색
	public static final int BOX_COLOR3 = Color.rgb(230, 230, 230);	//계란색
	public static final int BOX_COLOR4 = Color.rgb(230, 250, 250);	//청회색
	public static final int BOX_COLOR5 = Color.rgb(250, 230, 200);	//하늘색
	public static final int BOX_COLOR6 = Color.rgb(250, 250, 230);	//연미색
}
