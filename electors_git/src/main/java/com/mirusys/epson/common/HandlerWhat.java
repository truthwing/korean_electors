package com.mirusys.epson.common;

public class HandlerWhat {
	public static final int PRINT_START = 11;
	public static final int PRINT_STOP = 12;
	public static final int STATUS_INK_REMAIN = 14;
	public static final int STATUS_PRINT_STATUS = 15;
	public static final int PROGRESS_START = 16;
	public static final int PROGRESS_WORKING = 17;
	public static final int PROGRESS_STOP = 18;
	public static final int TOAST_MSG_OUTPUT = 19;
	
}
