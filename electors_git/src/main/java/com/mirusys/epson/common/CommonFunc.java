package com.mirusys.epson.common;

import android.util.Log;

public class CommonFunc {
	public static int MM2Pixel(int x_mm, int Resolution)
	{
		return (int)((float)x_mm/25.4f*Resolution);
	}
	
	public static int MM2PixelX(long x_mm, int Resolution){
		Log.i("MM2", "x_mm = " + x_mm + ", Resolution = " + Resolution);
		Log.i("MM2", "(int)((float)x_mm / 25.4f * Resolution) = " + ((int)((float)x_mm / 25.4f * Resolution)));
		return (int)((float)x_mm / 25.4f * Resolution);
	}
	
	public static int MM2PixelY(long x_mm, int Resolution)
	{
		return (int)((float)x_mm /25.4f * Resolution);
	}


	public static int Pixel2MM(long x_px, int Resolution)
	{
		return (int)((float)x_px * 25.4f / Resolution);
	}
	
}
