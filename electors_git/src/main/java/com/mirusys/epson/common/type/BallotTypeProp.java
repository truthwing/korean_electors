package com.mirusys.epson.common.type;

public final class BallotTypeProp {
	public final static String BALLOT_TYPE_1_2 			= "ballotSet1_2.properties";
	public final static String BALLOT_TYPE_1_3 			= "ballotSet1_3.properties";
	public final static String BALLOT_TYPE_1_6			= "ballotSet1_6.properties";
	public final static String BALLOT_TYPE_1_7to12  	= "ballotSet1_7-12.properties";
	public final static String BALLOT_TYPE_1_13 		= "ballotSet1_13.properties";
	public final static String BALLOT_TYPE_1_18over 	= "ballotSet1_18over.properties";
	
	public final static String BALLOT_TYPE_2_2 			= "ballotSet2_2.properties";
	public final static String BALLOT_TYPE_2_3 			= "ballotSet2_3.properties";
	public final static String BALLOT_TYPE_2_6 			= "ballotSet2_6.properties";
	public final static String BALLOT_TYPE_2_7 			= "ballotSet2_7-12.properties";
	public final static String BALLOT_TYPE_2_13 		= "ballotSet2_13.properties";
	public final static String BALLOT_TYPE_2_18over 	= "ballotSet2_18over.properties";
	
	public final static String BALLOT_TYPE_4 			= "ballotSet4.properties";
	public final static String BALLOT_TYPE_5 			= "ballotSet5.properties";
	
	public final static String BALLOT_TYPE_6_2 			= "ballotSet6_2.properties";
	public final static String BALLOT_TYPE_6_3 			= "ballotSet6_3.properties";
	public final static String BALLOT_TYPE_6_4 			= "ballotSet6_4.properties";
	public final static String BALLOT_TYPE_6_5 			= "ballotSet6_5.properties";
	public final static String BALLOT_TYPE_6_6 			= "ballotSet6_6.properties";
	public final static String BALLOT_TYPE_6_6_BELOW 	= "ballotSet6_6_below.properties";
	public final static String BALLOT_TYPE_6_7_12 		= "ballotSet6_7-12.properties";
	public final static String BALLOT_TYPE_6_7 			= "ballotSet6_7.properties";
	public final static String BALLOT_TYPE_6_8 			= "ballotSet6_8.properties";
	public final static String BALLOT_TYPE_6_13 		= "ballotSet6_13.properties";
	
	public final static String BALLOT_TYPE_LABEL 	    = "";
	public final static String BALLOT_SLEEPSHEET		= "";
}
