package com.mirusys.epson.common.type;

public enum AlignType {
	CENTER, BOTH, AUTO, AUTO_LEFT, AUTO_RIGHT, RIGHT, CENTER_TOP, ELLIPSIS_TOP, MULTILINE, ELLIPSIS, LEFT
	
	
}
