package com.mirusys.epson.common;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

import com.mirusys.epson.common.type.AlignType;
import com.mirusys.epson.common.type.BallotType;
import com.mirusys.epson.common.type.BallotTypeProp;
import com.mirusys.epson.dto.Ballot;
import com.mirusys.epson.dto.Paper;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.Log;

public class CommonMethod {
	
	//백그라운드 투명화 작업
	public static Bitmap getBitmapWithTransparentBG(Bitmap srcBitmap, int fromBgColor, int toBgColor) {
		int width = srcBitmap.getWidth();
		int height = srcBitmap.getHeight();
		
		Bitmap finalBitmap = srcBitmap.copy(Bitmap.Config.ARGB_8888, true);
		
		for(int x = 0; x < width; x++)
		{
		    for(int y = 0; y < height; y++)
		    {
		    	int pixelColor = srcBitmap.getPixel(x, y);
		    	
		    	
		        if(pixelColor == fromBgColor)
		        {
		            finalBitmap.setPixel(x, y, toBgColor);
		        }
		        else
		        {
		            finalBitmap.setPixel(x, y, srcBitmap.getPixel(x, y));
		        }
		    }
		}

		
		return finalBitmap;
	}
	
	//투명화된 백그라운드 가장자리 제거 작업
	public static Bitmap getRemovedMargin(Bitmap bitmap){
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		Rect cuttedRect = new Rect();
		
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if(bitmap.getPixel(x, y) != Color.WHITE){
					cuttedRect.left = x;
					cuttedRect.top = y;
					Log.i("cut", "left = " + x + " top = " + y);
					break;
				}
			}
			if(cuttedRect.left != 0 || cuttedRect.top != 0){
				break;
			}
		}
		
		for (int x = width-1; x >= 0; x--) {
			for (int y = height-1; y >= 0; y--) {
				Log.i("cut", "x = " + x + " y = " + y);
				if(bitmap.getPixel(x, y) != Color.WHITE){
					cuttedRect.right = x;
					cuttedRect.bottom = y;
					Log.i("cut", "right = " + x + " bottom = " + y);
					break;
				}
			}
			if(cuttedRect.right != 0 || cuttedRect.bottom != 0){
				break;
			}

		}
		cuttedRect.right = bitmap.getWidth() - cuttedRect.left;
		cuttedRect.bottom = bitmap.getHeight() - cuttedRect.top;
		
		Bitmap cuttedBitmap = Bitmap.createBitmap(bitmap, cuttedRect.left, cuttedRect.top, cuttedRect.width(), cuttedRect.height());
		
		return cuttedBitmap;
	}
	
	//후보자 수와 선거타입으로 Properties의 파일명을 얻는다.
	public static String getPropertiesNameByNumber(int ballotType, int ballotNumber){
		String ballotTypeProp = "";
		
		switch(ballotType){
		case BallotType.BALLOT_TYPE0 :
			if(ballotNumber == 1 || ballotNumber == 2){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_1_2;
			}else if(ballotNumber == 3){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_1_3;
			}else if(ballotNumber <= 6){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_1_6;
			}else if(ballotNumber < 12){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_1_7to12;
			}else if(ballotNumber <= 17){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_1_13;
			}else{
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_1_18over;
			}
			break;
		case BallotType.BALLOT_TYPE1 :
			if(ballotNumber == 1 || ballotNumber == 2){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_2_2;
			}else if(ballotNumber == 3){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_2_3;
			}else if(ballotNumber <= 6){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_2_6;
			}else if(ballotNumber < 12){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_2_7;
			}else if(ballotNumber <= 17){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_2_13;
			}else{
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_2_18over;
			}
			break;
		case BallotType.BALLOT_TYPE4 :
			ballotTypeProp = BallotTypeProp.BALLOT_TYPE_4;
			break;
		case BallotType.BALLOT_TYPE12 :
			if(ballotNumber == 2){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_6_2;
			}else if(ballotNumber == 3){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_6_3;
			}else if(ballotNumber == 4){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_6_4;
			}else if(ballotNumber == 5){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_6_5;
			}else if(ballotNumber == 6){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_6_6;
			}else if(ballotNumber >= 7 && ballotNumber <= 12){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_6_8;
			}else if(ballotNumber >= 13){
				ballotTypeProp = BallotTypeProp.BALLOT_TYPE_6_8;
			}
			break;
		case BallotType.BALLOT_TYPE_LABEL :
			
			break;
		}
		
		return ballotTypeProp;
	}
	
	public static Bitmap setBallotLength(int ballotType){
		
		/*****************선거타입에 따른 투표용지 길이 설정*********************/
		int width = 100;
		int height = 155;
		
		if(ballotType == BallotType.BALLOT_TYPE4){	//주민소환투표
			height = 165;
		}else if(ballotType == BallotType.BALLOT_TYPE12){	//교육감
			if(Ballot.m_HBJNum > 7){
				height = 160 + ( (Ballot.m_HBJNum-8) * 15 );
			}else{
				height = 160;
			}
		}else if(ballotType == BallotType.BALLOT_TYPE_LABEL){	//라벨
			height = 100;
		}else{
			height = (int)CommonVal.lBallotLength[Ballot.m_HBJNum];
		}
		/*************************************************************/
		
		int w = (int) ((width - 4) * 180 / 25.4f);
		int h = (int) ((height - 4) * 180 / 25.4f);
		
		Bitmap ballotBitmap = Bitmap.createBitmap(w, h, Config.ARGB_8888);
		
		return ballotBitmap;
	}
	
	public static void drawRectForTest(Canvas canvas, Rect rect){
			Paint paint = new Paint();
			paint.setColor(Color.RED);
			paint.setStyle(Style.STROKE);
			canvas.drawRect(rect, paint);
	}

	//파일을 복사하는 메소드
	public static void fileCopy(String inFileName, String outFileName) {
		try {
			FileInputStream fis = new FileInputStream(inFileName);
			FileOutputStream fos = new FileOutputStream(outFileName);

			int data = 0;
			while((data=fis.read())!=-1) {
				fos.write(data);
			}
			fis.close();
			fos.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static int getFontSizeWidth(long left, long top, long right, long bottom, int fontSize, AlignType fontAlign, boolean isBold, String szText){
		int[] dataType = new int[szText.length()];
		int dataSetNum = 0;
		int iCharSize = 0;
		int iSpace = 0;
		float fSpace;
		int i2Byte = 0, i1Byte = 0;
		int iPixSpace;
		int iWFntSize = 0;
		int loop = 0;
		
		Paint paint = new Paint();
		
		Rect rect = new Rect();

		rect.set(
				CommonFunc.MM2PixelX(left + Ballot.m_nMargin, Paper.m_dpiX) + 5 + Ballot.m_FloatMargin, 
				CommonFunc.MM2PixelY(top, Paper.m_dpiY),
				CommonFunc.MM2PixelX(right - Ballot.m_nMargin, Paper.m_dpiX) - 5 + Ballot.m_FloatMargin,
				CommonFunc.MM2PixelY(bottom, Paper.m_dpiY)
				);
		
		Log.i("getFontSizeWidth", "left = " + left + ", right = " + right + ", m_nMargin = " + Ballot.m_nMargin + ", m_dpiX = " + Paper.m_dpiX);
		paint.setTypeface(CommonVal.typefaceBatang);
		
		if(isBold){
			paint.setStyle(Style.FILL_AND_STROKE);
			paint.setStrokeWidth(0.7f);
		}
		
		paint.setTextSize(fontSize * Paper.m_dpiY / 72);
		paint.setTextLocale(Locale.KOREA);
		paint.setTextAlign(Align.LEFT);
		
		if(fontAlign == AlignType.BOTH || fontAlign == AlignType.AUTO || fontAlign == AlignType.AUTO_LEFT || fontAlign == AlignType.AUTO_RIGHT){
			
			for (int i = 0; i < szText.length(); i++) {
				if (szText.charAt(i) > 127) {
					dataType[i] = 2;
					i2Byte += 1;
					dataSetNum++;
					Log.i("charAt[" + i + "]", "if :: " + szText.charAt(i) + " | type size = " + 2);
				} else if (szText.charAt(i) >= '0' && szText.charAt(i) <= '9') {
					dataType[i] = 1;
					i1Byte += 1;
					dataSetNum++;
					Log.i("charAt[" + i + "]", "else if :: " + szText.charAt(i) + " | type size = " + 1);
				} else {
					dataType[i] = 1;
					i1Byte += 1;
					dataSetNum++;
					Log.i("charAt[" + i + "]", "else :: " + szText.charAt(i) + " | type size = " + 1);
				}
			}// end for

			if(dataSetNum <= 1) return iWFntSize = 0;
			if(dataSetNum <= 2 && i2Byte == 0 && right - left <= 20) return iWFntSize = 0;
			
			float tWidth = paint.measureText(szText); //150908 :: Windows와 맞추기 위해 폰트크기가 18 이상이면 폰트 하나당 1을 더해야 한다?
			
			iCharSize = (int)(tWidth / (i2Byte * 2 + i1Byte));
			
			if( (i2Byte + i1Byte - 1) != 0 ){
				fSpace = ( (float)(rect.width() - tWidth) ) / ( (float)(i2Byte + i1Byte - 1) );
				iSpace = (int)fSpace;
			}
			
		
			iPixSpace = (int)(iSpace * 72) / Ballot.m_dpiY;
			
			iWFntSize = (int)((fontSize + iPixSpace) / 2) + 1;
			
			
			Log.i("FontSizeWidth", "iCharSize = " + iCharSize + ", size.cx = " + tWidth + ", rc.width = " + rect.width() + ", i2Byte = " + i2Byte + ", i1Byte = " + i1Byte + ", iSpace = " + iSpace + ", iWFntSize = " + iWFntSize + ", iFntSize = " + fontSize + ", iPixSpace = " + iPixSpace + ", szText = " + szText);
			Paint originPaint = paint;
			
			while(iSpace < -14 && loop < 5){
				paint = originPaint;
				
				iWFntSize -= loop;
				
				float scaleX = (float)((float)iWFntSize / (float)fontSize * 2f);
				Log.i("FontSizeWidth", "scaleX = " + scaleX);
				paint.setTextScaleX(scaleX);
				
				iCharSize = (int)((paint.measureText(szText) - (fontSize >= 18 ? szText.length() : 0) ) / (i2Byte * 2 + i1Byte) );
				
				tWidth = (paint.measureText(szText) - (fontSize >= 18 ? szText.length() : 0) );
				fSpace = ( (float)(rect.width() - tWidth) ) / ((float)(i2Byte + i1Byte - 1) );
				iSpace = (int)fSpace;
				iPixSpace = (int)(iSpace * 72f / Ballot.m_dpiY);
				Log.i("FontSizeWidth[다시계산]", "iCharSize = " + iCharSize + ", size.cx = " + tWidth + ", rc.width = " + rect.width() + ", i2Byte = " + i2Byte + ", i1Byte = " + i1Byte + ", iSpace = " + iSpace + ", iWFntSize = " + iWFntSize + ", iFntSize = " + fontSize + ", iPixSpace = " + iPixSpace + ", szText = " + szText);
				loop++;
			}
			return iWFntSize;
		}else{
			return iWFntSize = 0;
		}
	}
}
