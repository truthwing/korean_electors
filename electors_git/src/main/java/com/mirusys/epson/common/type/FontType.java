package com.mirusys.epson.common.type;

public class FontType {
	public static final int BATANG = 0;
	public static final int MALGUN = 1;
	public static final int H2HDRM = 2;
}
