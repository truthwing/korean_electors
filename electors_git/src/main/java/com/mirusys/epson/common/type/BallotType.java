package com.mirusys.epson.common.type;

public final class BallotType {
	public static final int BALLOT_TYPE0 = 0;
	public static final int BALLOT_TYPE1 = 1;
	public static final int BALLOT_TYPE4 = 2;
	public static final int BALLOT_TYPE12 = 3;
	public static final int BALLOT_TYPE_LABEL = 4;
}
