package com.mirusys.epson.draw;

import java.util.Locale;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.mirusys.epson.common.CommonFunc;
import com.mirusys.epson.common.CommonMethod;
import com.mirusys.epson.common.CommonVal;
import com.mirusys.epson.common.type.AlignType;
import com.mirusys.epson.common.type.FontType;
import com.mirusys.epson.dto.Ballot;
import com.mirusys.epson.dto.Paper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.Log;

public class DrawCommon {
	public static boolean isTest = false;
	public static boolean isAntialias = false;
	public final String TAG = "DrawCommon";
	Canvas canvas;
	Context context;
	private Typeface mFontTypeface = null;
	private int mFontType = -1;
	
	public DrawCommon(Context context, Canvas canvas) {
		this.context = context;
		this.canvas = canvas;
		
		if(CommonVal.typefaceBatang == null) CommonVal.typefaceBatang = Typeface.createFromAsset(context.getAssets(), "fonts/nBatang_0.TTF");  
		if(CommonVal.typefaceMalgun == null) CommonVal.typefaceMalgun = Typeface.createFromAsset(context.getAssets(), "fonts/malgun.ttf");
		if(CommonVal.typefaceH2HDRM == null) CommonVal.typefaceH2HDRM = Typeface.createFromAsset(context.getAssets(), "fonts/H2HDRM.TTF");
			
	}

	public void drawLine(long left, long top, long right, long bottom, Style lineType, int strokeWidth, int color) {
		Rect rect = new Rect();
		rect.set(
				CommonFunc.MM2PixelX(left, Paper.m_dpiX) + Paper.m_FloatMargin,
				CommonFunc.MM2PixelY(top, Paper.m_dpiY),
				CommonFunc.MM2PixelX(right, Paper.m_dpiX) + Paper.m_FloatMargin,
				CommonFunc.MM2PixelY(bottom, Paper.m_dpiY));

		Paint paint = new Paint();
		paint.setStrokeWidth(strokeWidth);
		paint.setColor(color);
		paint.setStyle(lineType);

		if (strokeWidth == 1) {
			paint.setPathEffect(new DashPathEffect(new float[] { 20, 5 }, 0));
		}

		Path path = new Path();

		path.moveTo(rect.left, rect.top);
		path.lineTo(rect.right, rect.bottom);

		canvas.drawPath(path, paint);

//		if (strokeWidth == 4) {
//			Log.i("DrawLine", "strokeWidth == 4");
//			Log.i("DrawLine", "left = " + rect.left + " top = " + rect.top
//					+ " right = " + rect.right + " bottom = " + rect.bottom);
//		}
	}

	public void drawText(long left, long top, long right, long bottom,
			int fontSize, AlignType fontAlign, boolean isBold, String szText, int fontSizeWidth,
			int fontType, boolean isRotate, int color) {
		
		Rect rect = new Rect();

		rect.set(
				CommonFunc.MM2PixelX(left, Paper.m_dpiX), 
				CommonFunc.MM2PixelY(top, Paper.m_dpiY),
				CommonFunc.MM2PixelX(right, Paper.m_dpiX),
				CommonFunc.MM2PixelY(bottom, Paper.m_dpiY)
				);

		Log.i(TAG, "RECT :: left = " + rect.left + ", top = " + rect.top + ", right = " + rect.right + ", bottom = " + rect.bottom);
		
		Paint fontPaint = new Paint();

		if(mFontType == -1 || mFontType != fontType) mFontType = fontType;
		
		switch (fontType) {
			
		case FontType.BATANG:	//바탕체
			fontPaint.setTypeface(CommonVal.typefaceBatang);
			break;
		case FontType.MALGUN:	//맑은고딕
			fontPaint.setTypeface(CommonVal.typefaceMalgun);
			break;
		default:	//기본값
			fontPaint.setTypeface(CommonVal.typefaceBatang);
			break;
		}
		
		if (isBold) {
			fontPaint.setStyle(Style.FILL_AND_STROKE);
			fontPaint.setStrokeWidth(0.65f);
		}

		fontPaint.setAntiAlias(DrawCommon.isAntialias);
		fontPaint.setTextSize(fontSize * Paper.m_dpiY / 72);

		if (fontType == FontType.MALGUN) {	//라벨일 경우에 글자 크기를 줄인다.
			fontPaint.setTextSize(fontPaint.getTextSize() * 0.7f);
		}

		fontPaint.setTextLocale(Locale.KOREA);
		fontPaint.setColor(color);
		fontPaint.setTextAlign(Align.LEFT);

		
		float textHalf = fontPaint.getTextSize() / 2;
		float rectCenterX = rect.centerX();
		float rectCenterY = rect.centerY() + textHalf / 2;

		float textWidth = fontPaint.measureText(szText);
		float textSize = fontPaint.getTextSize();
		float centerX = rect.centerX();
		float centerY = rect.centerY();

		float fontX = centerX - (textWidth / 2);
		float fontY = centerY + (textSize / 3);

		/****************** 글자가 겹칠경우 자간 줄임 ***********************/
		if(fontType != FontType.MALGUN){
			Rect textBounds = new Rect();
			fontPaint.getTextBounds(szText, 0, szText.length(), textBounds);
			float scaleX = 1.0f;
			float fSize = fontSize;

			if(!isRotate){	//세로형 투표용지(일반 투표)

//				if (textBounds.width() > rect.width()) {
//					while (textBounds.width() > rect.width()) {
//						scaleX -= 0.01f;
//						fontPaint.setTextScaleX(scaleX);
//						fontPaint.getTextBounds(szText, 0, szText.length(), textBounds);
//					}
//				}
				
			}else if(isRotate){	//가로형 투표용지(교육감)

				if (textBounds.width() > rect.width()) {
					while (textBounds.width() > rect.width()) {
						fSize -= 0.5;
						fontPaint.setTextSize(fSize * Paper.m_dpiY / 72);
						fontPaint.getTextBounds(szText, 0, szText.length(), textBounds);
					}
				}
			}
		}
		/**********************************************************/
		
		if (fontAlign == AlignType.CENTER) {
			if (isRotate) {
				canvas.save();
				canvas.rotate(-90, rect.centerX(), rect.centerY());
				canvas.drawText(szText, fontX, fontY, fontPaint);
				canvas.restore();
			} else {
				canvas.drawText(szText, fontX, fontY, fontPaint);
			}

			if (isTest) CommonMethod.drawRectForTest(canvas, rect);

		} else if (fontAlign == AlignType.BOTH) {
			commonDrawText(rect, fontX, fontY, textWidth, fontAlign, szText, fontSizeWidth, isRotate, fontSize, fontPaint);
		} else if (fontAlign == AlignType.AUTO) {
			commonDrawText(rect, fontX, fontY, textWidth, fontAlign, szText, fontSizeWidth, isRotate, fontSize,  fontPaint);
		} else if (fontAlign == AlignType.AUTO_LEFT) {
			commonDrawText(rect, fontX, fontY, textWidth, fontAlign, szText, fontSizeWidth, isRotate, fontSize,  fontPaint);
		} else if (fontAlign == AlignType.AUTO_RIGHT) {
			commonDrawText(rect, fontX, fontY, textWidth, fontAlign, szText, fontSizeWidth, isRotate, fontSize,  fontPaint);
		} else if (fontAlign == AlignType.RIGHT) {
			fontPaint.setTextAlign(Align.RIGHT);
			canvas.drawText(szText, rectCenterX, rectCenterY, fontPaint);
		} else if (fontAlign == AlignType.CENTER_TOP) {
			fontPaint.setTextAlign(Align.CENTER);
			canvas.drawText(szText, rectCenterX, rectCenterY, fontPaint);
		} else if (fontAlign == AlignType.ELLIPSIS_TOP) {
			fontPaint.setTextAlign(Align.CENTER);
			canvas.drawText(szText, rectCenterX, rectCenterY, fontPaint);
		} else if (fontAlign == AlignType.MULTILINE) {
			int rLeft = rect.left;
			int rTop = rect.top;

			rTop += textSize;

			for (String line : szText.split("\n")) {
				canvas.drawText(line, rLeft, rTop, fontPaint);
				rTop += -fontPaint.ascent() + fontPaint.descent();
			}

			if (isTest) CommonMethod.drawRectForTest(canvas, rect);

		} else if (fontAlign == AlignType.ELLIPSIS) {
			fontPaint.setTextAlign(Align.CENTER);
			canvas.drawText(szText, rectCenterX, rectCenterY, fontPaint);
		} else {

			canvas.drawText(szText, rect.left, fontY, fontPaint);

			if (isTest) CommonMethod.drawRectForTest(canvas, rect);
		}
		
	}

	public Paint commonDrawText(Rect rect, float fontX, float fontY, float textWidth, AlignType fontAlign, 
								String szText, int fontSizeWidth, boolean isRotate, int fontSize, Paint fontPaint) {
		
		int[] dataType = new int[szText.length()];
		int i2Byte = 0, i1Byte = 0;
		int dataSetNum = 0;
		float fSpace;
		int iSpace = 0;
		int tmpSize = 0;
		int iCharSize = 0;
		int iPixSpace = 0;
		int iWFntSize = 0;
		int loop = 0;
		
		for (int i = 0; i < szText.length(); i++) {
			if (szText.charAt(i) > 127) {
				dataType[i] = 2;
				i2Byte += 1;
				dataSetNum++;
				Log.i("charAt[" + i + "]", "if :: " + szText.charAt(i) + " | type size = " + 2);
			} else if (szText.charAt(i) >= '0' && szText.charAt(i) <= '9') {
				dataType[i] = 1;
				i1Byte += 1;
				dataSetNum++;
				Log.i("charAt[" + i + "]", "else if :: " + szText.charAt(i) + " | type size = " + 1);
			} else {
				dataType[i] = 1;
				i1Byte += 1;
				dataSetNum++;
				Log.i("charAt[" + i + "]", "else :: " + szText.charAt(i) + " | type size = " + 1);
			}
		}// end for

		if (dataSetNum <= 1) {
			canvas.drawText(szText, fontX, fontY, fontPaint);

			if (isTest) CommonMethod.drawRectForTest(canvas, rect);

			return null;
		}

		if (dataSetNum < 2 && i2Byte == 0 && rect.right - rect.left <= 20) {

			canvas.drawText(szText, fontX, fontY, fontPaint);
			
			return null;
		}

		if(fontSizeWidth != 0){
			
			float scaleX = (float)((float)fontSizeWidth / (float)fontSize * 2f);
			
			fontPaint.setTextScaleX(scaleX);
			
			Log.i(TAG, "fontSizeWidth != 0 :: fontSizeWidth = " + fontSizeWidth + ", fontSize = " + fontSize + ", scaleX = " + scaleX + ", szText = " + szText);
			
			float fWidth = fontPaint.measureText(szText) - fontSize > 18 ? szText.length() : 0;
			Log.i(TAG, "size.cx = " + fWidth + ", size.cy = " + fontPaint.getTextSize() + "szText = " + szText);
			Log.i(TAG, "fontSizeWidth = " + fontSizeWidth);
			
			iCharSize = (int)(fWidth / (i2Byte * 2 + i1Byte) );
			fSpace = (float)(rect.width() - fWidth) / (float)(i2Byte + i1Byte - 1);
			iSpace = (int)fSpace;
			iPixSpace = (int)(iSpace * 72 / Ballot.m_dpiY);
			
		}else{
			
//			float textX = fontPaint.measureText(szText);
			float textX = fontPaint.measureText(szText) + (fontSize >= 18 ? szText.length() : 0); //Windows와 맞추기 위해 폰트크기가 18 이상이면 폰트 하나당 1을 더해야 한다?
			Log.i(TAG, "font width = " + textX);
			
			iCharSize = (int)(textX / (i2Byte * 2 + i1Byte));
			
			if( (i2Byte + i1Byte - 1) != 0 ){
				fSpace = ( (float)(rect.width() - textX) ) / ( (float)(i2Byte + i1Byte - 1) );
				iSpace = (int)fSpace;
				tmpSize = (rect.width() - (int)textX );
				Log.i(TAG, "tmpSize = " + tmpSize);
			}
			
			iPixSpace = (int)(iSpace * 72 / Ballot.m_dpiY);
			
			
			iWFntSize = (int)( (fontSize + iPixSpace) / 2f ) + 1;
			
			
			Log.i(TAG, "iCharSize = " + iCharSize + ", size.cx = " + textX + ", rc.width = " + rect.width() + ", i2Byte = " + i2Byte + ", i1Byte = " + i1Byte + ", iSpace = " + iSpace + ", iWFntSize = " + iWFntSize + ", iPixSpace = " + iPixSpace);
			float scaleX = 1.0f;
			
			Paint originPaint = fontPaint;
			
			while(iSpace < -14 && loop < 10){
				
				fontPaint = originPaint;
				
				iWFntSize -= loop;
				
				scaleX = (float)((float)iWFntSize / (float)fontSize * 2f);
				
				Log.i(TAG, "iWFntSize = " + iWFntSize + ", scaleX = " + scaleX);
				
				fontPaint.setTextScaleX(scaleX);
				
				iCharSize = (int)((fontPaint.measureText(szText) + ((fontPaint.getTextSize()/Ballot.m_dpiY*72f) >= 18 ? szText.length() : 0)) / (i2Byte * 2 + 1 + i1Byte) );
				
				textX = (fontPaint.measureText(szText)) + ((fontPaint.getTextSize()/Ballot.m_dpiY*72f) >= 18 ? szText.length() : 0);
				tmpSize = rect.width() - (int)textX;
				fSpace = ( (float)(rect.width() - textX) ) / ((float)(i2Byte + i1Byte) );
				iSpace = (int)fSpace;
				iPixSpace = (int)(iSpace * 72f / Ballot.m_dpiY);
				
				Log.i(TAG, "다시계산 iCharSize = " + iCharSize + ", size.cx = " + fontPaint.measureText(szText) + ", rc.width = " + rect.width() + ", i2Byte = " + i2Byte + ", i1Byte = " + i1Byte + ", iSpace = " + iSpace + ", iWFntSize = " + iWFntSize + ", iPixSpace = " + iPixSpace +" loop = " + loop);
				
				loop++;
			}
		}
		
		if (fontAlign == AlignType.BOTH || fontAlign == AlignType.AUTO) {
			commonDrawBothAuto(rect, dataSetNum, szText, fontPaint, dataType, i2Byte, i1Byte, fontSizeWidth, isRotate);
			return null;
		}

		if (fontAlign == AlignType.AUTO_LEFT) {
			canvas.drawText(szText, rect.left, fontY, fontPaint);

			if (isTest) CommonMethod.drawRectForTest(canvas, rect);

		} else if (fontAlign == AlignType.AUTO_RIGHT) {
			canvas.drawText(szText, rect.right - textWidth, fontY, fontPaint);

			if (isTest) CommonMethod.drawRectForTest(canvas, rect);
		}
		
		
		
		return fontPaint;
	}

	public void commonDrawBothAuto(Rect rect, int dataSetNum, String szText, Paint fontPaint, int[] dataType, int i2Byte, int i1Byte, int fontSizeWidth, boolean isRotate) {
		
		float fPos1, fPos2;

		fPos1 = (float) rect.left;
		fPos2 = fPos1;

		Rect newRect = new Rect();
		newRect.set(rect.left, rect.top, rect.right, rect.bottom);

		int accDataType = 0;

		Rect fontBounds = new Rect();
		fontPaint.getTextBounds(szText, 0, szText.length(), fontBounds);
		
		float textX = rect.left;
		
		for (int i = 0; i < dataSetNum; i++) {

			if (i == 0) { // 첫문자
				fPos1 = rect.left;
				fPos2 = rect.left + (float) (rect.width()) / (float) (i2Byte * 2 + i1Byte) * dataType[i];
				Log.i("String.value", "fPos1 = " + fPos1 + " fPos2 = " + fPos2);
				Log.i("String.value", "첫문자");
				Log.i("String.value", "dataType[" + i + "] = " + dataType[i]);
				// iAlignTemp = DT_SINGLELINE | DT_VCENTER | DT_NOCLIP |
				// DT_LEFT;
			} else if (i == dataSetNum - 1) { // 끝문자
				fPos1 = rect.right - ((float) (rect.width()) / (float) (i2Byte * 2 + i1Byte)) * dataType[i];
				fPos2 = rect.right;
				Log.i("String.value", "fPos1 = " + fPos1 + " fPos2 = " + fPos2);
				Log.i("String.value", "끝문자");
				Log.i("String.value", "dataType[" + i + "] = " + dataType[i]);
				// iAlignTemp = DT_SINGLELINE | DT_VCENTER | DT_NOCLIP |
				// DT_RIGHT;
			} else {
				fPos1 = rect.left + ((float) (rect.width()) / (float) (i2Byte * 2 + i1Byte)) * accDataType;
				fPos2 = fPos1 + (((float) rect.width()) / ((float) (i2Byte * 2 + i1Byte))) * dataType[i];
				Log.i("String.value", "fPos1 = " + fPos1 + " fPos2 = " + fPos2);
				Log.i("String.value", "아무거나");
				Log.i("String.value", "accDataType = " + accDataType + " :: "
						+ "dataType[" + i + "] = " + dataType[i]);
				// iAlignTemp = DT_SINGLELINE | DT_VCENTER | DT_NOCLIP |
				// DT_CENTER;
			}

			float charWidth = fontPaint.measureText( String.valueOf( szText.charAt(dataSetNum - 1) ) );
			
			if(fontSizeWidth == 0)
				charWidth += ( ( (fontPaint.getTextSize() * 72 / Ballot.m_dpiY) >= 18 ? 1 : 0) );	//글자 넓이를 계산할 때 크기가 18이상이면 1씩 더해주었으므로 여기서는 1식 빼준다. 그래야 계산이 맞다
			else
				charWidth -= ( ( (fontPaint.getTextSize() * 72 / Ballot.m_dpiY) >= 18 ? 1 : 0) );	//글자 넓이를 계산할 때 크기가 18이상이면 1씩 더해주었으므로 여기서는 1식 빼준다. 그래야 계산이 맞다
			
			float fTmpSize = (float)((float)(rect.width() - charWidth) / (float)(i2Byte * 2f + i1Byte - (szText.charAt(dataSetNum-1) > 127 ? 2:1)) );
			int iTemp = (int)fTmpSize;
			float fTemp = fTmpSize - iTemp;
			
			Log.i(TAG, "iTemp = " + iTemp + ", fTemp = " + fTemp);
			
			Log.i("DrawCommon", "rect.width = " + rect.width() + " ,charWidth = " + charWidth);
			Log.i("DrawCommon", "fTmpSize 원래값 = " + fTmpSize);
			
			
//			if(fTemp >= 0.6){
//				fTmpSize = Math.round(fTmpSize);
//			}else{
//				fTmpSize = (float)Math.floor(fTmpSize);
//			}
			int tmpSize = (int)fTmpSize;
			Log.i("DrawCommon", "tmpSize = " + tmpSize);
			
			fTmpSize = (float)(fTmpSize - (float)tmpSize) * (i2Byte * 2 + i1Byte) / 2;
			Log.i("DrawCommon", "fTmpSize = " + fTmpSize);
			
			
			Rect bounds = new Rect();
			fontPaint.getTextBounds(szText, 0, szText.length(), bounds);
			
			float textY = rect.top + ( ( (rect.bottom - rect.top) / 2 ) - (bounds.exactCenterY()) ) ;

			if(i == 0){
				textX += fTmpSize;
			}
			
			int iTextX = (int)textX;
			Log.i(TAG, "iTextX = " + iTextX + ", tempY = " + textY + ", szText[" + i + "] = " + szText.charAt(i));

			canvas.drawText(String.valueOf(szText.charAt(i)), iTextX, textY, fontPaint);
			
			textX += szText.charAt(i) > 127 ? (tmpSize * 2) : tmpSize;
			
			if (isTest) CommonMethod.drawRectForTest(canvas, newRect);

			accDataType += dataType[i];

		}// end for
		
	}

	public void drawTextW(long left, long top, long right, long bottom, int fontSize, AlignType fontAlign, boolean isBold, String wszText, int fontSizeWidth, int fontType){
		int[] dataType = new int[wszText.length()];
		int i2Byte = 0, i1Byte = 0;
		int dataSetNum = 0;
		float fSpace;
		int iSpace = 0;
		int tmpSize = 0;
		int iCharSize = 0;
		int iPixSpace = 0;
		int iWFntSize = 0;
		int loop = 0;
		
		Rect rect = new Rect();
		
		rect.set(
				CommonFunc.MM2PixelX(left + Ballot.m_nMargin, Paper.m_dpiX) + 5 + Ballot.m_FloatMargin, 
				CommonFunc.MM2PixelY(top, Paper.m_dpiY),
				CommonFunc.MM2PixelX(right - Ballot.m_nMargin, Paper.m_dpiX) - 5 + Ballot.m_FloatMargin,
				CommonFunc.MM2PixelY(bottom, Paper.m_dpiY)
		);
		
		Paint fontPaint = new Paint();

		if(mFontType == -1 || mFontType != fontType) mFontType = fontType;
		
		switch (fontType) {
			
		case FontType.BATANG:	//바탕체
			fontPaint.setTypeface(CommonVal.typefaceBatang);
			break;
		case FontType.MALGUN:	//맑은고딕
			fontPaint.setTypeface(CommonVal.typefaceMalgun);
			break;
		default:	//기본값
			fontPaint.setTypeface(CommonVal.typefaceBatang);
			break;
		}

		if (isBold) {
			fontPaint.setStyle(Style.FILL_AND_STROKE);
			fontPaint.setStrokeWidth(0.7f);
		}

		fontPaint.setAntiAlias(DrawCommon.isAntialias);
		fontPaint.setTextSize(fontSize * Paper.m_dpiY / 72);

		fontPaint.setTextLocale(Locale.KOREA);
		fontPaint.setTextAlign(Align.LEFT);
		
		for (int i = 0; i < wszText.length(); i++) {
			if (wszText.charAt(i) > 127) {
				dataType[i] = 2;
				i2Byte += 1;
				dataSetNum++;
				Log.i("charAt[" + i + "]", "if :: " + wszText.charAt(i) + " | type size = " + 2);
			} else if (wszText.charAt(i) >= '0' && wszText.charAt(i) <= '9') {
				dataType[i] = 1;
				i1Byte += 1;
				dataSetNum++;
				Log.i("charAt[" + i + "]", "else if :: " + wszText.charAt(i) + " | type size = " + 1);
			} else {
				dataType[i] = 1;
				i1Byte += 1;
				dataSetNum++;
				Log.i("charAt[" + i + "]", "else :: " + wszText.charAt(i) + " | type size = " + 1);
			}
		}// end for
		
		if(fontSizeWidth != 0){
			float scaleX = (float)fontSizeWidth / (float)fontSize * 2f;
			
			fontPaint.setTextScaleX(scaleX);
			
			Log.i(TAG, "fontSizeWidth != 0 :: fontSizeWidth = " + fontSizeWidth + ", fontSize = " + fontSize + ", scaleX = " + scaleX + ", szText = " + wszText);
			
			float fWidth = fontPaint.measureText(wszText);
			iCharSize = (int)(fWidth / (i2Byte * 2 + i1Byte) );
			fSpace = (float)(rect.width() - fWidth) / (float)(i2Byte + i1Byte - 1);
			iSpace = (int)fSpace;
			iPixSpace = (int)(iSpace * 72 / Ballot.m_dpiY);
		}else{
			float textX = fontPaint.measureText(wszText) + (fontSize >= 18 ? wszText.length() : 0); //Windows와 맞추기 위해 폰트크기가 18 이상이면 폰트 하나당 1을 더해야 한다?
			Log.i(TAG, "font width = " + textX);
			
			iCharSize = (int)(textX / (i2Byte * 2 + i1Byte));
			
			if( (i2Byte + i1Byte - 1) != 0 ){
				fSpace = ( (float)(rect.width() - textX) ) / ( (float)(i2Byte + i1Byte - 1) );
				iSpace = (int)fSpace;
				tmpSize = (rect.width() - (int)textX );
				Log.i(TAG, "tmpSize = " + tmpSize);
			}
			
			iPixSpace = (int)(iSpace * 72 / Ballot.m_dpiY);
			
			
			iWFntSize = (int)( (fontSize + iPixSpace) / 2 ) + 1;
			
			
			Log.i(TAG, "iCharSize = " + iCharSize + ", size.cx = " + textX + ", rc.width = " + rect.width() + ", i2Byte = " + i2Byte + ", i1Byte = " + i1Byte + ", iSpace = " + iSpace + ", iWFntSize = " + iWFntSize + ", iPixSpace = " + iPixSpace);
			float scaleX = 1.0f;
			
			Paint originPaint = fontPaint;
			
			while(iSpace < -14 && loop < 5){
				
				fontPaint = originPaint;
				
				iWFntSize -= loop;
				
				scaleX = (float)((float)iWFntSize / (float)fontSize * 2f);
				
				Log.i(TAG, "iWFntSize = " + iWFntSize + ", scaleX = " + scaleX);
				
				fontPaint.setTextScaleX(scaleX);
				
				iCharSize = (int)((fontPaint.measureText(wszText) + ((fontPaint.getTextSize()/Ballot.m_dpiY*72f) >= 18 ? wszText.length() : 0)) / (i2Byte * 2 + 1 + i1Byte) );
				
				textX = (fontPaint.measureText(wszText)) + ((fontPaint.getTextSize()/Ballot.m_dpiY*72f) >= 18 ? wszText.length() : 0);
				tmpSize = rect.width() - (int)textX;
				fSpace = ( (float)(rect.width() - textX) ) / ((float)(i2Byte + i1Byte - 1) );
				iSpace = (int)fSpace;
				iPixSpace = (int)(iSpace * 72f / Ballot.m_dpiY);
				
				Log.i(TAG, "다시계산 iCharSize = " + iCharSize + ", size.cx = " + fontPaint.measureText(wszText) + ", rc.width = " + rect.width() + ", i2Byte = " + i2Byte + ", i1Byte = " + i1Byte + ", iSpace = " + iSpace + ", iWFntSize = " + iWFntSize + ", iPixSpace = " + iPixSpace +" loop = " + loop);
				
				loop++;
			}
		}
		
		float fPos1, fPos2;

		fPos1 = (float) rect.left;
		fPos2 = fPos1;

		Rect newRect = new Rect();
		newRect.set(rect.left, rect.top, rect.right, rect.bottom);

		int accDataType = 0;

		Rect fontBounds = new Rect();
		fontPaint.getTextBounds(wszText, 0, wszText.length(), fontBounds);
		
		float textX = rect.left;
		
		for (int i = 0; i < dataSetNum; i++) {

			if (i == 0) { // 첫문자
				fPos1 = rect.left;
				fPos2 = rect.left + (float) (rect.width()) / (float) (i2Byte * 2 + i1Byte) * dataType[i];
				Log.i("String.value", "fPos1 = " + fPos1 + " fPos2 = " + fPos2);
				Log.i("String.value", "첫문자");
				Log.i("String.value", "dataType[" + i + "] = " + dataType[i]);
				// iAlignTemp = DT_SINGLELINE | DT_VCENTER | DT_NOCLIP |
				// DT_LEFT;
			} else if (i == dataSetNum - 1) { // 끝문자
				fPos1 = rect.right - ((float) (rect.width()) / (float) (i2Byte * 2 + i1Byte)) * dataType[i];
				fPos2 = rect.right;
				Log.i("String.value", "fPos1 = " + fPos1 + " fPos2 = " + fPos2);
				Log.i("String.value", "끝문자");
				Log.i("String.value", "dataType[" + i + "] = " + dataType[i]);
				// iAlignTemp = DT_SINGLELINE | DT_VCENTER | DT_NOCLIP |
				// DT_RIGHT;
			} else if(i == 1){
				fPos1 = rect.left + 20;
				fPos2 = rect.left + (float)(rect.width()) / (float)(i2Byte * 2 + i1Byte) * dataType[i] + 20;
				
				if(dataSetNum > 5){
					fPos1 -= 8;
				}
			} else if(i == (dataSetNum - 2) ){
				fPos1 = rect.right - ((float)rect.width()) / (float)(i2Byte * 2 + i1Byte) * dataType[i] - 20;
				fPos2 = rect.right - 20;
				if(dataSetNum > 5){
					fPos1 -= 8;
				}else{
					fPos1 += 3;
				}
			}else {
				fPos1 = rect.left + 20 + ((float) (rect.width() - 40) / (float) (i2Byte * 2)) * (accDataType - 1);
				fPos2 = fPos1 + (((float) rect.width() - 40) / ((float) (i2Byte * 2))) * dataType[i];
				Log.i("String.value", "fPos1 = " + fPos1 + " fPos2 = " + fPos2);
				Log.i("String.value", "아무거나");
				Log.i("String.value", "accDataType = " + accDataType + " :: "
						+ "dataType[" + i + "] = " + dataType[i]);
				// iAlignTemp = DT_SINGLELINE | DT_VCENTER | DT_NOCLIP |
				// DT_CENTER;
			}

			Rect bounds = new Rect();

			newRect.set((int) fPos1, rect.top, (int) fPos2, rect.bottom);

			fontPaint.getTextBounds(String.valueOf(wszText.charAt(i)), 0, 1, bounds);
			bounds.set((int) fPos1, rect.top, (int) fPos2, rect.bottom);

			float textWidth = fontPaint.measureText(String.valueOf(wszText.charAt(i)));
			float textSize = fontPaint.getTextSize();
			float cx = bounds.centerX();
			float cy = bounds.centerY();

			if (i == 0) {
				Log.i("fitst", "첫글자 = " + wszText.charAt(i));
				Log.i("fitst", "textSize = " + fontPaint.getTextSize());
				canvas.drawText(String.valueOf(wszText.charAt(i)), newRect.left, cy + (textSize / 3), fontPaint);
			} else if (i == 1){
				canvas.drawText(String.valueOf(wszText.charAt(i)), newRect.left, cy + (textSize / 3), fontPaint);
			} else if (i == (dataSetNum - 2)){
				if(dataSetNum <= 5){
					canvas.drawText(String.valueOf(wszText.charAt(i)), newRect.left - 10, cy + (textSize / 3), fontPaint);
				}else{
					canvas.drawText(String.valueOf(wszText.charAt(i)), newRect.left, cy + (textSize / 3), fontPaint);
				}
			} else if (i == (dataSetNum - 1)) {
				Log.i("fitst", "마지막글자 = " + wszText.charAt(i));
				Log.i("fitst", "textSize = " + fontPaint.getTextSize());
				canvas.drawText(String.valueOf(wszText.charAt(i)), newRect.right - textWidth, cy + (textSize / 3), fontPaint);
			} else {
				Log.i("fitst", "중간 = " + wszText.charAt(i));
				Log.i("fitst", "textSize = " + fontPaint.getTextSize());
				canvas.drawText(String.valueOf(wszText.charAt(i)), cx - (textWidth / 2), cy + (textSize / 3), fontPaint);
			}


//			canvas.drawText(String.valueOf(wszText.charAt(i)), textX, textY, fontPaint);
			
			if (isTest) CommonMethod.drawRectForTest(canvas, newRect);

			accDataType += dataType[i];

		}// end for
		
	}
	// 교육감 가로출력 전용 글자 회전
	public void drawTextRotation(
			long left, long top, long right, long bottom,
			int fontSize, AlignType fontAlign, int isBold, String szText,
			int fontSizeWidth, int fontType, int escapement, int orientation,
			int color ) {
		int i2Byte = 0, i1Byte = 0;
		int dataSetNum = 0;
		float fPos1, fPos2;
		int[] dataType = new int[100];

		Rect rect = new Rect();

		rect.set(
				CommonFunc.MM2PixelX(left + Paper.m_nMargin, Paper.m_dpiX) + 5 + Paper.m_FloatMargin, 
				CommonFunc.MM2PixelY(top, Paper.m_dpiY),
				CommonFunc.MM2PixelX(right - Paper.m_nMargin, Paper.m_dpiX) - 5 + Paper.m_FloatMargin,
				CommonFunc.MM2PixelY(bottom, Paper.m_dpiY)
				);
		
		Paint fontPaint = new Paint();
		
		switch (fontType) {

		case FontType.BATANG:
			fontPaint.setTypeface(CommonVal.typefaceBatang);
			break;
		case FontType.MALGUN:
			fontPaint.setTypeface(CommonVal.typefaceMalgun);
			break;
		case FontType.H2HDRM:
			fontPaint.setTypeface(CommonVal.typefaceH2HDRM);
			break;
		default:
			fontPaint.setTypeface(CommonVal.typefaceBatang);
			break;
		}

		
		
		if(mFontTypeface == null){
			//mFontTypeface = Typeface.createFromAsset(context.getAssets(), fontFilePath);
		}
				
		//fontPaint.setTypeface(mFontTypeface);

		fontPaint.setAntiAlias(DrawCommon.isAntialias);

		fontPaint.setTextSize(fontSize * Paper.m_dpiY / 72);

		fontPaint.setTextLocale(Locale.KOREAN);
		fontPaint.setColor(color);

		float textHalf = fontPaint.getTextSize() / 2;
		float rectCenterX = rect.centerX();
		float rectCenterY = rect.centerY() + textHalf / 2;

		Rect bounds = new Rect();
		fontPaint.getTextBounds(szText, 0, szText.length(), bounds);

		if (fontAlign == AlignType.CENTER) {
			float textSize = fontPaint.getTextSize();
//			float textWidth = fontPaint.measureText(szText);
			float cx = rect.right;
			float cy = rect.bottom;

			canvas.save();
			canvas.rotate(-180, rect.centerX(), rect.bottom - (textSize / 3));
			canvas.drawText(szText, cx, cy, fontPaint);
			canvas.restore();

			if (isTest)CommonMethod.drawRectForTest(canvas, rect);

		} else if (fontAlign == AlignType.BOTH || fontAlign == AlignType.AUTO
				|| fontAlign == AlignType.AUTO_LEFT
				|| fontAlign == AlignType.AUTO_RIGHT) {

			for (int i = 0; i < szText.length(); i++) {
				if (szText.charAt(i) > 127) {
					dataType[i] = 2;
					i2Byte += 1;
					dataSetNum++;
					Log.i("charAt[" + i + "]", "if :: " + szText.charAt(i));
				} else if (szText.length() >= '0' && szText.length() <= '9') {
					dataType[i] = 1;
					i1Byte += 1;
					dataSetNum++;
					Log.i("charAt[" + i + "]", "else if :: " + szText.charAt(i));
				} else {
					dataType[i] = 1;
					i1Byte += 1;
					dataSetNum++;
					Log.i("charAt[" + i + "]", "else :: " + szText.charAt(i));
				}
			}// end for

			if (dataSetNum <= 1) {
				canvas.drawText(szText, rectCenterX, rectCenterY, fontPaint);
				return;
			}

			if (dataSetNum <= 2 && i2Byte == 0 && right - left <= 20) {
				canvas.drawText(szText, rectCenterX, rectCenterY, fontPaint);
				return;
			}

			if (fontAlign == AlignType.BOTH || fontAlign == AlignType.AUTO) {

				Rect rc2 = new Rect();
				rc2.set(rect);
				fPos1 = (float) rect.bottom;
				fPos2 = fPos1;

				int accDataType = 0;

				for (int i = 0; i < dataSetNum; i++) {

					if (i == 0) { // 첫문자
						fPos1 = rect.bottom - ((float) (dataType[i] * rect.height()) / (float) (i2Byte * 2 + i1Byte));
						fPos2 = rect.bottom;
					} else if (i == dataSetNum - 1) { // 끝문자
						fPos1 = rect.top;
						fPos2 = rect.top + ((float) (dataType[i] * rect.height()) / (float) (i2Byte * 2 + i1Byte));
					} else {
						fPos2 = rect.bottom - (((float) (rect.height()) / ((float) (i2Byte * 2 + i1Byte)))) * accDataType;
						fPos1 = fPos2 - (((float) dataType[i] * rect.height()) / ((float) (i2Byte * 2 + i1Byte)));
					}

					Rect tempRect = new Rect();

					rc2.set(rc2.left, (int) fPos1, rc2.right, (int) fPos2);
					if (isTest) CommonMethod.drawRectForTest(canvas, rc2);

					fontPaint.getTextBounds(String.valueOf(szText.charAt(i)), 0, 1, tempRect);

					float textWidth = fontPaint.measureText(String.valueOf(szText.charAt(i)));
					float textSize = fontPaint.getTextSize();
					float centerX = rc2.centerX() - (textWidth / 2);
					float centerY = rc2.centerY() + (textSize / 3);

					canvas.save();
					canvas.rotate(-90, rc2.centerX(), rc2.centerY());
					canvas.drawText(String.valueOf(szText.charAt(i)), centerX, centerY, fontPaint);
					canvas.restore();

					accDataType += dataType[i];
				}

			}
		} else if (fontAlign == AlignType.RIGHT) {
			fontPaint.setTextAlign(Align.RIGHT);
			canvas.drawText(szText, rectCenterX, rectCenterY, fontPaint);
		} else if (fontAlign == AlignType.CENTER_TOP) {
			fontPaint.setTextAlign(Align.RIGHT);
			canvas.drawText(szText, rectCenterX, rectCenterY, fontPaint);
		} else if (fontAlign == AlignType.ELLIPSIS_TOP) {
			fontPaint.setTextAlign(Align.RIGHT);
			canvas.drawText(szText, rectCenterX, rectCenterY, fontPaint);
		} else if (fontAlign == AlignType.MULTILINE) {
			int x = rect.left;
			int y = rect.top;
			float textSize = fontPaint.getTextSize();

			y += textSize;
			for (String line : szText.split("\n")) {
				canvas.drawText(line, x, y, fontPaint);
				y += -fontPaint.ascent() + fontPaint.descent();
			}

			if (isTest) CommonMethod.drawRectForTest(canvas, rect);

		} else if (fontAlign == AlignType.ELLIPSIS) {
			fontPaint.setTextAlign(Align.CENTER);
			canvas.drawText(szText, rectCenterX, rectCenterY, fontPaint);
		} else {
			Rect rt = new Rect();

			rt.set(CommonFunc.MM2PixelX(left + Paper.m_nMargin, Paper.m_dpiX)
					+ 5 + Paper.m_FloatMargin,
					CommonFunc.MM2PixelY(top, Paper.m_dpiY),
					CommonFunc.MM2PixelX(right - Paper.m_nMargin, Paper.m_dpiX)
							- 5 + Paper.m_FloatMargin,
					CommonFunc.MM2PixelY(bottom, Paper.m_dpiY));
			float textSize = fontPaint.getTextSize();
//			float textWidth = fontPaint.measureText(szText);

			float cx = rt.centerX();
			float cy = rt.centerY();

			cx = rt.right;
			cy = rt.bottom - textSize;

			canvas.save();
			canvas.rotate(-180, rt.centerX(), rt.centerY());
			canvas.drawText(szText, cx, cy, fontPaint);
			canvas.restore();

			if (isTest) CommonMethod.drawRectForTest(canvas, rect);

		}
	}

	public void drawBox(long left, long top, long right, long bottom,
			Style lineType, int strokeWidth, boolean isFill, int color) {

		Log.i("box", "left = " + left + " top = " + top + " right = " + right
				+ " bottom = " + bottom);
		Rect rect = new Rect();
		rect.set(
				CommonFunc.MM2PixelX(left, Paper.m_dpiX) + Paper.m_FloatMargin,
				CommonFunc.MM2PixelY(top, Paper.m_dpiY),
				CommonFunc.MM2PixelX(right, Paper.m_dpiX) + Paper.m_FloatMargin,
				CommonFunc.MM2PixelY(bottom, Paper.m_dpiY)
				);

		Log.i("box", "left = " + rect.left + " top = " + rect.top + " right = "
				+ rect.right + " bottom = " + rect.bottom);

		Paint paint;

		if (isFill) {
			paint = new Paint();
			
			paint.setColor(color);
//			paint.setAlpha(200);
			paint.setStyle(Style.FILL);
			canvas.drawColor(Color.TRANSPARENT);
			canvas.drawRect(rect, paint);
		} else {
			paint = new Paint();
			
			paint.setStyle(lineType);
			paint.setStrokeWidth(strokeWidth);
			paint.setColor(color);
			canvas.drawRect(rect, paint);
			// Rectangle(m_hdcPrt, rc.left, rc.top, rc.right, rc.bottom);
			//
			// SelectObject(m_hdcPrt, OldPen);
			// DeleteObject(MyPen);
			// MyPen = NULL;
		}
	}

	public void drawBitmap(Bitmap bitmap, long left, long top, long right,
			long bottom, Style stampStyle, AlignType bitmapAlign, int reSize,
			int grayMode, int angle) {

		Rect rect = new Rect();

		rect.set(
				CommonFunc.MM2PixelX(left, Paper.m_dpiX) + Paper.m_FloatMargin,
				CommonFunc.MM2PixelY(top, Paper.m_dpiY),
				CommonFunc.MM2PixelX(right, Paper.m_dpiX) + Paper.m_FloatMargin,
				CommonFunc.MM2PixelY(bottom, Paper.m_dpiY));

		Paint paint = new Paint();
		paint.setAntiAlias(false);

		ColorMatrix cm = new ColorMatrix();

		switch (grayMode) {

		case 1:
			cm.setSaturation(0);
			ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
			paint.setColorFilter(f);
			break;
		case 2:
			float b = 10f;
			cm.set(new float[] { 1, 0, 0, 0, b, 0, 1, 0, 0, b, 0, 0, 1, 0, b,
					0, 0, 0, 1, 0 });
			break;
		case 3:
			paint.setColor(Color.RED);
			break;
		}

		paint.setColorFilter(new ColorMatrixColorFilter(cm));

		// Bitmap Angle Setting
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);

		int i = 0;
		int j = 0;
		float aspRatio = 0;
		int iRect = 0;
		int iRectW = 0;
		int iRectH = 0;

		if (bitmapAlign == AlignType.RIGHT) {
			i = rect.width() - bitmap.getWidth();
			canvas.drawBitmap(bitmap, rect.left + i, rect.top + j, paint);
		} else if (bitmapAlign == AlignType.CENTER) {
			i = (rect.width() - bitmap.getWidth()) / 2;
			j = (rect.height() - bitmap.getHeight()) / 2;
			canvas.drawBitmap(bitmap, rect.left + i, rect.top + j, paint);
		} else if (bitmapAlign == AlignType.BOTH) {
			aspRatio = (float) bitmap.getWidth() / (float) bitmap.getHeight();
			iRect = CommonFunc.MM2PixelX(reSize, Paper.m_dpiX);

			if (aspRatio > 1.0f) {
				iRectW = iRect;
				iRectH = (int) ((float) iRect / aspRatio);
			} else {
				iRectW = (int) ((float) iRect / aspRatio);
				iRectH = iRect;
			}

			i = (rect.width() - iRectW) / 2;
			j = (rect.height() - iRectH) / 2;

			if ((iRect == bitmap.getWidth()) || (iRect == bitmap.getHeight())) {
				iRect -= 1;
			}

			bitmap = Bitmap.createScaledBitmap(bitmap, iRect, iRect, true);

			bitmap = CommonMethod.getBitmapWithTransparentBG(bitmap, Color.WHITE, Paper.boxColor);
			

			canvas.drawBitmap(bitmap, rect.left + i, rect.top + j, paint);
		} else {
			bitmap = CommonMethod.getBitmapWithTransparentBG(bitmap, Color.WHITE, Paper.boxColor);

			canvas.drawBitmap(bitmap, rect.left + i, rect.top + j, paint);
		}

	}

	public void drawLabelBarcode(int left, int top, int height,
			BarcodeFormat format, String barcodeNumber) {
		Bitmap barcodeBitmap = null;
		int barcodeWidth = CommonFunc.MM2Pixel(58, Paper.m_dpiX);
		int barcodeHeight = CommonFunc.MM2Pixel(height, Paper.m_dpiY);

		int barcodeLeft = 0;
		int barcodeTop = 0;

		if (format == BarcodeFormat.CODE_39) {
			barcodeLeft = CommonFunc.MM2PixelX(left + 7, Paper.m_dpiX);
		} else if (format == BarcodeFormat.CODE_93) {
			barcodeLeft = CommonFunc.MM2PixelX(left + 3, Paper.m_dpiX);
		} else if (format == BarcodeFormat.ITF) {
			barcodeLeft = CommonFunc.MM2PixelX(left + 6, Paper.m_dpiX);
		} else if (format == BarcodeFormat.CODE_128) {
			barcodeLeft = CommonFunc.MM2PixelX(left - 2, Paper.m_dpiX);
		} else if (format == BarcodeFormat.EAN_13) {
			barcodeLeft = CommonFunc.MM2PixelX(left + 6, Paper.m_dpiX);
		}
		barcodeTop = CommonFunc.MM2PixelY(top, Paper.m_dpiY);

		try {
			barcodeBitmap = DrawBarcode.encodeAsBitmap(barcodeNumber, format,
					barcodeWidth, barcodeHeight);
		} catch (WriterException e) {
			e.printStackTrace();
		}

		Paint paint = new Paint();

		canvas.drawBitmap(barcodeBitmap, barcodeLeft, barcodeTop, paint);
	}

	public void drawQRCode(int barcodeX, int barcodeY, String codeNum) {
		QRCodeWriter writer = new QRCodeWriter();
		int width = 0;
		int height = 0;
		int[] pixels = null;

		try {
			BitMatrix bm = writer.encode(codeNum, BarcodeFormat.QR_CODE, 150,
					150);
			width = bm.getWidth();
			height = bm.getHeight();
			pixels = new int[width * height];

			for (int y = 0; y < height; y++) {
				int offset = y * width;
				for (int x = 0; x < width; x++) {
					pixels[offset + x] = bm.get(x, y) ? Color.BLACK
							: Color.WHITE;
				}
			}
		} catch (WriterException e) {
			e.printStackTrace();
		}

		Bitmap bmpQr = Bitmap.createBitmap(width, height,
				Bitmap.Config.ARGB_8888);
		bmpQr.setPixels(pixels, 0, width, 0, 0, width, height);

		Paint paint = new Paint();
		paint.setColor(Color.BLACK);

		bmpQr = CommonMethod.getRemovedMargin(bmpQr);
		// bmpQr = CommonMethod.getBitmapWithTransparentBG(bmpQr, Color.WHITE,
		// Paper.BoxColor());

		canvas.drawBitmap(bmpQr, barcodeX, barcodeY, paint);
	}
	
	public void drawTextSample(long left, long top, long right, long bottom,
			int fontSize, AlignType fontAlign, boolean isBold, String szText){
		Rect rect = new Rect();
		
		rect.set(CommonFunc.MM2PixelX(left + Paper.m_nMargin, Paper.m_dpiX) + 5 + Paper.m_FloatMargin, CommonFunc.MM2PixelY(top, Paper.m_dpiY), 
				CommonFunc.MM2PixelX(right - Paper.m_nMargin, Paper.m_dpiX) - 5 + Paper.m_FloatMargin, CommonFunc.MM2PixelY(bottom, Paper.m_dpiY));
		
		int fontSizeWidth = CommonFunc.MM2PixelX(right-Paper.m_nMargin, Paper.m_dpiX) - CommonFunc.MM2PixelX(left+Paper.m_nMargin, Paper.m_dpiX)-10;

		Paint fontPaint = new Paint();
		
		if (isBold) {
			fontPaint.setFakeBoldText(true);
		} else {
			fontPaint.setFakeBoldText(false);
		}

		fontPaint.setTypeface(CommonVal.typefaceH2HDRM);
		fontPaint.setAntiAlias(DrawCommon.isAntialias);
		
		fontPaint.setTextSize(fontSize * Paper.m_dpiY / 72);


		fontPaint.setTextLocale(Locale.KOREAN);
		fontPaint.setColor(Color.rgb(255, 0, 0));
		fontPaint.setTextAlign(Align.LEFT);
		
		float textHalf = fontPaint.getTextSize() / 2;
		float rectCenterX = rect.centerX();
		float rectCenterY = rect.centerY() + textHalf / 2;

		float textWidth = fontPaint.measureText(szText);
		float textSize = fontPaint.getTextSize();
		float centerX = rect.centerX();
		float centerY = rect.centerY();

		float fontX = centerX - (textWidth / 2);
		float fontY = centerY + (textSize / 3);
		
		if (fontAlign == AlignType.CENTER) {
				canvas.drawText(szText, fontX, fontY, fontPaint);

			if (isTest) CommonMethod.drawRectForTest(canvas, rect);

		} else if (fontAlign == AlignType.BOTH) {
			commonDrawText(rect, fontX, fontY, textWidth, fontAlign, szText, fontSizeWidth, false, fontSize,  fontPaint);
		} else if (fontAlign == AlignType.AUTO) {
			commonDrawText(rect, fontX, fontY, textWidth, fontAlign, szText, fontSizeWidth, false, fontSize,  fontPaint);
		} else if (fontAlign == AlignType.AUTO_LEFT) {
			commonDrawText(rect, fontX, fontY, textWidth, fontAlign, szText, fontSizeWidth, false, fontSize,  fontPaint);
		} else if (fontAlign == AlignType.AUTO_RIGHT) {
			commonDrawText(rect, fontX, fontY, textWidth, fontAlign, szText, fontSizeWidth, false, fontSize,  fontPaint);
		} else if (fontAlign == AlignType.RIGHT) {
			fontPaint.setTextAlign(Align.RIGHT);
			canvas.drawText(szText, rectCenterX, rectCenterY, fontPaint);
		} else if (fontAlign == AlignType.CENTER_TOP) {
			fontPaint.setTextAlign(Align.CENTER);
			canvas.drawText(szText, rectCenterX, rectCenterY, fontPaint);
		} else if (fontAlign == AlignType.ELLIPSIS_TOP) {
			fontPaint.setTextAlign(Align.CENTER);
			canvas.drawText(szText, rectCenterX, rectCenterY, fontPaint);
		} else if (fontAlign == AlignType.MULTILINE) {
			int rLeft = rect.left;
			int rTop = rect.top;

			rTop += textSize;

			for (String line : szText.split("\n")) {
				canvas.drawText(line, rLeft, rTop, fontPaint);
				rTop += -fontPaint.ascent() + fontPaint.descent();
			}

			if (isTest) CommonMethod.drawRectForTest(canvas, rect);

		} else if (fontAlign == AlignType.ELLIPSIS) {
			fontPaint.setTextAlign(Align.CENTER);
			canvas.drawText(szText, rectCenterX, rectCenterY, fontPaint);
		} else {

			canvas.drawText(szText, rect.left, fontY, fontPaint);

			if (isTest) CommonMethod.drawRectForTest(canvas, rect);
		}
	}
}
