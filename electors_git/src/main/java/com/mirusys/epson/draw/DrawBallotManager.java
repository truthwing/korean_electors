package com.mirusys.epson.draw;

import com.mirusys.epson.common.CommonMethod;
import com.mirusys.epson.common.type.BallotType;
import com.mirusys.epson.common.type.FillType;
import com.mirusys.epson.draw.inter.DrawBallotFactoryInter;
import com.mirusys.epson.draw.inter.DrawPaperManagerInter;
import com.mirusys.epson.dto.Ballot;
import com.mirusys.epson.dto.Label;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;

@SuppressLint("DrawAllocation")
public class DrawBallotManager extends View implements DrawPaperManagerInter{
	Context context;
	Ballot ballot;
	int ballotType;
	DrawBallotFactoryInter ballotManager;
	Bitmap ballotBitmap = null;
	Bitmap finalBitmap = null;
	public static int serialNumber = 1;
	final Canvas canvas = new Canvas();	//반드시 전역변수로 선언하여야 한다.
	DrawLabelManager labelManager;
	
	public DrawBallotManager(Context context, Ballot ballot, int ballotType) {
		super(context);
		this.context = context;
		this.ballot = ballot;
		this.ballotType = ballotType;
	}
	
	public DrawBallotManager(Context context, Label label, int ballotType) {
		super(context);
		labelManager = new DrawLabelManager(context, label, ballotType);
	}
	
	@Override
	public void drawPaperToBitmap(){
		
		if(ballotBitmap == null){
			ballotBitmap = CommonMethod.setBallotLength(ballotType);
			canvas.setDensity(DRAWING_CACHE_QUALITY_LOW);
			canvas.setBitmap(ballotBitmap);
			ballotBitmap.eraseColor(Color.WHITE);
			this.draw(canvas);
		}else{
			ballotBitmap = finalBitmap.copy(Config.ARGB_8888, true);	//절취선을 그리기전 Bitmap부터 다시 그린다.
			canvas.setDensity(DRAWING_CACHE_QUALITY_LOW);
			canvas.setBitmap(ballotBitmap);
			ballotManager.dottedLineDrawing(serialNumber);
		}
	}
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		
		switch(ballotType){
		
		case BallotType.BALLOT_TYPE0 :
			ballotManager = new DrawBallotType0(context, canvas, ballot);
			drawBallotType0();
			break;
		case BallotType.BALLOT_TYPE1 :
			ballotManager = new DrawBallotType1(context, canvas, ballot);
			drawBallotType1();
			break;
		case BallotType.BALLOT_TYPE4 :
			ballotManager = new DrawBallotType4(context, canvas, ballot);
			drawBallotType4();
			break;
		case BallotType.BALLOT_TYPE12 :
			ballotManager = new DrawBallotType12(context, canvas, ballot);
			drawBallotType12();
			break;
		}

		finalBitmap = ballotBitmap.copy(Config.ARGB_8888, true);	//절취선을 그리기 전 Bitmap을 복사해 놓는다.
		
		ballotManager.dottedLineDrawing(serialNumber);	//복사가 끝났으니 절취선을 그린다.
	}

	//국회의원
	public void drawBallotType0(){
		ballotManager.boxDrawing(FillType.FILL_FULL);

		ballotManager.stampDrawing();
		
		ballotManager.titleNameDrawing();

		ballotManager.subTitleNameDrawing();

//		ballotManager.verticalLineDrawing();
		ballotManager.horizontalLineDrawing();

		ballotManager.voteManagerDrawing();

//		ballotManager.dottedLineDrawing();

		ballotManager.managerSignDrawing();
		
		if(ballot.isSample()){
			ballotManager.sampleTextDrawing();
		}
	}
	
	//비례대표
	public void drawBallotType1(){
		ballotManager.boxDrawing(FillType.FILL_FULL);
		ballotManager.stampDrawing();
		ballotManager.titleNameDrawing();
		ballotManager.subTitleNameDrawing();
		//ballotManager.verticalLineDrawing();
		ballotManager.horizontalLineDrawing();
		ballotManager.voteManagerDrawing();
		ballotManager.managerSignDrawing();
		if(ballot.isSample()){
			ballotManager.sampleTextDrawing();
		}
	}
	
	//주민소환투표
	public void drawBallotType4(){
		ballotManager.boxDrawing(FillType.FILL_FULL);
		ballotManager.stampDrawing();
		ballotManager.titleNameDrawing();
		ballotManager.subTitleNameDrawing();
		ballotManager.managerSignDrawing();
		ballotManager.voteManagerDrawing();
		ballotManager.firstStampLineDrawing();
		ballotManager.secondStampLineDrawing();
		if(ballot.isSample()){
			ballotManager.sampleTextDrawing();
		}
	}
	
	private void drawBallotType5() {
		
	}
	
	//교육감 가로출력
	public void drawBallotType12(){
		ballotManager.boxDrawing(FillType.FILL_FULL);
		ballotManager.stampDrawing();
//		ballotManager.managerSignDrawing();
//		ballotManager.managerSignTextDrawing();
		ballotManager.superintendentNotPartyDrawing(12);	//교육감 후보자는 정당과 관련이 없습니다
		ballotManager.titleNameDrawing();
		ballotManager.subTitleNameDrawing();
		ballotManager.horizontalLineDrawing();
		ballotManager.verticalLineDrawing();
		//ballotManager.voteManagerDrawing();
		if(ballot.isSample()){
			ballotManager.sampleTextDrawing();
		}
		
	}
	
	@SuppressWarnings("deprecation")
	public Drawable getDrawable(){
		return new BitmapDrawable(ballotBitmap);
	}
	
	public Bitmap getBitmap(){
		
		return this.ballotBitmap;
	}
	
}
