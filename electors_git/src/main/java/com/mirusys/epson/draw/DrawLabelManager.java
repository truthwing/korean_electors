package com.mirusys.epson.draw;

import com.mirusys.epson.common.CommonMethod;
import com.mirusys.epson.draw.inter.DrawBallotFactoryInter;
import com.mirusys.epson.draw.inter.DrawLabelFactoryInter;
import com.mirusys.epson.draw.inter.DrawPaperManagerInter;
import com.mirusys.epson.dto.Label;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.View;

public class DrawLabelManager extends View implements DrawPaperManagerInter{
	Context context;
	Label label;
	int ballotType;
	DrawBallotFactoryInter ballotManager;
	DrawLabelFactoryInter labelManager;
	Bitmap ballotBitmap = null;
	Bitmap finalBitmap = null;
	public static int serialNumber = 1;
	
	public DrawLabelManager(Context context, Label label, int ballotType) {
		super(context);
		this.context = context;
		this.label = label;
		this.ballotType = ballotType;
		
	}
	
	@Override
	public void drawPaperToBitmap(){
		Canvas canvas = new Canvas();

		if(ballotBitmap == null){
			ballotBitmap = CommonMethod.setBallotLength(ballotType);
			canvas.setBitmap(ballotBitmap);
			ballotBitmap.eraseColor(Color.WHITE);
			this.draw(canvas);
		}else{
			canvas.setBitmap(ballotBitmap);
		}
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		labelManager = new DrawLabelType0(context, canvas, label);
		drawLabelType0();
	}
	//라벨
	public void drawLabelType0(){
		labelManager.labelHorizontalLineDrawing();
		labelManager.labelSection1Drawing();
		labelManager.labelSection2Drawing();
		labelManager.labelSection3Drawing();
		labelManager.labelSection4Drawing();
	}
	
	@Override
	public Bitmap getBitmap() {
		// TODO Auto-generated method stub
		return ballotBitmap;
	}
}
