package com.mirusys.epson.draw;

import java.util.Locale;

import com.mirusys.epson.common.CommonFunc;
import com.mirusys.epson.common.CommonVal;
import com.mirusys.epson.common.type.AlignType;
import com.mirusys.epson.common.type.FontType;
import com.mirusys.epson.draw.inter.DrawBallotFactoryInter;
import com.mirusys.epson.dto.Ballot;
import com.mirusys.epson.dto.Paper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint.Style;

public class DrawBallotType1 implements DrawBallotFactoryInter {
	Context context;
	Ballot ballot;
	DrawCommon drawBallot;
	
	public DrawBallotType1(Context context, Canvas canvas, Ballot ballot) {
		this.context = context;
		this.ballot = ballot;

		drawBallot = new DrawCommon(context, canvas);
	}
	@Override
	public void boxDrawing(int fillType) {
		drawBallot.drawBox(ballot.getlOffsetX(),ballot.getlOffsetY(),ballot.getColorBoxW()+ballot.getlOffsetX(),ballot.getColorBoxH()+ballot.getlOffsetY(),Style.FILL,0,true,ballot.getBoxColor());
	}
	
	@Override
	public void stampDrawing() {
		//Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.stamp);
		if(ballot.getStamp() == null  || ballot.getStamp().length <= 0) return;
		
		Bitmap bitmap = BitmapFactory.decodeByteArray(ballot.getStamp(), 0, ballot.getStamp().length);
		if(bitmap==null)return;
//		drawBallot.drawBitmap(bitmap, left, top, right, bottom, stampStyle, bitmapAlign, reSize, grayMode, angle)
		drawBallot.drawBitmap(bitmap, ballot.getStampX(),ballot.getStampY(),ballot.getStampX()+ballot.getStampW(),ballot.getStampY()+ballot.getStampH(),Style.FILL,AlignType.BOTH,(int)ballot.getStampW(),2,0);
		bitmap.recycle();
	}

	@Override
	public void titleNameDrawing() {
		drawBallot.drawText(
				ballot.getTitleX(), 
				ballot.getTitleY(), 
				ballot.getTitleX() + ballot.getTitleW(), 
				ballot.getTitleY() + ballot.getTitleH(), 
				23,
				AlignType.AUTO,
				false, 
				ballot.getTitleName(), 
				0,
				FontType.BATANG, 
				false,
				Color.rgb(0, 0, 0)
			);
	}

	@Override
	public void subTitleNameDrawing() {
		if(ballot.getSubTitleName().length() == 0) ballot.setSubTitleName("");
		
		drawBallot.drawText(
				ballot.getSubTitleX(), 
				ballot.getSubTitleY(), 
				ballot.getSubTitleX() + ballot.getSubTitleW(), 
				ballot.getSubTitleY() + ballot.getSubTitleH(), 
				16, 
				AlignType.AUTO, 
				false, 
				ballot.getSubTitleName(),
				0,
				FontType.BATANG, 
				false,
				Color.rgb(0, 0, 0));
	}

	@Override
	public void verticalLineDrawing() {
		long boxW1_2 = ballot.getBoxW1() + ballot.getBoxW2();
		long boxW1_3 = boxW1_2 + ballot.getBoxW3();
		
		drawBallot.drawLine(ballot.getBoxX(), ballot.getBoxY(), ballot.getBoxX(), ballot.getBoxY()+ballot.getBoxTotalH(), Style.STROKE, 2, Color.rgb(0, 0, 0));			// 가로 가는선 1
		drawBallot.drawLine(ballot.getBoxX()+ballot.getBoxW1(),ballot.getBoxY(),ballot.getBoxX()+ballot.getBoxW1(),ballot.getBoxY()+ballot.getBoxTotalH(), Style.STROKE, 2, Color.rgb(0, 0, 0));			// 가로 가는선 2
		drawBallot.drawLine(ballot.getBoxX()+boxW1_2,ballot.getBoxY(),ballot.getBoxX()+boxW1_2, ballot.getBoxY()+ballot.getBoxTotalH(), Style.STROKE, 4, Color.rgb(0, 0, 0));		// 가로 가는선 3
		drawBallot.drawLine(ballot.getBoxX()+boxW1_3,ballot.getBoxY(),ballot.getBoxX()+boxW1_3, ballot.getBoxY()+ballot.getBoxTotalH(), Style.STROKE, 4, Color.rgb(0, 0, 0));		// 가로 굵은선 1
	}

	@Override
	public void horizontalLineDrawing() {
		long boxW1_2 = ballot.getBoxW1() + ballot.getBoxW2();
		long boxW1_3 = boxW1_2 + ballot.getBoxW3();
		long textMargin = 0;
		
		for (int i = 0; i < ballot.getmHbjnum(); i++) {
			drawBallot.drawLine(
					ballot.getBoxX(),
					ballot.getBoxY() + (i * ballot.getBoxH()) + i * ballot.getBoxSpace(),
					ballot.getBoxX() + boxW1_2,
					ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					Style.FILL_AND_STROKE, 2, Color.rgb(0, 0, 0)
					);			// 가로 가는선
			drawBallot.drawLine(
					ballot.getBoxX(),
					ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					ballot.getBoxX(),
					ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					Style.FILL_AND_STROKE,2,Color.rgb(0, 0, 0));			// 가로 가는선 좌측 구분선
			drawBallot.drawLine(
					ballot.getBoxX() + ballot.getBoxW1(),
					ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					ballot.getBoxX() + ballot.getBoxW1(),
					ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					Style.FILL_AND_STROKE,2,Color.rgb(0, 0, 0));	// 세로 가는선 기호, 정당 구분선
			drawBallot.drawLine(
					ballot.getBoxX(),
					ballot.getBoxY() + ((i + 1) * ballot.getBoxH()) + i * ballot.getBoxSpace(),
					ballot.getBoxX() + boxW1_2,
					ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					Style.FILL_AND_STROKE,2,Color.rgb(0, 0, 0));	// 세로 가는선 정당명, 후보자명 구분선		
			
			drawBallot.drawLine(
					ballot.getBoxX() + boxW1_2,
					ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					ballot.getBoxX() + ballot.getBoxTotalW(),
					ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					Style.FILL_AND_STROKE, 4, Color.rgb(0, 0, 0)
					);	// 가로 굵은선
			drawBallot.drawLine(
					ballot.getBoxX() + boxW1_2, 
					ballot.getBoxY() + (i * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
					ballot.getBoxX() + boxW1_2, 
					ballot.getBoxY() + ((i+1) * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
					Style.STROKE, 4, Color.rgb(0, 0, 0));			// 가로 가는선 1
			drawBallot.drawLine(
					ballot.getBoxX() + ballot.getBoxTotalW(), 
					ballot.getBoxY() + (i * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
					ballot.getBoxX() + ballot.getBoxTotalW(), 
					ballot.getBoxY() + ( (i+1) * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
					Style.STROKE, 4, Color.rgb(0, 0, 0));			// 가로 가는선 1
	 		drawBallot.drawLine(
					ballot.getBoxX() + boxW1_2, 
					ballot.getBoxY() + ((i+1) * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
					ballot.getBoxX() + ballot.getBoxTotalW(), 
					ballot.getBoxY() + ((i+1) * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
					Style.STROKE, 4, Color.rgb(0, 0, 0));			// 가로 가는선 1
	 		
	 		
			if(i < ballot.getmHbjnum()){
				textMargin = 0;
				drawBallot.drawText(
						ballot.getBoxX(),
						ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(), 
						ballot.getBoxX() + ballot.getBoxW1(), 
						ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + i * ballot.getBoxSpace(), 
						20,
						ballot.getCandidate()[i].getGiho().contains("-")?AlignType.BOTH : AlignType.CENTER,
						false, 
						ballot.getCandidate()[i].getGiho(), 
						0,
						FontType.BATANG, 
						false, Color.rgb(0, 0, 0)
						);
				textMargin = 1;
				drawBallot.drawText(
						ballot.getBoxX() + ballot.getBoxW1() + textMargin, 
						ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(), 
						ballot.getBoxX() + boxW1_2 - textMargin, 
						ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + i * ballot.getBoxSpace(), 
						20, 
						AlignType.BOTH, 
						false, 
						ballot.getCandidate()[i].getJdName(), 
						0,
						FontType.BATANG, 
						false, 
						Color.rgb(0, 0, 0));
				textMargin = 0;
				drawBallot.drawText(
						ballot.getBoxX() + boxW1_2+textMargin, 
						ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(), 
						ballot.getBoxX() + boxW1_3 - textMargin, 
						ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + i * ballot.getBoxSpace(), 
						20, 
						AlignType.BOTH, 
						false, 
						ballot.getCandidate()[i].getHbjStatus(), 
						0,
						FontType.BATANG, 
						false, 
						Color.rgb(0, 0, 0)
						);
			}
		}
	}

	//투표관리관
	@Override
	public void voteManagerDrawing() {
		drawBallot.drawLine(ballot.getAdminBoxX(),ballot.getAdminBoxY(),ballot.getAdminBoxX()+ballot.getAdminBoxW(),ballot.getAdminBoxY(),Style.STROKE,2,Color.rgb(0, 0, 0));							// 가로 가는선
		drawBallot.drawLine(ballot.getAdminBoxX(),ballot.getAdminBoxY()+5,ballot.getAdminBoxX()+ballot.getAdminBoxW(),ballot.getAdminBoxY()+5,Style.STROKE,2,Color.rgb(0, 0, 0));						// 가로 가는선
		drawBallot.drawLine(ballot.getAdminBoxX(),ballot.getAdminBoxY()+5+ballot.getAdminBoxH(),ballot.getAdminBoxX()+ballot.getAdminBoxW(),ballot.getAdminBoxY()+5+ballot.getAdminBoxH(),Style.STROKE,2,Color.rgb(0, 0, 0));	// 가로 가는선
		
		
		drawBallot.drawLine(ballot.getAdminBoxX(),ballot.getAdminBoxY(),ballot.getAdminBoxX(),ballot.getAdminBoxY()+5+ballot.getAdminBoxH(),Style.STROKE,2,Color.rgb(0, 0, 0));							// 가로 가는선
		drawBallot.drawLine(ballot.getAdminBoxX()+ballot.getAdminBoxW(),ballot.getAdminBoxY(),ballot.getAdminBoxX()+ballot.getAdminBoxW(),ballot.getAdminBoxY()+5+ballot.getAdminBoxH(),Style.STROKE,2,Color.rgb(0, 0, 0));		// 가로 가는선
		
		drawBallot.drawText(
				ballot.getAdminBoxX(),
				ballot.getAdminBoxY(),
				ballot.getAdminBoxX() + ballot.getAdminBoxW(),
				ballot.getAdminBoxY() + 5,
				11,
				AlignType.CENTER,
				false,
				"투표관리관",
				0,
				FontType.BATANG,
				false,
				Color.rgb(0, 0, 0)
				);
	}

	@Override
	public void dottedLineDrawing(int serialNumber) {
		String serialNum = String.format(Locale.KOREAN, "%07d", serialNumber);

		if(ballot.isUseModeSerial()){	//시리얼 모드 일 때
			long cutFontW = 5;
			long cutFontH = 5;

			drawBallot.drawLine(ballot.getCutLineP1X(), ballot.getCutLineP1Y(), ballot.getCutLineP2X(), ballot.getCutLineP2Y(),Style.STROKE, 1, Color.rgb(0, 0, 0));
			
			long[] cutTextPosX;
			long[] cutTextPosY;
			
			if(ballot.getmHbjnum() > 6){
				cutTextPosX = CommonVal.Cut50TextPosX;
				cutTextPosY = CommonVal.Cut50TextPosY;
			}else{
				cutTextPosX = CommonVal.Cut55TextPosX;
				cutTextPosY = CommonVal.Cut55TextPosY;
			}
			
			drawBallot.drawText(
						ballot.getCutLineP1X() + cutTextPosX[0],
						ballot.getCutLineP2Y() - cutTextPosY[0],
						ballot.getCutLineP1X() + cutTextPosX[0] + cutFontW,
						ballot.getCutLineP2Y() - cutTextPosY[0] + cutFontH, 
						7,
						AlignType.CENTER, 
						false, 
						"절", 
						0,
						FontType.BATANG,
						false,
						Color.rgb(0, 0, 0));
			drawBallot.drawText(
						ballot.getCutLineP1X() + cutTextPosX[1],
						ballot.getCutLineP2Y() - cutTextPosY[1],
						ballot.getCutLineP1X() + cutTextPosX[1] + cutFontW,
						ballot.getCutLineP2Y() - cutTextPosY[1] + cutFontH, 
						7,
						AlignType.CENTER, 
						false, 
						"취", 
						0,
						FontType.BATANG,
						false,
						Color.rgb(0, 0, 0)
					);
			drawBallot.drawText(
						ballot.getCutLineP1X() + cutTextPosX[2],
						ballot.getCutLineP2Y() - cutTextPosY[2],
						ballot.getCutLineP1X() + cutTextPosX[2] + cutFontW,
						ballot.getCutLineP2Y() - cutTextPosY[2] + cutFontH, 
						7,
						AlignType.CENTER, 
						false, 
						"선", 
						0,
						FontType.BATANG,
						false,
						Color.rgb(0, 0, 0)
					);
			drawBallot.drawText(
						4 + ballot.getlOffsetX(),
						ballot.getBallotH() - 10,
						30 + ballot.getlOffsetX(),
						ballot.getBallotH() - 3,
						10,
						AlignType.LEFT,
						false,
						"No.",
						0,
						FontType.BATANG,
						false,
						Color.rgb(0, 0, 0)
					);
			
			if(ballot.getTypeQRCode() == 1){
					drawBallot.drawText(
							13 + ballot.getlOffsetX(), 
							ballot.getBallotH() - 10, 
							25 + ballot.getlOffsetX(), 
							ballot.getBallotH() - 3, 
							13, 
							AlignType.CENTER, 
							true, 
							serialNum, 
							0,
							FontType.BATANG, 
							false, 
							Color.rgb(255, 0, 0)
							);
			}else{
				drawBallot.drawText(
						13 + ballot.getlOffsetX(),
						ballot.getBallotH() - 10, 
						25 + ballot.getlOffsetX(), 
						ballot.getBallotH() - 3, 
						10, 
						AlignType.CENTER, 
						true, 
						serialNum, 
						0,
						FontType.BATANG, 
						false, 
						Color.rgb(255, 0, 0)
						);
			}
		}
		
		if(ballot.isUseModeQRCode()){
			//4.바코드 출력
			int barcodeX = CommonFunc.MM2Pixel(10, ballot.getM_dpiX() + Paper.m_FloatMargin);
			int barcodeY = CommonFunc.MM2PixelY(ballot.getAdminBoxY() + 5 + ballot.getAdminBoxH() - 12, ballot.getM_dpiY()) - 18;
			ballot.setBarcodeX(10);
			ballot.setBarcodeY(ballot.getAdminBoxY() + 5 + ballot.getAdminBoxH() - 12);
			drawBallot.drawQRCode(barcodeX, barcodeY, serialNum);
		}
	}

	@Override
	public void managerSignDrawing() {
		//Bitmap adminStamp = BitmapFactory.decodeResource(context.getResources(), R.drawable.adminstamp);
		BitmapFactory.Options opt = new BitmapFactory.Options();
		opt.inPreferredConfig = Bitmap.Config.ARGB_8888;
		
		if(ballot.getAdminStamp() == null || ballot.getAdminStamp().length <= 0) return;
		
		Bitmap adminStamp= BitmapFactory.decodeByteArray(ballot.getAdminStamp(), 0, ballot.getAdminStamp().length, opt);
		
		drawBallot.drawBitmap(
				adminStamp, 
				ballot.getAdminBoxX(), 
				ballot.getAdminBoxY()+5, 
				ballot.getAdminBoxX()+ballot.getAdminBoxW(), 
				ballot.getAdminBoxY()+5+ballot.getAdminBoxH(), 
				Style.STROKE, 
				AlignType.BOTH,
				17, 
				0, 
				0
				);
	}

	@Override
	public void managerSignTextDrawing() {
		drawBallot.drawText(
				ballot.getAdminBoxX(), 
				ballot.getAdminBoxY() + 5, 
				ballot.getAdminBoxX() + ballot.getAdminBoxW(), 
				ballot.getAdminBoxY() + 5 + ballot.getAdminBoxH(), 
				28, 
				AlignType.CENTER, 
				true, 
				"관리자 사인", 
				0,
				FontType.BATANG, 
				false,
				Color.rgb(0, 0, 0)
			);
	}
	@Override
	public void firstStampLineDrawing() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void secondStampLineDrawing() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void superintendentNotPartyDrawing(int ballotType) {
		// TODO Auto-generated method stub
		
	}
	
	//견본 글씨 출력
	@Override
	public void sampleTextDrawing() {
		drawBallot.drawLine(
				ballot.getAdminBoxX() - ballot.getAdminBoxW() - 5,
				ballot.getAdminBoxY(),
				ballot.getAdminBoxX() - 5,
				ballot.getAdminBoxY(),
				Style.STROKE, 5, Color.rgb(255,0,0)
				);							// 가로 가는선
		drawBallot.drawLine(
				ballot.getAdminBoxX() - ballot.getAdminBoxW() - 5,
				ballot.getAdminBoxY() + ballot.getAdminBoxH(),
				ballot.getAdminBoxX() - 5,
				ballot.getAdminBoxY() + ballot.getAdminBoxH(),
				Style.STROKE, 5, Color.rgb(255,0,0) );	// 가로 가는선

		drawBallot.drawLine(
				ballot.getAdminBoxX() - ballot.getAdminBoxW() - 5,
				ballot.getAdminBoxY(),
				ballot.getAdminBoxX() - ballot.getAdminBoxW() - 5,
				ballot.getAdminBoxY() + ballot.getAdminBoxH(),
				Style.STROKE, 5, Color.rgb(255,0,0) );							// 세로 가는선
		drawBallot.drawLine(
				ballot.getAdminBoxX() - ballot.getAdminBoxW() - 5 + ballot.getAdminBoxW(),
				ballot.getAdminBoxY(),
				ballot.getAdminBoxX() - ballot.getAdminBoxW() - 5 + ballot.getAdminBoxW(),
				ballot.getAdminBoxY() + ballot.getAdminBoxH(),
				Style.STROKE, 5, Color.rgb(255,0,0) );		// 세로 가는선

		drawBallot.drawTextSample(
				ballot.getAdminBoxX() - ballot.getAdminBoxW() - 5,
				ballot.getAdminBoxY(),
				ballot.getAdminBoxX() - 5,
				ballot.getAdminBoxY() + ballot.getAdminBoxH(),
				30, AlignType.CENTER, false, "견본");		
	}
	

}
