package com.mirusys.epson.draw;

import com.mirusys.epson.common.CommonVal;
import com.mirusys.epson.common.type.AlignType;
import com.mirusys.epson.common.type.FillType;
import com.mirusys.epson.common.type.FontType;
import com.mirusys.epson.draw.inter.DrawBallotFactoryInter;
import com.mirusys.epson.dto.Ballot;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint.Style;

public class DrawBallotType12 implements DrawBallotFactoryInter{
	Context context;
	Ballot ballot;
	DrawCommon drawBallot;
	Canvas canvas;
	
	public DrawBallotType12(Context context, Canvas canvas, Ballot ballot) {
		this.context = context;
		this.ballot = ballot;
		this.canvas = canvas;
		
		drawBallot = new DrawCommon(context, canvas);
	}
	
	@Override
	public void boxDrawing(int fillType) {
		switch(fillType){
		case FillType.FILL_TOP :
			drawBallot.drawBox(
								ballot.getlOffsetX(),
								ballot.getBallotH() - 10 + ballot.getlOffsetY(),
								ballot.getColorBoxW() + ballot.getlOffsetX(),
								ballot.getBallotH() - 10 + ballot.getColorBoxH() + ballot.getlOffsetY(),
								Style.FILL,
								0,
								true,
								ballot.getBoxColor()
							);
		
		case FillType.FILL_FULL :
				drawBallot.drawBox(
									ballot.getlOffsetX(),
									ballot.getlOffsetY(),
									ballot.getColorBoxW() + ballot.getlOffsetX(),
									ballot.getColorBoxH() + ballot.getlOffsetY(),
									Style.FILL,
									0,
									true,
									ballot.getBoxColor()
								);
		}
	}

	@Override
	public void stampDrawing() {
		//Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.stamp);
		
		if(ballot.getStamp() == null || ballot.getStamp().length <= 0) return;
		
		Bitmap bitmap = BitmapFactory.decodeByteArray(ballot.getStamp(), 0, ballot.getStamp().length);
		drawBallot.drawBitmap(bitmap, ballot.getStampX()-5,ballot.getStampY(),ballot.getStampX()+ballot.getStampW(),ballot.getStampY()+ballot.getStampH(),Style.FILL,AlignType.BOTH,(int)ballot.getStampW(),2,-90);
//		drawBallot.drawBitmap(bitmap, ballot.getAdminBoxX() +5, ballot.getAdminBoxY() +5, ballot.getAdminBoxX() + ballot.getAdminBoxW(), ballot.getAdminBoxY() + 5 + ballot.getAdminBoxH() - 10, Style.FILL, AlignType.BOTH, 17, 2,1);
		bitmap.recycle();
	}

	@Override
	public void titleNameDrawing() {
		drawBallot.drawTextRotation(ballot.getTitleX(),ballot.getTitleY(),ballot.getTitleX()+ballot.getTitleW(),ballot.getTitleY()+ballot.getTitleH(),21,AlignType.AUTO,0,ballot.getTitleName(),0,0,0,0,Color.rgb(0, 0, 0));
	}

	@Override
	public void subTitleNameDrawing() {
		if(ballot.getSubTitleName().length() == 0) ballot.setSubTitleName("");
		
		ballot.setSubTitleY(ballot.getStampY() + ballot.getStampH());	//선거구명인 청인 왼쪽으로 써진다. 선거구 Y값이 변경되어야 한다.
		
		drawBallot.drawTextRotation(
				ballot.getSubTitleX() + 3,
				ballot.getSubTitleY(),
				ballot.getSubTitleX() + ballot.getSubTitleW(),
				ballot.getSubTitleY() + ballot.getSubTitleH(),
				16, AlignType.AUTO, 0, ballot.getSubTitleName(), 0, 1, 900, 900, Color.rgb(0, 0, 0)
				);
	}

	@Override
	public void verticalLineDrawing() {
		long BoxW1_2 = ballot.getBoxW1() + ballot.getBoxW2();
		
		drawBallot.drawLine(ballot.getBoxX(),ballot.getBoxY(),ballot.getBoxX(),ballot.getBoxY()+ballot.getBoxTotalH(),Style.STROKE,2,Color.rgb(0, 0, 0));
		drawBallot.drawLine(ballot.getBoxX()+ballot.getBoxW1(),ballot.getBoxY(),ballot.getBoxX()+ballot.getBoxW1(),ballot.getBoxY()+ballot.getBoxTotalH(),Style.STROKE,4,Color.rgb(0, 0, 0));
		drawBallot.drawLine(ballot.getBoxX()+BoxW1_2,ballot.getBoxY(),ballot.getBoxX()+BoxW1_2,ballot.getBoxY()+ballot.getBoxTotalH(),Style.STROKE,4,Color.rgb(0, 0, 0));
		
	}

	@Override
	public void horizontalLineDrawing() {
		long BoxW1_2 = ballot.getBoxW1() + ballot.getBoxW2();
		
		int MinHBJFontSizeWidth = 10;
		int TempHBJFontSizeWidth = 0;
		
		int textMargin = 0;
		
		for (int i = 0; i <= ballot.getCandidate().length; i++) {
			drawBallot.drawLine(ballot.getBoxX(),ballot.getBoxY()+(i*ballot.getBoxH()),ballot.getBoxX()+ballot.getBoxW1(),ballot.getBoxY()+(i*ballot.getBoxH()),Style.STROKE,2, Color.rgb(0, 0, 0));
			drawBallot.drawLine(ballot.getBoxX()+ballot.getBoxW1(),ballot.getBoxY()+(i*ballot.getBoxH()),ballot.getBoxX()+ballot.getBoxTotalW(),ballot.getBoxY()+(i*ballot.getBoxH()),Style.STROKE,4,Color.rgb(0, 0, 0));
			
			if(i < ballot.getmHbjnum()){
				
				if(ballot.getCandidate()[i].getHbjHanja().length() > 3){
					textMargin = 2;
					drawBallot.drawText(
							ballot.getBoxX() + textMargin,
							ballot.getBoxY() + (i*ballot.getBoxH()),
							ballot.getBoxX() + ballot.getBoxW1() - textMargin,
							ballot.getBoxY() + ((i+1)*ballot.getBoxH()) - ballot.getBoxHalfH(),
							20, 
							AlignType.BOTH, 
							true, 
							ballot.getCandidate()[i].getHbjName(), 
							0,
							FontType.BATANG,
							true,
							Color.rgb(0, 0, 0)
							);
					drawBallot.drawText(
							ballot.getBoxX()+textMargin,
							ballot.getBoxY()+(i*ballot.getBoxH())+ballot.getBoxHalfH(),
							ballot.getBoxX()+ballot.getBoxW1()-textMargin,
							ballot.getBoxY()+((i+1)*ballot.getBoxH()),
							20,
							AlignType.BOTH, 
							true, 
							ballot.getCandidate()[i].getHbjHanja(), 
							0,
							FontType.BATANG,
							true,
							Color.rgb(0, 0, 0)
							);
				}else{
					textMargin = 2;
					drawBallot.drawText(
							ballot.getBoxX()+textMargin,
							ballot.getBoxY()+(i*ballot.getBoxH()),
							ballot.getBoxX()+ballot.getBoxW1()-textMargin,
							ballot.getBoxY()+((i+1)*ballot.getBoxH()),
							20,
							AlignType.BOTH, 
							true, 
							ballot.getCandidate()[i].getHbjName(),
							0,
							FontType.BATANG, 
							true,
							Color.rgb(0, 0, 0)
							);
				}
				
				if(ballot.getmHbjnum() > 5){
					if(ballot.getCandidate()[i].getHbjStatus().equals("등록무효")){
						drawBallot.drawTextRotation(ballot.getBoxX()+ballot.getBoxW1(),ballot.getBoxY()+(i*ballot.getBoxH()),ballot.getBoxX()+BoxW1_2,ballot.getBoxY()+((i+1)*ballot.getBoxH()),10,AlignType.BOTH,0,ballot.getCandidate()[i].getHbjStatus(),11,0,0,0, Color.rgb(0, 0, 0));
					}else{
						drawBallot.drawTextRotation(ballot.getBoxX()+ballot.getBoxW1(),ballot.getBoxY()+(i*ballot.getBoxH()),ballot.getBoxX()+BoxW1_2,ballot.getBoxY()+((i+1)*ballot.getBoxH()),20,AlignType.BOTH,0,ballot.getCandidate()[i].getHbjStatus(),0,0,0,0, Color.rgb(0, 0, 0));
					}
				}else{
					drawBallot.drawTextRotation(ballot.getBoxX() + ballot.getBoxW1(),ballot.getBoxY()+(i*ballot.getBoxH()),ballot.getBoxX()+BoxW1_2,ballot.getBoxY()+((i+1)*ballot.getBoxH()),20,AlignType.BOTH,0,ballot.getCandidate()[i].getHbjStatus(),0,0,0,0, Color.rgb(0, 0, 0));
				}
			}
		}
	}

	@Override
	public void voteManagerDrawing() {
		drawBallot.drawLine(ballot.getAdminBoxX()+ballot.getlOffsetX(), ballot.getAdminBoxY(), ballot.getAdminBoxX()+ballot.getlOffsetX()+ballot.getAdminBoxW()+5, ballot.getAdminBoxY(), Style.STROKE, 2, Color.rgb(0, 0, 0));		//가로가는선
		drawBallot.drawLine(ballot.getAdminBoxX()+ballot.getlOffsetX(), ballot.getAdminBoxY()+ballot.getAdminBoxH(), ballot.getAdminBoxX()+ballot.getlOffsetX()+ballot.getAdminBoxW()+5, ballot.getAdminBoxY()+ballot.getAdminBoxH(), Style.STROKE, 2, Color.rgb(0, 0, 0));		//가로가는선
		drawBallot.drawLine(ballot.getAdminBoxX()+ballot.getlOffsetX(), ballot.getAdminBoxY(), ballot.getAdminBoxX()+ballot.getlOffsetX(), ballot.getAdminBoxY()+ballot.getAdminBoxH(), Style.STROKE, 2, Color.rgb(0, 0, 0));	//세로 가는선
		drawBallot.drawLine(ballot.getAdminBoxX()+ballot.getlOffsetX()+5, ballot.getAdminBoxY(), ballot.getAdminBoxX()+ballot.getlOffsetX()+5, ballot.getAdminBoxY()+ballot.getAdminBoxH(), Style.STROKE, 2, Color.rgb(0, 0, 0));	//세로 가는선
		drawBallot.drawLine(ballot.getAdminBoxX()+ballot.getlOffsetX()+5+ballot.getAdminBoxW(), ballot.getAdminBoxY(), ballot.getAdminBoxX()+ballot.getlOffsetX()+5+ballot.getAdminBoxW(), ballot.getAdminBoxY()+ballot.getAdminBoxH(), Style.STROKE, 2, Color.rgb(0, 0, 0));	//세로 가는선
		
		drawBallot.drawTextRotation(ballot.getAdminBoxX() + ballot.getlOffsetX(), ballot.getAdminBoxY()+1, ballot.getAdminBoxX()+ballot.getlOffsetX()+5, ballot.getAdminBoxY()+ballot.getAdminBoxH()-1, 11, AlignType.AUTO, 0, "투표관리관", 0, 0, 0, 0, Color.rgb(0, 0, 0));		//관리관 타이틀
	}

	@Override
	public void dottedLineDrawing(int serialNumber) {
		String serialNum = String.format("%07d", serialNumber);
		
		
		if(!ballot.isUseModeQRCode() && ballot.isUseModeSerial()){
			long cutFontW = 5;
			long cutFontH = 5;

			drawBallot.drawLine(ballot.getCutLineP1X() + 4, ballot.getCutLineP1Y(), ballot.getCutLineP2X(), ballot.getCutLineP2Y(),Style.STROKE, 1, Color.rgb(0, 0, 0));
			
			long[] cutTextPosX;
			long[] cutTextPosY;
			
			cutTextPosX = CommonVal.CutEduTextPosX;
			cutTextPosY = CommonVal.CutEduTextPosY;
			
			//절
			drawBallot.drawText(
						ballot.getCutLineP1X() + cutTextPosX[0],
						ballot.getCutLineP2Y() - cutTextPosY[0],
						ballot.getCutLineP1X() + cutTextPosX[0] + cutFontW,
						ballot.getCutLineP2Y() - cutTextPosY[0] + cutFontH, 
						7,
						AlignType.CENTER, 
						false, 
						"절", 
						0,
						FontType.MALGUN,
						true,
						Color.rgb(0, 0, 0));
			drawBallot.drawText(
						ballot.getCutLineP1X() + cutTextPosX[1],
						ballot.getCutLineP2Y() - cutTextPosY[1],
						ballot.getCutLineP1X() + cutTextPosX[1] + cutFontW,
						ballot.getCutLineP2Y() - cutTextPosY[1] + cutFontH, 
						7,
						AlignType.CENTER, 
						false, 
						"취", 
						0,
						FontType.MALGUN,
						true,
						Color.rgb(0, 0, 0)
					);
			drawBallot.drawText(
						ballot.getCutLineP1X() + cutTextPosX[2],
						ballot.getCutLineP2Y() - cutTextPosY[2],
						ballot.getCutLineP1X() + cutTextPosX[2] + cutFontW,
						ballot.getCutLineP2Y() - cutTextPosY[2] + cutFontH, 
						7,
						AlignType.CENTER, 
						false, 
						"선", 
						0,
						FontType.MALGUN,
						true,
						Color.rgb(0, 0, 0)
					);
			
			
			drawBallot.drawTextRotation(ballot.getBallotW()-4+ballot.getlOffsetX(),ballot.getlOffsetY(),30+ballot.getlOffsetX(),16+ballot.getlOffsetY(),10,AlignType.LEFT,0,"No.",0,1,1800,1800, Color.rgb(0, 0, 0));
			
			if(ballot.isUseSerialNum() == false) return;
			
			if(ballot.getTypeQRCode() == 1){
					drawBallot.drawTextRotation(ballot.getBallotW()-10+ballot.getlOffsetX(),ballot.getlOffsetY()-10,30+ballot.getlOffsetX(),5+ballot.getlOffsetY(),13,AlignType.CENTER,1,serialNum,6,1,1800,1800, Color.rgb(255, 0, 0));
			}else{
				drawBallot.drawTextRotation(ballot.getBallotW()-10+ballot.getlOffsetX(),ballot.getlOffsetY()-10,30+ballot.getlOffsetX(),5+ballot.getlOffsetY(),10,AlignType.CENTER,1,serialNum,6,1,1800,1800, Color.rgb(255, 0, 0));
			}
		}else if(ballot.isUseModeQRCode()){
			//4.바코드 출력
		}
	}

	@Override
	public void managerSignDrawing() {
		drawBallot.drawTextRotation(
								ballot.getAdminBoxX(),
								ballot.getAdminBoxY(),
								(long)(ballot.getAdminBoxX() + 0.5),
								ballot.getAdminBoxY() + ballot.getAdminBoxH(),
								5,
								AlignType.CENTER,
								0,
								"관리자 사인",
								0,
								0,
								900,
								900,
								Color.rgb(0, 0, 0)
							);
	}

	@Override
	public void managerSignTextDrawing() {
		
	}
	
	//'교육감(교육의원) 선거는 정당과 관련이 없습니다.'
	public void superintendentNotPartyDrawing(int ballotType){
		//교육감 일때
		if(ballotType == 12){
			long bannerX = 15 + ballot.getlOffsetX();
			long bannerY = ballot.getBallotH() - 10 + ballot.getlOffsetY() - 2;
			long bannerW = 24;
			long bannerLineY = 10 + ballot.getlOffsetY();
			
			drawBallot.drawText(
					bannerX,
					bannerY,
					bannerX+bannerW,
					ballot.getBallotH(),
					14,
					AlignType.AUTO,
					true,
					"교육감 후보자",
					0,
					FontType.MALGUN,true,Color.rgb(0, 0, 0));
			
			drawBallot.drawText(
					bannerX+bannerW,
					bannerY,
					bannerX+ballot.getBallotW()-bannerW,
					ballot.getBallotH(),
					11,
					AlignType.AUTO,
					false,
					"는 정당이 추천하지 않습니다.",
					0,
					FontType.BATANG,true,Color.rgb(0, 0, 0));
			
			drawBallot.drawLine(ballot.getlOffsetX(),ballot.getBallotH()-10+ballot.getlOffsetY(),ballot.getBallotW()+ballot.getlOffsetX(),ballot.getBallotH()+ballot.getlOffsetY()-10,Style.STROKE,2, Color.rgb(0, 0, 0));
		}
		//교육의원 일 때
		if(ballotType == 6){
			long bannerX = 15 + ballot.getlOffsetX();
			long bannerY = ballot.getBallotH() - 10 + ballot.getlOffsetY() - 2;
			long bannerW = 24;
			long bannerLineY = 10 + ballot.getlOffsetY();


			drawBallot.drawText(
					bannerX-2,
					bannerY,
					bannerX-4+bannerW,
					ballot.getBallotH(),
					14,
					AlignType.CENTER,
					false,
					"교육의원선거",
					0,
					FontType.BATANG,
					false,
					Color.rgb(0, 0, 0)
					);

			drawBallot.drawText(
					bannerX+bannerW,
					bannerY,
					bannerX+ballot.getBallotW()-bannerW,
					ballot.getBallotH(),
					11,
					AlignType.AUTO,
					false,
					"는 정당과 관련이 없습니다.",
					0,
					FontType.BATANG,
					false,
					Color.rgb(0, 0, 0));
			drawBallot.drawLine(ballot.getlOffsetX(),ballot.getBallotH()-10+ballot.getlOffsetY(),ballot.getBallotW()+ballot.getlOffsetX(),ballot.getBallotH()+ballot.getlOffsetY()-10,Style.STROKE,2,Color.rgb(0, 0, 0));	
		}
	}
//	public void rotationText(long left, long top, long right, long bottom, int fontSize, Align fontAlign, boolean isBold, String text, int fontType, int escapement, int orientation, int color){
//		int titleWidth;
//		Rect rect2 = new Rect();
//		float fPos1, fPos2;
//		int accDataType = 0;
//		
//		Rect rect = new Rect();
//		rect.set(CommonFunc.MM2PixelX(left+ballot.getM_nMargin(), ballot.getM_dpiX())+5+ballot.getM_FloatMargin(), CommonFunc.MM2PixelY(top, ballot.getM_dpiY()), 
//				CommonFunc.MM2PixelX(right-ballot.getM_nMargin(), ballot.getM_dpiX())-5+ballot.getM_FloatMargin(), CommonFunc.MM2PixelY(bottom, ballot.getM_dpiY()));
//		titleWidth = CommonFunc.MM2PixelX(bottom - ballot.getM_nMargin(), ballot.getM_dpiX()) - CommonFunc.MM2PixelX(top+ballot.getM_nMargin(), ballot.getM_dpiX()) -10;
//		
//		//font type
//		
//		//isBold
//		
//		rect2 = rect;
//		fPos1 = (float)rect.bottom;
//		fPos2 = fPos1;
//		accDataType = 0;
//		
//		rect.bottom = rect.top;
//		rect.top = rect.top;
//		
////		drawBallot.drawText(50, 50, 100, 100, fontSize, Align.CENTER, Typeface.NORMAL, "교육감", 0, 0, Color.rgb(0, 0, 0));
//		Log.i("rotationText","left = " + rect.left + " top = " + rect.top + " right = " + rect.right + " bottom = " + rect.bottom);
//		
//	}

	@Override
	public void firstStampLineDrawing() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void secondStampLineDrawing() {
		// TODO Auto-generated method stub
		
	}

	//견본 글씨 출력
	@Override
	public void sampleTextDrawing() {
		drawBallot.drawLine(
				ballot.getlOffsetX() + ballot.getTitleX() - 2,
				ballot.getTitleY() + ballot.getTitleH() + 5 + ballot.getlOffsetY() - 1,
				ballot.getlOffsetX() + 26,
				ballot.getTitleY() - 1 + ballot.getTitleH() + 5 + ballot.getlOffsetX() - 1,
				Style.STROKE, 5, Color.rgb(255,0,0)
				);							// 가로 가는선
		drawBallot.drawLine(
				ballot.getlOffsetX() + ballot.getTitleX() - 2,
				ballot.getTitleY() + ballot.getTitleH() + 16 - ballot.getlOffsetY() + 1,
				ballot.getlOffsetX() + 26,
				ballot.getTitleY() + ballot.getTitleH() + 16 - ballot.getlOffsetY() + 1,
				Style.STROKE, 5, Color.rgb(255,0,0)
				);	// 가로 가는선

		drawBallot.drawLine(
				ballot.getlOffsetX() + ballot.getTitleX() - 2,
				ballot.getTitleY() + ballot.getTitleH() + 5 + ballot.getlOffsetY() - 1,
				ballot.getlOffsetX() + ballot.getTitleX() - 2,
				ballot.getTitleY() + 1 + ballot.getTitleH() + 16 - ballot.getlOffsetX() + 1,
				Style.STROKE, 5, Color.rgb(255,0,0)
				);							// 세로 가는선
		drawBallot.drawLine(
				ballot.getlOffsetX() + 26,
				ballot.getTitleY() + ballot.getTitleH() + 5 + ballot.getlOffsetY() - 1,
				ballot.getlOffsetX() + 26,
				ballot.getTitleY() + 1 + ballot.getTitleH() + 16 - ballot.getlOffsetX() + 1,
				Style.STROKE, 5, Color.rgb(255,0,0)
				);		// 세로 가는선
		
		drawBallot.drawTextRotation(
				ballot.getlOffsetX() + ballot.getTitleX() - 2,
				ballot.getTitleY() + ballot.getTitleH() + 5,
				ballot.getlOffsetX() + 26,
				ballot.getTitleY() + ballot.getTitleH() + 16,
				20, AlignType.AUTO, 0, "견본", 0, 1, 900, 900, Color.rgb(255, 0, 0));	
		
		//pPrintApi->DH_DrawTextRotation(lOffsetX+TitleX-2,TitleY+TitleH+5,lOffsetX + 26,TitleY+TitleH+16,20,ALIGN_AUTO+400,FALSE,(BYTE*)szTemp,0,2,0,0);
		
//		drawBallot.drawTextRotation(
//				ballot.getSubTitleX() + 3,
//				ballot.getSubTitleY(),
//				ballot.getSubTitleX() + ballot.getSubTitleW(),
//				ballot.getSubTitleY() + ballot.getSubTitleH(),
//				16, AlignType.AUTO, 0, ballot.getSubTitleName(), 0, 1, 900, 900, Color.rgb(0, 0, 0));
	}

}
