package com.mirusys.epson.draw.inter;


public interface DrawBallotFactoryInter {
	
	//0. Box그리기
	public abstract void boxDrawing(int fillType);
	
	//1. 청인
	public abstract void stampDrawing();

	//2. 선거명
	public abstract void titleNameDrawing();

	//2. 선거구명
	public abstract void subTitleNameDrawing();

	//3. 세로선
	public abstract void verticalLineDrawing();

	//4. 가로선, 기표란 정보(기호, 정당명, 후보자명, 상태)
	public abstract void horizontalLineDrawing();

	//9. 투표관리관
	public abstract void voteManagerDrawing();

	//8. 절취선
	public abstract void dottedLineDrawing(int serialNumber);

	//10.관리자 사인
	public abstract void managerSignDrawing();

	//11. 관리자 사인란 글씨
	public abstract void managerSignTextDrawing();
	
	//견본 글씨
	public abstract void sampleTextDrawing();
		
	public abstract void firstStampLineDrawing();

	public abstract void secondStampLineDrawing();

	public abstract void superintendentNotPartyDrawing(int ballotType);
	
	

}