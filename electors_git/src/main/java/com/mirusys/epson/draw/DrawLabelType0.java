package com.mirusys.epson.draw;

import com.google.zxing.BarcodeFormat;
import com.mirusys.epson.common.type.AlignType;
import com.mirusys.epson.common.type.FontType;
import com.mirusys.epson.draw.inter.DrawLabelFactoryInter;
import com.mirusys.epson.dto.Label;

import android.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint.Style;
import android.util.Log;

public class DrawLabelType0 implements DrawLabelFactoryInter {
	Context context;
	Label label;
	DrawCommon drawBallot;
	
	public DrawLabelType0(Context context, Canvas canvas, Label label) {
		this.context = context;
		this.label = label;

		drawBallot = new DrawCommon(context, canvas);
	}
	
	@Override
	public void labelHorizontalLineDrawing(){
		//가로 가는선1
		for (int i = 1; i < 4; i++) {
			drawBallot.drawLine(
									label.getlSecX()[i] + label.getlOffsetX(), 
									label.getlSecY()[i] + label.getlOffsetY(), 
									label.getlSecX()[i] + label.getSectionW() + label.getlOffsetX(), 
									label.getlSecY()[i] + label.getlOffsetY(),
									Style.STROKE, 4, Color.rgb(0, 0, 0));
			Log.i("label","secX = " + label.getlSecX()[i] + " offsetX = " + label.getlOffsetX() + " secY = " + label.getlSecY()[i] + "sectionW = " + label.getSectionW());
		}
	}
	
	@Override
	public void labelSection1Drawing(){
		drawBallot.drawBox(
				label.getlSecX()[0] + 58 + label.getlOffsetX(), 
				label.getlSecY()[0] +  2 + label.getlOffsetY(), 
				label.getlSecX()[0] + 96 + label.getlOffsetX(), 
				label.getlSecY()[0] + 15 + label.getlOffsetY(), 
				Style.STROKE, 3, false, Color.rgb(0, 0, 0));
		drawBallot.drawBox(
				label.getlSecX()[0] + 58 + label.getlOffsetX(), 
				label.getlSecY()[0] +  2 + label.getlOffsetY(), 
				label.getlSecX()[0] + 96 + label.getlOffsetX(), 
				label.getlSecY()[0] + 15 + label.getlOffsetY(), 
				Style.FILL_AND_STROKE, 3, true, Color.rgb(0, 0, 0));
		
		drawBallot.drawBox(
				label.getlSecX()[0] + 58 + label.getlOffsetX(), 
				label.getlSecY()[0] + 15 + label.getlOffsetY(), 
				label.getlSecX()[0] + 96 + label.getlOffsetX(), 
				label.getlSecY()[0] + 28 + label.getlOffsetY(), 
				Style.STROKE, 3, false, Color.rgb(0, 0, 0));
		
		drawBallot.drawText(
				label.getlSecX()[0] + 58 + label.getlOffsetX(),
				label.getlSecY()[0] +  2 + label.getlOffsetY(),
				label.getlSecX()[0] + 96 + label.getlOffsetX(),
				label.getlSecY()[0] +  8 + label.getlOffsetY(),
				16, AlignType.AUTO, true, "우편요금", 0, FontType.MALGUN, false, Color.rgb(255, 255, 255));
		
		drawBallot.drawText(
				label.getlSecX()[0] + 58 + label.getlOffsetX(), 
				label.getlSecY()[0] +  8 + label.getlOffsetY(), 
				label.getlSecX()[0] + 96 + label.getlOffsetX(), 
				label.getlSecY()[0] +  14 + label.getlOffsetY(), 
				16, AlignType.AUTO, true, "수취인후납부담", 0, FontType.MALGUN,false, Color.rgb(255, 255, 255));
		
		drawBallot.drawText(
				label.getlSecX()[0] + 58 + label.getlOffsetX(),
				label.getlSecY()[0] + 16 + label.getlOffsetY(),
				label.getlSecX()[0] + 96 + label.getlOffsetX(),
				label.getlSecY()[0] + 21 + label.getlOffsetY(),
				14, AlignType.AUTO, false, "광화문우체국승인", 0, FontType.MALGUN,false, Color.rgb(0, 0, 0));
		drawBallot.drawText(
				label.getlSecX()[0] + 58 + label.getlOffsetX(),
				label.getlSecY()[0] + 21 + label.getlOffsetY(),
				label.getlSecX()[0] + 96 + label.getlOffsetX(),
				label.getlSecY()[0] + 26 + label.getlOffsetY(),
				14, AlignType.AUTO, false, "제11호", 0, FontType.MALGUN,false, Color.rgb(0, 0, 0));
		
		
		drawBallot.drawLabelBarcode(
				label.getlSecX()[0] + 6 + (int)label.getlOffsetX(), 
				label.getlSecY()[0] + 6 + (int)label.getlOffsetY(), 
				17, 
				label.getBarcodeType(),
				label.getBarcodeNum_sec1()
				);
		drawBallot.drawText(
				label.getlSecX()[0] + 18 + label.getlOffsetX(),
				label.getlSecY()[0] + 23 + label.getlOffsetY(),
				label.getlSecX()[0] + 53 + label.getlOffsetX(),
				label.getlSecY()[0] + 28 + label.getlOffsetY(),
				14, AlignType.CENTER, false, "10739100170060", 0, FontType.MALGUN,false, Color.rgb(0, 0, 0)
			);
		drawBallot.drawText(
				label.getlSecX()[0] + 12 + label.getlOffsetX(), 
				label.getlSecY()[0] + 23 + label.getlOffsetY(), 
				label.getlSecX()[0] + 10 + label.getlOffsetX(), 
				label.getlSecY()[0] + 28 + label.getlOffsetY(), 
				14, AlignType.CENTER, false, "우체국등기", 0, FontType.MALGUN, false, Color.rgb(255, 0, 0));
		//잠정투표
		if(label.isProvisional()){
			drawBallot.drawText(
					label.getlSecX()[0] + 4 + label.getlOffsetX(), 
					label.getlSecY()[0] + 2 + label.getlOffsetY(), 
					label.getlSecX()[0] + 56 + label.getlOffsetX(), 
					label.getlSecY()[0] + 14 + label.getlOffsetY(), 
					24,AlignType.AUTO_LEFT, true, "잠정투표", 0, FontType.MALGUN,false,Color.rgb(0, 0, 0));
		}
	}
	
	public void labelSection2Drawing(){
		drawBallot.drawText(
				label.getlSecX()[1] + 4 + label.getlOffsetX(), 
				label.getlSecY()[1] + 1 + label.getlOffsetY(), 
				label.getlSecX()[1] + 37 + label.getlOffsetX(), 
				label.getlSecY()[1] + 7 + label.getlOffsetY(), 
				16, AlignType.AUTO_LEFT, false, "청운효자동", 0, FontType.MALGUN, false,Color.rgb(0, 0, 0));
		
		drawBallot.drawText(
				label.getlSecX()[1] + 4 + label.getlOffsetX(), 
				label.getlSecY()[1] + 8 + label.getlOffsetY(), 
				label.getlSecX()[1] + 37 + label.getlOffsetX(), 
				label.getlSecY()[1] + 14 + label.getlOffsetY(), 
				16, AlignType.AUTO_LEFT, false, "제01투표구", 0, FontType.MALGUN,false, Color.rgb(0, 0, 0));
		
		drawBallot.drawText(
				label.getlSecX()[1] + 4 + label.getlOffsetX(), 
				label.getlSecY()[1] + 16 + label.getlOffsetY(), 
				label.getlSecX()[1] + 21 + label.getlOffsetX(), 
				label.getlSecY()[1] + 22 + label.getlOffsetY(), 
				16, AlignType.AUTO_RIGHT, false, "300번", 0, FontType.MALGUN,false, Color.rgb(0, 0, 0));
		drawBallot.drawText(
				label.getlSecX()[1] + 21 + label.getlOffsetX(), 
				label.getlSecY()[1] + 16 + label.getlOffsetY(), 
				label.getlSecX()[1] + 37 + label.getlOffsetX(), 
				label.getlSecY()[1] + 22 + label.getlOffsetY(), 
				16, AlignType.AUTO_RIGHT, false, "(233)", 0, FontType.MALGUN,false, Color.rgb(0, 0, 0));
		drawBallot.drawText(
				label.getlSecX()[1] + 40 + label.getlOffsetX(), 
				label.getlSecY()[1] + 22 + label.getlOffsetY(), 
				label.getlSecX()[1] + 94 + label.getlOffsetX(), 
				label.getlSecY()[1] + 28 + label.getlOffsetY(), 
				14, AlignType.CENTER, false, "부재자투표소투표자", 0, FontType.MALGUN,false, Color.rgb(0, 0, 0));
		drawBallot.drawText(
				label.getlSecX()[1] + 40 + label.getlOffsetX(), 
				label.getlSecY()[1] + 18 + label.getlOffsetY(), 
				label.getlSecX()[1] + 94 + label.getlOffsetX(), 
				label.getlSecY()[1] + 23 + label.getlOffsetY(), 
				14, AlignType.CENTER, false, "0041280200100010", 0, FontType.MALGUN,false, Color.rgb(0, 0, 0));
		
		drawBallot.drawLabelBarcode(
								label.getlSecX()[1] + 40 + (int)label.getlOffsetX(),
								label.getlSecY()[1] + 5 + (int)label.getlOffsetY(),
								14, 
								label.getBarcodeType(),
								label.getBarcodeNum_sec2()
									);
		if(label.isBasedElection()){
			drawBallot.drawBox(
					label.getlSecX()[1] + 4 + label.getlOffsetX(), 
					label.getlSecY()[1] + 22 + label.getlOffsetY(), 
					label.getlSecX()[1] + 26 + label.getlOffsetX(), 
					label.getlSecY()[1] + 29 + label.getlOffsetY(), 
					Style.STROKE, 5, false, Color.rgb(255, 0, 0)
					);
			
			drawBallot.drawText(
					label.getlSecX()[1] + 4 + label.getlOffsetX(), 
					label.getlSecY()[1] + 25 + label.getlOffsetY(), 
					label.getlSecX()[1] + 26 + label.getlOffsetX(), 
					label.getlSecY()[1] + 25 + label.getlOffsetY(),  
					17, AlignType.CENTER, false, "기초선거구", 0, FontType.MALGUN,false, Color.rgb(255, 0, 0)
					);
		}
		
	}
	
	public void labelSection3Drawing(){
		long stampW = 20;
		long stampH = 20;
		
		//Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.stamp);
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.btn_default);
		
		drawBallot.drawBitmap(
				bitmap,
				label.getlSecX()[2] + 75 + label.getlOffsetX(),
				label.getlSecY()[2] + 8 + label.getlOffsetY(),
				label.getlSecX()[2] + 75 + stampW + label.getlOffsetX(), 
				label.getlSecY()[2] + 8 + stampH + label.getlOffsetY(),
				Style.FILL,
				AlignType.BOTH,
				(int)stampW, 2, 0);
		
		drawBallot.drawText(
				label.getlSecX()[2] + 4 + label.getlOffsetX(), 
				label.getlSecY()[2] + 1 + label.getlOffsetY(), 
				label.getlSecX()[2] +90 + label.getlOffsetX(), 
				label.getlSecY()[2] + 13 + label.getlOffsetY(),
				14, AlignType.MULTILINE, false, "(수취인) 성남시 분당구 삼평동 625 판교세븐밴쳐밸\n리 1단지 2동 1001호 미루시스템즈", 0, FontType.MALGUN,false,Color.rgb(0, 0, 0)
				);
		drawBallot.drawText(
				label.getlSecX()[2] + 6 + label.getlOffsetX(), 
				label.getlSecY()[2] + 15 + label.getlOffsetY(), 
				label.getlSecX()[2] + 73 + label.getlOffsetX(), 
				label.getlSecY()[2] + 22 + label.getlOffsetY(),
				18, AlignType.AUTO, true, "종로구선거관리위원회 귀중", 0, FontType.MALGUN,false,Color.rgb(0, 0, 0)
				);
		drawBallot.drawText(
				label.getlSecX()[2] + 6 + label.getlOffsetX(), 
				label.getlSecY()[2] + 23 + label.getlOffsetY(), 
				label.getlSecX()[2] + 70 + label.getlOffsetX(), 
				label.getlSecY()[2] + 25 + label.getlOffsetY(),
				12, AlignType.AUTO_LEFT, false, "(전화번호 : 02-742-1390)", 0, FontType.MALGUN,false,Color.rgb(0, 0, 0)
				);
		drawBallot.drawText(
				label.getlSecX()[2] + 10 + label.getlOffsetX(), 
				label.getlSecY()[2] + 23 + label.getlOffsetY(), 
				label.getlSecX()[2] + 72 + label.getlOffsetX(), 
				label.getlSecY()[2] + 27 + label.getlOffsetY(),
				20, AlignType.AUTO_RIGHT, false, "110-844", 0, FontType.MALGUN,false,Color.rgb(0, 0, 0)
				);
	}
	
	public void labelSection4Drawing(){
		drawBallot.drawText(
				label.getlSecX()[3] + 4 + label.getlOffsetX(), 
				label.getlSecY()[3] + 1 + label.getlOffsetY(), 
				label.getlSecX()[3] + 90 + label.getlOffsetX(), 
				label.getlSecY()[3] + 13 + label.getlOffsetY(), 
				10, AlignType.MULTILINE, false, "무투표선거  선거구 : 종로구청장,송파구청장,노원구청장,강남구청장,강서\n구청장,강북구청장", 0, FontType.MALGUN,false, Color.rgb(0, 0, 0)
				);
	}
	
	@Override
	public void labelBarcodeDrawing(int left, int top, int height, BarcodeFormat format, String barcodeNum){
		
//		int iL = CommonFunc.MM2PixelX(left+6, label.getM_dpiX());		
		
	}
	
}
