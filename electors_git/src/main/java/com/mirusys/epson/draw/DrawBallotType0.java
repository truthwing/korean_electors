package com.mirusys.epson.draw;

import java.util.Locale;

import com.mirusys.epson.common.CommonFunc;
import com.mirusys.epson.common.CommonMethod;
import com.mirusys.epson.common.CommonVal;
import com.mirusys.epson.common.type.AlignType;
import com.mirusys.epson.common.type.FontType;
import com.mirusys.epson.draw.inter.DrawBallotFactoryInter;
import com.mirusys.epson.dto.Ballot;
import com.mirusys.epson.dto.Paper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint.Style;
import android.util.Log;

public class DrawBallotType0 implements DrawBallotFactoryInter {
	String LOG_TAG = "DrawBallotType0";
	Context context;
	Ballot ballot;
	DrawCommon drawBallot;
	
	public DrawBallotType0(Context context, Canvas canvas, Ballot ballot) {
		this.context = context;
		this.ballot = ballot;
		
		drawBallot = new DrawCommon(context, canvas);
		Log.i(LOG_TAG, "DrawCommon("+context + ", " + canvas + ", " + ballot);
		Log.i(LOG_TAG, "DrawBallotType0생성자 호출 OK");
	}
	
	//0. 박스
	@Override
	public void boxDrawing(int fillType) {
		Log.i(LOG_TAG, "boxDrawing 호출전");
		drawBallot.drawBox(ballot.getlOffsetX()-10, ballot.getlOffsetY()-10, ballot.getColorBoxW() + ballot.getlOffsetX()+10 ,ballot.getColorBoxH() + ballot.getlOffsetY() + 10,Style.STROKE, 0, true, ballot.getBoxColor());
		Log.i(LOG_TAG, "boxDrawing 호출OK");
	}
	
	//1. 청인
	@Override
	public void stampDrawing() {
		Log.i(LOG_TAG, "stampDrawing 호출전");
		//Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.stamp);
		
		if(ballot.getStamp() == null || ballot.getStamp().length <= 0) return;

		Bitmap bitmap = BitmapFactory.decodeByteArray(ballot.getStamp(), 0, ballot.getStamp().length);	
		
		
		drawBallot.drawBitmap(bitmap, ballot.getStampX(), ballot.getStampY(), ballot.getStampX() + ballot.getStampW(), ballot.getStampY() + ballot.getStampH(), Style.FILL_AND_STROKE, AlignType.BOTH, (int)ballot.getStampW(), 2, 0);
		
		bitmap.recycle();
		Log.i(LOG_TAG, "stampDrawing 호출OK");
	}
	
	//2. 선거명
	@Override
	public void titleNameDrawing(){
		Log.i(LOG_TAG, "titleNameDrawing 호출전");
		drawBallot.drawText(
								ballot.getTitleX(), 
								ballot.getTitleY(), 
								ballot.getTitleX() + ballot.getTitleW(), 
								ballot.getTitleY() + ballot.getTitleH(), 
								23,
								AlignType.AUTO,
								false, 
								ballot.getTitleName(), 
								0,
								FontType.BATANG,
								false,
								Color.rgb(0, 0, 0)
							);
		Log.i(LOG_TAG, "titleName 호출OK");
	}
	
	//2. 선거구명
	@Override
	public void subTitleNameDrawing(){
		Log.i(LOG_TAG, "subTitleNameDrawing 호출전");
		
		if(ballot.getSubTitleName().length() == 0) ballot.setSubTitleName("");
		
		drawBallot.drawText(
								ballot.getSubTitleX(), 
								ballot.getSubTitleY(), 
								ballot.getSubTitleX() + ballot.getSubTitleW(), 
								ballot.getSubTitleY() + ballot.getSubTitleH(), 
								16, 
								AlignType.AUTO, 
								false, 
								ballot.getSubTitleName(),
								0,
								FontType.BATANG,
								false,
								Color.rgb(0, 0, 0));
		Log.i(LOG_TAG, "subTitleNameDrawing 호출OK");
	}
	
	//3. 세로선
	@Override
	public void verticalLineDrawing(){
		long boxW1_2 = ballot.getBoxW1() + ballot.getBoxW2();
		long boxW1_3 = boxW1_2 + ballot.getBoxW3();
		long boxW1_4 = boxW1_3 + ballot.getBoxW4();
		
		Log.i(LOG_TAG, "verticalLineDrawing 호출전");
		for (int i = 0; i < 10; i++) {
			
	
		drawBallot.drawLine(
				ballot.getBoxX(),
				ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(),
				ballot.getBoxX() + boxW1_3,
				ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(),
				Style.FILL_AND_STROKE,2,Color.rgb(0, 0, 0));			// 가로 가는선 상단 구분선
		drawBallot.drawLine(
				ballot.getBoxX(),
				ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(),
				ballot.getBoxX(),
				ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + i * ballot.getBoxSpace(),
				Style.FILL_AND_STROKE,2,Color.rgb(0, 0, 0));			// 가로 가는선 좌측 구분선
		drawBallot.drawLine(
				ballot.getBoxX() + ballot.getBoxW1(),
				ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(),
				ballot.getBoxX() + ballot.getBoxW1(),
				ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + i + ballot.getBoxSpace(),
				Style.FILL_AND_STROKE,2,Color.rgb(0, 0, 0));	// 세로 가는선 기호, 정당 구분선
		drawBallot.drawLine(
				ballot.getBoxX() + boxW1_2,
				ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(),
				ballot.getBoxX() + boxW1_2,
				ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + i + ballot.getBoxSpace(),
				Style.FILL_AND_STROKE,2,Color.rgb(0, 0, 0));	// 세로 가는선 정당명, 후보자명 구분선		
		drawBallot.drawLine(
				ballot.getBoxX(),
				ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + 1 * ballot.getBoxSpace(),
				ballot.getBoxX() + boxW1_3,
				ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + i + ballot.getBoxSpace(),
				Style.FILL_AND_STROKE,2,Color.rgb(0, 0, 0));	// 세로 가는선 하단 구분선
		
		
		drawBallot.drawLine(
				ballot.getBoxX() + boxW1_3, 
				ballot.getBoxY() + (i * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
				ballot.getBoxX() + ballot.getBoxTotalW(), 
				ballot.getBoxY() + (i * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
				Style.STROKE, 4, Color.rgb(0, 0, 0));			// 가로 가는선 1
		drawBallot.drawLine(
				ballot.getBoxX() + boxW1_3, 
				ballot.getBoxY() + (i * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
				ballot.getBoxX() + boxW1_3, 
				ballot.getBoxY() + ((i+1) * ballot.getBoxH()) + i + ballot.getBoxSpace(), 
				Style.STROKE, 4, Color.rgb(0, 0, 0));			// 가로 가는선 1
		drawBallot.drawLine(
				ballot.getBoxX() + ballot.getBoxTotalW(), 
				ballot.getBoxY() + (i * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
				ballot.getBoxX() + ballot.getBoxTotalW(), 
				ballot.getBoxY() + ( (i+1) * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
				Style.STROKE, 4, Color.rgb(0, 0, 0));			// 가로 가는선 1
		drawBallot.drawLine(
				ballot.getBoxX() + boxW1_3, 
				ballot.getBoxY() + ((i+1) * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
				ballot.getBoxX() + ballot.getBoxTotalW(), 
				ballot.getBoxY() + ((i+1) * ballot.getBoxH()) + i + ballot.getBoxSpace(), 
				Style.STROKE, 4, Color.rgb(0, 0, 0));			// 가로 가는선 1
		}
		Log.i(LOG_TAG, "verticalLineDrawing 호출OK");
	}
	
	//4. 가로선, 기표란 정보(기호, 정당명, 후보자명, 상태)
	@Override
	public void horizontalLineDrawing(){
		long boxW1_2 = ballot.getBoxW1() + ballot.getBoxW2();
		long boxW1_3 = boxW1_2 + ballot.getBoxW3();
		long boxW1_4 = boxW1_3 + ballot.getBoxW4();
		
		int minHbjFontSizeWidth = 10;
		int tempHbjFontSizeWidth = 0;
		int minJdFontSizeWidth = 10;
		int tempJdFontSizeWidth = 0;
		
		long textMargin = 0;
		
		Log.i(LOG_TAG, "horizontalLineDrawing 호출전");

		for (int i = 0; i < ballot.getmHbjnum(); i++) {
			
			if(i < ballot.getmHbjnum()){
				textMargin = 1;
				Log.i("DrawBallotType0", "BoxX = " + ballot.getBoxX() + ", BoxW1 = " + ballot.getBoxW1() + ", textMargin = " + textMargin);
				
				tempJdFontSizeWidth = CommonMethod.getFontSizeWidth(
						ballot.getBoxX() + ballot.getBoxW1() + textMargin,
						ballot.getBoxY() + ( (i*ballot.getBoxH()) + i * ballot.getBoxSpace()),
						ballot.getBoxX() + boxW1_2 - textMargin,
						ballot.getBoxY() + ( (i + 1) * ballot.getBoxH() ) + i * ballot.getBoxSpace(),
						20, AlignType.BOTH, false, ballot.getCandidate()[i].getJdName() );

				if(tempJdFontSizeWidth < minJdFontSizeWidth) minJdFontSizeWidth = tempJdFontSizeWidth;
				textMargin = 2;

				if(ballot.getmHbjnum() > 11){
					tempHbjFontSizeWidth = CommonMethod.getFontSizeWidth(
							ballot.getBoxX() + boxW1_2 + textMargin,
							ballot.getBoxY() + (i * ballot.getBoxH()) + i * ballot.getBoxSpace(),
							ballot.getBoxX() + boxW1_3 - textMargin,
							ballot.getBoxY() + ((i + 1) * ballot.getBoxH() ) + i * ballot.getBoxSpace(),
							10, AlignType.BOTH, true, ballot.getCandidate()[i].getHbjName() );
					Log.i("DrawBallotType0", "tempHbjFontSizeWidth up = " + tempHbjFontSizeWidth);
					if(tempHbjFontSizeWidth < minHbjFontSizeWidth) {
						minHbjFontSizeWidth = tempHbjFontSizeWidth;
						Log.i("DrawBallotType0", "minHbjFontSizeWidth up = " + minHbjFontSizeWidth);
					}
				}else{
					tempHbjFontSizeWidth = CommonMethod.getFontSizeWidth(
							ballot.getBoxX() + boxW1_2 + textMargin,
							ballot.getBoxY() + (i * ballot.getBoxH()) + i * ballot.getBoxSpace(),
							ballot.getBoxX() + boxW1_3 - textMargin,
							ballot.getBoxY() + ((i + 1) * ballot.getBoxH() ) + i * ballot.getBoxSpace(),
							20, AlignType.BOTH, true, ballot.getCandidate()[i].getHbjName() );
					Log.i("DrawBallotType0", "tempHbjFontSizeWidth down = " + tempHbjFontSizeWidth);
					if(tempHbjFontSizeWidth < minHbjFontSizeWidth){
						minHbjFontSizeWidth = tempHbjFontSizeWidth;
						Log.i("DrawBallotType0", "minHbjFontSizeWidth down = " + minHbjFontSizeWidth);
					}
				}
			}
		}
		
		
		for (int i = 0; i < ballot.getmHbjnum(); i++) {
			
			drawBallot.drawLine(
					ballot.getBoxX(),
					ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					ballot.getBoxX() + boxW1_3,
					ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					Style.FILL_AND_STROKE,2,Color.rgb(0, 0, 0));			// 가로 가는선 상단 구분선
			drawBallot.drawLine(
					ballot.getBoxX(),
					ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					ballot.getBoxX(),
					ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					Style.FILL_AND_STROKE,2,Color.rgb(0, 0, 0));			// 가로 가는선 좌측 구분선
			drawBallot.drawLine(
					ballot.getBoxX() + ballot.getBoxW1(),
					ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					ballot.getBoxX() + ballot.getBoxW1(),
					ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					Style.FILL_AND_STROKE,2,Color.rgb(0, 0, 0));	// 세로 가는선 기호, 정당 구분선
			drawBallot.drawLine(
					ballot.getBoxX() + boxW1_2,
					ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					ballot.getBoxX() + boxW1_2,
					ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					Style.FILL_AND_STROKE,2,Color.rgb(0, 0, 0));	// 세로 가는선 정당명, 후보자명 구분선		
			drawBallot.drawLine(
					ballot.getBoxX(),
					ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					ballot.getBoxX() + boxW1_3,
					ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + i * ballot.getBoxSpace(),
					Style.FILL_AND_STROKE,2,Color.rgb(0, 0, 0));	// 세로 가는선 하단 구분선
			
			
			drawBallot.drawLine(
					ballot.getBoxX() + boxW1_3, 
					ballot.getBoxY() + (i * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
					ballot.getBoxX() + ballot.getBoxTotalW(), 
					ballot.getBoxY() + (i * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
					Style.STROKE, 4, Color.rgb(0, 0, 0));			// 가로 가는선 1
			drawBallot.drawLine(
					ballot.getBoxX() + boxW1_3, 
					ballot.getBoxY() + (i * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
					ballot.getBoxX() + boxW1_3, 
					ballot.getBoxY() + ((i+1) * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
					Style.STROKE, 4, Color.rgb(0, 0, 0));			// 가로 가는선 1
			drawBallot.drawLine(
					ballot.getBoxX() + ballot.getBoxTotalW(), 
					ballot.getBoxY() + (i * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
					ballot.getBoxX() + ballot.getBoxTotalW(), 
					ballot.getBoxY() + ( (i+1) * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
					Style.STROKE, 4, Color.rgb(0, 0, 0));			// 가로 가는선 1
	 		drawBallot.drawLine(
					ballot.getBoxX() + boxW1_3, 
					ballot.getBoxY() + ((i+1) * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
					ballot.getBoxX() + ballot.getBoxTotalW(), 
					ballot.getBoxY() + ((i+1) * ballot.getBoxH()) + i * ballot.getBoxSpace(), 
					Style.STROKE, 4, Color.rgb(0, 0, 0));			// 가로 가는선 1
			
			Log.i("DrawLine" , "boxX = " + ballot.getBoxX() + " boxW1_3 = " + boxW1_3 + " boxH = " + ballot.getBoxH() + " boxTotalW = " + ballot.getBoxTotalW());
			
			if(i < ballot.getmHbjnum()){
				if(ballot.getCandidate()[i].getGiho().contains("-")){
					Log.i("ballotText", "contains =  true");
					drawBallot.drawText(
							ballot.getBoxX(),
							ballot.getBoxY() + ( i * ballot.getBoxH() ) + i * ballot.getBoxSpace(),
							ballot.getBoxX() + ballot.getBoxW1(),
							ballot.getBoxY() + ( ((i + 1) * ballot.getBoxH())  + i * ballot.getBoxSpace()),
							20,
							AlignType.BOTH,
							false,
							ballot.getCandidate()[i].getGiho(),
							(int)(20/2*0.6f),
							FontType.BATANG,
							false,
							Color.rgb(0, 0, 0)
						);
				}else{
				Log.i("ballotText", "contains =  false");
				drawBallot.drawText(
							ballot.getBoxX(),
							ballot.getBoxY() + ( i * ballot.getBoxH() ) + i * ballot.getBoxSpace(),
							ballot.getBoxX() + ballot.getBoxW1(),
							ballot.getBoxY() + ( (i+1) * ballot.getBoxH() + i * ballot.getBoxSpace()),
							20,
							AlignType.CENTER,
							false,
							ballot.getCandidate()[i].getGiho(),
							0,
							FontType.BATANG,
							false,
							Color.rgb(0, 0, 0)
						);

				}
			
//				m_HBJAspRatio = m_JDAspRatio = 0;
				textMargin = 1;
				drawBallot.drawText(
							ballot.getBoxX() + ballot.getBoxW1() + textMargin,
							ballot.getBoxY() + ((i * ballot.getBoxH()) + i * ballot.getBoxSpace()),
							ballot.getBoxX() + boxW1_2 - textMargin,
							ballot.getBoxY() + ( (i + 1) * ballot.getBoxH() ) + i * ballot.getBoxSpace(),
							20,
							AlignType.AUTO,
							false,
							ballot.getCandidate()[i].getJdName(),
							minJdFontSizeWidth,
							FontType.BATANG,
							false,
							Color.rgb(0, 0, 0)
						);// 정당명
				
				textMargin = 2;
				
				if(ballot.getCandidate()[i].getHbjHanja().length() >= 3)	{
					Log.i("ballotText", "한자길이 = " + ballot.getCandidate()[i].getHbjHanja().length());
					
					if(ballot.getmHbjnum() > 11){
						drawBallot.drawText(
								ballot.getBoxX() + boxW1_2 + textMargin,
								ballot.getBoxY() + (i * ballot.getBoxH()) + i * ballot.getBoxSpace(),
								ballot.getBoxX() + boxW1_3 - textMargin,
								ballot.getBoxY() + ((i + 1) * ballot.getBoxH()) - ballot.getBoxHalfH() + 2 + i * ballot.getBoxSpace(),
								20,
								AlignType.BOTH,
								true,
								ballot.getCandidate()[i].getHbjName(),
								minHbjFontSizeWidth,
								FontType.BATANG,
								false,
								Color.rgb(0, 0, 0)
								);		// 후보자명
						drawBallot.drawTextW(
								ballot.getBoxX() + boxW1_2 + textMargin,
								ballot.getBoxY() + ( i * ballot.getBoxH() ) + ballot.getBoxHalfH() + 2 + i * ballot.getBoxSpace(),
								ballot.getBoxX() + boxW1_3 - textMargin,
								ballot.getBoxY() + ( (i + 1) * ballot.getBoxH() ) + i * ballot.getBoxSpace() - 1,
								9,
								AlignType.BOTH,
								true,
								ballot.getCandidate()[i].getHbjHanja(),
								0,
								FontType.BATANG
								);	// 후보자명(한자)

					}else{
						drawBallot.drawText(
								ballot.getBoxX() + boxW1_2 + textMargin,
								ballot.getBoxY() + (i * ballot.getBoxH()) + i * ballot.getBoxSpace(),
								ballot.getBoxX() + boxW1_3 - textMargin,
								ballot.getBoxY() + ((i + 1) * ballot.getBoxH()) - ballot.getBoxHalfH() + i * ballot.getBoxSpace(),
								20,
								AlignType.BOTH,
								true,
								ballot.getCandidate()[i].getHbjName(),
								minHbjFontSizeWidth,
								FontType.BATANG,
								false,
								Color.rgb(0, 0, 0)
								);		// 후보자명
						drawBallot.drawTextW(
								ballot.getBoxX() + boxW1_2 + textMargin,
								ballot.getBoxY() + ( i * ballot.getBoxH() ) + ballot.getBoxHalfH() + i * ballot.getBoxSpace(),
								ballot.getBoxX() + boxW1_3 - textMargin,
								ballot.getBoxY() + ( (i + 1) * ballot.getBoxH() ) + i * ballot.getBoxSpace(),
								20,
								AlignType.BOTH,
								true,
								ballot.getCandidate()[i].getHbjHanja(),
								minHbjFontSizeWidth,
								FontType.BATANG
								);	// 후보자명(한자)
					}
				} else {
					Log.i("ballotText", "후보자 한자 < 3 = " + ballot.getCandidate()[i].getHbjHanja().length());
					
					drawBallot.drawText(
								ballot.getBoxX() + boxW1_2 + textMargin,
								ballot.getBoxY() + (i*ballot.getBoxH()) + i * ballot.getBoxSpace(),
								ballot.getBoxX() + boxW1_3 - textMargin,
								ballot.getBoxY() + ((i+1)*ballot.getBoxH()) + i * ballot.getBoxSpace(),
								20,
								AlignType.BOTH,
								true,
								ballot.getCandidate()[i].getHbjName(),
								minHbjFontSizeWidth,
								FontType.BATANG,
								false,
								Color.rgb(0, 0, 0)
							);// 후보자명
				}

				drawBallot.drawText(
							ballot.getBoxX() + boxW1_3,
							ballot.getBoxY() + (i * ballot.getBoxH()) + i * ballot.getBoxSpace(),
							ballot.getBoxX() + boxW1_4,
							ballot.getBoxY() + ((i + 1) * ballot.getBoxH()) + i * ballot.getBoxSpace(),
							20,
							AlignType.BOTH,
							false,
							ballot.getCandidate()[i].getHbjStatus(),
							0,
							FontType.BATANG,
							false,
							Color.rgb(0, 0, 0)
						);	
			}
		}
		Log.i(LOG_TAG, "horizontalLineDrawing 호출OK");
	}
	
	//9. 투표관리관
	@Override
	public void voteManagerDrawing(){
//		long BoxW1_2 = ballot.getBoxW1()+ballot.getBoxW2();
//		long BoxW1_3 = BoxW1_2+ballot.getBoxW3();
//		long BoxW1_4 = BoxW1_3+ballot.getBoxW4();
//		char[] szGiho = new char[128];
//		char[] szjDName = new char[128];
//		char[] szHBJName = new char[128];
//		char[] szHBJHanja = new char[128];
//		char[] szHBJStatus = new char[128];
//		long TextMargin = 0;
		Log.i(LOG_TAG, "voteManagerDrawing 호출전");
		drawBallot.drawLine(ballot.getAdminBoxX(),ballot.getAdminBoxY(),ballot.getAdminBoxX()+ballot.getAdminBoxW(),ballot.getAdminBoxY(),Style.STROKE,2,Color.rgb(0, 0, 0));							// 가로 가는선
		drawBallot.drawLine(ballot.getAdminBoxX(),ballot.getAdminBoxY()+5,ballot.getAdminBoxX()+ballot.getAdminBoxW(),ballot.getAdminBoxY()+5,Style.STROKE,2,Color.rgb(0, 0, 0));						// 가로 가는선
		drawBallot.drawLine(ballot.getAdminBoxX(),ballot.getAdminBoxY()+5+ballot.getAdminBoxH(),ballot.getAdminBoxX()+ballot.getAdminBoxW(),ballot.getAdminBoxY()+5+ballot.getAdminBoxH(),Style.STROKE,2,Color.rgb(0, 0, 0));	// 가로 가는선
		Log.i("VoteManager","" +ballot.getAdminBoxX() + "  " + ballot.getAdminBoxY()+5+ballot.getAdminBoxH() +"   "+ballot.getAdminBoxX()+ballot.getAdminBoxW() +"    "+ballot.getAdminBoxY()+5+ballot.getAdminBoxH());
		
		
		drawBallot.drawLine(ballot.getAdminBoxX(),ballot.getAdminBoxY(),ballot.getAdminBoxX(),ballot.getAdminBoxY()+5+ballot.getAdminBoxH(),Style.STROKE,2,Color.rgb(0, 0, 0));							// 가로 가는선
		drawBallot.drawLine(ballot.getAdminBoxX()+ballot.getAdminBoxW(),ballot.getAdminBoxY(),ballot.getAdminBoxX()+ballot.getAdminBoxW(),ballot.getAdminBoxY()+5+ballot.getAdminBoxH(),Style.STROKE,2,Color.rgb(0, 0, 0));		// 가로 가는선
		
//		drawText(canvas, ballot.getAdminBoxX(),ballot.getAdminBoxY(),ballot.getAdminBoxX()+ballot.getAdminBoxW(),ballot.getAdminBoxY()+5,11,0,false,"투표관리관",0,0);
		drawBallot.drawText(
				ballot.getAdminBoxX(),
				ballot.getAdminBoxY(),
				ballot.getAdminBoxX() + ballot.getAdminBoxW(),
				ballot.getAdminBoxY() + 5,
				11,
				AlignType.CENTER,
				false,
				"투표관리관",
				0,
				FontType.BATANG,
				false,
				Color.rgb(0, 0, 0)
				);
		Log.i(LOG_TAG, "boxDrawing 호출OK");
	}
	
	//8. 절취선
	@Override
	public void dottedLineDrawing(int serialNumber){
		Log.i(LOG_TAG, "dottedLineDrawing 호출전");
		
		String serialNum = String.format(Locale.KOREAN, "%07d", serialNumber);
		
		if(ballot.isUseModeSerial()){		//QR코드는 찍지 않고 시리얼번호를 찍는다.
			long cutFontW = 5;
			long cutFontH = 5;

			drawBallot.drawLine(ballot.getCutLineP1X(), ballot.getCutLineP1Y(), ballot.getCutLineP2X(), ballot.getCutLineP2Y(),Style.STROKE, 1, Color.rgb(0, 0, 0));
			
			long[] cutTextPosX;
			long[] cutTextPosY;
			
			if(ballot.getmHbjnum() > 6){
				cutTextPosX = CommonVal.Cut50TextPosX;
				cutTextPosY = CommonVal.Cut50TextPosY;
			}else{
				cutTextPosX = CommonVal.Cut55TextPosX;
				cutTextPosY = CommonVal.Cut55TextPosY;
			}
			
			drawBallot.drawText(
						ballot.getCutLineP1X() + cutTextPosX[0],
						ballot.getCutLineP2Y() - cutTextPosY[0],
						ballot.getCutLineP1X() + cutTextPosX[0] + cutFontW,
						ballot.getCutLineP2Y() - cutTextPosY[0] + cutFontH, 
						7,
						AlignType.CENTER, 
						false, 
						"절", 
						0,
						FontType.BATANG,
						false,
						Color.rgb(0, 0, 0));
			drawBallot.drawText(
						ballot.getCutLineP1X() + cutTextPosX[1],
						ballot.getCutLineP2Y() - cutTextPosY[1],
						ballot.getCutLineP1X() + cutTextPosX[1] + cutFontW,
						ballot.getCutLineP2Y() - cutTextPosY[1] + cutFontH, 
						7,
						AlignType.CENTER, 
						false, 
						"취", 
						0,
						FontType.BATANG,
						false,
						Color.rgb(0, 0, 0)
					);
			drawBallot.drawText(
						ballot.getCutLineP1X() + cutTextPosX[2],
						ballot.getCutLineP2Y() - cutTextPosY[2],
						ballot.getCutLineP1X() + cutTextPosX[2] + cutFontW,
						ballot.getCutLineP2Y() - cutTextPosY[2] + cutFontH, 
						7,
						AlignType.CENTER, 
						false, 
						"선", 
						0,
						FontType.BATANG,
						false,
						Color.rgb(0, 0, 0)
					);
			drawBallot.drawText(
						4 + ballot.getlOffsetX(),
						ballot.getBallotH() - 10,
						30 + ballot.getlOffsetX(),
						ballot.getBallotH() - 3,
						10,
						AlignType.LEFT,
						false,
						"No.",
						0,
						FontType.BATANG,
						false,
						Color.rgb(0, 0, 0)
					);
			
			if(ballot.isUseSerialNum() == false) return;
			
			if(ballot.getTypeQRCode() == 1){
				drawBallot.drawText(
						13 + ballot.getlOffsetX(), 
						ballot.getBallotH() - 10, 
						25 + ballot.getlOffsetX(), 
						ballot.getBallotH() - 3, 
						13, 
						AlignType.CENTER, 
						true, 
						serialNum, 
						0,
						FontType.BATANG, 
						false,Color.rgb(255, 0, 0)
						);
			}else{
				drawBallot.drawText(
						13 + ballot.getlOffsetX(), 
						ballot.getBallotH() - 10, 
						25 + ballot.getlOffsetX(), 
						ballot.getBallotH() - 3, 
						10, 
						AlignType.CENTER, 
						true, 
						serialNum, 
						0,
						FontType.BATANG, 
						false,Color.rgb(255, 0, 0));
			}
		}
		
		if(ballot.isUseModeQRCode()){
			//4.바코드 출력
			int barcodeX = CommonFunc.MM2Pixel(10, ballot.getM_dpiX() + Paper.m_FloatMargin);
			int barcodeY = CommonFunc.MM2PixelY(ballot.getAdminBoxY() + 5 + ballot.getAdminBoxH() - 12, ballot.getM_dpiY()) - 18;
			ballot.setBarcodeX(10);
			ballot.setBarcodeY(ballot.getAdminBoxY() + 5 + ballot.getAdminBoxH() - 12);
			drawBallot.drawQRCode(barcodeX, barcodeY, "12345678");
		}
		Log.i(LOG_TAG, "dottedLineDrawing 호출OK");
	}
	
	//10.관리자 사인
	@Override
	public void managerSignDrawing(){
		Log.i(LOG_TAG, "managerSignDrawing 호출전");
		
		if(ballot.getAdminStamp() == null || ballot.getAdminStamp().length <= 0) return;
		
		BitmapFactory.Options opt = new BitmapFactory.Options();
		opt.inPreferredConfig = Bitmap.Config.ARGB_8888;
		
		//Bitmap adminStamp = BitmapFactory.decodeResource(context.getResources(), R.drawable.adminstamp, opt);
		Bitmap adminStamp= BitmapFactory.decodeByteArray(ballot.getAdminStamp(), 0, ballot.getAdminStamp().length);
		
		drawBallot.drawBitmap(
				adminStamp, 
				ballot.getAdminBoxX(), 
				ballot.getAdminBoxY()+5, 
				ballot.getAdminBoxX()+ballot.getAdminBoxW(), 
				ballot.getAdminBoxY()+5+ballot.getAdminBoxH(), 
				Style.STROKE, 
				AlignType.BOTH,
				17, 
				0, 
				0
				);
		
		
		Log.i(LOG_TAG, "managerSignDrawing 호출OK");
	}
	
	//11. 관리자 사인란 글씨
	@Override
	public void managerSignTextDrawing(){
		Log.i(LOG_TAG, "managerSignTextDrawing 호출전");
			drawBallot.drawText(
									ballot.getAdminBoxX(), 
									ballot.getAdminBoxY() + 5, 
									ballot.getAdminBoxX() + ballot.getAdminBoxW(), 
									ballot.getAdminBoxY() + 5 + ballot.getAdminBoxH(), 
									28, 
									AlignType.CENTER, 
									true, 
									"관리자 사인", 
									0,
									FontType.BATANG, 
									false,
									Color.rgb(0, 0, 0)
								);
			Log.i(LOG_TAG, "managerSignTextDrawing 호출OK");
	}

	public void residenceElection(){
		if(true)
		{
			drawBallot.drawLine(10+ballot.getlOffsetX(),3+ballot.getlOffsetY(),30+ballot.getlOffsetX(),3+ballot.getlOffsetY(),Style.STROKE,4,Color.rgb(0, 0, 255));								// 가로 가는선
			drawBallot.drawLine(10+ballot.getlOffsetX(),9+ballot.getlOffsetY(),30+ballot.getlOffsetX(),9+ballot.getlOffsetY(),Style.STROKE,4,Color.rgb(0, 0, 255));								// 가로 가는선
			drawBallot.drawLine(10+ballot.getlOffsetX(),3+ballot.getlOffsetY(),10+ballot.getlOffsetX(),9+ballot.getlOffsetY(),Style.STROKE,4,Color.rgb(0, 0, 255));								// 가로 가는선
			drawBallot.drawLine(30+ballot.getlOffsetX(),3+ballot.getlOffsetY(),30+ballot.getlOffsetX(),9+ballot.getlOffsetY(),Style.STROKE,4,Color.rgb(0, 0, 255));								// 가로 가는선

			drawBallot.drawText(
					10 + ballot.getlOffsetX(),
					3 + ballot.getlOffsetY(),
					30 + ballot.getlOffsetX(),
					9 + ballot.getlOffsetY(),
					12,
					AlignType.CENTER,
					false,
					"잠정투표", 
					0,
					FontType.BATANG, 
					false, 
					Color.rgb(0, 0, 0)); //Blue
		}
	}

	//견본 출력
	public void sampleTextDrawing(){
		drawBallot.drawLine(ballot.getAdminBoxX()-ballot.getAdminBoxW()-5,ballot.getAdminBoxY(),ballot.getAdminBoxX()-5,ballot.getAdminBoxY(), Style.STROKE, 5, Color.rgb(255,0,0) ); // 가로 가는선
		drawBallot.drawLine(ballot.getAdminBoxX()-ballot.getAdminBoxW()-5,ballot.getAdminBoxY()+ballot.getAdminBoxH(),ballot.getAdminBoxX()-5,ballot.getAdminBoxY()+ballot.getAdminBoxH(), Style.STROKE, 5, Color.rgb(255,0,0) );	// 가로 가는선

		drawBallot.drawLine(ballot.getAdminBoxX()-ballot.getAdminBoxW()-5,ballot.getAdminBoxY(),ballot.getAdminBoxX()-ballot.getAdminBoxW()-5,ballot.getAdminBoxY()+ballot.getAdminBoxH(), Style.STROKE, 5, Color.rgb(255,0,0) );							// 세로 가는선
		drawBallot.drawLine(ballot.getAdminBoxX()-ballot.getAdminBoxW()-5+ballot.getAdminBoxW(),ballot.getAdminBoxY(),ballot.getAdminBoxX()-ballot.getAdminBoxW()-5+ballot.getAdminBoxW(),ballot.getAdminBoxY()+ballot.getAdminBoxH(), Style.STROKE, 5, Color.rgb(255,0,0) );		// 세로 가는선

		drawBallot.drawTextSample(
									ballot.getAdminBoxX() - ballot.getAdminBoxW() - 5, 
									ballot.getAdminBoxY(), 
									ballot.getAdminBoxX() - 5, 
									ballot.getAdminBoxY() + ballot.getAdminBoxH(), 
									30, 
									AlignType.CENTER, 
									false, 
									"견본"
								);
	}
	
	@Override
	public void firstStampLineDrawing() {}

	@Override
	public void secondStampLineDrawing() {}

	@Override
	public void superintendentNotPartyDrawing(int ballotType) {}

	
	//12. 거소투표
	
	
	
}
