package com.mirusys.epson.draw.inter;

import com.google.zxing.BarcodeFormat;

public interface DrawLabelFactoryInter {

	public abstract void labelHorizontalLineDrawing();

	public abstract void labelSection1Drawing();

	public abstract void labelBarcodeDrawing(int left, int top, int height, BarcodeFormat format, String barcodeNum);

	public abstract void labelSection2Drawing();

	public abstract void labelSection3Drawing();

	public abstract void labelSection4Drawing();

}