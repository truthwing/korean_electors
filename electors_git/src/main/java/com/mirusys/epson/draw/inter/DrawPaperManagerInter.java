package com.mirusys.epson.draw.inter;

import android.graphics.Bitmap;

public interface DrawPaperManagerInter {
	public void drawPaperToBitmap();
	public Bitmap getBitmap();
}
