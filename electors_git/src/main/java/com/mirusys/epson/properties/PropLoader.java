package com.mirusys.epson.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import android.content.Context;
import android.content.res.AssetManager;

public class PropLoader {
	Context context;
	AssetManager assetManager;
	Properties prop;
	
	public PropLoader(Context context, String ballotType) {
		this.context = context;
		this.assetManager = context.getAssets();
		this.prop = new Properties();
		
		propLoad(ballotType);
	}
	
	public void propLoad(String ballotType){
		try {
			InputStream is = assetManager.open(ballotType);
			prop.load(is);
		} catch (IOException io) {
			io.printStackTrace();
		} catch (Exception ex){
			
		}
		
	}
	
	public Properties getProp(){
		return prop;
	}
	
	
}
