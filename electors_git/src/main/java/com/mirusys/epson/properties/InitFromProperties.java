package com.mirusys.epson.properties;

import java.util.Properties;

import com.mirusys.epson.common.CommonMethod;
import com.mirusys.epson.common.CommonVal;
import com.mirusys.epson.common.type.BallotType;
import com.mirusys.epson.dto.Ballot;
import com.mirusys.epson.dto.Label;

import android.content.Context;

public class InitFromProperties {
	private Properties properties;
	private Ballot ballot;
	private Label label;
	private int mHbjNum;
	
	public InitFromProperties(Context context, int ballotType, int candidateNumber) {
		this.mHbjNum = candidateNumber;
		String propName = CommonMethod.getPropertiesNameByNumber(ballotType, candidateNumber);	//선거타입과 후보자수를 이용하여 Propetie 가 선택된다.
		
		properties = new PropLoader(context, propName).getProp(); 	//PropLoader로부터 용지타입에 따른 Properties객체 를 가져온다.
		injectBallot(ballotType);
	}
	
	//Properties로 부터 가져온 값을 Ballot객체에 주입 한다.
	public void injectBallot(int ballotType){
		
		if(ballotType == BallotType.BALLOT_TYPE_LABEL){
			label = new Label();
		}else{
			ballot = new Ballot(ballotType);
			ballot.setmHbjNum(mHbjNum);
		}
		
		switch(ballotType){
		
		case BallotType.BALLOT_TYPE0 :
			injectBallotType0();
			break;
		case BallotType.BALLOT_TYPE1 :
			injectBallotType1();
			break;
		case BallotType.BALLOT_TYPE4 :
			injectBallotType4();
			break;
		case BallotType.BALLOT_TYPE12 :
			injectBallotType12();
			break;
		}
	}
	

	private void injectBallotType0() {
		ballot.setBallotW( getLongTypeFromProperty("BallotW", "100") );
		ballot.setBallotH( CommonVal.lBallotLength[ballot.getmHbjnum()]);

		ballot.setTitleX( getLongTypeFromProperty("TitleX", "15")	+ ballot.getlOffsetX() );
		ballot.setTitleY( getLongTypeFromProperty("TitleY", "16")	+ ballot.getlOffsetY() );
		ballot.setTitleW( getLongTypeFromProperty("TitleW", "70") );
		ballot.setTitleH( getLongTypeFromProperty("TitleH", "12") );

		ballot.setSubTitleX( getLongTypeFromProperty("SubTitleX", "18") + ballot.getlOffsetX() );
		ballot.setSubTitleY( getLongTypeFromProperty("SubTitleY", "28") + ballot.getlOffsetY() );
		ballot.setSubTitleW( getLongTypeFromProperty("SubTitleW", "65") );
		ballot.setSubTitleH( getLongTypeFromProperty("SubTitleH", "6") );

		ballot.setStampW( getLongTypeFromProperty("StampW", "25") );
		ballot.setStampH( getLongTypeFromProperty("StampH", "25") );
		ballot.setStampX( getLongTypeFromProperty("StampX", "10") );
		ballot.setStampX( (long) (ballot.getBallotW() - ballot.getStampX() - ballot.getStampW()) + ballot.getlOffsetX() );
		ballot.setStampY( getLongTypeFromProperty("StampY", "10") + ballot.getlOffsetY());
		
		ballot.setAdminBoxW( getLongTypeFromProperty("AdminBoxW", "25") );
		ballot.setAdminBoxH( getLongTypeFromProperty("AdminBoxH", "25") );
		ballot.setAdminBoxX( getLongTypeFromProperty("AdminBoxX", "15") );
		ballot.setAdminBoxX( (long) (ballot.getBallotW() - ballot.getAdminBoxX() - ballot.getAdminBoxW()) + ballot.getlOffsetX() );
		ballot.setAdminBoxY( getLongTypeFromProperty("AdminBoxY", "10") );
		ballot.setAdminBoxY( (long) (ballot.getBallotH() - ballot.getAdminBoxY() - ballot.getAdminBoxH() - 5) + ballot.getlOffsetY() );

		ballot.setCutLineW( getLongTypeFromProperty("CutLineW", "55") );
		ballot.setCutLineH( getLongTypeFromProperty("CutLineH", "20") );
		
		ballot.setBoxX( getLongTypeFromProperty("BoxX", "5") + ballot.getlOffsetX() );
		ballot.setBoxY( getLongTypeFromProperty("BoxY", "10") );
		ballot.setBoxY( (long) (ballot.getBoxY() + ballot.getStampY() + ballot.getStampW() ));
		
		ballot.setBoxW1( getLongTypeFromProperty("BoxW1", "10") );
		ballot.setBoxW2( getLongTypeFromProperty("BoxW2", "35") );
		ballot.setBoxW3( getLongTypeFromProperty("BoxW3", "30") );
		
		ballot.setBoxH( getLongTypeFromProperty("BoxH", "15") );
		ballot.setBoxHalfH( (long) (ballot.getBoxH() / 2) );
		ballot.setBoxTotalH( (long) (ballot.getBoxH() * ballot.getmHbjnum()) );
		ballot.setBoxTotalW( (long) (ballot.getBoxW1() + ballot.getBoxW2() + ballot.getBoxW3() + ballot.getBoxW4() ) );
		ballot.setBoxSpace( getLongTypeFromProperty("BoxSpace", "10") );
		
		//절취선 관련
		ballot.setCutLineP1X( (long) ballot.getlOffsetX() );
		ballot.setCutLineP1Y( (long) (ballot.getBallotH() - ballot.getCutLineH()) + ballot.getlOffsetY() );
		ballot.setCutLineP2X( (long) (ballot.getCutLineW()) + ballot.getlOffsetX() );
		ballot.setCutLineP2Y( (long) (ballot.getBallotH() )+ ballot.getlOffsetY() );
	}

	
	private void injectBallotType1(){
		ballot.setBallotW( getLongTypeFromProperty("BallotW", "100") );
		ballot.setBallotH( CommonVal.lBallotLength[ballot.getmHbjnum()]);

		ballot.setTitleX( getLongTypeFromProperty("TitleX", "15")	+ ballot.getlOffsetX() );
		ballot.setTitleY( getLongTypeFromProperty("TitleY", "16")	+ ballot.getlOffsetY() );
		ballot.setTitleW( getLongTypeFromProperty("TitleW", "70") );
		ballot.setTitleH( getLongTypeFromProperty("TitleH", "12") );

		ballot.setSubTitleX( getLongTypeFromProperty("SubTitleX", "18") + ballot.getlOffsetX() );
		ballot.setSubTitleY( getLongTypeFromProperty("SubTitleY", "28") + ballot.getlOffsetY() );
		ballot.setSubTitleW( getLongTypeFromProperty("SubTitleW", "65") );
		ballot.setSubTitleH( getLongTypeFromProperty("SubTitleH", "6") );

		ballot.setStampW( getLongTypeFromProperty("StampW", "25") );
		ballot.setStampH( getLongTypeFromProperty("StampH", "25") );
		ballot.setStampX( getLongTypeFromProperty("StampX", "10") );
		ballot.setStampX( (long) (ballot.getBallotW() - ballot.getStampX() - ballot.getStampW()) + ballot.getlOffsetX() );
		ballot.setStampY( getLongTypeFromProperty("StampY", "10") + ballot.getlOffsetY());
		
		ballot.setAdminBoxW( getLongTypeFromProperty("AdminBoxW", "25") );
		ballot.setAdminBoxH( getLongTypeFromProperty("AdminBoxH", "25") );
		ballot.setAdminBoxX( getLongTypeFromProperty("AdminBoxX", "15") );
		ballot.setAdminBoxX( (long) (ballot.getBallotW() - ballot.getAdminBoxX() - ballot.getAdminBoxW()) + ballot.getlOffsetX() );
		ballot.setAdminBoxY( getLongTypeFromProperty("AdminBoxY", "10") );
		ballot.setAdminBoxY( (long) (ballot.getBallotH() - ballot.getAdminBoxY() - ballot.getAdminBoxH() - 5) + ballot.getlOffsetY() );
		ballot.setBoxSpace( getLongTypeFromProperty("BoxSpace", "10") );

		ballot.setCutLineW( getLongTypeFromProperty("CutLineW", "55") );
		ballot.setCutLineH( getLongTypeFromProperty("CutLineH", "20") );
		
		ballot.setBoxX( getLongTypeFromProperty("BoxX", "5") + ballot.getlOffsetX() );
		ballot.setBoxY( getLongTypeFromProperty("BoxY", "10") );
		ballot.setBoxY( (long) (ballot.getBoxY() + ballot.getStampY() + ballot.getStampW() ));
		
		ballot.setBoxW1( getLongTypeFromProperty("BoxW1", "10") );
		ballot.setBoxW2( getLongTypeFromProperty("BoxW2", "35") );
		ballot.setBoxW3( getLongTypeFromProperty("BoxW3", "30") );
		
		ballot.setBoxH( getLongTypeFromProperty("BoxH", "15") );
		ballot.setBoxHalfH( (long) (ballot.getBoxH() / 2) );
		ballot.setBoxTotalH( (long) (ballot.getBoxH() * ballot.getmHbjnum()) );
		ballot.setBoxTotalW( (long) (ballot.getBoxW1() + ballot.getBoxW2() + ballot.getBoxW3() ) );
		
		//절취선 관련
		ballot.setCutLineP1X( (long) ballot.getlOffsetX() );
		ballot.setCutLineP1Y( (long) (ballot.getBallotH() - ballot.getCutLineH()) + ballot.getlOffsetY() );
		ballot.setCutLineP2X( (long) (ballot.getCutLineW()) + ballot.getlOffsetX() );
		ballot.setCutLineP2Y( (long) (ballot.getBallotH() )+ ballot.getlOffsetY() );
	}
	
	//주민소환투표 초기화
	private void injectBallotType4(){
		ballot.setBallotW(getLongTypeFromProperty("BallotW", "100"));
		ballot.setBallotH(getLongTypeFromProperty("BallotH", "165"));

		ballot.setTitleX(getLongTypeFromProperty("TitleX", "15")	+ ballot.getlOffsetX());
		ballot.setTitleY(getLongTypeFromProperty("TitleY", "17")	+ ballot.getlOffsetY());
		ballot.setTitleW(getLongTypeFromProperty("TitleW", "73"));
		ballot.setTitleH(getLongTypeFromProperty("TitleH", "12"));

		ballot.setSubTitleX(getLongTypeFromProperty("SubTitleX", "10")	+ ballot.getlOffsetX());
		ballot.setSubTitleY(getLongTypeFromProperty("SubTitleY", "27")	+ ballot.getlOffsetY());
		ballot.setSubTitleW(getLongTypeFromProperty("SubTitleW", "70"));
		ballot.setSubTitleH(getLongTypeFromProperty("SubTitleH", "6"));

		ballot.setBarcodeX(getLongTypeFromProperty("BarcodeX", "5")	+ ballot.getlOffsetX());
		ballot.setBarcodeY(getLongTypeFromProperty("BarcodeY", "34")	+ ballot.getlOffsetY());

		ballot.setStampW(getLongTypeFromProperty("StampW", "25"));
		ballot.setStampH(getLongTypeFromProperty("StampH", "25"));
		ballot.setStampX(getLongTypeFromProperty("StampX", "10"));
		ballot.setStampX(ballot.getBallotW() - ballot.getStampX() - ballot.getStampW() + ballot.getlOffsetX());													// 투표지 좌측 기준으로 변경
		ballot.setStampY(getLongTypeFromProperty("StampY", "10") + ballot.getlOffsetY());

		ballot.setAdminBoxW(getLongTypeFromProperty("AdminBoxW", "25"));
		ballot.setAdminBoxH(getLongTypeFromProperty("AdminBoxH", "25"));
		ballot.setAdminBoxX(getLongTypeFromProperty("AdminBoxX", "15")); //	+ lOffsetX);
		ballot.setAdminBoxX(ballot.getBallotW() - ballot.getAdminBoxX() - ballot.getAdminBoxW() + ballot.getlOffsetX());												// 투표지 좌측 기준으로 변경
		ballot.setAdminBoxY(getLongTypeFromProperty("AdminBoxY", "10"));				
		ballot.setAdminBoxY(ballot.getBallotH() - ballot.getAdminBoxY() - ballot.getAdminBoxH() - 5 + ballot.getlOffsetY());											// 투표지 상단 기준으로 변경		

		ballot.setCutLineW(getLongTypeFromProperty("CutLineW", "55"));
		ballot.setCutLineH(getLongTypeFromProperty("CutLineH", "20"));

		//찬반출력 간격
		ballot.setBoxX(getLongTypeFromProperty("BoxX", "10") + ballot.getlOffsetX());
		ballot.setBoxY(getLongTypeFromProperty("BoxY", "40"));
		ballot.setBoxY(ballot.getBoxY() + ballot.getStampY() + ballot.getStampW());																	// 투표지 상단 기준으로 변경

		ballot.setBoxW1(getLongTypeFromProperty("BoxW1", "60"));
		ballot.setBoxW2(getLongTypeFromProperty("BoxW2", "20"));
		ballot.setBoxH(getLongTypeFromProperty("BoxH", "15"));
		ballot.setBoxS(getLongTypeFromProperty("BoxS", "10"));		//기표란 사이 간격

		ballot.setCutLineP1X(ballot.getlOffsetX());
		ballot.setCutLineP1Y(ballot.getBallotH() - ballot.getCutLineH() + ballot.getlOffsetY() );
		ballot.setCutLineP2X(ballot.getCutLineW() + ballot.getlOffsetX() );
		ballot.setCutLineP2Y(ballot.getBallotH() + ballot.getlOffsetY() );
	}
	
	private void injectBallotType5() {
		
	}

	//교육감 가로형 초기화
	private void injectBallotType12(){
		ballot.setBallotW( getLongTypeFromProperty("BallotW", "100") );
		ballot.setBallotH(160);
		
		if(ballot.getmHbjnum() > 7){
			ballot.setBallotH(ballot.getBallotH() + (ballot.getmHbjnum() - 8) * 15);
		}
		
		ballot.setTitleX( getLongTypeFromProperty("TitleX", "17")	+ ballot.getlOffsetX() );
		ballot.setTitleY( getLongTypeFromProperty("TitleY", "50")	+ ballot.getlOffsetY() );
		ballot.setTitleW( getLongTypeFromProperty("TitleW", "10") );
		ballot.setTitleH( getLongTypeFromProperty("TitleH", "80") );

		ballot.setSubTitleX( getLongTypeFromProperty("SubTitleX", "24") + ballot.getlOffsetX() );
		ballot.setSubTitleY( getLongTypeFromProperty("SubTitleY", "75") + ballot.getlOffsetY() );
		ballot.setSubTitleW( getLongTypeFromProperty("SubTitleW", "10") );
		ballot.setSubTitleH( getLongTypeFromProperty("SubTitleH", "50") );

		ballot.setStampW( getLongTypeFromProperty("StampW", "10") );
		ballot.setStampH( getLongTypeFromProperty("StampH", "45") );
		ballot.setStampX( getLongTypeFromProperty("StampX", "25")+2 );
		ballot.setStampY( getLongTypeFromProperty("StampY", "25") + ballot.getlOffsetY());
		
		ballot.setAdminBoxW( getLongTypeFromProperty("AdminBoxW", "20") );
		ballot.setAdminBoxH( getLongTypeFromProperty("AdminBoxH", "20") );
		ballot.setAdminBoxX( getLongTypeFromProperty("AdminBoxX", "10") );
		ballot.setAdminBoxY( getLongTypeFromProperty("AdminBoxY", "10") );
		ballot.setAdminBoxY(ballot.getAdminBoxY() + ballot.getlOffsetY());

		ballot.setCutLineW( getLongTypeFromProperty("CutLineW", "55") );
		ballot.setCutLineH( getLongTypeFromProperty("CutLineH", "20") );
		
		ballot.setBoxX( getLongTypeFromProperty("BoxX", "40") + ballot.getlOffsetX() );
		ballot.setBoxY( getLongTypeFromProperty("BoxY", "25") + ballot.getlOffsetY() );
		
		ballot.setBoxW1( getLongTypeFromProperty("BoxW1", "35") );
		ballot.setBoxW2( getLongTypeFromProperty("BoxW2", "15") );
		
		ballot.setBoxH( getLongTypeFromProperty("BoxH", "15") );
		ballot.setBoxHalfH( (long) (ballot.getBoxH() / 2) );
		ballot.setBoxTotalH( (long) (ballot.getBoxH() * ballot.getmHbjnum()) );
		ballot.setBoxTotalW( (long) (ballot.getBoxW1() + ballot.getBoxW2() ) );
		
		//절취선 관련
		ballot.setCutLineP1X( (long) ballot.getBallotW() - ballot.getCutLineW() + ballot.getlOffsetX() );
		ballot.setCutLineP1Y( (long) (ballot.getlOffsetY() ));
		ballot.setCutLineP2X( (long) (ballot.getBallotW() + ballot.getlOffsetX() ));
		ballot.setCutLineP2Y( (long) (ballot.getCutLineH() )+ ballot.getlOffsetY() );
		
		if(ballot.getmHbjnum() > 8){
			ballot.setTitleY(ballot.getTitleY() + (ballot.getmHbjnum() - 8) * 7);
			ballot.setSubTitleY(ballot.getSubTitleY() + (ballot.getmHbjnum() - 8) * 7);
			ballot.setStampY(ballot.getStampY() + (ballot.getmHbjnum() - 8) * 7);
		}
	}
	
	public Label getLabel(){
		return this.label;
	}
	public Ballot getBallot(){
		return this.ballot;
	}
	
	public long getLongTypeFromProperty(String name, String defaultValue){
		//Properties로부터 받은 String값을 long타입으로 변환하여 return한다.
		return Long.parseLong(properties.getProperty(name, defaultValue));
	}
}
