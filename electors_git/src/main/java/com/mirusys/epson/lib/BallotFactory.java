package com.mirusys.epson.lib;

import com.google.zxing.BarcodeFormat;
import com.mirusys.epson.dto.Ballot;
import com.mirusys.epson.dto.Candidate;
import com.mirusys.epson.dto.Label;
import com.mirusys.epson.properties.InitFromProperties;

import android.content.Context;

public class BallotFactory {
	Context context;
	int ballotType;
	int candidateNum;
	Ballot ballot;
	Label label;

	public BallotFactory(Context context, int ballotType, int candidateNum) {
		this.context = context;
		this.ballotType = ballotType;
		this.candidateNum = candidateNum;
	}

	public void initBallot(String titleName, String subTitleName, Candidate[] candidate, int ballotColor, int fillType, boolean isQrCode) {
		InitFromProperties prop = new InitFromProperties(context, ballotType, candidateNum);

		ballot = prop.getBallot();

		ballot.putTitleName(titleName)
			  .putSubTitleName(subTitleName)
			  .putCandidate(candidate)
			  .putBoxColor(ballotColor)
			  .putFillType(fillType);
		
		if (isQrCode) {
			ballot.putUseModeQRCode(true);
			ballot.putUseModeSerial(false);
		} else {
			ballot.putUseModeQRCode(false);
			ballot.putUseModeSerial(true);
		}
	}
	
	public void initLabel(String barcodeNumSection1, String barcodeNumSection2, BarcodeFormat barcodeFormat, boolean isBasedElection, boolean isPostRegistered, boolean isProvisional){
		InitFromProperties prop = new InitFromProperties(context, ballotType, candidateNum);
		
		label = prop.getLabel();
		
		
		label.putBarcodeNum_Sec1(barcodeNumSection1);
		label.putBarcodeNum_Sec2(barcodeNumSection2);
		label.putBarcodeType(barcodeFormat);
		label.putBasedElection(isBasedElection);	//기초선거구
		label.putPostRegistered(isPostRegistered);	//우체국등기
		label.putProvisional(isProvisional);	//잠정투표
	}

}
