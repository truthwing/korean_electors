package com.mirusys.epson;

import java.util.ArrayList;
import java.util.Timer;

import com.epson.tmcprint.Print;
import com.epson.tmcprint.Status;
import com.epson.tmcprint.TmcException;
import com.haesin.entities.PrintIntentObject;
import com.mirusys.electors.ResourceFactory;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.epson.common.CommonVal;
import com.mirusys.epson.draw.DrawBallotManager;
import com.mirusys.epson.dto.Ballot;
import com.mirusys.epson.dto.Candidate;
import com.mirusys.epson.print.PrintManager;
import com.mirusys.epson.properties.InitFromProperties;

import android.content.Context;

public class BallotPrint {
	private static final String TAG = "BallotPrint";
	private Timer mTimer;
	private ResourceFactory mFactory;
	private DrawBallotManager drawBallotManager;
	
	private byte[] stamp, adminstamp;
	private int[] ballotType, candidateNum, ballotColor, fillType, startSerialNum, endSerialNum;
	private int mNowNo;
	private boolean[] isSerial;
	private boolean mIsOpen;
	private Print mPrint = null;
	private String[] titleName, subTitleName;
	private Candidate[] candidates;
	
	public BallotPrint(Context context){
		this.mFactory = (ResourceFactory) context;
		if(mPrint == null)
			mPrint = PrintManager.getPrint(mFactory);
	}
	
	public boolean isOpen() {
		return mIsOpen;
	}

	@SuppressWarnings("finally")
	public int getPrintStatus() {
		int sts = 0;
		try {
			if (mPrint == null)
				mPrint = PrintManager.getPrint(mFactory);
			if (!mIsOpen) {
				mPrint.openPrinter();
				mIsOpen = true;
			}
			Status status = mPrint.getPrinterStatus();
			Log.i(TAG, "PrintStatus" + status.toString());
			if (status == Status.ST_READY) {
				sts = ResourceFactory.PRINT_READY;
			} else
				sts = ResourceFactory.PRINT_BUSY;
		} catch (Exception e) {
			sts = ResourceFactory.PRINT_BUSY;
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
		} finally {
			return sts;
		}
	};
	
	public boolean printClose(){
		if (mIsOpen) {
			mIsOpen = false;
			mPrint.closePrinter();
			Log.i(TAG, "print close ...");
		}
		return !mIsOpen;
	}
	
	public boolean printBallot(ArrayList<PrintIntentObject> arrPrintObj){
		CommonVal.doPrint = true;
		final int ballotCount = arrPrintObj.size();
		ArrayList<Candidate[]> candidatesList = new ArrayList<Candidate[]>();
		
		if(arrPrintObj == null){
			return false;
		}else if(ballotCount == 0){
			return false;
		}else if(ballotCount >= 1){
			startSerialNum = new int[ballotCount];
			endSerialNum = new int[ballotCount];
			titleName = new String[ballotCount];
			subTitleName = new String[ballotCount];
			ballotType = new int[ballotCount];
			ballotColor = new int[ballotCount];
			candidateNum = new int[ballotCount];
			fillType = new int[ballotCount];
			isSerial = new boolean[ballotCount];
			for (int i = 0; i < ballotCount; i++) {
				startSerialNum[i] = arrPrintObj.get(i).getStartSerialNum();
				endSerialNum[i] = arrPrintObj.get(i).getEndSerialNum();
				titleName[i] = arrPrintObj.get(i).getTitleName();
				subTitleName[i] = arrPrintObj.get(i).getSubTitleName();
				ballotType[i] = arrPrintObj.get(i).getBallotType();
				ballotColor[i] = arrPrintObj.get(i).getBallotColor();
				candidateNum[i] = arrPrintObj.get(i).getCandidateNum();
				fillType[i] = arrPrintObj.get(i).getFillType();
				isSerial[i] = arrPrintObj.get(i).isSerial();
				candidatesList.add(arrPrintObj.get(i).getCandidate());
				Log.d(TAG, "start serial : "+startSerialNum[i]+"  end serial : "+endSerialNum[i]);
			}
			stamp = arrPrintObj.get(0).getStamp();
			adminstamp = arrPrintObj.get(0).getAdminstamp();
		}
		
		Ballot[] ballots = new Ballot[ballotCount];
		
		int serial = startSerialNum[0];
		
		for (int i = 0; i < ballotCount; i++) {
			InitFromProperties prop = new InitFromProperties(mFactory, ballotType[i], candidateNum[i]);
			ballots[i] = prop.getBallot();
			ballots[i].putTitleName(titleName[i])
				.putSubTitleName(subTitleName[i])
				.putCandidate(candidatesList.get(i))
				.putBoxColor(ballotColor[i])
				.putFillType(fillType[i])
				.putStamp(stamp)
				.putAdminStamp(adminstamp);
			ballots[i].setSample(titleName[i].equals("테스트 선거")&&subTitleName[i].equals("테스트 선거구"));
			if (isSerial[i]) {
				ballots[i].putUseModeQRCode(false);
				ballots[i].putUseModeSerial(true);
				ballots[i].setUseSerialNum(true);
			} else {
				ballots[i].putUseModeQRCode(false);
				ballots[i].putUseModeSerial(false);
				ballots[i].setUseSerialNum(false);
			}
			if(mPrint == null)
				mPrint = PrintManager.getPrint(mFactory);
			if(!mIsOpen){
				try {
					mPrint.openPrinter();
				} catch (TmcException e) {
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				}
			}
		}

		final Ballot[] finalBallots = ballots;
		
		new Thread(){
			public int now = 0; 

			public void run() {
				try {
					mPrint.startJob();
					for (int i = startSerialNum[0]; (i <= endSerialNum[0]) && CommonVal.doPrint; i++) {
						DrawBallotManager.serialNumber = i;
						for (int j = 0; j < finalBallots.length; j++) {
							drawBallotManager = new DrawBallotManager(mFactory, finalBallots[j], ballotType[j]);
							Ballot.m_HBJNum = candidateNum[j];
							drawBallotManager.drawPaperToBitmap();
							mPrint.printBitmap(drawBallotManager.getBitmap());
							
						} 
					}
					mPrint.endJob();
				}catch (TmcException e) {
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				}finally {
					if(mIsOpen){
						mIsOpen = false;
						mPrint.closePrinter();
						Log.i(TAG, "print end close ...");
					}
				}
			}
		}.start();

		return true;
	}
	
	public void doStop(){
		CommonVal.doPrint = false;
	}
	
	public int getmNowNo() {
		return mNowNo;
	}
}
