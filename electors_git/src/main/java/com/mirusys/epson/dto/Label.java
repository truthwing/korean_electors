package com.mirusys.epson.dto;

import com.google.zxing.BarcodeFormat;

import android.graphics.Color;

public class Label extends Paper{

	int[] lSecX = new int[]{0, 0, 0, 0};
	int[] lSecY = new int[]{0, 30, 60, 90};

	long SectionW = 100;
	long SectionH = 30;
	

	String barcodeNum_sec1;
	String barcodeNum_sec2;
	
	BarcodeFormat barcodeType;
	
	boolean isPostRegistered = false;	//우체국 등기 유무
	boolean isProvisional = false;	//잠정투표 유무
	boolean isBasedElection = false;	//기초선거구 유무
	
	public Label(){
		initLabelType0();
	}
	
	private void initLabelType0(){
		lOffsetX =-2;
		lOffsetY =-2;

		lSecX = new int[]{0, 0, 0, 0};
		lSecY = new int[]{0, 30, 60, 90};

		SectionW = 100;
		SectionH = 30;
		
		barcodeNum_sec1 = "";
		barcodeNum_sec2 = "";
		barcodeType = BarcodeFormat.ITF;
		
		Paper.boxColor = Color.WHITE;
		
	}
	
	public Label putBarcodeNum_Sec1(String barcodeNum1){
		this.barcodeNum_sec1 = barcodeNum1;
		return this;
	}
	
	public Label putBarcodeNum_Sec2(String barcodeNum2){
		this.barcodeNum_sec2 = barcodeNum2;
		return this;
	}
	
	public String getBarcodeNum_sec1() {
		return barcodeNum_sec1;
	}

	public String getBarcodeNum_sec2() {
		return barcodeNum_sec2;
	}

	
	public long getlOffsetX() {
		return lOffsetX;
	}

	public void setlOffsetX(long lOffsetX) {
		this.lOffsetX = lOffsetX;
	}

	public long getlOffsetY() {
		return lOffsetY;
	}

	public void setlOffsetY(long lOffsetY) {
		this.lOffsetY = lOffsetY;
	}

	public int[] getlSecX() {
		return lSecX;
	}

	public void setlSecX(int[] lSecX) {
		this.lSecX = lSecX;
	}

	public int[] getlSecY() {
		return lSecY;
	}

	public void setlSecY(int[] lSecY) {
		this.lSecY = lSecY;
	}

	public long getSectionW() {
		return SectionW;
	}

	public void setSectionW(long sectionW) {
		SectionW = sectionW;
	}

	public long getSectionH() {
		return SectionH;
	}

	public void setSectionH(long sectionH) {
		SectionH = sectionH;
	}

	public boolean isPostRegistered() {
		return isPostRegistered;
	}

	public void putPostRegistered(boolean isPostRegistered) {
		this.isPostRegistered = isPostRegistered;
	}

	public boolean isProvisional() {
		return isProvisional;
	}

	public void putProvisional(boolean isProvisional) {
		this.isProvisional = isProvisional;
	}

	public boolean isBasedElection() {
		return isBasedElection;
	}

	public void putBasedElection(boolean isBasedElection) {
		this.isBasedElection = isBasedElection;
	}

	public BarcodeFormat getBarcodeType() {
		return barcodeType;
	}

	public void putBarcodeType(BarcodeFormat barcodeType) {
		this.barcodeType = barcodeType;
	}
}
