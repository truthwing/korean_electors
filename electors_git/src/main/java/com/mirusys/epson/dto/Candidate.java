package com.mirusys.epson.dto;

import java.io.Serializable;

public class Candidate implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String giho;
	private String jdName;
	private String hbjName;
	private String hbjHanja;
	private String id;
	
	public Candidate() {
		// TODO Auto-generated constructor stub
	}
	public Candidate(String giho, String jdName, String hbjName,
			String hbjHanja, String hbjStatus, String id) {
		super();
		this.giho = giho;
		this.jdName = jdName;
		this.hbjName = hbjName;
		this.hbjHanja = hbjHanja;
		this.hbjStatus = hbjStatus;
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGiho() {
		return giho;
	}
	public void setGiho(String giho) {
		this.giho = giho;
	}
	public String getJdName() {
		return jdName;
	}
	public void setJdName(String jdName) {
		this.jdName = jdName;
	}
	public String getHbjName() {
		return hbjName;
	}
	public void setHbjName(String hbjName) {
		this.hbjName = hbjName;
	}
	public String getHbjHanja() {
		return hbjHanja;
	}
	public void setHbjHanja(String hbjHanja) {
		this.hbjHanja = hbjHanja;
	}
	public String getHbjStatus() {
		return hbjStatus;
	}
	public void setHbjStatus(String hbjStatus) {
		this.hbjStatus = hbjStatus;
	}
	private String hbjStatus;
	
}
