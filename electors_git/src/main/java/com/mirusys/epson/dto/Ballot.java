package com.mirusys.epson.dto;

import com.mirusys.epson.common.CommonVal;
import com.mirusys.epson.common.type.BallotType;
import com.mirusys.epson.common.type.FillType;

import android.graphics.Typeface;

public class Ballot extends Paper{
	public static int m_HBJNum = 2; //후보자 수
//	private int m_HBJNum;
	private Typeface typeface;
	public Typeface getTypeface() {
		return typeface;
	}

	public void setTypeface(Typeface typeface) {
		this.typeface = typeface;
	}

	/**************************투표용지 정보*******************************/
	public String titleName = "테스트용 선거명";
	private String subTitleName = "테스트용 선거구";
	private Candidate[] candidate = new Candidate[]{};
	
	/******************************************************************/
	
	/****************Serial And QR_CODE OPTION**************************/
	private boolean useModeSerial;
	private boolean useModeQRCode;
	private int typeQRCode;
	private boolean useSerialNum;
	
	
	private boolean isSample = false;
	

	public boolean isSample() {
		return isSample;
	}

	public void setSample(boolean isSample) {
		this.isSample = isSample;
	}

	/*******************************************************************/
	
	private byte[] stamp;
	private byte[] adminStamp;
	
	private int ballotType;
	private int boxColor;

	private long TitleX; // 투표지 좌측 기준
	private long TitleY; // 투표지 상단 기준
	private long TitleW;
	private long TitleH;

	private long SubTitleX; // 투표지 좌측 기준
	private long SubTitleY; // 투표지 상단 기준
	private long SubTitleW;
	private long SubTitleH;

	private long BarcodeX; // 투표지 좌측 기준
	private long BarcodeY; // 투표지 상단 기준

	private long StampX; // 투표지 우측 기준
	private long StampY; // 투표지 상단 기준
	private long StampW;
	private long StampH;

	private long AdminBoxX; // 투표지 우측 기준
	private long AdminBoxY; // 투표지 하단 기준
	private long AdminBoxW;
	private long AdminBoxH; // 선거관리관 글씨 부분 제외한 높이(도장 찍는 부분만)

	private long CutLineP1X;
	private long CutLineP1Y;
	private long CutLineP2X;
	private long CutLineP2Y;
	private long CutLineW;
	private long CutLineH;

	private long BoxX; // 투표지 좌측 기준
	private long BoxY; // 청인 이미지 하단 기준

	private long BoxS;

	//교육감만 적용
	private long SampleTextX;
	private long SampleTextY;
	private long SampleTextW;
	private long SampleTextH;
	private long BoxW1;
	private long BoxW2;
	private long BoxW3;
	private long BoxW4;
	private long BoxH; // 기표란 한 칸 높이
	private long BoxHalfH; // 기표란 반 칸 높이(한자병기)
	private long BoxTotalH; // 기표란 전체 높이
	private long BoxTotalW; // 기표란 전체 폭


	private long BallotW;
	private long BallotH = CommonVal.lBallotLength[this.m_HBJNum];

	private long ColorBoxW;
	private long ColorBoxH;

	private int iColorR;
	private int iColorG;
	private int iColorB;

//	private int m_FloatMargin;
//	private int m_nMargin;
	private int m_iPaperWidth;
	private int m_iPaperHeight;
	private int m_Xpage;
	private int m_Ypage;
	private int m_iPrtType;
	private int m_cnt;
	private int BitPerPixel;
	private int m_PclCreateCnt;
	private int m_PclReleaseCnt;

	private long boxSpace;	//2015.09.01 후보자 박스 간격 추가
	public long getBoxSpace() {
		return boxSpace;
	}

	public void setBoxSpace(long boxSpace) {
		this.boxSpace = boxSpace;
	}

	private long YN_Offset; //교육감 전용

	/*********Label관련***********/
	int[] lSecX;
	int[] lSecY;
	int SectionW;
	int SectionH;
	/****************************/
	
	public Ballot() {
		// TODO Auto-generated constructor stub
	}
	
	//Ballot 생성자
	public Ballot(int ballotType) {
		this.ballotType = ballotType;
		
		initCommon();	//공통사항 초기화
		
		switch(ballotType){
		
		case BallotType.BALLOT_TYPE0 :	//국회의원
			initBallotType0();
			break;
		case BallotType.BALLOT_TYPE1 :	//비례대표
			initBallotType1();
			break;
		case BallotType.BALLOT_TYPE4 :	//주민소환
			initBallotType4();
			break;
		case BallotType.BALLOT_TYPE12 :	//교육감(가로)
			initBallotType12();
			break;
		case BallotType.BALLOT_TYPE_LABEL :	//라벨
//			initLabelType0();
			break;
		}
	}

	public void initCommon(){
		/******************************************************/
		// 20130205 소숫점 미세 조정을 위해 추가함
		//m_FloatMargin = (int)((float)lXOffsetX10/254.0f*m_dpiX);
//		m_nMargin = 0; // 1;// mm left/right margin
		m_iPaperWidth = 100;
		m_iPaperHeight = 800;
		m_Xpage = 0;
		m_Ypage = 0;
		m_iPrtType = 1;
		m_cnt = 0;
		BitPerPixel = 0;
		m_PclCreateCnt = 0;
		m_PclReleaseCnt = 0;
	}
	
	//국회의원 지역구 초기화
	public void initBallotType0(){
		lOffsetX = -2; // m_lOffsetX;
		lOffsetY = -2; // m_lOffsetY;

		TitleX = 15; // 투표지 좌측 기준
		TitleY = 17; // 투표지 상단 기준
		TitleW = 70;
		TitleH = 12;
		
		SubTitleX = 18; // 투표지 좌측 기준
		SubTitleY = 27; // 투표지 상단 기준
		SubTitleW = 65;
		SubTitleH = 6;
		
		BarcodeX = 5; // 투표지 좌측 기준
		BarcodeY = 34; // 투표지 상단 기준
		
		StampX = 10; // 투표지 우측 기준
		StampY = 10; // 투표지 상단 기준
		StampW = 25;
		StampH = 25;
		
		AdminBoxX = 15; // 투표지 우측 기준
		AdminBoxY = 10; // 투표지 하단 기준
		AdminBoxW = 25;
		AdminBoxH = 25; // 선거관리관 글씨 부분 제외한 높이(도장 찍는 부분만)
		
		CutLineP1X = 0;
		CutLineP1Y = 0;
		CutLineP2X = 0;
		CutLineP2Y = 0;
		CutLineW = 55;
		CutLineH = 20;
		
		BoxX = 5; // 투표지 좌측 기준
		BoxY = 10; // 청인 이미지 하단 기준
		BoxW1 = 10;
		BoxW2 = 35;
		BoxW3 = 30;
		BoxW4 = 15;
		BoxH = 15; // 기표란 한 칸 높이
		BoxHalfH = 7; // 기표란 반 칸 높이(한자병기)
		BoxTotalH = 0; // 기표란 전체 높이
		BoxTotalW = 90; // 기표란 전체 폭
		BallotW = 100;
		BallotH = CommonVal.lBallotLength[this.m_HBJNum];

		ColorBoxW = 0;
		ColorBoxH = 0;
	}

	//국회의원 비례대표 초기화
	public void initBallotType1(){
		lOffsetX=		-2;
		lOffsetY=		-2;

		TitleX=		15;	// 투표지 좌측 기준
		TitleY=		17;	// 투표지 상단 기준
		TitleW=		70;
		TitleH=		12;

		SubTitleX=		18;	// 투표지 좌측 기준
		SubTitleY=		27;	// 투표지 상단 기준
		SubTitleW=		65;
		SubTitleH=		6;

		BarcodeX=		5;	// 투표지 좌측 기준
		BarcodeY=		34;	// 투표지 상단 기준

		StampX=		10;	// 투표지 우측 	기준
		StampY=		10;	// 투표지 상단 	기준
		StampW=		25;
		StampH=		25;

		AdminBoxX=		15;	// 투표지 우측 	기준
		AdminBoxY=		10;	// 투표지 하단 	기준
		AdminBoxW=		25;
		AdminBoxH=		25;	// 선거관리관 글씨 부분 제외한 높이(도장 찍는 부분만)

		CutLineP1X=	0;
		CutLineP1Y=	0;
		CutLineP2X=	0;
		CutLineP2Y=	0;
		CutLineW=		55;
		CutLineH=		20;

		BoxX=			5;	// 투표지 좌측 기준
		BoxY=			10;	// 청인 이미지 하단 기준
		BoxW1=			10;
		BoxW2=			35;
		BoxW3=			30;
		BoxW4=			15;

		BoxH=			15;	// 기표란 한 칸 높이
		BoxHalfH=		7;	// 기표란 반 칸 높이(한자병기)

		BoxTotalH=		0;	// 기표란 전체 높이
		BoxTotalW=		90;	// 기표란 전체 폭

		BallotW=		100;
		BallotH=		CommonVal.lBallotLength[m_HBJNum];

		ColorBoxW=		0;
		ColorBoxH=		0;

	}

	//주민소환투표 초기화
	public void initBallotType4(){
		lOffsetX =-2;
		lOffsetY =-2;

		//투표용지 전체간격
		BallotW = 100;
		BallotH = 165;
		ColorBoxW = 0;
		ColorBoxH = 0;

		//타이틀 좌표
		TitleX = 10;	// 투표지 좌측 기준
		TitleY = 17;	// 투표지 상단 기준
		TitleW = 73;
		TitleH = 12;

		//서브타이트 좌표
		SubTitleX = 10;	// 투표지 좌측 기준
		SubTitleY = 27;	// 투표지 상단 기준
		SubTitleW = 55;
		SubTitleH = 6;

		BarcodeX =	5;	// 투표지 좌측 기준
		BarcodeY =	34;	// 투표지 상단 기준

		//청인이미지 좌표
		StampX = 10;	// 투표지 우측 	기준
		StampY = 10;	// 투표지 상단 	기준
		StampW = 25;
		StampH = 25;

		//투표관리관 좌표
		AdminBoxX = 15;	// 투표지 우측 	기준
		AdminBoxY = 10;	// 투표지 하단 	기준
		AdminBoxW = 25;
		AdminBoxH = 25;	// 선거관리관 글씨 부분 제외한 높이(도장 찍는 부분만)

		CutLineP1X = 0;
		CutLineP1Y = 0;
		CutLineP2X = 0;
		CutLineP2Y = 0;
		CutLineW =	55;
		CutLineH =	20;

		BoxX =	10;	// 투표지 좌측 기준
		BoxY =	40;	// 청인 이미지 하단 기준
		BoxW1 = 60;
		BoxW2 = 20;

		BoxH =	15;	// 기표란 한 칸 높이
		BoxS =	10;	// 기표란 간의 간격

		/******************************************************/
		// 20130205 소숫점 미세 조정을 위해 추가함
//		m_FloatMargin = 0; // 7이 1mm를 의미한다. (1/25.4)*180 = 7--> 1이 0.14mm
		//m_FloatMargin = (int)((float)lXOffsetX10/254.0f*m_dpiX);
//		m_nMargin = 0; // 1;// mm left/right margin
		m_iPaperWidth = 100;
		m_iPaperHeight = 800;
		m_Xpage = 0;
		m_Ypage = 0;
		m_iPrtType = 1;
		m_cnt = 0;
		BitPerPixel = 0;
		m_PclCreateCnt = 0;
		m_PclReleaseCnt = 0;
	}

	public void initBallotType5() {
		// TODO Auto-generated method stub
	}

	//교육감 가로 초기화
	public void initBallotType12(){
		lOffsetX = -2;
		lOffsetY = -1;

		TitleX = 15;	// 투표지 좌측 기준
		TitleY = 17;	// 투표지 상단 기준
		TitleW = 70;
		TitleH = 12;

		SubTitleX =	18;	// 투표지 좌측 기준
		SubTitleY =	27;	// 투표지 상단 기준
		SubTitleW =	65;
		SubTitleH =	6;

		BarcodeX = 5;	// 투표지 좌측 기준
		BarcodeY = 34;	// 투표지 상단 기준

		StampX = 10;	// 투표지 우측 	기준
		StampY = 10;	// 투표지 상단 	기준
		StampW = 25;
		StampH = 25;

		AdminBoxX =	10;	// 투표지 우측 	기준
		AdminBoxY =	20;	
		AdminBoxW =	25;
		AdminBoxH =	25;	// 선거관리관 글씨 부분 제외한 높이(도장 찍는 부분만)

		CutLineP1X = 0;
		CutLineP1Y = 0;
		CutLineP2X = 0;
		CutLineP2Y = 0;

		CutLineW = 55;
		CutLineH = 20;

		BoxX = 5;	// 투표지 좌측 기준
		BoxY = 10;	// 청인 이미지 하단 기준

		BoxW1 = 10;
		BoxW2 = 35;
		BoxW3 = 30;
		BoxW4 = 15;

		BoxH = 15;	// 기표란 한 칸 높이
		BoxHalfH = 7;	// 기표란 반 칸 높이(한자병기)
		BoxTotalH =	0;	// 기표란 전체 높이
		BoxTotalW =	90;	// 기표란 전체 폭

		BallotW = 100;
		BallotH = CommonVal.lBallotLength[m_HBJNum] + 10;

		ColorBoxW =	0;
		ColorBoxH =	0;

		SampleTextX = 0;
		SampleTextY = 0;
		SampleTextW = 0;
		SampleTextH = 0;

		YN_Offset = 0;
	}

//	//라벨 초기화
//	public void initLabelType0(){
//		lOffsetX =-2;
//		lOffsetY =-2;
//
//		lSecX = new int[]{0, 0, 0, 0};
//		lSecY = new int[]{0, 30, 60, 90};
//
//		SectionW = 100;
//		SectionH = 30;
//	}

	//박스 채우기 타입 설정
	public void setFillType(int fillType) {

		switch(fillType){

		case FillType.FILL_TOP :
			this.ColorBoxW = BallotW;
			this.ColorBoxH = (ballotType == BallotType.BALLOT_TYPE12 ? 10 : 5);	//투표용지가 교육감일 때는 높이 값이 달라진다.
			break;
		case FillType.FILL_FULL :
			this.ColorBoxW = BallotW;
			this.ColorBoxH = BallotH;
			break;
		default :
			this.ColorBoxW = 0;
			this.ColorBoxH = 0;
			break;
		}
	}

	//setter getter
	public long getlOffsetX() {
		return lOffsetX;
	}

	public void setlOffsetX(long lOffsetX) {
		this.lOffsetX = lOffsetX;
	}

	public long getlOffsetY() {
		return lOffsetY;
	}

	public void setlOffsetY(long lOffsetY) {
		this.lOffsetY = lOffsetY;
	}

	public long getTitleX() {
		return TitleX;
	}

	public void setTitleX(long titleX) {
		TitleX = titleX;
	}

	public long getTitleY() {
		return TitleY;
	}

	public void setTitleY(long titleY) {
		TitleY = titleY;
	}

	public long getTitleW() {
		return TitleW;
	}

	public void setTitleW(long titleW) {
		TitleW = titleW;
	}

	public long getTitleH() {
		return TitleH;
	}

	public void setTitleH(long titleH) {
		TitleH = titleH;
	}

	public long getSubTitleX() {
		return SubTitleX;
	}

	public void setSubTitleX(long subTitleX) {
		SubTitleX = subTitleX;
	}

	public long getSubTitleY() {
		return SubTitleY;
	}

	public void setSubTitleY(long subTitleY) {
		SubTitleY = subTitleY;
	}

	public long getSubTitleW() {
		return SubTitleW;
	}

	public void setSubTitleW(long subTitleW) {
		SubTitleW = subTitleW;
	}

	public long getSubTitleH() {
		return SubTitleH;
	}

	public void setSubTitleH(long subTitleH) {
		SubTitleH = subTitleH;
	}

	public long getBarcodeX() {
		return BarcodeX;
	}

	public void setBarcodeX(long barcodeX) {
		BarcodeX = barcodeX;
	}

	public long getBarcodeY() {
		return BarcodeY;
	}

	public void setBarcodeY(long barcodeY) {
		BarcodeY = barcodeY;
	}

	public long getStampX() {
		return StampX;
	}

	public void setStampX(long stampX) {
		StampX = stampX;
	}

	public long getStampY() {
		return StampY;
	}

	public void setStampY(long stampY) {
		StampY = stampY;
	}

	public long getStampW() {
		return StampW;
	}

	public void setStampW(long stampW) {
		StampW = stampW;
	}

	public long getStampH() {
		return StampH;
	}

	public void setStampH(long stampH) {
		StampH = stampH;
	}

	public long getAdminBoxX() {
		return AdminBoxX;
	}

	public void setAdminBoxX(long adminBoxX) {
		AdminBoxX = adminBoxX;
	}

	public long getAdminBoxY() {
		return AdminBoxY;
	}

	public void setAdminBoxY(long adminBoxY) {
		AdminBoxY = adminBoxY;
	}

	public long getAdminBoxW() {
		return AdminBoxW;
	}

	public void setAdminBoxW(long adminBoxW) {
		AdminBoxW = adminBoxW;
	}

	public long getAdminBoxH() {
		return AdminBoxH;
	}

	public void setAdminBoxH(long adminBoxH) {
		AdminBoxH = adminBoxH;
	}

	public long getCutLineP1X() {
		return CutLineP1X;
	}

	public void setCutLineP1X(long cutLineP1X) {
		CutLineP1X = cutLineP1X;
	}

	public long getCutLineP1Y() {
		return CutLineP1Y;
	}

	public void setCutLineP1Y(long cutLineP1Y) {
		CutLineP1Y = cutLineP1Y;
	}

	public long getCutLineP2X() {
		return CutLineP2X;
	}

	public void setCutLineP2X(long cutLineP2X) {
		CutLineP2X = cutLineP2X;
	}

	public long getCutLineP2Y() {
		return CutLineP2Y;
	}

	public void setCutLineP2Y(long cutLineP2Y) {
		CutLineP2Y = cutLineP2Y;
	}

	public long getCutLineW() {
		return CutLineW;
	}

	public void setCutLineW(long cutLineW) {
		CutLineW = cutLineW;
	}

	public long getCutLineH() {
		return CutLineH;
	}

	public void setCutLineH(long cutLineH) {
		CutLineH = cutLineH;
	}

	public long getBoxX() {
		return BoxX;
	}

	public void setBoxX(long boxX) {
		BoxX = boxX;
	}

	public long getBoxY() {
		return BoxY;
	}

	public void setBoxY(long boxY) {
		BoxY = boxY;
	}

	public long getBoxW1() {
		return BoxW1;
	}

	public void setBoxW1(long boxW1) {
		BoxW1 = boxW1;
	}

	public long getBoxW2() {
		return BoxW2;
	}

	public void setBoxW2(long boxW2) {
		BoxW2 = boxW2;
	}

	public long getBoxW3() {
		return BoxW3;
	}

	public void setBoxW3(long boxW3) {
		BoxW3 = boxW3;
	}

	public long getBoxW4() {
		return BoxW4;
	}

	public void setBoxW4(long boxW4) {
		BoxW4 = boxW4;
	}

	public long getBoxH() {
		return BoxH;
	}

	public void setBoxH(long boxH) {
		BoxH = boxH;
	}

	public long getBoxHalfH() {
		return BoxHalfH;
	}

	public void setBoxHalfH(long boxHalfH) {
		BoxHalfH = boxHalfH;
	}

	public long getBoxTotalH() {
		return BoxTotalH;
	}

	public void setBoxTotalH(long boxTotalH) {
		BoxTotalH = boxTotalH;
	}

	public long getBoxTotalW() {
		return BoxTotalW;
	}

	public void setBoxTotalW(long boxTotalW) {
		BoxTotalW = boxTotalW;
	}

	public long getBallotW() {
		return BallotW;
	}

	public void setBallotW(long ballotW) {
		BallotW = ballotW;
	}

	public long getBallotH() {
		return BallotH;
	}

	public void setBallotH(long ballotH) {
		BallotH = ballotH;
	}

	public long getColorBoxW() {
		return ColorBoxW;
	}

	public void setColorBoxW(long colorBoxW) {
		ColorBoxW = colorBoxW;
	}

	public long getColorBoxH() {
		return ColorBoxH;
	}

	public void setColorBoxH(long colorBoxH) {
		ColorBoxH = colorBoxH;
	}

	public int getiColorR() {
		return iColorR;
	}

	public void setiColorR(int iColorR) {
		this.iColorR = iColorR;
	}

	public int getiColorG() {
		return iColorG;
	}

	public void setiColorG(int iColorG) {
		this.iColorG = iColorG;
	}

	public int getiColorB() {
		return iColorB;
	}

	public void setiColorB(int iColorB) {
		this.iColorB = iColorB;
	}

//
//
//	public int getM_FloatMargin() {
//		return m_FloatMargin;
//	}
//
//	public void setM_FloatMargin(int m_FloatMargin) {
//		this.m_FloatMargin = m_FloatMargin;
//	}
//
//	public int getM_nMargin() {
//		return m_nMargin;
//	}
//
//	public void setM_nMargin(int m_nMargin) {
//		this.m_nMargin = m_nMargin;
//	}

	public int getM_iPaperWidth() {
		return m_iPaperWidth;
	}

	public void setM_iPaperWidth(int m_iPaperWidth) {
		this.m_iPaperWidth = m_iPaperWidth;
	}

	public int getM_iPaperHeight() {
		return m_iPaperHeight;
	}

	public void setM_iPaperHeight(int m_iPaperHeight) {
		this.m_iPaperHeight = m_iPaperHeight;
	}

	public int getM_Xpage() {
		return m_Xpage;
	}

	public void setM_Xpage(int m_Xpage) {
		this.m_Xpage = m_Xpage;
	}

	public int getM_Ypage() {
		return m_Ypage;
	}

	public void setM_Ypage(int m_Ypage) {
		this.m_Ypage = m_Ypage;
	}

	public int getM_dpiX() {
		return m_dpiX;
	}

	public int getM_dpiY() {
		return m_dpiY;
	}

	public int getM_iPrtType() {
		return m_iPrtType;
	}

	public void setM_iPrtType(int m_iPrtType) {
		this.m_iPrtType = m_iPrtType;
	}

	public int getM_cnt() {
		return m_cnt;
	}

	public void setM_cnt(int m_cnt) {
		this.m_cnt = m_cnt;
	}

	public int getBitPerPixel() {
		return BitPerPixel;
	}

	public void setBitPerPixel(int bitPerPixel) {
		BitPerPixel = bitPerPixel;
	}

	public int getM_PclCreateCnt() {
		return m_PclCreateCnt;
	}

	public void setM_PclCreateCnt(int m_PclCreateCnt) {
		this.m_PclCreateCnt = m_PclCreateCnt;
	}

	public int getM_PclReleaseCnt() {
		return m_PclReleaseCnt;
	}

	public void setM_PclReleaseCnt(int m_PclReleaseCnt) {
		this.m_PclReleaseCnt = m_PclReleaseCnt;
	}

	public void setmHbjNum(int num){
		this.m_HBJNum = num;
	}
	public int getmHbjnum() {
		return m_HBJNum;
	}

	public int getBoxColor(){
		return boxColor;
	}

	//컬러 타입 설정
	public void setBoxColor(int boxColor) {
		this.boxColor = boxColor;
	}
	
	public long getYN_Offset() {
		return YN_Offset;
	}

	public void setYN_Offset(long yN_Offset) {
		YN_Offset = yN_Offset;
	}

	public int[] getlSecX() {
		return lSecX;
	}

	public void setlSecX(int[] lSecX) {
		this.lSecX = lSecX;
	}

	public int[] getlSecY() {
		return lSecY;
	}

	public void setlSecY(int[] lSecY) {
		this.lSecY = lSecY;
	}

	public long getSectionW() {
		return SectionW;
	}

	public void setSectionW(int sectionW) {
		SectionW = sectionW;
	}

	public long getSectionH() {
		return SectionH;
	}

	public void setSectionH(int sectionH) {
		SectionH = sectionH;
	}
	

	public long getSampleTextX() {
		return SampleTextX;
	}

	public void setSampleTextX(long sampleTextX) {
		SampleTextX = sampleTextX;
	}

	public long getSampleTextY() {
		return SampleTextY;
	}

	public void setSampleTextY(long sampleTextY) {
		SampleTextY = sampleTextY;
	}

	public long getSampleTextW() {
		return SampleTextW;
	}

	public void setSampleTextW(long sampleTextW) {
		SampleTextW = sampleTextW;
	}

	public long getSampleTextH() {
		return SampleTextH;
	}

	public void setSampleTextH(long sampleTextH) {
		SampleTextH = sampleTextH;
	}

	public long getBoxS() {
		return BoxS;
	}

	public void setBoxS(long boxS) {
		BoxS = boxS;
	}
	
	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public String getSubTitleName() {
		return subTitleName;
	}

	public void setSubTitleName(String subTitleName) {
		this.subTitleName = subTitleName;
	}

	public Candidate[] getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate[] candidate) {
		this.candidate = candidate;
	}

	
	
	/********************QR_Code And SerialNumber*************************/
	public boolean isUseModeSerial() {
		return useModeSerial;
	}

	public void putUseModeSerial(boolean useModeSerial) {
		this.useModeSerial = useModeSerial;
	}

	public boolean isUseModeQRCode() {
		return useModeQRCode;
	}

	public void putUseModeQRCode(boolean useModeQRCode) {
		this.useModeQRCode = useModeQRCode;
	}

	public int getTypeQRCode() {
		return typeQRCode;
	}

	public void putTypeQRCode(int typeQRCode) {
		this.typeQRCode = typeQRCode;
	}

	
	
	/*********************Ballot 변수***************************/
	public Ballot putTitleName(String titleName){
		this.titleName = titleName;
		return this;
	}
	
	public Ballot putSubTitleName(String subTitleName){
		this.subTitleName = subTitleName;
		return this;
	}
	
	public Ballot putBoxColor(int boxColor){
		this.boxColor = boxColor;
		
		Paper.boxColor = boxColor;
		return this;
	}
	
	public Ballot putFillType(int fillType){
		setFillType(fillType);
		return this;
	}

	public Ballot putCandidate(Candidate[] candidate){
		this.candidate = candidate;
		return this;
	}
	
	public Ballot putQRCodeUse(boolean isQRCode){
		this.useModeQRCode = isQRCode;
		return this;
	}
	
	public byte[] getStamp() {
		return stamp;
	}

	public Ballot putStamp(byte[] stamp) {
		this.stamp = stamp;
		return this;
	}

	public byte[] getAdminStamp() {
		return adminStamp;
	}

	public Ballot putAdminStamp(byte[] adminStamp) {
		this.adminStamp = adminStamp;
		return this;
	}
	
	public boolean isUseSerialNum() {
		return useSerialNum;
	}

	public void setUseSerialNum(boolean useSerialNum) {
		this.useSerialNum = useSerialNum;
	}
//	public int getSerialNumber() {
//		return serialNumber;
//	}
//
//	public void setSerialNumber(int serialNumber) {
//		this.serialNumber = serialNumber;
//	}
	
	/*********************************************************************/
}
