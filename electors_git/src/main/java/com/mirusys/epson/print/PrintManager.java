package com.mirusys.epson.print;

import com.epson.tmcprint.LogOption;
import com.epson.tmcprint.LogType;
import com.epson.tmcprint.Print;
import com.epson.tmcprint.Status;
import com.epson.tmcprint.TmcException;

import android.content.Context;
import android.util.Log;

public class PrintManager {
	private static final String TAG = "PrintManager";
	private static Print print;
	
	public static Print getPrint(Context context){
		if(print == null){
			try {
				print = new Print("TM-C3400", context);
				Log.i(TAG, "new Print");
			} catch (Throwable e) {
				Log.e(TAG, "new Print error = " + e.getMessage());
				e.printStackTrace();
			}
		}
		Log.i(TAG, "return print");
		print.setLog(LogType.DEBUG_LEVEL1, context.getFilesDir().getAbsolutePath()+"print.log", LogOption.START);
		return print;
	}
	
	public static boolean printOpen(){
		try {
			if(print != null){
				print.openPrinter();
			}
		} catch (TmcException e) {
			Log.i(TAG, "Print Open error = " + e.getMessage());
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	public static boolean printStart(){
		Status status;
		
		if(print == null){
			Log.w(TAG, "Print Start warning :: print = null");
			return false;
		}
		
		try {
			status = print.getPrinterStatus();

			if(status == Status.ST_READY){
				print.startJob();
			}else{
				Log.w(TAG, "Print Start warning :: status != ST_READY");
				printEnd();
				return false;
			}
		} catch (TmcException e) {
			print.closePrinter();
			e.printStackTrace();
		}
		
		return true;
	}
	
	public static boolean printEnd(){
		Status status;
		
		if(print == null){
			Log.w(TAG, "Print End warning :: print = null");
			return false;
		}

		try {
			print.endJob();
		} catch (TmcException e) {
			e.printStackTrace();
		}
		
		return true;
	}
}
