package com.mirusys.electors;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.haesin.manager.IDScanCommander;
import com.haesin.util.CardBackupDataSQLite;
import com.haesin.util.EditTextWithClearButton;
import com.haesin.util.IPSaveSQLite;
import com.haesin.util.SignatureView;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.voting.MiruSmartCard;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Presentation;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.display.DisplayManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public abstract class BasicActivity extends Activity implements
		ResourceFactory.OnAttachStateListener{
	public final static String TAG = "Basic";
	
	private final static int EVENT_DATETIME = 0;

	final static int TOAST_TIMEOUT = 1500;
	final static int TOAST_INFINITE = 0;
	final static int MSG_NODELAY = 5;
	// event for dismiss (toast)
	private final static int MSG_DEFAULT = 4;
	private final static int MSG_TOAST = 5;
	private final static int MSG_WAIT = 6;
	private final static int MSG_DISMISS = 7;
	private final static int MSG_ENABLE_ELECTORINFO = 8;
	private final static int MSG_SET_ELECTORINFO = 9;
    final static int 			ELECTOR_SEX_MAN = 0;
    final static int 			ELECTOR_SEX_WOMAN = 1;
    final static int			ELECTOR_VOTE_READY = 0;
    final static int			ELECTOR_VOTE_COMPLETED = 1;
    final static int			ELECTOR_WAIT_ISSUE = 2;
    final static int			ELECTOR_PLEASE_VOTE = 3;
	private final static int MSG_ACTIVE_SIGNPAD = 10;
	private final static int MSG_ACTIVE_FINGERPRINT = 11;
	private final static int MSG_UPGRADE_POPUP = 12;
	private final static int MSG_UPGRADE_DOING = 13;
	private String[] USB_DISK = { "/mnt/usb1/sda/sda1", "/mnt/usb1/sda/sda2",
			"/mnt/usb2/sda/sda1","/mnt/usb2/sda/sda2",
			"/mnt/usb3/sda/sda1","/mnt/usb3/sda/sda2", 
			"/mnt/usb4/sda/sda1","/mnt/usb4/sda/sda2"};


	private String usbDirectory;
	public static IPSaveSQLite IPSAVE;
	public static CardBackupDataSQLite CARDSQLITE;
	
	public String getUsbDirectory() {
		return usbDirectory;
	}

	public void setUsbDirectory(String usbDirectory) {
		this.usbDirectory = usbDirectory;
	}

	public boolean usbChecker() {
		boolean ret = false;
		for (int i = 0; i < USB_DISK.length; i++) {
			File f = new File(USB_DISK[i]);
			ret = f.exists();
			usbDirectory = USB_DISK[i];
			if (ret)
				return ret;
		}
		return false;
	}

	private String mApkName;
	private String mUdbName;
	private String mUsbPassword;
	private Dialog mUsbDialog = null;
	private boolean mCardCheckable = false;
	private boolean mCardAttachable = false;

	private Dialog mMessageDialog = null;
	private Dialog mToastDialog = null;
	private Dialog mWaitDialog = null;
	private TextView mWaitTextView = null;
	private TextView mDefaultTextView = null;

	private int mMsgTimeout = TOAST_INFINITE;
	private String mMsgText;
	private String mWaitText;
	private String mToastText;
	private Object mMsgTag;

	protected ResourceFactory mFactory;
	
	protected Presentation mPresentation = null;
	protected View mLayoutElector = null;
    protected TextView mElectorHelper = null;
    private boolean mbActiveSignpad = false;
	private SignatureView mSignpad = null;
	private View mImgFinger = null;
	private View mBtnAccept = null;
	private View mBtnClear = null;
	private TextView mToday = null;

	final static SimpleDateFormat mDateFormat = new SimpleDateFormat(
			"yyyy.MM.dd  a hh:mm", ResourceFactory.getInstance().getResources().getConfiguration().locale);

	private View mElectorConsole = null;

	@Override
	@SuppressLint("InflateParams")
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mFactory = ResourceFactory.getInstance();
		Display[] displays = ((DisplayManager) getSystemService(Context.DISPLAY_SERVICE))
				.getDisplays(DisplayManager.DISPLAY_CATEGORY_PRESENTATION);

		if (displays == null || displays.length < 1)
			mLayoutElector = null;
		else {
			mPresentation = new Presentation(this, displays[0]);

			if ((mLayoutElector = LayoutInflater.from(this).inflate(
					R.layout.elector, null)) != null) {
				if (mBtnAccept != null
						|| (mBtnAccept = mLayoutElector
								.findViewById(R.id.btnAccept)) != null) {
					mBtnAccept.setOnClickListener(mSignListener);
				}
				if (mBtnClear != null
						|| (mBtnClear = mLayoutElector
								.findViewById(R.id.btnClear)) != null) {
					mBtnClear.setOnClickListener(mSignListener);
				}
			}

			setVoteTitle(mFactory.getProperty(ResourceFactory.CONFIG_VTTITLE));
			mPresentation.setContentView(mLayoutElector);
		}

		mBasicHandler.sendEmptyMessage(EVENT_DATETIME); // update datetime
	}

	@Override
	protected void onResume() {
		super.onResume();

		mFactory.setOnAttachStateListener(this);

		if (mPresentation != null) {
			if (mPresentation.isShowing() == false) {
				mPresentation.show();
			}
			activeElectorLayout(false);
		}
	}

	@Override
	protected void onPause() {
		mCardCheckable = false;

		if (mUsbDialog != null && mUsbDialog.isShowing()) {
			mUsbDialog.dismiss();
		}
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		if (mPresentation != null) {
			mPresentation.dismiss();
		}

		super.onDestroy();
	}

	protected void setVoteTitle(String title) {
		if (mLayoutElector != null) {
			((TextView) mLayoutElector.findViewById(R.id.textUserTitle))
					.setText(title);
		}
	}

	@Override
	public void onAttached(MiruSmartCard card) {
		if (mCardCheckable) {
			mCardCheckable = false;
			disposeMessageDelayed(0);

			byte[] cType = new byte[2];

			if (card.ProcGetCardType(cType) == MiruSmartCard.SUCCESS
					&& cType[0] == 'B'){
				mBasicHandler.sendMessage(mBasicHandler.obtainMessage(MSG_UPGRADE_DOING,card.CardVerify(mUsbPassword) == MiruSmartCard.SUCCESS));
			mUsbDialog.findViewById(R.id.btnOkay).setClickable(true);
			}else {
				if(mUsbDialog!=null&&mUsbDialog.isShowing()){
				mCardAttachable = true;
				showDefaultMessage(R.string.error_password_nomaster);
				mUsbDialog.findViewById(R.id.btnOkay).setClickable(false);
				}else{
					mUsbDialog.findViewById(R.id.btnOkay).setClickable(true);
				}
			}
		}
	}

	@Override
	public void onDetached(MiruSmartCard card) {
		if (mCardAttachable) {
			mCardCheckable = true;
			mCardAttachable = false;
		}
	}

	@Override
	public void onIdle(MiruSmartCard card) {
		if (mCardCheckable && mFactory.isCardAttached()) {
			onAttached(card);
		}
	}
	
	boolean canDisplayElector() {
		return mPresentation != null;
	}

	boolean isActiveSignpad() {
		return mbActiveSignpad;
	}

	void activeElectorLayout(boolean enable) {
		mBasicHandler.sendMessageDelayed(
				mBasicHandler.obtainMessage(MSG_ENABLE_ELECTORINFO, enable),
				MSG_NODELAY);
	}

	void setElectorInfo(String strInfo, int iSex, int iHelper) {
		mBasicHandler.sendMessageDelayed(mBasicHandler.obtainMessage(
				MSG_SET_ELECTORINFO, iSex, iHelper, strInfo), MSG_NODELAY);
	}

	void activeFingerprint() {
		mBasicHandler.sendEmptyMessageDelayed(MSG_ACTIVE_FINGERPRINT,
				MSG_NODELAY);
	}

	void activeSignpad() {
		mBasicHandler.sendEmptyMessageDelayed(MSG_ACTIVE_SIGNPAD, MSG_NODELAY);
	}

	public void onSignature(Bitmap bitmap) {
		
	}

	public final void disposeMessageDelayed(int delayMillis) {
		mBasicHandler.sendEmptyMessageDelayed(MSG_DISMISS, delayMillis);
	}

	public void showToastMessage(int msgId, int timeout) {
		showToastMessage(getString(msgId), timeout);
	}

	public void showToastMessage(String message, int timeout) {
		mToastText = message;
		mMsgTimeout = timeout;
		mBasicHandler.sendEmptyMessageDelayed(MSG_TOAST, MSG_NODELAY);
	}

	public void showDefaultMessage(int msgId) {
		showQuestionMessage(getString(msgId), null);
	}

	public void showDefaultMessage(String message) {
		showQuestionMessage(message, null);
	}

	public void showQuestionMessage(int msgId, Object tag) {
		showQuestionMessage(getString(msgId), tag);
	}

	public void showQuestionMessage(String message, Object tag) {
		mMsgTag = tag;
		mMsgText = message;
		mBasicHandler.sendEmptyMessageDelayed(MSG_DEFAULT, MSG_NODELAY);
	}

	public void showWaitMessage(int msgId) {
		showWaitMessage(getString(msgId));
	}

	public void showWaitMessage(String message) {
		mWaitText = message;
		mBasicHandler.sendEmptyMessageDelayed(MSG_WAIT, MSG_NODELAY);
	}
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_HOME:
			Log.d(TAG, "key home");
			return false;
		case KeyEvent.KEYCODE_MOVE_HOME:
			Log.d(TAG, "key home");
			return false;
		case KeyEvent.KEYCODE_ESCAPE:
			Log.d(TAG, "key ESC");
			return false;
		case KeyEvent.KEYCODE_BACK:
			Log.d(TAG, "key BACK");
			return false;
		default:
			
			break;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Log.d(TAG, "BACK press");
	}
	public void dismissedMessageBox() {
		if (mWaitDialog != null && mWaitDialog.isShowing()){
			mWaitDialog.dismiss();
		}
		if (mToastDialog != null && mToastDialog.isShowing()){
			mToastDialog.dismiss();
			}
		if (mMessageDialog != null && mMessageDialog.isShowing()){
			mMessageDialog.cancel();
			}
	}

	void upgrade(boolean verbose) {
		mBasicHandler.sendMessageDelayed(
				mBasicHandler.obtainMessage(MSG_UPGRADE_POPUP, verbose),
				MSG_NODELAY);
	}

	void refreshDateTime(String today) {
		if (mToday != null
				|| mLayoutElector != null
				&& (mToday = (TextView) mLayoutElector
						.findViewById(R.id.textDateTime)) != null) {
			if (today.compareTo(mToday.getText().toString()) != 0) {
				mToday.setText(today);
			}
		}
	}

	protected void onMessageResult(View v, Object tag) {
		mMessageDialog.dismiss();
	}

	private View.OnClickListener mSignListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			v.setClickable(false);

			if (mSignpad != null
					|| (mSignpad = (SignatureView) mLayoutElector
							.findViewById(R.id.viewSignpad)) != null) {
				if (v.getId() == R.id.btnClear)
					((SignatureView) mSignpad).clearCanvas();
				else {
					mBtnClear.setEnabled(false);
					v.setEnabled(false);
					
					Bitmap bitmapOrigin = null;
					Bitmap original = mSignpad.getBitmap();
					try {
						bitmapOrigin = Bitmap.createBitmap(original, 0, 0, original.getWidth(), original.getHeight());
						if (original != bitmapOrigin) {
							original.recycle();
						}
					} catch (OutOfMemoryError e) {
						bitmapOrigin = original;
					}

					onSignature(bitmapOrigin);
					mSignpad.setDrawing(false);
				}
			}

			v.setClickable(true);
		}
	};

	@SuppressLint("HandlerLeak")
	protected Handler mBasicHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			
			case EVENT_DATETIME:
				refreshDateTime(mDateFormat.format(Calendar.getInstance()
						.getTime()));
				sendEmptyMessageDelayed(EVENT_DATETIME, 1000);
				break;

			case MSG_DISMISS:
				dismissedMessageBox();
				break;

			case MSG_TOAST:
				if (mToastDialog == null) {
					mToastDialog = new Dialog(BasicActivity.this);
					mToastDialog.setContentView(R.layout.popup_toast);
					mToastDialog.setCancelable(false);
					mToastDialog
							.setOnShowListener(new DialogInterface.OnShowListener() {
								@Override
								public void onShow(DialogInterface dialog) {
									if (mMsgTimeout != TOAST_INFINITE)
										disposeMessageDelayed(mMsgTimeout);
								}
							});
				}

				((TextView) mToastDialog.findViewById(android.R.id.message))
						.setText(mToastText);

				if (mToastDialog.isShowing() == false) {
					mToastDialog.show();
				}
				break;

			case MSG_DEFAULT:
				if (mMessageDialog == null) {
					mMessageDialog = new Dialog(BasicActivity.this);
					mMessageDialog.setContentView(R.layout.popup_message);
					mDefaultTextView = (TextView) mMessageDialog
							.findViewById(android.R.id.message);
					mMessageDialog.setCancelable(false);

					OnClickListener l = new OnClickListener() {
						@Override
						public void onClick(View v) {
							onMessageResult(v, mMsgTag);
						}
					};

					mMessageDialog.findViewById(android.R.id.button1)
							.setOnClickListener(l);
					mMessageDialog.findViewById(android.R.id.button2)
							.setOnClickListener(l);
				} else if (mMessageDialog.isShowing()) {
					mMessageDialog.dismiss();
					mMessageDialog.cancel();
				}

				((Button) mMessageDialog.findViewById(android.R.id.button1))
						.setText(R.string.ok);
				mMessageDialog.findViewById(android.R.id.button2)
						.setVisibility(
								mMsgTag != null ? View.VISIBLE : View.GONE);
				mDefaultTextView.setText(mMsgText);
				if(mWaitDialog!=null&&mWaitDialog.isShowing())
					mWaitDialog.dismiss();
				mMessageDialog.show();
				break;

			case MSG_WAIT:
				if (mWaitDialog == null) {
					mWaitDialog = new Dialog(BasicActivity.this);
					mWaitDialog.setContentView(R.layout.popup_loading);
					mWaitTextView = (TextView) mWaitDialog
							.findViewById(android.R.id.message);
					mWaitDialog.setCancelable(false);
					mWaitDialog
							.setOnShowListener(new DialogInterface.OnShowListener() {
								public void onShow(DialogInterface d) {
									((AnimationDrawable) mWaitDialog
											.findViewById(R.id.imgLogo)
											.getBackground()).start();
								}
							});
				}

				mWaitTextView.setText(mWaitText);
				
				if (mWaitDialog.isShowing() == false) {
					mWaitDialog.show();
				}
				break;

			case MSG_ENABLE_ELECTORINFO:
				if (mLayoutElector != null) {
					if (mElectorConsole != null
							|| (mElectorConsole = mLayoutElector
									.findViewById(R.id.layoutElectorConsole)) != null) {
						mElectorConsole
								.setVisibility((Boolean) msg.obj ? View.VISIBLE
										: View.GONE);
					}

					if ((Boolean) msg.obj == false) {
						if (mbActiveSignpad) {
							mbActiveSignpad = false;

							if (mSignpad != null) {
								((SignatureView) mSignpad).clearCanvas();
							}
						} else if (mImgFinger != null) {
							((ImageView) mImgFinger).setImageDrawable(null);
						}
						break;
					}
				}
			case MSG_ACTIVE_SIGNPAD:
				if (mLayoutElector != null) {
					if (mImgFinger != null
							|| (mImgFinger = mLayoutElector
									.findViewById(R.id.imgFingerprint4Elector)) != null)
						mImgFinger.setVisibility(View.GONE);

					if (mSignpad != null
							|| (mSignpad = (SignatureView) mLayoutElector
									.findViewById(R.id.viewSignpad)) != null)
						mSignpad.setVisibility(View.VISIBLE);

					mBtnAccept.setEnabled(true);
					mBtnAccept.setVisibility(View.VISIBLE);
					mBtnClear.setEnabled(true);
					mBtnClear.setVisibility(View.VISIBLE);
					mbActiveSignpad = true;
				}
				break;

			case MSG_ACTIVE_FINGERPRINT:
				if (mLayoutElector != null) {
					mImgFinger.setVisibility(View.VISIBLE);
					mSignpad.setVisibility(View.GONE);
					mBtnAccept.setVisibility(View.GONE);
					mBtnClear.setVisibility(View.GONE);
					mbActiveSignpad = false;
				}
				break;

			case MSG_SET_ELECTORINFO:
				if (mLayoutElector != null) {
					((TextView) mLayoutElector.findViewById(R.id.infoElector))
							.setText((String) msg.obj);
					((ImageView) mLayoutElector.findViewById(R.id.imgGender))
							.setImageResource(msg.arg1 == ELECTOR_SEX_MAN ? R.drawable.ic_gender_m
									: (msg.arg1 == ELECTOR_SEX_WOMAN ? R.drawable.ic_gender_w
											: R.drawable.ic_gender_x));

   		    		if((mElectorHelper != null) || (mElectorHelper = (TextView)mLayoutElector.findViewById(R.id.textHelper)) != null) {
   		    			mElectorHelper.setText(
   		    				msg.arg2 == ELECTOR_VOTE_COMPLETED?
   		    				R.string.menu_issue_01_already_issued :
   		    				R.string.elector_helper_finger_or_signpad
   		    			);
   		    		}
				}
				break;

			case MSG_UPGRADE_DOING:
				if (mUsbDialog!=null && mUsbDialog.isShowing()) {
					EditText et = (EditText) mUsbDialog
							.findViewById(R.id.et_password);
					et.setEnabled(true);
					mUsbDialog.findViewById(R.id.btnOkay).setClickable(true);
					if ((Boolean) msg.obj == false)
						showDefaultMessage(R.string.error_password_wrong_manage);
					else {
						mUsbDialog.dismiss();
						et.setText("");
						// 설치 관리자 실행
						if(mApkName!=null){
							Intent deleteIntent = new Intent(Intent.ACTION_DELETE)
							.setData(Uri.parse("package:" + "com.mirusys.eletors"));
							startActivity(deleteIntent);
						Intent intent = new Intent(Intent.ACTION_VIEW);
						intent.setDataAndType(Uri.fromFile(new File(mApkName)),
								"application/vnd.android.package-archive");
						startActivity(intent);}
						new AsyncTask<Void, Void, Void>() {
							int msg = R.string.error_no_database;
							File f;
							protected void onPreExecute() {
								showWaitMessage(R.string.usb_environment_install);
								f = new File(ResourceFactory.UDB_PATH+"/Backup/");
								Log.d(TAG, "path = "+ f.getPath());
								if (!f.exists())
									Log.d(TAG,"folder create = "+ f.mkdirs());
							};
							@Override
							protected Void doInBackground(Void... params) {
								try {
									if(mUdbName!=null){
									FileInputStream is = new FileInputStream(mUdbName);
									FileOutputStream os = new FileOutputStream(ResourceFactory.UDB_PATH + "/tsvs16.udb");
									FileOutputStream bos= new FileOutputStream(f.getPath() + "/tsvs16.udb");
									byte[] osb = new byte[is.available()];
									is.read(osb);
									os.write(osb);
									os.flush();
									os.close();
									bos.write(osb);
									bos.flush();
									is.close();
									bos.close();
									try {new Thread().sleep(1000);} catch (InterruptedException e) {Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();}
									msg = R.string.okay_storage;
									}
								} catch (IOException e) {
									msg = R.string.error_no_database;
									Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
								}finally {
									return null;
								}
							}
							@Override
							protected void onPostExecute(Void result) {
								super.onPostExecute(result);
								Process p;
								try {
									p = Runtime.getRuntime().exec("chmod -R 777 /"+ResourceFactory.UDB_PATH);
									BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
									br.readLine();
								} catch (IOException e) {
									Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
								}
								mBasicHandler.sendEmptyMessage(MSG_DISMISS);
								showDefaultMessage(msg);
							}
							
						}.execute();
					}
				}
				break;

			case MSG_UPGRADE_POPUP:
				if (usbChecker()) {
					File dir = new File(usbDirectory);
					String[] apkFiles;
					String[] udbFiles;
					if (dir.exists()) {
						if ((udbFiles = dir.list(new FilenameFilter() {
							@Override
							public boolean accept(File dir, String filename) {return filename.endsWith(".udb") && !filename.startsWith(".");}
						})) != null && udbFiles.length==1)
							// udb도 가져와야 해서 변경 pjw
							mUdbName = dir.getAbsolutePath() + '/' + udbFiles[0];
						else
							mUdbName=null;

						if ((apkFiles = dir.list(new FilenameFilter() {
							@Override
							public boolean accept(File dir, String filename) {return filename.endsWith(".apk") && !filename.startsWith(".");}
						})) != null && apkFiles.length==1)
							mApkName = dir.getAbsolutePath() + '/' + apkFiles[0];
						else
							mApkName=null;
						
						if (mUsbDialog == null) {
							mUsbDialog = new Dialog(BasicActivity.this);
							mUsbDialog.setContentView(R.layout.popup_environment_install);
							mUsbDialog.findViewById(R.id.btnCancel).setOnClickListener(
											new View.OnClickListener() {
												@Override
												public void onClick(View v) {
													mUsbDialog.dismiss();
													mUsbDialog=null;
												}
											});
							mUsbDialog.findViewById(R.id.btnOkay).setOnClickListener(
											new View.OnClickListener() {
												@Override
												public void onClick(View v) {
													v.setClickable(false);
													EditTextWithClearButton et = (EditTextWithClearButton) mUsbDialog.findViewById(R.id.et_password);
													if (et.isEnabled()) {
														mUsbPassword = et.getText().toString();
														if (mUsbPassword.length() == 0)
															showDefaultMessage(R.string.error_password_empty);
														else if(mUsbPassword.length()>8){
															showDefaultMessage(R.string.error_password_wrong_manage);
														}else {
															mCardCheckable = true;
															et.setEnabled(false);
															onAttached(mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER));
															if (mFactory.isCardAttached() == false) {
																showDefaultMessage(R.string.error_password_nomaster);
																return;
															}
															
														}}
														v.setClickable(true);
												}
											});
						}
						mUsbDialog.show();
					} else {
						showDefaultMessage(R.string.err_empty_storage);
					}
				} else if ((Boolean) msg.obj) {
					showDefaultMessage(R.string.err_nodev_storage);
				}
				break;
			}
		}
	};
	public void clearEnabler(){
		if(mBtnClear!=null)
			mBtnClear.setEnabled(true);
	}

}