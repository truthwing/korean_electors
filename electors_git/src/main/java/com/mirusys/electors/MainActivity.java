package com.mirusys.electors;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.haesin.entities.PrintIntentObject;
import com.haesin.entities.SGGInfoObject;
import com.haesin.manager.IBScanCommander;
import com.haesin.manager.IDScanCommander;
import com.haesin.util.AnimationLayout;
import com.haesin.util.CardBackupDataSQLite;
import com.haesin.util.SignatureMirrorView;
import com.haesin.util.SignatureView;
import com.haesin.util.SingleChoiceAdapter;
import com.haesin.util.XmlParsing;
import com.haesinit.common.Base64Util;
import com.haesinit.ultralite.database.SQLNO;
import com.integratedbiometrics.ibscancommon.IBCommon.ImageDataExt;
import com.ksign.util.encoders.Base64;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.electors.ResourceFactory.QueryID;
import com.mirusys.epson.BallotPrint;
import com.mirusys.epson.dto.Candidate;
import com.mirusys.sign.Common;
import com.mirusys.sign.SimpleCrypto;
import com.mirusys.voting.MiruSmartCard;
import com.mirusys.vt.fragment.BasicFragment;
import com.mirusys.vt.fragment.IssuedTicketFragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import static com.haesin.manager.IBScanCommander.State.IBSCAN_FAILED_CAPTURING;

public class MainActivity extends BasicActivity
implements AnimationLayout.Listener, View.OnClickListener, AdapterView.OnItemClickListener,
		   IDScanCommander.OnStateListener, IBScanCommander.OnStateListener,
		   DialogInterface.OnShowListener, DialogInterface.OnDismissListener,
		   MediaPlayer.OnCompletionListener, OnKeyListener, ResourceFactory.OnSQListener
{
    public final static String TAG = "MainActivity";
    
    public final static String PREVIEW = "StatusPreview";
    public final static String CLOSE = "Close";
    private final String TAG_CLOSE="USBChecker";
    private final String TAG_CLOSE_DELETE="DeleteUsb";
    private AnimationLayout mSidebar;
    private TextView mToday = null;
    private TextView mTitle = null;
    private Fragment mFragment = null;
//    private PlaySound mBeeper = new PlaySound();

    private ImageView mIvPreviewFingerprint = null;
	private ImageView mIvFingerprint = null;
	private SignatureMirrorView mIvSignature = null;
	private ImageView mIvSignatureRead = null;
	private SignatureView mIvSignpad = null;
	private ImageView mIvSignpadRead = null;
	
	private Button mBtnVerify = null;

    private View mIssueFragment = null; // default (embedded)
	private ViewGroup mIssueDetail = null;
//	private ViewGroup mIssueStepper = null;
//	private ViewGroup mIssueActions = null;
	private IDScanCommander mIDScanner = new IDScanCommander(this);

	private ListView mListView = null;
	private ArrayList<Object[]> mElectors = null;
	private int mIssueStep;
	private boolean mNotChangeable;
	private boolean mCardCheckable;
	private boolean mIsCardChecking;
	private Timestamp mIssueDate;
	private SimpleDateFormat mIssueDateFormat = null;
	private ByteArrayOutputStream mBinaryStream = null;
	private boolean mCardAttachable = true;
	private int mManagerCaller;
	private String mStrReqDesc;
	private boolean mIsCompleted = false;
	private boolean mIsShowActionBar = false;
	
	private Timer mTimer = null;
	private Timer mClosingUSBCheckTimer = null;
    private ArrayList<String> mPlaceCodes = new ArrayList<String>();
	
//	private View[] mNotiAlreadyIssued = null;
//	private ImageView mIvGender = null;
	private TextView mTvName = null;
	private TextView mTvBirthNo = null;
	private TextView mTvRegNo = null;
	private TextView mTvAddress = null;
	private TextView mTvDesc = null;
	private TextView mTvIssueDate = null;
	private TextView mTvIssuePlace = null;

	private Dialog mReqDialog = null;
	private Dialog mVerifyFingerDialog = null;
	
    private View mSearchBar = null;
	private ImageView mIvIDCard = null;
    private EditText[] mSearchMethods = null;
    private String[] mSearchData = new String[3];
    private boolean mRebuidByRecorgnizing = false;
	private Bitmap mRecorgnizedBitmap = null;
	private String mRecorgnizedName = null;
	private String mRecorgnizedNo = null;
    private Object[] mElector = null;
    private Button mBtnVoteOpenClose = null;
    private Dialog mClosingDialog = null;
	private Dialog mClosingConfirmDialog = null;
	private ArrayList<Object> mClosingFactors = null;
    private View mMenuSelected = null;
    private boolean mIsEnter = true;
    private boolean mIsClose = false;
    private boolean mIsNoneUSB = false;
    private boolean mIsCardCert = false;
    
    private int mClosingStep = 0;
    private MediaPlayer mMediaPlayer;
    private String mMediaFileName[]={"issueProc1.wav","issueProc2.wav", "issueProc4a.wav"};
    private boolean mIsNoDBUpdate = false;
    
    private GestureDetector mGesture;
    private GestureDetector mGestureItem;
    private int mPosition;
    
    private ArrayList<SGGInfoObject> mSggInfos = new ArrayList<SGGInfoObject>();
    private ArrayList<Candidate> mCandidates = new ArrayList<Candidate>();
    private ArrayList<PrintIntentObject> mPios = new ArrayList<PrintIntentObject>();
    private int mPioNo = 0;
    private int mPrintSN = 0;
    
    private Thread mFingerThread;
    
	private final static int MSG_CLOSING_STEP_NEXT = 0;
    private final static int MSG_CLOSING_COMPLETED = 1;
    private final static int MSG_SWITCHING_VIEW = 2;
    private final static int MSG_REBUILD_LISTVIEW = 3;
    private final static int MSG_UPDATE_SEAL = 4;
    private final static int MSG_ISSUE_STEP_READY = 5;
    private final static int MSG_ISSUE_STEP_NEXT = 6;
    private final static int	ISSUE_STEP_READY = 0;
    private final static int	ISSUE_STEP_WAIT_FOR_SEAL = 1;
    private final static int	ISSUE_STEP_WAIT_FOR_SCARD = 2;
    private final static int	ISSUE_STEP_WORKING = 3;
    private final static int	ISSUE_STEP_FINISHING = 4;
    private final static int MSG_RESULT_MATCH = 7;
    private final static int MSG_MANAGER_READY = 8;
    private final static int MSG_UPDATE_CLOSING_INFO = 9;
    private final static int MSG_ERROR = 10;
    private final static int MSG_DISMISS = 11;
	protected static final int MSG_PRINT = 12;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        if(savedInstanceState==null){
        	savedInstanceState=getIntent().getExtras();
        }
        if(savedInstanceState!=null&&savedInstanceState.getString("tag")!=null){
        	Button view = new Button(getApplicationContext());
        	view.setText(savedInstanceState.getString("name"));
        	if(savedInstanceState.getString("tag").equals(PREVIEW))
        		view.setTag("com.mirusys.vt.fragment.PrintStatisticsFragment");
        	else if(savedInstanceState.getString("tag").equals(CLOSE)){
        		mBtnVoteOpenClose = (Button)findViewById(R.id.btnMenuStart);
        		view = mBtnVoteOpenClose;
        	}else
        		view.setTag(savedInstanceState.getString("tag"));
        	onMenu(view);
        }
        
        mSidebar = (AnimationLayout) findViewById(R.id.animation_layout);
        mSidebar.setListener(this);
        CARDSQLITE = new CardBackupDataSQLite(MainActivity.this, "Cardbackup.db", null, 1);
        mIssueFragment = findViewById(R.id.issueVotingTicket);
		mListView = (ListView)findViewById(R.id.lvElectors);
		mListView.setOnItemClickListener(this);
		((TextView)findViewById(R.id.textMenuTitlePolls)).setText(mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME));
		mGesture = new GestureDetector(this, new DoubleTap());
		mGestureItem = new GestureDetector(this, new DoubleItemTap());
		// 개시 화면이 활성화 시에는 투표권 발급 메뉴 버튼을 선택 상태로 반전 (개시와 투표권 발급 화면은 하나로 간주)
		(mMenuSelected = findViewById(R.id.btnMenuIssue01)).setSelected(true);
		((TextView)findViewById(R.id.textDateTime)).setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return mGesture.onTouchEvent(event);
			}
		});
//		Button v1=(Button)findViewById(R.id.imgStep00);
//        v1.setBackground(getResources().getDrawable(R.drawable.img_step_08_01));
//		((Button)v1).setTextColor(Color.WHITE);
//		((AnimationDrawable)v1.getBackground()).start();
		mIDScanner.open();
		mIDScanner.fingerOn(true);
    }

    public void onToggleSidebar(View v) {
    	mSidebar.toggle();
    }

    private boolean vtOpened() {
    	return ResourceFactory.VOTE_working.equals(mFactory.getVtStatus());
    }

    private void setupVtStatus() {
		if (mBtnVoteOpenClose != null || (mBtnVoteOpenClose = (Button)findViewById(R.id.btnMenuStart)) != null) {
			mBtnVoteOpenClose.setText(vtOpened()? R.string.menu_vote_close : R.string.menu_vote_open);

			if (vtOpened())
				showSearchBar(true);
			else {
				limitMenuClickable(false);
				mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_SWITCHING_VIEW, mBtnVoteOpenClose), MSG_NODELAY);
			}
		}
    }

	@Override
	protected void onStart() {
		super.onStart();
		
		new NetworkInfo().execute();
		// DB Listener 연결
        mFactory.setOnSQListener(this);

		if(!mFactory.getProperty(ResourceFactory.CONFIG_SERVER_MODE).equals(ResourceFactory.MODE_server))
			setupVtStatus();//개시 여부에 따른 화면 변경(서버모드에서는 해당 화면이 필요 없음)

        String method = mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD);
        String mode = mFactory.getProperty(ResourceFactory.CONFIG_SERVER_MODE);
        int methodValue = method.equals(ResourceFactory.ISSUE_card)?1:method.equals(ResourceFactory.ISSUE_paper)?2:3;
        switch (methodValue) {
		case 1:
			findViewById(R.id.btnMenuIssue03).setVisibility(View.VISIBLE);
			findViewById(R.id.btnMenuIssue01).setVisibility(View.VISIBLE);
			findViewById(R.id.btnMenuIssue02).setVisibility(View.VISIBLE);
			findViewById(R.id.btnMenuIssue04).setVisibility(View.GONE);
			break;
		case 2:
			findViewById(R.id.imgStep04).setVisibility(View.GONE);
			findViewById(R.id.btnMenuIssue03).setVisibility(View.GONE);
			((TextView)findViewById(R.id.imgStep02)).setText(R.string.menu_issue_01_step_02_1);
			((TextView)findViewById(R.id.imgStep03)).setText(R.string.menu_issue_01_step_03_1);
			break;
		case 3:
			findViewById(R.id.imgStep03).setVisibility(View.GONE);
			findViewById(R.id.imgStep04).setVisibility(View.GONE);
			findViewById(R.id.btnMenuIssue03).setVisibility(View.GONE);
			((TextView)findViewById(R.id.imgStep02)).setText(R.string.menu_issue_01_step_04);
			findViewById(R.id.btnMenuIssue04).setVisibility(View.VISIBLE);
			break;
		default:
			break;
		}
        if (method == null || method.equals(ResourceFactory.ISSUE_card)){
        }else if(method != null && method.equals(ResourceFactory.ISSUE_paper)){
        }else{
        	
        }
        if(mode == null || !mode.equals(ResourceFactory.MODE_server)){
        	findViewById(R.id.relativeAdmin).setVisibility(View.VISIBLE);
        	findViewById(R.id.relativeManage).setVisibility(View.VISIBLE);
        	findViewById(R.id.btnMenuManage02).setVisibility(View.VISIBLE);
        	findViewById(R.id.btnMenuAdmin02).setVisibility(View.GONE);
        	findViewById(R.id.btnMenuAdmin03).setVisibility(View.GONE);
        }else{
        	findViewById(R.id.relativeAdmin).setVisibility(View.VISIBLE);
        	findViewById(R.id.relativeIssue).setVisibility(View.GONE);
        	findViewById(R.id.btnMenuManage03).setVisibility(View.GONE);
        	findViewById(R.id.btnMenuManage04).setVisibility(View.GONE);
        	if (mBtnVoteOpenClose != null || (mBtnVoteOpenClose = (Button)findViewById(R.id.btnMenuStart)) != null)
        	mBtnVoteOpenClose.setVisibility(View.INVISIBLE);
        }

		mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        findViewById(R.id.btnScreenClean).setClickable(false);
        findViewById(R.id.btnClear).setEnabled(false);
    }

	private void cleanupStatus() {
		Log.d(TAG, "log clean up status....");
		mNotChangeable = false;
		mCardCheckable = false;
		mIsCardChecking = false;
		mCardAttachable = false;
		mManagerCaller = 0;
		mFactory.releaseSmartCard();
		if(mIvSignpad!=null){
		mIvSignpad.clearCanvas();
		mIvSignpad.initCanvas();
		mIvSignature.startThread();
		mIvSignature.stopThread();}
		mIDScanner.open();
		try {new Thread().sleep(100);} catch (InterruptedException e) {e.printStackTrace();}
		mIDScanner.setScannable(true);
		mIDScanner.fingerOn(false);
		mFactory.openIBScanner(MainActivity.this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mFactory.dbQuery(QueryID.Q19);
		cleanupStatus();
//		if(mFactory.isCapturable())
//			mFactory.forceOpenIBScanner(this);
		mIssueStep = ISSUE_STEP_READY;
		mIsEnter = true;
		if(mFactory.getProperty(ResourceFactory.CONFIG_SERVER_MODE).equals(ResourceFactory.MODE_server))
				findViewById(R.id.btnMenuManage02).performClick();//서버모드일때 보여줄 화면 변경
	}

	@Override
	protected void onPause() {
		super.onPause();
		new Thread(){
			public void run() {
				mFactory.stopCapture();
				ibScanClose();
			};
		}.start();
		Log.d("IBSCAN","pause issue");
    }

	@Override
	protected void onStop() {
		super.onStop();
		new Thread(){
			public void run() {
				mIDScanner.close();
				if (mMediaPlayer != null) {
					mMediaPlayer.release();
					mMediaPlayer = null;
				}
			};
		}.start();
	}

	@Override
    public void onBackPressed() {
		if (mNotChangeable){
   			Log.d(TAG, "[Back] Not allow in procssing.");
   			showDefaultMessage(R.string.error_sgi_issue_fragment);
		}else
			mHandler.sendEmptyMessageDelayed(MSG_CLOSING_COMPLETED, MSG_NODELAY);
    }

    /* Callback of AnimationLayout.Listener to monitor status of Sidebar */
    @Override
    public void onSidebarOpened() {
        //Log.d(TAG, "opened");
    }

    /* Callback of AnimationLayout.Listener to monitor status of Sidebar */
    @Override
    public void onSidebarClosed() {
        //Log.d(TAG, "closed");
    }

    /* Callback of AnimationLayout.Listener to monitor status of Sidebar */
    @Override
    public void onTouchedSidebarWhenOpened() {
        // the sidebar area is touched when was opened, close sidebar
        //Log.d(TAG, "closing");

        if (mSidebar.isClosed() == false) {
        	mSidebar.toggle();
        }
	}

    @Override
    public void onCompletion(MediaPlayer player) {
    	if (player.isPlaying()) player.stop();
    }
    
	@Override
    public void onSqlStatus(final ResourceFactory.SQLState state) {
		if (state == ResourceFactory.SQLState.SQL_CONNECTED ||
			state == ResourceFactory.SQLState.SQL_DISCONNECTED) {
    		runOnUiThread(new Runnable() {
				@Override
				public void run() {
					TextView v = (TextView)findViewById(R.id.infoDbState);
					v.setBackgroundResource(state == ResourceFactory.SQLState.SQL_CONNECTED? R.drawable.menu_db_on : R.drawable.menu_db_off);
					v.setText(state == ResourceFactory.SQLState.SQL_CONNECTED? R.string.db_on : R.string.db_off);
				}
			});
		}

		if (mFragment != null)
			((BasicFragment)mFragment).onSqlStatus(state);
		else if(state != ResourceFactory.SQLState.SQL_CONNECTED) {
			dismissedMessageBox();
			showDefaultMessage(R.string.error_database_failed_connection);
			mHandler.sendMessageAtFrontOfQueue(mHandler.obtainMessage(MSG_REBUILD_LISTVIEW, new ArrayList<Object[]>()));
			if (mClosingDialog != null && mClosingStep > 2) {
				writeClosingInSCard(true); // 마감시간 초기화
			}
		}
    }

	@Override
    public Object[] getParameters(QueryID id) {
		if(QueryID.Q48 == id){
			return new Object[] {
					ResourceFactory.UI_TEST?mPlaceCodes.get(0):mFactory.getProperty(ResourceFactory.CONFIG_TPGID),
					mFactory.isJson()?new SimpleDateFormat("yy-MM-dd").format(new Date(mFactory.getVtDate())):new java.sql.Date(mFactory.getVtDate())
				};
		}else if(id == QueryID.Q49 || QueryID.Q50 == id)
				return new Object[] {
					mFactory.getProperty(ResourceFactory.CONFIG_TPGID)
				};
			else if(id == QueryID.Q51){
				return mFactory.isJson()? new Object[]{
						new SimpleDateFormat("yyyyMMdd").format(new Date())
						+new SimpleDateFormat("HHmmss").format(mClosingFactors.get(2)), // CLOSE_DATE
						mClosingFactors.get(3), // BK_MCNT
						mClosingFactors.get(4), // VT_MCNT
						mClosingFactors.get(5), // VT_XCNT
						mClosingFactors.get(6), // VT_PCNT
						mClosingFactors.get(7), // VT_CCNT
						mClosingFactors.get(8), // VT_ECNT
						mClosingFactors.get(9), // NVT_CCNT
						mFactory.getProperty(ResourceFactory.CONFIG_TPGID),
						new SimpleDateFormat("yyyyMMddHHmmss").format(new Date(mFactory.getVtDate()))
				}:
				 new Object[] {
//					mClosingFactors.get(2), // CLOSE_DATE
					mClosingFactors.get(3), // BK_MCNT
					mClosingFactors.get(4), // VT_MCNT
					mClosingFactors.get(5), // VT_XCNT
					mClosingFactors.get(6), // VT_PCNT
					mClosingFactors.get(7), // VT_CCNT
					mClosingFactors.get(8), // VT_ECNT
					mClosingFactors.get(9), // NVT_CCNT
					mFactory.getProperty(ResourceFactory.CONFIG_TPGID),
					new java.sql.Date(mFactory.getVtDate())
				};
		}else if (mFragment != null)
			return ((BasicFragment)mFragment).getParameters(id);
		else {
			String strTpgId = mFactory.getProperty(ResourceFactory.CONFIG_TPGID);

			if (id == QueryID.Q32)
				return new Object[] {
					ResourceFactory.UI_TEST?mPlaceCodes.get(0):strTpgId,
					mFactory.isJson()?new SimpleDateFormat("yy-MM-dd").format(new Date(mFactory.getVtDate())):new java.sql.Date(mFactory.getVtDate())
				};
			else if(id == QueryID.Q14)
				return mFactory.isJson()?
						new Object[] {
					mElector[0].toString(),
					mElector[12].toString()
				}
			:new Object[] {
					mElector[12].toString(),
					mElector[0].toString()
				};
			else if(id == QueryID.Q72)
						return 	new Object[] {
										mElector[0].toString()
					};
			else if(id == QueryID.Q38 || id == QueryID.Q71) {
				SingleChoiceAdapter adapter = (SingleChoiceAdapter)mListView.getAdapter();
		        if (adapter.getSelectedIndex() < 0)
		        	return null;
		        else {
			        String method = mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD);
					mElector = adapter.getItem(adapter.getSelectedIndex());
					Object bytesImage = null;

					if (mBinaryStream == null && (mBinaryStream = new ByteArrayOutputStream()) == null)
						bytesImage = null;
					else {
						mBinaryStream.reset();

						if ("F".equals(mElector[9]))
							mFactory.saveImageToBuffer((ImageDataExt)mElector[10], mBinaryStream);
						else
							if(mElector[10]!=null)
							((Bitmap)mElector[10]).compress(CompressFormat.PNG, 0, mBinaryStream);
						if(!ResourceFactory.UI_TEST)
							try {
								bytesImage = 
										SimpleCrypto.Encrypt(
												mBinaryStream.toByteArray()
												, mFactory.getTcardKey());

							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					}
					if(ResourceFactory.UI_TEST)
						return mFactory.isJson()?
								new Object[]{
						"N",
						mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_card)?"E":"P",
						mFactory.getProperty(ResourceFactory.CONFIG_TPGID),
						mElector[9], Base64Util.encode(mBinaryStream.toByteArray()), 
						"4", 
						mElector[0]
						}:new Object[]{
						"N",
						new java.sql.Date(Calendar.getInstance().getTime().getTime()),
						mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_card)?"E":"P",
						mFactory.getProperty(ResourceFactory.CONFIG_TPGID),
						mElector[9], 
						mBinaryStream.toByteArray(), 
						"4", 
						mElector[0]
							};
					else
						try {
							return mFactory.isJson()?
									new Object[]{
								"N",
//						new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").format(Calendar.getInstance().getTime()), now값이 들어가도록 쿼리 변경.
								method == null || method.equals(ResourceFactory.ISSUE_card)? "E" : "P", // 'E'lectronic, 'P'aper
								mFactory.getProperty(ResourceFactory.CONFIG_TPGID),
								mElector[9], // 서명날인 타입 ('S'ign, 'F'ingerprint)
								Base64Util.encode(
//								mBinaryStream.toByteArray()),
								SimpleCrypto.Encrypt(
										mBinaryStream.toByteArray()
										, mFactory.getTcardKey())), // 이미지
								"4",
								mFactory.getLocalIP(),
								mElector[0],
								mElector[12]
							}:new Object[] {
								mIssueDate, // server side time-stamp
								method == null || method.equals(ResourceFactory.ISSUE_card)? "E" : "P", // 'E'lectronic, 'P'aper
								mFactory.getProperty(ResourceFactory.CONFIG_TPGID),
								mElector[9], // 서명날인 타입 ('S'ign, 'F'ingerprint)
								bytesImage, // 이미지
								"4",
								mFactory.getLocalIP(),
								mElector[0]  // 등재번호
							};
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return null;
						}
		        }
			}
			else if(id == QueryID.Q18)
				return mFactory.isJson() ? new Object[]{
					mStrReqDesc,
					mElector[0].toString(),
					mElector[12]
			}
			:new Object[] {
					mStrReqDesc,
					mElector[0].toString()
				};
			else if(id == QueryID.Q17)
				return new Object[] {
					mManagerCaller==R.id.btnReissue?"A":"B",//발급취소
					mStrReqDesc,
					mElector[0].toString(),
					mElector[12]
				};
			else if(id == QueryID.Q13 || id == QueryID.Q70){
					return new Object[] {
							mSearchData[0],
							mSearchData[0].length()==0?mSearchData[1]:"",
									mSearchData[0].length()==0?
									mRecorgnizedNo==null?mSearchData[2]:
							mFactory.getProperty(ResourceFactory.CONFIG_RECOGNIZE).equals(ResourceFactory.RECOGNIZE_left)?
							mRecorgnizedNo.substring(0, 6)+"%":
								mFactory.getProperty(ResourceFactory.CONFIG_RECOGNIZE).equals(ResourceFactory.RECOGNIZE_right)?
										"%"+mRecorgnizedNo.substring(6, mRecorgnizedNo.length()):mRecorgnizedNo:""
				};}
			else if(id == QueryID.Q57)
				return new Object[]{mSggInfos!=null?mSggInfos.get(mPioNo).getSggId():null};
			else if(id == QueryID.Q58)
				return new Object[]{mSggInfos!=null?mSggInfos.get(mPioNo).getSggId():null};
			else if(id == QueryID.Q60){
				return new Object[]{mSggInfos!=null?mSggInfos.get(mPioNo).getSggId():null, Integer.toString(mPios.get(mPioNo).getEndSerialNum())};}
			else if(id == QueryID.Q61)
				return new Object[]{ResourceFactory.UI_TEST?mPlaceCodes.get(0):mElector[12]};
			else if(id == QueryID.Q62)
				return new Object[]{ResourceFactory.UI_TEST?mPlaceCodes.get(0):mElector[12]};
			else if(id == QueryID.Q69)
				return new Object[]{ResourceFactory.UI_TEST?mPlaceCodes.get(0):mFactory.getProperty(ResourceFactory.CONFIG_TPGID)};
			else
				return null;
		}
    }

	@Override
	@SuppressWarnings("incomplete-switch")
    public boolean onResultSet(ResultSet rs, QueryID id) throws SQLException {
		Log.d(TAG, "onResultSet for " + id);

		if (mFragment != null)
			return ((BasicFragment)mFragment).onResultSet(rs, id);
		else {
			boolean moreQuery = true;
			mElectors = null;

			switch(id) {
			case Q19: // 투표구 목록
				while (rs.next()) {
					mPlaceCodes.remove(rs.getString("TPG_ID"));
					mPlaceCodes.add(rs.getString("TPG_ID"));
				}
				break;
			case Q32:
				if (rs.next()
						&& ResourceFactory.UI_TEST||ResourceFactory.VOTE_working.equals(rs
								.getString("STATUS"))){
					mIssueDate = rs.getTimestamp("NOW");
				}else {
					// 개시 상태가 아닌 경우 (DB기준) 오류 팝업
					mIsEnter = true;
					showDefaultMessage(R.string.already_vote_finished);
					mElectors = new ArrayList<Object[]>();
					moreQuery = false;
				}
				break;
			case Q72:
			case Q14:
				if (rs.next()) { // only 1
					byte[] image;
					String kindOf;
					SingleChoiceAdapter adapter = (SingleChoiceAdapter) mListView
							.getAdapter();

					if (adapter.getSelectedIndex() < 0)
						Log.d(TAG, "has SEAL but loses owner");
					else if ((kindOf = rs.getString("SEAL_CD")) != null && // 'S'ign,
																			// 'F'ingerprint
							(image = rs.getBytes("SEAL_IMG")) != null) {
						Object[] elector = adapter.getItem(adapter
								.getSelectedIndex());
						if(!ResourceFactory.UI_TEST)
							try {
								image = SimpleCrypto.Decrypt(image,
									mFactory.getTcardKey());
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						if ("F".equals(elector[9] = kindOf))
							elector[10] = mFactory.loadImageFromBuffer(image);
						else
							elector[10] = BitmapFactory.decodeByteArray(image,
									0, image.length);
						mIssueStep = ISSUE_STEP_READY;
						mHandler.sendMessage(mHandler.obtainMessage(
								MSG_UPDATE_SEAL, 0, 1, elector));
					}
				}

				break;
			case Q23: // 가중치 그룹
				while (rs.next()) {
					byte[] SGIG = rs.getString("SGIG_ID").getBytes();
					if (SGIG.length == 2)
						mSGIG = SGIG;
					else
						mSGIG = "".getBytes();
				}
				break;
			case Q48:
				if (rs.next() == false || mClosingFactors == null
						&& (mClosingFactors = new ArrayList<Object>()) == null)
					moreQuery = false;
				else {
					mClosingFactors.clear();

					for (int i = 1; i < 14; ++i) {
						Object o = rs.getObject(i);
						if (o instanceof BigDecimal)
							Integer.valueOf(o.toString());
						mClosingFactors.add(o);
					}
				}
				break;
			case Q49:
				if (rs.next())
					mClosingFactors
							.set(7, Integer.valueOf(rs.getObject("VT_CCNT")
									.toString()));
				break;
			case Q50:
				if (rs.next())
					mClosingFactors.set(9, Integer.valueOf(rs.getObject(
							"NVT_CCNT").toString()));
				mHandler.sendEmptyMessage(MSG_UPDATE_CLOSING_INFO); // 조회된 집계정보
																	// 표시
				break;
			case Q70:
			case Q13:
				String str = null;
				boolean isTpgId = false;
				boolean isTPGIDYn = false;
				mElectors = new ArrayList<Object[]>();
				while (rs.next()) {
					
					if(!ResourceFactory.UI_TEST)
					for (String i : mPlaceCodes) {
						if (i.equals(rs.getString("TPG_ID")))
							isTpgId = true;
					}
					Object[] elector = new Object[15];
					if (ResourceFactory.UI_TEST || (isTpgId
							&& mFactory.getProperty(ResourceFactory.CONFIG_TPGID).equals(
									rs.getString("TPG_ID_YN").equals("Y") ? 
											rs.getString("VT_TPG_ID"): 
												mFactory.getProperty(ResourceFactory.CONFIG_TPGID)))) {
						if(rs.getString("VT_TPG_ID")==null)
							elector[13] = null;
						else
							elector[13] = rs.getString("VT_TPG_ID").equals("") ? "": rs.getString("VT_TPG_ID");
						elector[0] = rs.getString("DJ_NO");
						elector[1] = rs.getString("ADDR");
						elector[2] = rs.getString("NAME");
						elector[6] = rs.getString("JUMIN_NO");
						elector[5] = rs.getString("ETC");
						elector[4] = rs.getString("VT_YN"); // 'Y'발급가능, 'N'발급불가
						elector[7] = rs.getDate("VT_DATE"); // 발급일자
						if(!ResourceFactory.UI_TEST){
							elector[8] = rs.getString("TPG_NAME"); // 발급장소
							elector[12] = rs.getString("TPG_ID");
							elector[3] = rs.getString("BIRTH_DATE");
							elector[14] = rs.getString("ISSUED_LOCATION");
						}
						elector[11] = true;
						mElectors.add(elector);
					}else if(!ResourceFactory.UI_TEST){
						isTPGIDYn=rs.getString("TPG_ID_YN").equals("Y");//지정여부
						str = rs.getString("TPG_NAME");
				}
				}
				if (mElectors.size() == 0) {
					if(isTPGIDYn)
						showDefaultMessage(getString(R.string.error_TPS_elector,str));//지정일경우에 해당 투표소 안내
					else
						showDefaultMessage(R.string.err_no_search_result);
					mIsCompleted = true;
					mHandler.sendEmptyMessage(MSG_ERROR);
				}
				mIsEnter = true;
				break;

			case Q58:
				while (rs.next()) {
					mPrintSN = rs.getInt("PRT_SN");
					mPios.get(mPioNo).setStartSerialNum(mPrintSN+1);
					mPios.get(mPioNo).setEndSerialNum(mPrintSN+1);
					mPios.get(mPioNo).setBallotColor(ResourceFactory.BOX_COLOR2);
					mPios.get(mPioNo).setFillType(ResourceFactory.FILL_FULL);
					mPios.get(mPioNo).setSerial(true);
					if(!ResourceFactory.UI_TEST){
						mFactory.dbQuery(QueryID.Q60);
					}else{
						mHandler.sendEmptyMessage(MSG_PRINT);
					}
				}
				break;
			case Q61:
				mCandidates.clear();
				while (rs.next()) {
					Candidate paper = new Candidate(rs.getString("HBJ_GIHO"),
							rs.getString("jd_name").equals("")?rs.getString("HBJ_NAME"):rs.getString("jd_name"),
							!rs.getString("HBJ_NAME").equals("")?rs.getString("HBJ_NAME"):rs.getString("jd_name"),
						"", "",rs.getString("SGG_ID"));
					mCandidates.add(paper);
				}
				for (int j = 0; j < mPios.size(); j++) {
					ArrayList<Candidate> candidates = new ArrayList<Candidate>();
					for (int i = 0; i < mCandidates.size(); i++) {
						Candidate paper = mCandidates.get(i);
						if(mSggInfos.get(j).getSggId().equals(paper.getId())){
							if (!paper.getHbjName().equals("")
									&& !paper.getJdName().equals("")
									&& paper.getJdName().equals(paper.getHbjName())
									)
								mPios.get(j).setBallotType(ResourceFactory.BALLOT_TYPE1);
							else if (!paper.getHbjName().equals("")
									&& !paper.getJdName().equals("")
									&& !paper.getJdName().equals(paper.getHbjName()))
								mPios.get(j).setBallotType(ResourceFactory.BALLOT_TYPE0);
							else
								mPios.get(j).setBallotType(ResourceFactory.BALLOT_TYPE4);
							candidates.add(paper);
						}
						mPios.get(j).setCandidate(candidates);
						mPios.get(j).setCandidateNum(candidates.size());
					}
				}
				mFactory.dbQuery(QueryID.Q58);
				break;
			case Q62:
				int i=0;
				while (rs.next()) {
					SGGInfoObject sggInfo = new SGGInfoObject();
					sggInfo.setSggId(rs.getString("SGG_ID"));
					PrintIntentObject pio = new PrintIntentObject();
					pio.setTitleName(rs.getString("SGG_NAME"));
					pio.setSubTitleName(rs.getString("SGG_BPS_NAME"));
					if(rs.getString("SGG_BPS_STAMP").length()>0)
						try {
							pio.setStamp(rs.getBytes("SGG_BPS_STAMP"));
						} catch (Exception e) {
							Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
						}
					else
						pio.setStamp(null);
					if(mFactory.getAdminStamp()==null || mFactory.getAdminStamp().length>0)
						pio.setAdminstamp(mFactory.getAdminStamp());
					else
						pio.setAdminstamp(null);
					mSggInfos.add(i, sggInfo);
					mPios.add(i++, pio);
				}
				mFactory.dbQuery(QueryID.Q61);
				break;
			case Q69:
				if (rs.next()) {
					if(mFactory.getAdminStamp()==null)
						mFactory.setAdminStamp(rs.getBytes("IMG_MGR_STAMP"));
				}
			}

			if (mElectors != null&&mIssueStep<1) {
				mHandler.sendMessageAtFrontOfQueue(mHandler.obtainMessage(MSG_REBUILD_LISTVIEW, mElectors));
			}

			return moreQuery;
		}
    }

private byte[] mSGIG = new byte[2];

    @SuppressLint("SimpleDateFormat")
	private boolean writeVotingCard(MiruSmartCard card) {
    	boolean ret;
    	/*
    	  	write
			투표구ID (10)
			원적투표소ID(10)
			발급일시(14) = "현재시간"
			선거인그룹ID(2) = ""
			[전자서명1(24) = 발급일시(14) + Random Value(10)]
			[서명값 = 전자서명(투표소 개인키, 전자서명1)]
			서명값
    	 */
    	byte[] InData = new byte[10+10+14+2+344+3+14+344];
    	byte[] byTpgId = mFactory.getProperty(ResourceFactory.CONFIG_TPGID).getBytes();
    	//투표구 ID
    	System.arraycopy(byTpgId, 0, InData,  0, byTpgId.length);
    	//투표소 ID
    	byte[] byTpsId = ((String) mElector[12]).getBytes();
    	System.arraycopy(byTpsId, 0, InData, 10, byTpsId.length);
    	//날짜
    	mIssueDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    	byte[] byDate = mIssueDateFormat.format(mIssueDate).getBytes();
    	Log.d(TAG, "date : "+mIssueDateFormat.format(mIssueDate).toString());
    	System.arraycopy(byDate, 0, InData, 20, byDate.length);
    	//선거인 그룹
    	if(mFactory.dbQuery(QueryID.Q23)){
    		try {Thread.sleep(500);} catch (InterruptedException e) {Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();}
				System.arraycopy(mSGIG, 0, InData, 34, mSGIG.length);
    	}
    	//DN전자서명 값 인증
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {
				((Button)findViewById(R.id.imgStep03)).setText(getResources().getString(R.string.menu_issue_01_step_04_2));
			}
		});
    	ret=mFactory.getSign().verifySign(mFactory.getVcardDn(), mFactory.getTcardCenterCert());
    	Log.d(TAG, "write Vcard Center Cert Verify ret :"+ret);
    	//발급일시, 랜덤값
    	byte[] dnData = null;
    	try {
    	Log.d(TAG, "rand value : "+new String(mFactory.getVtOpeningValue().getBytes("MS949")));
			dnData = (new String(byDate)+mFactory.getVtOpeningValue()).getBytes("MS949");
		} catch (UnsupportedEncodingException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
		}
    	
    	//서명값 생성
    	byte[] signData = mFactory.getSign().digitalSign(dnData, mFactory.getTcardPriKey());
    	//서명값 인증
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {
				((Button)findViewById(R.id.imgStep03)).setText(getResources().getString(R.string.menu_issue_01_step_04_3));
			}
		});
    	ret =ret? mFactory.getSign().verifySign(signData, mFactory.getTcardCert(), dnData):false;
    	Log.d(TAG, "write Vcard Verify Sign ret :"+ret+" issue step = "+mIssueStep);
    	ret = ret?mIssueStep==ISSUE_STEP_WORKING||mFactory.isJson():false;
    	//서명값 byte배열에 삽입
    	signData = ksign.jce.util.Base64.encode(signData);
    	System.arraycopy(signData, 0, InData, 36, signData.length);
    	//전체 값 write
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {
				((Button)findViewById(R.id.imgStep03)).setText(getResources().getString(R.string.menu_issue_01_step_04_4));
			}
		});
    	ret=ret?card.VCardWriteBSection(InData) == MiruSmartCard.SUCCESS:false;
    	Log.d(TAG, "write Vcard B Section ret :"+ret);
    	ret = ret?mIssueStep==ISSUE_STEP_WORKING||mFactory.isJson():false;
    	return ret;
    }

	// 관리카드의 개시 정보를 업데이트
    @SuppressLint("SimpleDateFormat")
    private boolean writeClosingInSCard(boolean rollback) {
		MiruSmartCard card = mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER); // 관리자 카드장치

		if (ResourceFactory.UI_TEST || mFactory.isCardAttached()) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("HHmmss");
			byte[] bytesClosingState = new byte[13];

			System.arraycopy(dateFormat.format(mClosingFactors.get(1)).getBytes(), 0, bytesClosingState, 1, 6); // 개시시간

			if (rollback)
				bytesClosingState[0] = (byte)ResourceFactory.VOTE_working.charAt(0);
			else {
				bytesClosingState[0] = (byte)ResourceFactory.VOTE_finished.charAt(0);
				System.arraycopy(dateFormat.format((java.util.Date)mClosingFactors.get(2)).getBytes(), 0, bytesClosingState, 7, 6); // 마감시간
			}

			if (ResourceFactory.UI_TEST)
				return true;
			else synchronized(card) {
				if (card.TCardWriteTPGData(bytesClosingState) == MiruSmartCard.SUCCESS) {
					return true;
				}
			}
		}
		showDefaultMessage(R.string.error_write_card);
		return false;
    }

	@Override
    public boolean onResultSet(int rows, QueryID id) throws SQLException {
		Log.d(TAG, "onResultSet for " + id + ", changed: " + rows);

		if (mFragment != null)
			return ((BasicFragment)mFragment).onResultSet(rows, id);
		else if(id == QueryID.Q60){
			if(++mPioNo<mPios.size())
				mFactory.dbQuery(QueryID.Q58);
			else
				mHandler.sendEmptyMessage(MSG_PRINT);
		}
		else if(id == QueryID.Q38 || id == QueryID.Q71) {
			mPios.clear();
			mSggInfos.clear();
			closeTimer();
			if(!mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_card)){
				mFactory.transaction(ResourceFactory.TRANSACTION_COMMIT);
				mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT); //발급 완료
				try {
					if (mMediaPlayer != null){
						if (mMediaPlayer.isPlaying())
							mMediaPlayer.stop();
							setSoundFile(mMediaFileName[mMediaFileName.length-1]);
							mMediaPlayer.seekTo(0);
							mMediaPlayer.start();
					}
				} catch(IllegalStateException ise) {
					Log.e(TAG, "[issue] Can't play sound: " + ise.getMessage());
					mMediaPlayer.release();
					mMediaPlayer = null;
				}
				try {Thread.sleep(2000);} catch (InterruptedException e) {Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();	}
				mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT); //화면 삭제
			}
			else {	
				MiruSmartCard card = mFactory.getSmartCard(0); // 자동발급 카드장치
	
				synchronized (ResourceFactory.UI_TEST ? mFactory : card) {
					if (rows > 0 )
						if( // not updated
							ResourceFactory.UI_TEST || writeVotingCard(card)) // 카드발급 처리
					{
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								((Button)findViewById(R.id.imgStep03)).setText(getResources().getText(R.string.menu_issue_01_step_03));
								((Button)findViewById(R.id.imgStep04)).setText(getResources().getString(R.string.menu_issue_01_step_04));
							}
						});
						mFactory.transaction(ResourceFactory.TRANSACTION_COMMIT);
						mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT); // 카드발급 완료
	
						try {
							if (mMediaPlayer != null){
								if (mMediaPlayer.isPlaying())
									mMediaPlayer.stop();
									setSoundFile(mMediaFileName[mMediaFileName.length-1]);
									mMediaPlayer.seekTo(0);
									mMediaPlayer.start();
							}
						} catch(IllegalStateException ise) {
							Log.e(TAG, "[issue] Can't play sound: " + ise.getMessage());
							mMediaPlayer.release();
							mMediaPlayer = null;
						}
						mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT);
					} else {
						mFactory.transaction(ResourceFactory.TRANSACTION_ROLLBACK);
						mHandler.sendEmptyMessage(MSG_ERROR);
						if (ResourceFactory.UI_TEST)
							Log.d(TAG, "[issue] "
									+ getString(R.string.err_nores_writer));
						else if(mIssueStep!=ISSUE_STEP_WORKING)
							showDefaultMessage(R.string.err_nores_writer);
					}
					else
						showDefaultMessage(R.string.error_database_failed_update);
				}
			}
		}
		else if(id == QueryID.Q18) {
			if (rows == 1) {
				mFactory.transaction(ResourceFactory.TRANSACTION_COMMIT);
				issueCancle();
				}
		}
		else if(id == QueryID.Q51) {
			if (rows == 1 && mClosingDialog != null) {
				mFactory.transaction(ResourceFactory.TRANSACTION_COMMIT);
				mHandler.sendEmptyMessage(MSG_CLOSING_STEP_NEXT);
			}
			else {
				mFactory.transaction(ResourceFactory.TRANSACTION_ROLLBACK);
				mClosingDialog.dismiss();
			}
		}

		return true;
    }

	@Override
    public void onAttached(MiruSmartCard card) {
		mCardAttachable = true;
		byte[] type = new byte[2];
		boolean isType = card.ProcGetCardType(type)!=MiruSmartCard.SUCCESS;
		int msgError = 0;
		if(mFactory.getManagerKey()!=null&&
			!ResourceFactory.UI_TEST&&
			type[0]=='T'&&!isType&&
			card.CardVerify(mFactory.getManagerKey()) != MiruSmartCard.SUCCESS
			&& mCardCheckable){
			msgError = R.string.err_nores_writer;
		}
		
		if(mCardCheckable&&!isType&&type[0]=='T'&&mClosingDialog!=null&&mClosingDialog.isShowing()){
			mCardCheckable = false;
			byte[] bytesCert = new byte[1302];
			mHandler.sendEmptyMessage(MSG_CLOSING_STEP_NEXT);
			mIsCardCert = card.ProcReadTMCPKIDatainBK(Common.string2Bytearray(mFactory.getManagerKey(),mFactory.getManagerKey().length()),Common.string2Bytearray(mFactory.getHashValue(), 44), (byte)0x01, bytesCert)==MiruSmartCard.SUCCESS;
			mFactory.setTcardCert(bytesCert);
		}else{
		if(mIsCardChecking&&type[0]=='V'){
			dismissedMessageBox();
			mCardCheckable=true;
			mIsCardChecking = false;
			runOnUiThread(new Runnable() {@Override public void run() {findViewById(R.id.btnScreenClean).setClickable(true);}});
		}else if(mIsCardChecking&&mFragment == null){
			msgError = R.string.menu_issue_03_result_1;
			runOnUiThread(new Runnable() {@Override public void run() {((Button)findViewById(R.id.imgStep02)).setText(getResources().getString(R.string.menu_issue_01_step_02_2));}});
		}
			
		if(mFragment != null)
			((BasicFragment)mFragment).onAttached(card, type[0]);
		else if(mCardCheckable) {
			mCardCheckable = false;
			byte[] OutData = null;

			if (type[0] == 'T') {
				if (ResourceFactory.UI_TEST == false && (
					(OutData = new byte[51]) == null ||
					card.TCardReadBSection(OutData) != MiruSmartCard.SUCCESS ||
					card.CardVerify(mFactory.getManagerKey()) != MiruSmartCard.SUCCESS ||
					mFactory.getProperty(ResourceFactory.CONFIG_TPGID).equals(ResourceFactory.bytesToString(OutData, 0, 10)) == false))
					msgError = R.string.error_password_miscard;
//				else if(mManagerCaller != 0)
//					mHandler.sendEmptyMessage(MSG_MANAGER_READY);
			}
			else {
//				OutData = new byte[10+10+14+2+344+3+14+344];
//				Log.i(TAG, "ret = "+ret);
//				ret = card.VCardReadBSection(OutData);
//				Log.i(TAG, "ret = "+ret);
				runOnUiThread(new Runnable() {
					public void run() {
						((Button)findViewById(R.id.imgStep02)).setText(getResources().getString(R.string.menu_issue_01_step_02_3));
					}
				});
				byte[] pki = new byte[1302];
				byte[] dn = new byte[344];
				if(ResourceFactory.UI_TEST && !mIsShowActionBar){
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
					}
					mFactory.dbQuery(QueryID.Q71); // DB발급 처리
					mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT);
				}else 
					if (!ResourceFactory.UI_TEST &&
					!mIsShowActionBar && !(type[0]=='V'))
						msgError = R.string.error_password_elector;
						// 로그인 시 획득한 인증서(???)와 투표권 카드에서 읽은 인증서가 일치하는지 확인 필요
					else if(!ResourceFactory.UI_TEST &&
						(OutData = new byte[10+10+14+2+344+3+14+344]) == null 
					|| card.CardAuthentication()!=MiruSmartCard.SUCCESS)
						msgError = R.string.error_card_failed_auth;
					else if(ResourceFactory.UI_TEST == false &&card.VCardReadBSection(OutData) != MiruSmartCard.SUCCESS)
						msgError = R.string.error_card_failed_read;
					else if(ResourceFactory.UI_TEST == false &&card.VCardReadCertDNData(pki, dn) != MiruSmartCard.SUCCESS
						|| !mFactory.setVcardCert(pki)
						|| !mFactory.setVcardDn(dn))
						msgError = R.string.error_card_failed_cert;
					else if(mIssueStep == ISSUE_STEP_READY) {
					
					}
					else if(!mIsNoDBUpdate&&mFactory.dbQuery(QueryID.Q32, QueryID.Q38)) { // DB발급 처리
						mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT);
						runOnUiThread(new Runnable() {
							public void run() {
								if(mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_card)){
									((Button)findViewById(R.id.imgStep02)).setText(getResources().getString(R.string.menu_issue_01_step_02));
									((Button)findViewById(R.id.imgStep03)).setText(getResources().getText(R.string.menu_issue_01_step_03_2));
								}
							}
						});
					}else if(ResourceFactory.UI_TEST){	//체험용
						mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT);
					}else if(mIsNoDBUpdate){
						mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT);
						runOnUiThread(new Runnable() {
							public void run() {
								if(mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_card)){
									((Button)findViewById(R.id.imgStep02)).setText(getResources().getString(R.string.menu_issue_01_step_02));
									((Button)findViewById(R.id.imgStep03)).setText(getResources().getText(R.string.menu_issue_01_step_03));
									((Button)findViewById(R.id.imgStep04)).setText(getResources().getString(R.string.menu_issue_01_step_04));
								}
							}
						});
						mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT); 
					}
			}
		}

			if (msgError != 0) {
				mCardCheckable = false;
				if(!mIsCardChecking)
				if (mIssueStep!= ISSUE_STEP_READY) {
						mHandler.sendMessageAtFrontOfQueue(mHandler.obtainMessage(MSG_UPDATE_SEAL, 1, 0, null));
				}
	//			else if(mFactory.getSmartDeviceIndex() == ResourceFactory.SCARD_FOR_MANAGER)
	//				mCardAttachable = true;
	//			else
	//				mIDScanner.backward(mFactory.getSmartCard(1));
	
				if (msgError == R.string.error_password_miscard)
					showDefaultMessage(getString(msgError, mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME)));
				else if(msgError == R.string.menu_issue_03_result_1)
					showDefaultMessage(msgError);
				else{
					showDefaultMessage(msgError);
					mHandler.sendEmptyMessage(MSG_ERROR);
					}
			}
		}
    }
    @Override
	public void onDetached(MiruSmartCard card) {
    	mCardAttachable = false;
    	if (mFragment != null)
    		((BasicFragment)mFragment).onDetached(card);
//    	else if(mCardCheckable = mCardAttachable)
//    	mCardAttachable = false;
    	else if(mClosingDialog != null) {
    		showDefaultMessage(getString(R.string.error_detached_scard) + "\n" + getString(R.string.error_vote_closing));

    		if (mClosingConfirmDialog != null) {
    			mClosingConfirmDialog.dismiss();
    			mClosingConfirmDialog = null;
    		}

    		mClosingDialog.dismiss();
    		mClosingDialog = null;
    	}else if (mIsCompleted) {
    		mIsEnter=true;
    		mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT);
		}
    }
    
    @Override
    public void onIdle(MiruSmartCard card) {
    	if (mCardCheckable && mFactory.isCardAttached() || mFragment != null && ((BasicFragment)mFragment).onIdle(card))
    		onAttached(card);
    	else if(!mCardAttachable&&mFragment==null){
			mIsCardChecking = true;
			mCardCheckable = false;
			}
    	else
    		super.onIdle(card);
    }

    @Override
	protected void onMessageResult(View v, Object tag) {
		super.onMessageResult(v, tag);

		if (mFragment != null) {
			((BasicFragment)mFragment).onMessageResult(v, tag);
		}
		switch (v.getId()) {
		case android.R.id.button1:
			if(TAG_CLOSE.equals(tag)){
				mHandler.sendEmptyMessage(MSG_CLOSING_STEP_NEXT);
				mIsNoneUSB = true;
				Log.i(TAG, "Closing with None USB...");
			}else if(TAG_CLOSE_DELETE.equals(tag)){
				closeFileMake.start();
			}else if("FingerRestart".equals(tag)){
				new Thread(){
					@Override
					public void run() {
						super.run();
						mFactory.ibscannerClose();
						mIDScanner.fingerOn(true);
						try {Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}
						mIDScanner.fingerOn(false);
					}
				}.start();

			}
			break;
		case android.R.id.button2:
			if(TAG_CLOSE_DELETE.equals(tag)){
				if(mClosingDialog!=null&&mClosingDialog.isShowing()){
					mClosingDialog.dismiss();
					mClosingDialog=null;
					}
			}
			break;
		default:
			break;
		}
	}

	@Override
    public void onError(IDScanCommander.State status) {
		showDefaultMessage(R.string.err_no_recognized);
		mIDScanner.setScannable(true);
	}

    @Override
    public void onCompleted(Bitmap bitmap, String socialNo, String peopleName) {
//		mBeeper.playSound();

		if (ResourceFactory.UI_TEST) {
			socialNo = "66";
			peopleName = "천우형";
		}

		boolean hasSocialNo = !socialNo.isEmpty() && socialNo.length() > 0;

		if (mIvIDCard != null || (mIvIDCard = (ImageView)findViewById(R.id.imgIDCard)) != null) {
			mIvIDCard.setImageBitmap(bitmap);
		}

		if (hasSocialNo == false && (peopleName == null || peopleName.isEmpty())) {
			showDefaultMessage(R.string.err_no_recognized);
			mIDScanner.setScannable(true);
		}
		else {
			if (mRecorgnizedBitmap != null) mRecorgnizedBitmap.recycle();

			mRecorgnizedBitmap = bitmap;
			mRecorgnizedName = peopleName;
			mRecorgnizedNo = socialNo;

			mRebuidByRecorgnizing = true;
			mSearchMethods[0].setText(null);
			mSearchMethods[1].setText(hasSocialNo? null : peopleName);
			if(mFactory.getProperty(ResourceFactory.CONFIG_RECOGNIZE).equals(ResourceFactory.RECOGNIZE_full))
				mSearchMethods[2].setText(hasSocialNo? socialNo : null);
			else if(mFactory.getProperty(ResourceFactory.CONFIG_RECOGNIZE).equals(ResourceFactory.RECOGNIZE_left))
				mSearchMethods[2].setText(hasSocialNo? socialNo.subSequence(0, 6)+"*******" : null);
			else if(mFactory.getProperty(ResourceFactory.CONFIG_RECOGNIZE).equals(ResourceFactory.RECOGNIZE_right))
				mSearchMethods[2].setText(hasSocialNo? "******"+socialNo.subSequence(6, 13): null);
				
			findViewById(R.id.btnIssueSearch).performClick();
			
		}
    }

    @Override
	public void onStatus(IBScanCommander.State state) {
    	Log.d(TAG, "state : "+state.name());
    	if (state == IBSCAN_FAILED_CAPTURING) {
    		if (mVerifyFingerDialog != null && mVerifyFingerDialog.isShowing()) {
    			Log.d(TAG, "finger Match Failed...");
	    		mHandler.sendEmptyMessage(MSG_RESULT_MATCH);
    		}
    	}
		if(mFragment==null){
			switch (state){
				case IBSCAN_FAILED_CAPTURING:
				case IBSCAN_NOTSURPPORTED:
				case IBSCAN_BROKEN:
				case IBSCAN_NODEVICE:
				case IBSCAN_UNKNOWN:
					showQuestionMessage("restart Fingerprinter","FingerRestart");
					break;
			}
		}
    }

    @Override
	public void onPreview(final Bitmap bitmap) {
//    	if (mIssueStep == ISSUE_STEP_READY || mLayoutElector == null || ((SingleChoiceAdapter)mListView.getAdapter()).getSelectedIndex() < 0)
//    		bitmap.recycle();
    }

    @Override
	public void onCompleted(final ImageDataExt image) {
    	if(mIvSignpad!=null||(mIvSignpad = (SignatureView)mLayoutElector.findViewById(R.id.viewSignpad))!=null){
    		runOnUiThread(new Runnable() {
				@Override
				public void run() {
					findViewById(R.id.btnClear).setEnabled(false);
				}
			});
    	mIvSignpad.clearCanvas();}
//		mBeeper.playSound();
		SingleChoiceAdapter adapter = (SingleChoiceAdapter)mListView.getAdapter();
    	if (mVerifyFingerDialog != null&&mVerifyFingerDialog.isShowing()) {
				Log.d(TAG,"finger Match start...");
	    		mHandler.sendMessage(mHandler.obtainMessage(MSG_RESULT_MATCH, 0, 0, image));
		}else if(!(adapter.getSelectedIndex()<0)){

			Object[] elector = adapter.getItem(adapter.getSelectedIndex());
			if (mBinaryStream != null || (mBinaryStream = new ByteArrayOutputStream()) != null) {
				new Thread(){
					public void run() {
						mFactory.saveImageToBuffer(image, mBinaryStream);
					};
				}.start();
			}

			elector[10] = image;
			elector[ 9] = "F";
			elector[11] = false;

			mHandler.sendMessage(mHandler.obtainMessage(MSG_UPDATE_SEAL, 0, 1, elector));
		}
    }

    @Override
    public void onSignature(Bitmap bitmap) {
    	mFactory.stopCapture();
		SingleChoiceAdapter adapter = (SingleChoiceAdapter)mListView.getAdapter();
    	if ((adapter.getSelectedIndex() < 0 ||
    			mIssueStep != ISSUE_STEP_WAIT_FOR_SEAL)&&bitmap!=null)
    		bitmap.recycle();
    	else {
    		Object[] elector = adapter.getItem(adapter.getSelectedIndex());
    		elector[10] = bitmap;
    		elector[ 9] = "S";
			elector[11] = false;
    		mHandler.sendMessage(mHandler.obtainMessage(MSG_UPDATE_SEAL, 0, 1, elector));
    		Log.d(TAG, "signature end");
    		mFactory.stopCapture();
    		findViewById(R.id.btnClear).setEnabled(false);
		}
    }


	@Override
	public void onShow(DialogInterface dialog) {
		mNotChangeable = true;
		if(dialog.equals(mReqDialog)){
			InputMethodManager imm = (InputMethodManager) mFactory.getSystemService(INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(mReqDialog.findViewById(android.R.id.edit).getWindowToken(), 0);
		}else if(mVerifyFingerDialog !=null&&mVerifyFingerDialog.equals(dialog)){
			mFactory.startCapture();
		}else {
			mCardCheckable = true;

			if (mClosingDialog != null) {
				mFactory.getSmartCard(0);
			    mClosingStep = 0;
				mHandler.sendEmptyMessageDelayed(MSG_CLOSING_STEP_NEXT, MSG_NODELAY);
			}
		}
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		if (dialog.equals(mVerifyFingerDialog)){
			mFactory.stopCapture();
			Log.d("IBSCAN", "match dismiss");
			}

		mClosingDialog = null;
		mNotChangeable = false;
	}
	private class PdfMake extends AsyncTask<View, Void, Void>{
		
		@Override
		protected Void doInBackground(View... params) {
			try {
				InputStream is = getAssets().open("vto.pdf");
				File file = new File("/sdcard/cache/");
				if(!file.isDirectory())
					file.mkdir();
					file.createNewFile();
				FileOutputStream os = new FileOutputStream("/sdcard/cache/vto.pdf");
				byte[] buffer = new byte[is.available()];
				is.read(buffer);
				os.write(buffer);
				os.flush();
				is.close();
				os.close();
			} catch (IOException e) {
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}
			Log.d(TAG, "vto completed");
			mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_SWITCHING_VIEW, params[0]), MSG_NODELAY);
			return null;
		}
		
	};
    public void onMenu(View v) {
    	v.setClickable(false);
    	if (mIssueStep != ISSUE_STEP_READY){
   			Log.d(TAG, "[Menu] Not allow in issue procssing.");
   			showDefaultMessage(R.string.error_sgi_issue_fragment);
    	}else if (v.getId() == R.id.btnMenuLogout &&!mNotChangeable
		) {
			 mHandler.sendEmptyMessageDelayed(MSG_CLOSING_COMPLETED,MSG_NODELAY);
		}
    	else if(v.isSelected())
   			Log.i(TAG, "Already runned: " + v.getTag());
    	else if(vtOpened() == false || v != mBtnVoteOpenClose){
    		if(mSearchMethods!=null){
    			mSearchMethods[0].setEnabled(true);
    			mSearchMethods[1].setEnabled(true);
    			mSearchMethods[2].setEnabled(true);
    			mSearchMethods[0].setFocusableInTouchMode(true);
    			mSearchMethods[1].setFocusableInTouchMode(true);
    			mSearchMethods[2].setFocusableInTouchMode(true);}
    		if(v!=null&&v.getTag()!=null&&v.getTag().equals("help")){
    			Log.d(TAG, "help");
    			new PdfMake().execute(v);
    		}else
    			mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_SWITCHING_VIEW, v), MSG_NODELAY);
    		}
    	else if(mClosingDialog != null)
    		Log.d(TAG, "Already popup closing dialog");
		else {
			mClosingDialog = new Dialog(this);
			mClosingDialog.setOnShowListener(this);
			mClosingDialog.setOnDismissListener(this);
			mClosingDialog.setContentView(R.layout.popup_closing_process);
			mClosingDialog.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mClosingDialog.dismiss();
					mClosingDialog=null;
				}
			});

			mManagerCaller = mBtnVoteOpenClose.getId();
			
			mClosingDialog.show();
		}

    	v.setClickable(true);
    }

//    public boolean scannerForward() {
//		return mIDScanner.forward();
//    }
//	public boolean scannerBackward() {
//		if(mIDScanner!=null)
//		return mIDScanner.backward();
//		return false;
//    }

	private void limitMenuClickable(boolean clickable) {
		findViewById(R.id.btnMenuIssue02).setClickable(clickable);
		findViewById(R.id.btnMenuIssue03).setClickable(clickable);
	}
	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (event.getAction() == KeyEvent.ACTION_UP){
			if(keyCode==KeyEvent.KEYCODE_ENTER && mIsEnter){
				findViewById(R.id.btnIssueSearch).performClick();
			return true;
			}
		}
		return false;
	}
	@Override
	public void onClick(View v) {
		v.setClickable(false);

		switch (v.getId()) {
			case R.id.btnByeFragment: {
				boolean vtWorking = vtOpened();
				Object param = vtWorking? v.getTag() : null;
				relayout(mFragment, vtWorking? null : mBtnVoteOpenClose);
				if (param == null)
					break;
				else {
					mSearchMethods[0].setText(param.toString());
					mSearchMethods[1].setText(null);
					mSearchMethods[2].setText(null);
				}
			}
			case R.id.btnIssueSearch:
				if(!mFactory.isCapturable()){
					showDefaultMessage(R.string.conn4fingerprint);
					break;
				}
				mIsEnter = false;
				if(mIsCompleted){
					showDefaultMessage(R.string.error_no_detached_scard);
				}
				else{				if (mNotChangeable){
		   			Log.d(TAG, "[Search] Not allow in procssing.");
		   			showDefaultMessage(R.string.error_sgi_issue_fragment);
		   			}
				else if((mSearchData[0] = mSearchMethods[0].getText().toString()).length() > 10){
					showDefaultMessage(getString(R.string.format_over_limit, getString(R.string.menu_issue_01_search_by_regno_hint), 10));
				mIsEnter = true;}
				else if((mSearchData[1] = mSearchMethods[1].getText().toString()).length() > 50){
					showDefaultMessage(getString(R.string.format_over_limit, getString(R.string.menu_issue_01_search_by_name_hint), (mSearchData[1].length()/3)));
					mIsEnter = true;
				}
				else if((mSearchData[2] = mSearchMethods[2].getText().toString()).length() > 13){
					showDefaultMessage(getString(R.string.format_over_limit, getString(R.string.menu_issue_01_search_by_socialno_hint), 13));
				mIsEnter = true;	
				}
				else {
					boolean searchable = false;
					mListView.setFocusable(true);
					for(int i = 0; i < mSearchData.length; ++i) {
						if (mSearchData[i].length() <= (i & 1)) // 성명은 2자 이상
							mSearchData[i] = "";
						else {
							searchable = true;

							if (i == 1 && mSearchData[i].length() < 50 ||
								i == 2 && mSearchData[i].length() < 13)
							{
								mRecorgnizedNo = null;
								mRecorgnizedName = null;
								mSearchData[i] = '%' + mSearchData[i] + '%';
							}
						}
					}

					if(mPlaceCodes.size()<1)
						mFactory.dbQuery(QueryID.Q19);
						try {
							Thread.sleep(500);
						} catch (InterruptedException e) {
							Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
						}
						if(ResourceFactory.UI_TEST){
							if (searchable && mFactory.dbQuery(QueryID.Q70)) {
								findViewById(R.id.btnScreenClean).setEnabled(true);
								mIDScanner.setScannable(false);
							}
						}
						else{
							if (searchable && mFactory.dbQuery(QueryID.Q32, QueryID.Q13)) {
								findViewById(R.id.btnScreenClean).setClickable(true);
								mIDScanner.setScannable(false);
							}
						}
				}
				}
				break;

			case R.id.btnVerify: //종이일때 확인할것.
					if (mVerifyFingerDialog == null) {
						mVerifyFingerDialog = new Dialog(this);
						mVerifyFingerDialog.setCancelable(false);
						mVerifyFingerDialog.setContentView(R.layout.popup_verify_fingerprint);
						mVerifyFingerDialog.findViewById(R.id.btnCancel).setOnClickListener(this);
						mVerifyFingerDialog.setOnDismissListener(this);
						mVerifyFingerDialog.setOnShowListener(this);
					}

					((TextView)mVerifyFingerDialog.findViewById(R.id.infoStatus)).setText(R.string.menu_issue_01_popup_verify_fingerprint_ready);
					((ImageView)mVerifyFingerDialog.findViewById(R.id.img01)).setImageBitmap(null);
					((ImageView)mVerifyFingerDialog.findViewById(R.id.imgOriginFingerprint)).setImageBitmap(mFactory.convertImageToBitmap((ImageDataExt)mElector[10]));
					((ImageView)mVerifyFingerDialog.findViewById(R.id.imgVerifyFingerprint)).setImageBitmap(null);

					mVerifyFingerDialog.show();
				break;
		case R.id.btnCancel:
			if (mClosingConfirmDialog != null) {
				mClosingConfirmDialog.dismiss();
				mClosingConfirmDialog = null;
			if (mClosingDialog != null&&mClosingDialog.isShowing()){
				if(mIsClose)
					mHandler.sendEmptyMessageDelayed(MSG_CLOSING_COMPLETED,MSG_NODELAY);
				else{
					mClosingDialog.dismiss();
					mClosingDialog=null;}
				}
			}else if (mVerifyFingerDialog != null && mVerifyFingerDialog.isShowing())
				mVerifyFingerDialog.dismiss();
			else if (mFragment != null && mFragment.getClass().equals(IssuedTicketFragment.class))
				((IssuedTicketFragment) mFragment).onClick(v);
			else if (mReqDialog != null && mReqDialog.isShowing())
				mReqDialog.dismiss();

			break;
			case R.id.btnScreenClean:
				findViewById(R.id.btnClear).setEnabled(false);
				if(mFragment != null){
					mIsEnter = true;
					((BasicFragment)mFragment).onClick(v);
				}else{
					SingleChoiceAdapter adapter1 = (SingleChoiceAdapter)mListView.getAdapter();
//					mFactory.stopCapture();
					if(adapter1!=null&&mListView.getCount()>0){
					mIsCompleted=true;
					mIsEnter = true;
					mHandler.sendEmptyMessage(MSG_ERROR);
					}
				}
				break;
			case R.id.btnClear:
				if(isActiveSignpad()&&mIvSignpad.isDrawingComplete()){
					mLayoutElector.findViewById(R.id.btnClear).performClick();
				}else if(mIvSignpad ==null)
					showDefaultMessage(R.string.doesntSearch);
				else 
					if (mIvSignpad == null || !mIvSignpad.isDrawingComplete())
					showDefaultMessage(R.string.hasntSignature);
				break;
			case R.id.btnAccept:
				if(isActiveSignpad()&&mIvSignpad.isDrawingComplete()){
					mLayoutElector.findViewById(R.id.btnAccept).performClick();
					mIvSignpad.setDrawingComplete(false);
				}else if(mIvSignpad ==null){
					showDefaultMessage(R.string.doesntSearch);
				}else
					if(mIvSignpad==null||!mIvSignpad.isDrawingComplete())
					{showDefaultMessage(R.string.hasntSignature);
				}
				break;
			default:
				if(mFragment == null||mFragment.getClass().equals(IssuedTicketFragment.class))
					((IssuedTicketFragment)mFragment).onClick(v);
				break;
		}

		v.setClickable(true);
	}

	// Call by OpeningFragment
	public void openVoting() {
		mBtnVoteOpenClose.setText(R.string.menu_vote_close);
		limitMenuClickable(true);
		relayout(mFragment, null);
	}

	public synchronized void relayout(Fragment f, View caller) {
		dismissedMessageBox();
		if(caller == null && mFactory.getProperty(ResourceFactory.CONFIG_SERVER_MODE).equals(ResourceFactory.MODE_server))
			caller = findViewById(R.id.btnMenuManage02);//서버모드 일경우 투표 진행상황으로 화면 전환
		if (f == null && caller == null)
   			Log.e(TAG, "[relayout] Request a change view but wrong.");
		else if(mNotChangeable){
			showDefaultMessage(R.string.error_sgi_issue_fragment);
   			Log.d(TAG, "[relayout] Not allow in procssing.");
		}else try {
			boolean wantActive = caller != null && caller.getTag() != null;
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			if (f != null) ft.remove(f);
			if (wantActive) {
//				mFactory.stopCapture();
				f = (Fragment)Class.forName(caller.getTag().toString()).newInstance();
				ft.add(R.id.fragment, f);
			}
    		ft.commit();

			if (mMenuSelected != null) mMenuSelected.setSelected(false);

    		if (mFragment == f) {
				mMenuSelected = findViewById(R.id.btnMenuIssue01);
    			mFragment = null;
    		}
    		else {
	    		mMenuSelected = caller != mBtnVoteOpenClose? caller : findViewById(R.id.btnMenuIssue01);
    			mFragment = f;
    		}

    		if (wantActive){mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER);
				mHandler.sendMessageAtFrontOfQueue(mHandler.obtainMessage(MSG_REBUILD_LISTVIEW, new ArrayList<Object[]>()));
    		}else
					cleanupStatus();
    		mIsEnter = true;
    		mIssueStep = ISSUE_STEP_READY;
    		mIssueFragment.setVisibility(mFragment != null? View.GONE : View.VISIBLE);
			setMenuTitle(((Button)mMenuSelected).getText().toString());
    		showSearchBar(mFragment == null||mFragment.getClass().equals(IssuedTicketFragment.class));
    		mMenuSelected.setSelected(true);
    		if(!mSearchMethods[0].getText().toString().isEmpty()&&(mFragment == null||mFragment.getClass().equals(IssuedTicketFragment.class)))
    			findViewById(R.id.btnIssueSearch).performClick();
		} catch(ClassNotFoundException e) {
            Log.e(TAG, "[relayout] ClassNotFoundException: " + e.getMessage());
		} catch(IllegalAccessException e) {
            Log.e(TAG, "[relayout] IllegalAccessException: " + e.getMessage());
		} catch(InstantiationException e) {
            Log.e(TAG, "[relayout] InstantiationException: " + e.getMessage());
		} catch(Exception e) {
            Log.e(TAG, "[relayout] Exception: " + e.getMessage());
            Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
		}
	}
	
	private void setSoundFile(String FileName){
		try {
        	AssetFileDescriptor afd = getAssets().openFd(FileName);
        	mMediaPlayer.reset();
		    mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
        	afd.close();

	        mMediaPlayer.setOnCompletionListener(this);
			mMediaPlayer.prepare();
        } catch(IOException e) {
        	Log.e(TAG, e.getMessage());
        	mMediaPlayer.release();
        	mMediaPlayer = null;
        }
	}

	@SuppressLint({"SimpleDateFormat", "StringFormatInvalid"})
	public synchronized void snapshot() {
		String path = null;
		if (!ResourceFactory.UI_TEST && !usbChecker())
			showDefaultMessage(R.string.err_nodev_storage);
		else {
			File dir = new File((ResourceFactory.UI_TEST? getFilesDir() : getUsbDirectory()) + "/snapshot");

			View v = findViewById(R.id.animation_layout_content);
			final boolean cacheEanbled = v.isDrawingCacheEnabled();
			v.setDrawingCacheEnabled(true);
			v.buildDrawingCache(true);

			Bitmap bitmap = v.getDrawingCache();
			FileOutputStream fos = null;
			boolean okay = false;

			try {
				if(!dir.exists()) dir.mkdir();

				File file = new File(dir,
									 new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()) +
									 ".jpg");

				okay = bitmap.compress(CompressFormat.JPEG, 90, fos = new FileOutputStream(file));
				if(okay)
					path = file.getAbsolutePath();
			} catch(Exception e) {
				Log.e(TAG, "Snapshot: " + e.getMessage());
			} finally {
				try { fos.close(); } catch(Exception e) {}
			}

//			showToastMessage(okay? R.string.okay_save_snapshot : R.string.err_save_snapshot, TOAST_TIMEOUT);
			showToastMessage(okay?
					getResources().getString(R.string.okay_save_snapshot, path)
					:getResources().getString(R.string.err_save_snapshot)
						, TOAST_TIMEOUT);
			
			v.destroyDrawingCache();
			v.setDrawingCacheEnabled(cacheEanbled);
		}
	}

	private void showSearchBar(boolean show) {
		if (mSearchBar != null || (mSearchBar = findViewById(R.id.layoutSearchPanel)) != null) {
			if (mSearchMethods == null) {
				mSearchMethods = new EditText[3];
				mSearchMethods[0] = (EditText)findViewById(R.id.editSearchByRegNo);
				mSearchMethods[0].setOnKeyListener(this);
				mSearchMethods[1] = (EditText)findViewById(R.id.editSearchByName);
				mSearchMethods[1].setOnKeyListener(this);
				mSearchMethods[2] = (EditText)findViewById(R.id.editSearchBySocialNo);
				mSearchMethods[2].setOnKeyListener(this);
			}

			mIDScanner.setScannable(show); // DB검색 중인 경우에는 스캔이 안되도록 제한해야 한다.
		}
		mSearchBar.setVisibility(show? View.VISIBLE : View.GONE);
	}

    @Override
	void refreshDateTime(String today) {
    	super.refreshDateTime(today);

    	if (mToday != null || (mToday = (TextView)findViewById(R.id.textDateTime)) != null) {
    		if (today.compareTo(mToday.getText().toString()) != 0) {
    			mToday.setText(today);
    			mToday.setTextColor(!new SimpleDateFormat("yyyy.MM.dd").format(mFactory.getVtDate()).equals(today.split(" ")[0])?Color.RED:Color.BLACK);
    		}
    	}
	}

    public void setMenuTitle(String title) {
    	if (mTitle != null || (mTitle = (TextView)findViewById(R.id.textContentTitle)) != null) {
    		mTitle.setText(title);
    	}
    }

	@SuppressLint("HandlerLeak")
	private Handler mHandler = new Handler() {
		final int ISSUE_STEPPER_ID[] = { R.id.imgStep00,R.id.imgStep01, R.id.imgStep02, R.id.imgStep03, R.id.imgStep04 };

		@SuppressLint("StringFormatInvalid")
		@Override
		@SuppressWarnings("unchecked")
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case MSG_ERROR:
					new Thread(){
						public void run() {
							mFactory.stopCapture();
						}
					}.start();
					mSggInfos.clear();
					mPios.clear();
					mCandidates.clear();
					SingleChoiceAdapter clear = (SingleChoiceAdapter)mListView.getAdapter();
					Object[] clearElector;
					if(clear!=null&&clear.getSelectedIndex() != -1){
						clearElector = clear.getItem(clear.getSelectedIndex())!=null?clear.getItem(clear.getSelectedIndex()):new Object[15];
						clearElector[ 4] = null; // VT_YN
						clearElector[ 5] = null; // ETC
						clearElector[ 7] = null; // VT_DATE
						clearElector[ 9] = null; // SEAL_CD
						clearElector[10] = null; // SEAL_IMG
					}else{
						 clearElector= null;
					}
					if(mIvIDCard!=null||(mIvIDCard = (ImageView)findViewById(R.id.imgIDCard)) != null)
						mIvIDCard.setImageResource(R.drawable.img_idcard_default_01);
					mIsEnter = true;
					mIssueStep = ISSUE_STEP_READY;
			        mNotChangeable = false;
			        mIDScanner.setScannable(true);
			        mPosition = -1;
			        selectedItem();// re-select and update detail info.
			        cleanupStatus();
			        if(clear!=null){
			        clear.clear();
			        clear.setNotifyOnChange(true);}
//			        for (int i = 0; i < ISSUE_STEPPER_ID.length; i++) {
//						Button b=(Button)findViewById(ISSUE_STEPPER_ID[i]);
//						b.setBackgroundResource(R.drawable.img_step_08_01);
//						b.setTextColor(Color.WHITE);
//					}
			        if(mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_card)){
			        	((Button)findViewById(R.id.imgStep02)).setText(getResources().getString(R.string.menu_issue_01_step_02));
			        	((Button)findViewById(R.id.imgStep03)).setText(getResources().getString(R.string.menu_issue_01_step_03));
			        	((Button)findViewById(R.id.imgStep04)).setText(getResources().getString(R.string.menu_issue_01_step_04));
			        }
			        Button v1=(Button)findViewById(ISSUE_STEPPER_ID[0]);
//			        v1.setBackground(getResources().getDrawable(R.drawable.img_step_08_01));
//					((Button)v1).setTextColor(Color.WHITE);
//					((AnimationDrawable)v1.getBackground()).start();
			        if(mIsCompleted){
			        	mIsCompleted = false;
				        for (EditText et : mSearchMethods) {
				        	et.setText("");
				        	et.setEnabled(true);
				        	et.setFocusable(true);
				        	et.setFocusableInTouchMode(true);
				        }
				        mListView.setActivated(true);
			        }
			        findViewById(R.id.btnScreenClean).setClickable(false);
				break;
				case MSG_REBUILD_LISTVIEW: {
					SingleChoiceAdapter adapter = new SingleChoiceAdapter(MainActivity.this, (ArrayList<Object[]>)msg.obj);
					adapter.setGestureItem(mGestureItem);
					mListView.setAdapter(adapter);
					
					if (mRebuidByRecorgnizing)
						mRebuidByRecorgnizing = false;
					else {
						if (mIvIDCard != null) mIvIDCard.setImageBitmap(null);
						if (mRecorgnizedBitmap != null) mRecorgnizedBitmap.recycle();
						mRecorgnizedBitmap = null;
						mRecorgnizedName = null;
						mRecorgnizedNo = null;
					}

					if (adapter.getCount() == 1){ // 검색된 유권자가 1명인 경우 자동 선택
						mPosition = 0;
						selectedItem();
					}else {
						if (mIssueDetail != null) mIssueDetail.setVisibility(View.GONE);
						activeElectorLayout(false);
					}

					msg.obj = null;
					// 이어서 처리
				}
				case MSG_UPDATE_SEAL: {
					Object[] elector = (Object[])msg.obj;
					try {
						if (mIvFingerprint != null || (mIvFingerprint = (ImageView) findViewById(R.id.imgVerifyFingerprint)) != null) {
							Bitmap bitmap = elector == null || elector[9] == null || elector[9].toString().charAt(0) == 'S' ?
									null : mFactory.convertImageToBitmap((ImageDataExt) elector[10]);
							mIvFingerprint.setImageBitmap(bitmap);

							if (bitmap != null && mIvPreviewFingerprint != null) {
								if (isActiveSignpad()) activeFingerprint();
								mIvPreviewFingerprint.setImageBitmap(bitmap);
							}
						}

						//서명 이미지 처리
						if (mIvSignature == null) {
							mIvSignature = (SignatureMirrorView) findViewById(R.id.imgVerifySignatureMain);
						}
						if (mIvSignatureRead == null) {
							mIvSignatureRead = (ImageView) findViewById(R.id.imgVerifySignatureRead);
						}
						if (mIvSignpad == null && mLayoutElector != null) {
							mIvSignpad = (SignatureView) mLayoutElector.findViewById(R.id.viewSignpad);
						}
						if (mIvSignpadRead == null && mLayoutElector != null) {
							mIvSignpadRead = (ImageView) mLayoutElector.findViewById(R.id.viewSignpadRead);
						}

						if (elector != null && elector[9] != null && elector[9].toString().charAt(0) != 'F') {
							mIvSignature.setVisibility(View.GONE);
							mIvSignatureRead.setVisibility(View.VISIBLE);
							//						if (mIvSignpad == null && mLayoutElector != null && (mIvSignpad = (SignatureView)mLayoutElector.findViewById(R.id.viewSignpad))!=null
							//								&& mIvSignpadRead == null && mLayoutElector != null&&(mIvSignpadRead = (ImageView)mLayoutElector.findViewById(R.id.viewSignpadRead))!=null){
							//							mIvSignpad.setVisibility(View.GONE);
							//							mIvSignpadRead.setVisibility(View.VISIBLE);}
							mIvSignatureRead.setImageBitmap((Bitmap) elector[10]);
							mIvSignpadRead.setImageBitmap((Bitmap) elector[10]);
						} else {
							mIvSignature.setVisibility(View.VISIBLE);
							mIvSignatureRead.setVisibility(View.GONE);
							//						if (mIvSignpad == null && mLayoutElector != null && (mIvSignpad = (SignatureView)mLayoutElector.findViewById(R.id.viewSignpad))!=null
							//								&& mIvSignpadRead == null && mLayoutElector != null&&(mIvSignpadRead = (ImageView)mLayoutElector.findViewById(R.id.viewSignpadRead))!=null){
							//							mIvSignpad.setVisibility(View.VISIBLE);
							//							mIvSignpadRead.setVisibility(View.GONE);
							//						}
						}

						if (elector != null) {
							if ("N".equals(elector[4]) && (mBtnVerify != null || (mBtnVerify = (Button) findViewById(R.id.btnVerify)) != null)) {
								mBtnVerify.setVisibility("S".equals(elector[9]) ? View.GONE : View.VISIBLE);
								mBtnVerify.setText(R.string.menu_issue_01_cmpare_fingerprint);
								mBtnVerify.setEnabled(mIssueStep == ISSUE_STEP_READY && elector[10] != null);
							}

							if (msg.arg2 != 0) { // updated (or added) seal
								if (mIssueStep != ISSUE_STEP_READY) {
									sendEmptyMessage(MSG_ISSUE_STEP_NEXT);
								}
								break;
							}
							// 등록된 유권자의 지문 또는 사인 이미지를 아직 읽어오지 못했다면
							else if (msg.arg1 == 0 && (Boolean) elector[11]) {
								elector[11] = false;
								mElector = elector;
								mIsEnter = false;
								if (ResourceFactory.UI_TEST) {
									if (mFactory.dbQuery(QueryID.Q72)) mNotChangeable = true;
								} else if (mFactory.dbQuery(QueryID.Q32, QueryID.Q14))
									mNotChangeable = true;
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					// 이어서 처리
				}
				case MSG_ISSUE_STEP_READY: {
//					final int icons[] = { R.drawable.ic_step_01_01, R.drawable.ic_step_01_02, R.drawable.ic_step_01_03, R.drawable.ic_step_01_04 };

					if (mIssueStep != ISSUE_STEP_READY) {
						mIssueStep = ISSUE_STEP_READY;

						if (msg.arg1 != 0) { // issue ready or canceled (reset seal)
							SingleChoiceAdapter adapter = (SingleChoiceAdapter)mListView.getAdapter();
							Object[] elector = adapter.getItem(adapter.getSelectedIndex());
							elector[10] = null;
							elector[ 9] = null;
						}
					}

					removeMessages(MSG_ISSUE_STEP_NEXT);
					for(int i = 0; i < ISSUE_STEPPER_ID.length; ++i) {
						Button b = (Button)findViewById(ISSUE_STEPPER_ID[i]);

						if (b.getVisibility() == View.VISIBLE) {
//				        	b.setCompoundDrawablesWithIntrinsicBounds(icons[i], 0, 0, 0);
							b.setBackgroundResource(R.drawable.img_step_09);
							b.setTextColor(getResources().getColor(R.color.normal_steps));
						}
					}
					Button v2=(Button)findViewById(ISSUE_STEPPER_ID[0]);
			        v2.setBackground(getResources().getDrawable(R.drawable.img_step_08_01));
					((Button)v2).setTextColor(Color.WHITE);
//					((AnimationDrawable)v2.getBackground()).start();
					mIDScanner.setScannable(msg.arg1 == 0 && mFragment == null);
//					mIDScanner.backward(mFactory.getSmartCard(1)); // 카드가 걸려있을 수 있으니...
					if (msg.arg1 == 0) break;
					// 이어서 처리
					mManagerCaller = 0;
					mNotChangeable = false;
//					mCardAttachable = false;
				}
				case MSG_ISSUE_STEP_NEXT:
					if (mIssueStep > -1) {
						Button b;
						int prevStep = mIssueStep;
						while((b = (Button)findViewById(ISSUE_STEPPER_ID[mIssueStep])) != null && b.getVisibility() == View.GONE);
//			        	b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_step_01_05, 0, 0, 0);
						b.setBackgroundResource(R.drawable.img_step_09);
						b.setTextColor(Color.BLACK);
//						if (prevStep == ISSUE_STEP_READY) {mFactory.stopCapture();
//						Log.d("IBSCAN", "next Ready task...");
//						}
						if(mIsCompleted){
							SingleChoiceAdapter adapter = (SingleChoiceAdapter)mListView.getAdapter();
							Object[] elector = adapter.getItem(adapter.getSelectedIndex());
							elector[ 4] = null; // VT_YN
							elector[ 5] = null; // ETC
							elector[ 7] = null; // VT_DATE
							elector[ 9] = null; // SEAL_CD
							elector[10] = null; // SEAL_IMG
							dismissedMessageBox();
							cleanupStatus();
							mIssueStep = ISSUE_STEP_READY;
							mIsEnter=true;
					        mNotChangeable = false;
					        mIDScanner.setScannable(true);
					        mPosition = -1;
					        selectedItem();// re-select and update detail info.
					        adapter.clear();
					        adapter.setNotifyOnChange(true);
					        for (EditText et : mSearchMethods) {
					        	et.setText("");
					        	et.setEnabled(true);
					        	et.setFocusableInTouchMode(true);
					        }
					        mIsCompleted=false;
					        mListView.setActivated(true);
					        if(mIvIDCard!=null||(mIvIDCard = (ImageView)findViewById(R.id.imgIDCard)) != null)
					    		mIvIDCard.setImageResource(R.drawable.img_idcard_default_01);
					        findViewById(R.id.btnScreenClean).setClickable(false);
					        if(mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_card)){
					        	((Button)findViewById(R.id.imgStep02)).setText(getResources().getString(R.string.menu_issue_01_step_02));
					        	((Button)findViewById(R.id.imgStep03)).setText(getResources().getString(R.string.menu_issue_01_step_03));
					        	((Button)findViewById(R.id.imgStep04)).setText(getResources().getString(R.string.menu_issue_01_step_04));
					        }
					        break;
						}
					}

					if (mIssueStep+1 >= (mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_card)? ISSUE_STEPPER_ID.length:
						mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_paper)?ISSUE_STEPPER_ID.length-1:ISSUE_STEPPER_ID.length-2)) {
						// completed
//						Button b = (Button)findViewById(ISSUE_STEPPER_ID[4]);
//						b.setBackground(getResources().getDrawable(R.drawable.img_step_08_01));
//						b.setTextColor(Color.WHITE);
						showDefaultMessage(R.string.mcard_init_complete);
				        String method = mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD);
				        mIsCompleted = true;
						if (method == null || method.equals(ResourceFactory.ISSUE_card)) {
//							mIDScanner.backward(mFactory.getSmartCard(1)); // 발급 완료된 카드 배출
						}else if(mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_paper)){
							
//							try {
//								Thread.sleep(5000);
//								mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT);
//							} catch (InterruptedException e) {
//								// TODO Auto-generated catch block
//								Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
//							}
						}
						if (mElectorHelper != null) {
							mElectorHelper.setText(R.string.please_vote);
						}
					}
					else {
						Log.d(TAG, "mIssueStep:"+mIssueStep);
						switch(mIssueStep) { // 다음 단계
							case ISSUE_STEP_READY:
								mIDScanner.closeTimer();
								try {new Thread().sleep(500);} catch (InterruptedException e1) {e1.printStackTrace();}
								if((mFingerThread==null) || (!mFingerThread.isAlive())){
									mFingerThread= new Thread(){
										public void run(){
											mFactory.startCapture();
										}
									};
//									mFingerThread.setPriority(Thread.MAX_PRIORITY);
									mFingerThread.start();}
								try {new Thread().sleep(100);} catch (InterruptedException e1) {e1.printStackTrace();}
								mIvSignpad.startThread();
								mIvSignature.startThread();
								findViewById(R.id.btnClear).setEnabled(true);
								if(mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_paper))
									mFactory.dbQuery(QueryID.Q69);
//								Button v2 = (Button)findViewById(ISSUE_STEPPER_ID[0]);
//								v2.setBackgroundResource(R.drawable.img_step_09);
//								v2.setTextColor(getResources().getColor(R.color.normal_steps));
								
								break;
							case ISSUE_STEP_WAIT_FOR_SEAL: // 지문확인 완료 -> 카드투입 확인중
								mIvSignpad.stopThread();
								mIvSignature.stopThread();
								new Thread(){
									@Override
									public void run() {
										super.run();
										ibScanClose();
									}
								}.start();
								if(mIssueStep<3){
									if(mMediaPlayer!=null){
									setSoundFile(mMediaFileName[mIssueStep]);
									if(mMediaPlayer.isPlaying())
										mMediaPlayer.stop();
									mMediaPlayer.seekTo(0);
									mMediaPlayer.start();
									}
								}
								// 투표권 발급용 스마트카드 연결
								MiruSmartCard card = mFactory.getSmartCard(0);
								try {new Thread().sleep(100);} catch (InterruptedException e) {e.printStackTrace();}
								if (mFactory.getProperty(
										ResourceFactory.CONFIG_ISSUE_METHOD).equals(
										ResourceFactory.ISSUE_paper)){
										mFactory.dbQuery(QueryID.Q62);
										if (mElectorHelper != null) {
											mElectorHelper
											.setText(R.string.wait_for_issue_card);
										}
								}else if (mFactory.getProperty(
										ResourceFactory.CONFIG_ISSUE_METHOD).equals(
										ResourceFactory.ISSUE_batch))
									mFactory.dbQuery(ResourceFactory.UI_TEST?QueryID.Q71:QueryID.Q38);
								else if (card == null) {
									sendMessageAtFrontOfQueue(obtainMessage(MSG_UPDATE_SEAL, 0, 0, null));
									showDefaultMessage(getString(R.string.err_nodev_writer, 0));
									return;
								}else if(card.CardGetType().equals("V")){
									mCardCheckable = true;
									break;
								}else if(card.CheckCardInOut()!=MiruSmartCard.SUCCESS){
									((Button)findViewById(R.id.imgStep02)).setText(getResources().getString(R.string.menu_issue_01_step_02_2));
									showDefaultMessage(getString(R.string.menu_issue_03_result_3));
									mIsCardChecking = true;
									mCardCheckable = false;
								}
								else{
								mIsCardChecking = true;
								mCardCheckable = false;
								}
								if(!ResourceFactory.UI_TEST)
									Log.d(TAG,"type card : "+card.CardGetType());
								mIsEnter = false;
//								mIDScanner.forward(); // 투표권 카드 밀어 넣기 (스캐너 안쪽에 투표권 카드 장치가 있음)
								break;
							case ISSUE_STEP_WAIT_FOR_SCARD: // 카드투입 완료 -> 카드발급 처리중 (투표용지 인쇄중)
								if (mFactory.getProperty(
										ResourceFactory.CONFIG_ISSUE_METHOD).equals(
										ResourceFactory.ISSUE_paper)) {
										SingleChoiceAdapter adapter = (SingleChoiceAdapter)mListView.getAdapter();
										mElector = adapter.getItem(adapter.getSelectedIndex());
										}
								break;
							//default: // 카드발급 처리(투표용지 인쇄) 완료 -> 카드발급 완료
						}

						View v = findViewById(ISSUE_STEPPER_ID[mIssueStep+1]);
							v.setBackground(getResources().getDrawable(R.drawable.img_step_08_01));
							((Button)v).setTextColor(Color.WHITE);
//						((AnimationDrawable)v.getBackground()).start();
						
						// find next stepper
						while(++mIssueStep < (mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_card)? ISSUE_STEPPER_ID.length:
							mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_paper)?ISSUE_STEPPER_ID.length-1:ISSUE_STEPPER_ID.length-2)
							&& (v = findViewById(ISSUE_STEPPER_ID[mIssueStep])) != null && v.getVisibility() == View.GONE);
					}
					break;
				case MSG_RESULT_MATCH:
					if (mVerifyFingerDialog != null && mVerifyFingerDialog.isShowing()) {
						int a=mFactory.match((ImageDataExt)mElector[10],(ImageDataExt)msg.obj);
						Log.d(TAG, "finger bitmap handler : MSG_RESULT_MATCH");
						Bitmap bitmap = msg.obj != null? mFactory.convertImageToBitmap((ImageDataExt)msg.obj) : null;
			    		int resultSymbol, resultStatus;
			    		Log.d(TAG, "match score is " + msg.arg1);
			    		if (bitmap == null) { // failed
			    			resultSymbol = R.drawable.img_symbol_03;
			    			resultStatus = R.string.menu_issue_01_popup_verify_fingerprint_result_2;
			    		}
			    		else if(a > 0) { // equal
			    			resultSymbol = R.drawable.img_symbol_01;
			    			resultStatus = R.string.menu_issue_01_popup_verify_fingerprint_result_0;
			    		}
			    		else { // not equal
			    			resultSymbol = R.drawable.img_symbol_02;
			    			resultStatus = R.string.menu_issue_01_popup_verify_fingerprint_result_1;
			    		}
						((TextView) mVerifyFingerDialog.findViewById(R.id.infoStatus)).setText(resultStatus);
			    		((ImageView)mVerifyFingerDialog.findViewById(R.id.img01)).setImageResource(resultSymbol);
						((ImageView)mVerifyFingerDialog.findViewById(R.id.imgVerifyFingerprint)).setImageBitmap(bitmap);
					};
					break;
				case MSG_CLOSING_STEP_NEXT:
					if (mClosingDialog == null)
						break;
					else {
						final int steps[] = { R.id.imgStep01, R.id.imgStep02, R.id.imgStep03, R.id.imgStep04, R.id.imgStep05, R.id.imgStep06 };

						if (mClosingStep > 0) {
							Button b = (Button)mClosingDialog.findViewById(steps[mClosingStep-1]);
				        	b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_step_01_05, 0, 0, 0);
							b.setBackgroundResource(R.drawable.img_step_03);
							b.setTextColor(Color.WHITE);
						}	

						if (mClosingStep >= steps.length) {
							try {new Thread().sleep(3000);} catch (InterruptedException e) {e.printStackTrace();}
							limitMenuClickable(false);
							mBtnVoteOpenClose.setText(R.string.menu_vote_open);
							mFactory.setVtStatus(ResourceFactory.VOTE_finished);
							((Button)mClosingDialog.findViewById(R.id.btnCancel)).setText(R.string.ok);
							mClosingDialog.findViewById(R.id.btnCancel).setEnabled(true);
							mIsClose=true;
							Button b = (Button)mClosingDialog.findViewById(steps[5]);
				        	b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_step_01_05, 0, 0, 0);
							b.setBackgroundResource(R.drawable.img_step_03);
							b.setTextColor(Color.WHITE);
							((TextView)mClosingDialog.findViewById(R.id.textview01)).setText(R.string.menu_issue_01_popup_closing_completefile);
						}
						else {
							if (mClosingStep == 1) {
								((TextView)mClosingDialog.findViewById(R.id.textview01)).setText(R.string.menu_issue_01_popup_closing_notallow);
							    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd", getResources().getConfiguration().locale);

							    mClosingConfirmDialog = new Dialog(MainActivity.this);
								mClosingConfirmDialog.setContentView(R.layout.popup_closing_confirm);
								mClosingConfirmDialog.findViewById(R.id.btnCancel).setOnClickListener(MainActivity.this);
								mClosingConfirmDialog.findViewById(R.id.btnOkay).setOnClickListener(new OnClickListener() {
									
									@Override
									public void onClick(View v) {
										mClosingFactors.set(6, ((TextView)mClosingConfirmDialog.findViewById(R.id.editVotingPaperCount)).getText().toString());
										mClosingFactors.set(5, ((TextView)mClosingConfirmDialog.findViewById(R.id.editDeivceWriterCount)).getText().toString());
										if (mClosingConfirmDialog != null) {
											mClosingConfirmDialog.dismiss();
											mClosingConfirmDialog = null;
										}
										if (mFactory.dbQuery(QueryID.Q51)) {
											mClosingDialog.setCancelable(false);
											mClosingDialog.findViewById(R.id.btnCancel).setEnabled(false);
//											mHandler.sendEmptyMessageDelayed(MSG_CLOSING_STEP_NEXT, MSG_NODELAY);
										}
										
									}
								});

								((TextView)mClosingConfirmDialog.findViewById(R.id.infoVoteTitle)).setText(mFactory.getProperty(ResourceFactory.CONFIG_VTTITLE));
								((TextView)mClosingConfirmDialog.findViewById(R.id.infoVotePlace)).setText(mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME));
								((TextView)mClosingConfirmDialog.findViewById(R.id.infoVoteDate)).setText(dateFormat.format(new java.util.Date(mFactory.getVtDate())));
								mClosingConfirmDialog.show();
								if(mFactory.isJson())mFactory.dbQuery(QueryID.Q48);
								else
								mFactory.dbQuery(QueryID.Q48, QueryID.Q49, QueryID.Q50);
							}
							else {
								View v = mClosingDialog.findViewById(steps[mClosingStep]);
								v.setBackground(getResources().getDrawable(R.drawable.img_step_long));
//								((AnimationDrawable)v.getBackground()).start();
								if (mClosingStep==2) {//마감정보확인
									((TextView)mClosingDialog.findViewById(R.id.textview01)).setText(R.string.menu_issue_01_popup_closing_notallow);
									new Thread(){
										public void run() {
											if(!mIsCardCert)
											try {//카드 인증서 가져오는 작업 기다림.
												Thread.sleep(2000);
											} catch (InterruptedException e) {
												e.printStackTrace();
											}
											mIsCardCert = false;
											String[] statuses = new String[20];
											boolean isCardVote = true;
											for (int i = 1; i <= 20; i++) {
												byte cData[] = new byte[24];
												if(!ResourceFactory.UI_TEST&&
														(mFactory.getSmartCard(0).TCardReadCSectionRow(new byte[3], (byte)(0x00+i), cData))!=MiruSmartCard.SUCCESS &&
														(statuses[i-1]=new String(cData))!=null&&
														statuses[i-1].charAt(0)=='B'
														){
													showDefaultMessage(R.string.mcard_error_vote_machine);
													isCardVote = false;
													break;
												}
											}
											if(isCardVote)
												mHandler.sendEmptyMessage(MSG_CLOSING_STEP_NEXT);
										};
									}.start();
								}else if(mClosingStep==3){//USB여부 체크
									((TextView)mClosingDialog.findViewById(R.id.textview01)).setText(R.string.menu_issue_01_popup_closing_usbcheck);
									if(usbChecker()) sendEmptyMessage(MSG_CLOSING_STEP_NEXT);
									else {showQuestionMessage(R.string.err_nodev_storage_close,TAG_CLOSE);
									 mClosingUSBCheckTimer = new Timer();
						              mClosingUSBCheckTimer.scheduleAtFixedRate(new USBChecerTask(), 1000, 1000);
									}
								}else if(mClosingStep==4){
									dismissedMessageBox();
									if(mClosingUSBCheckTimer!=null){
										mClosingUSBCheckTimer.cancel();mClosingUSBCheckTimer.purge();
										mClosingUSBCheckTimer=null;
									}
									if(!writeClosingInSCard(false)){
										showDefaultMessage(R.string.error_vote_closing);
										mClosingDialog.findViewById(R.id.btnCancel).setEnabled(true);
									}
									if(mIsNoneUSB)
										sendEmptyMessage(MSG_CLOSING_STEP_NEXT);
									else{
										showQuestionMessage(R.string.menu_issue_01_popup_closing_delete_usb,TAG_CLOSE_DELETE);
										((TextView)mClosingDialog.findViewById(R.id.textview01)).setText(R.string.menu_issue_01_popup_closing_writefile);
									}
								}else if(mClosingStep==5){
									try {new Thread().sleep(2000);} catch (InterruptedException e) {e.printStackTrace();}
									sendEmptyMessage(MSG_CLOSING_STEP_NEXT);
								}
								
							}

							Log.d(TAG, "Closing step : "+mClosingStep);
							mClosingStep++;
						}
					}
					break;
				case MSG_CLOSING_COMPLETED:
					if (mClosingDialog != null&&mClosingDialog.isShowing()) mClosingDialog.dismiss();
					setResult(RESULT_OK);
					finish();
					break;
				case MSG_UPDATE_CLOSING_INFO:
					if (mClosingConfirmDialog != null) {
					    SimpleDateFormat dateFormat = new SimpleDateFormat("a hh : mm", Locale.KOREA);
					    View v;

					    int indexOfState;
						if (ResourceFactory.VOTE_working.equals(mClosingFactors.get(0)))
							indexOfState = 1;
						else
							indexOfState = ResourceFactory.VOTE_finished.equals(mClosingFactors.get(0))? 2 : 0;

						((TextView)mClosingConfirmDialog.findViewById(R.id.infoVoteStatus)).setText(getResources().getStringArray(R.array.voteSate)[indexOfState]);
						((TextView)mClosingConfirmDialog.findViewById(R.id.infoVoteDateStart)).setText(dateFormat.format((java.util.Date)mClosingFactors.get(1)));
						((TextView)mClosingConfirmDialog.findViewById(R.id.infoVoteDateFinish)).setText(dateFormat.format((java.util.Date)mClosingFactors.get(2)));
						// 카드 발급자 수
						((TextView)mClosingConfirmDialog.findViewById(R.id.infoVoteElectors)).setText(getString(R.string.format_issued_count, Integer.parseInt(mClosingFactors.get(7).toString())));
						// 선거인 수
						int a;
						if (indexOfState == 1) {
							a = Integer.parseInt(mClosingFactors.get(7).toString()) - Integer.parseInt(mClosingFactors.get(9).toString());
							mClosingFactors.set(8, Integer.toString(a));}
						else
							a=	Integer.parseInt(mClosingFactors.get(8).toString());
						
						((TextView)mClosingConfirmDialog.findViewById(R.id.infoVoters)).setText(getString(R.string.format_issued_count, a));

						((EditText)(v = mClosingConfirmDialog.findViewById(R.id.editElectorDeivceCount))).setText(mClosingFactors.get(3).toString());
						if (indexOfState == 1) v.setEnabled(true);
						((EditText)(v = mClosingConfirmDialog.findViewById(R.id.editVotingDeivceCount))).setText(mClosingFactors.get(4).toString());
						if (indexOfState == 1) v.setEnabled(true);
						((EditText)(v = mClosingConfirmDialog.findViewById(R.id.editDeivceWriterCount))).setText(mClosingFactors.get(5).toString());
						if (indexOfState == 1) v.setEnabled(true);
						((EditText)(v = mClosingConfirmDialog.findViewById(R.id.editVotingPaperCount))).setText(mClosingFactors.get(6).toString());

						TextView statusView = (TextView)mClosingConfirmDialog.findViewById(R.id.textview17);
						if (indexOfState != 1)
							statusView.setText(R.string.menu_issue_01_popup_closing_notallow);
						else {
							v.setEnabled(true); // R.id.editVotingPaperCount
							mClosingConfirmDialog.findViewById(R.id.btnOkay).setEnabled(true);
							statusView.setText(getString(R.string.menu_issue_01_popup_closing_descript, mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME)));
						}
					}
					break;
				case MSG_SWITCHING_VIEW:
					if (msg.obj == null)
			   			Log.e(TAG, "Request a change view but wrong.");
					else if(mFragment != null && ((View)msg.obj).getTag() != null && mFragment.getClass().getName().equals(((View)msg.obj).getTag()))
			   			Log.e(TAG, "Alrady runned: " + ((View)msg.obj).getTag());
					else{
						boolean isHelp = false;
						Button v = new Button(MainActivity.this);
						if(((View)msg.obj).getTag()!=null&&((View)msg.obj).getTag().equals("help")){
							isHelp = true;
							v.setTag("com.mirusys.vt.fragment.PrintPreviewFragment");
							v.setText(((Button)msg.obj).getText());
						}
						relayout(mFragment, vtOpened() || (((View)msg.obj).getTag() != null)?(isHelp?v: (View)msg.obj): mBtnVoteOpenClose);}
					break;
				case MSG_DISMISS:
					mReqDialog.dismiss();
					break;
				case MSG_PRINT:
					mPioNo=0;
					if(mFactory.getmBallotPrint()==null){
						mFactory.setmBallotPrint(new BallotPrint(mFactory));
					}
					if(mFactory.getmBallotPrint().getPrintStatus() == ResourceFactory.PRINT_READY){
						mFactory.getmBallotPrint().printBallot(mPios);
						mTimer = new Timer();
						mTimer.schedule(new PrintTask(), 3300, 300);
//						mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT);
					}else{
						showDefaultMessage(R.string.printBusy);
						mIsCompleted=true;
						mIsEnter = true;
						mHandler.sendEmptyMessage(MSG_ERROR);
					}
					break;
			}
		}
	};

    private void displayDetail(Object[] elector, boolean NotIssueable) {
		String juminNo = elector != null? elector[6].toString() : null;
		if (mTvName != null || (mTvName = (TextView)findViewById(R.id.infoElectorName)) != null) {
			mTvName.setText(elector != null? elector[2].toString() : null);
		}
		if (mTvBirthNo != null || (mTvBirthNo = (TextView)findViewById(R.id.infoElectorBirthday)) != null) {
			mTvBirthNo.setText(juminNo == null || juminNo.length() != 13? juminNo : juminNo.substring(0, 6) + '-' + juminNo.substring(6));
		}
		if (mTvRegNo != null || (mTvRegNo = (TextView)findViewById(R.id.infoElectorRegNo)) != null) {
			mTvRegNo.setText(elector != null? elector[0].toString() : null);
		}
		if (mTvAddress != null || (mTvAddress = (TextView)findViewById(R.id.infoElectorAddress)) != null) {
			mTvAddress.setText(elector == null || elector[1] == null? null : elector[1].toString());
		}
		if (mTvDesc != null || (mTvDesc = (TextView)findViewById(R.id.infoElectorDesc)) != null) {
			mTvDesc.setText(elector == null || elector[5] == null? null : elector[5].toString());
		}
//		if (mIvGender != null || (mIvGender = (ImageView)findViewById(R.id.imgGender)) != null) {
//			final int[] resGender = { R.drawable.ic_gender_w, R.drawable.ic_gender_m };
//			mIvGender.setImageResource(juminNo == null || juminNo.length() != 13? R.drawable.ic_gender_x : resGender[(juminNo.charAt(6) - '0') & 1]);
//		}
		

		if (mIvPreviewFingerprint != null ||
			mLayoutElector != null && (mIvPreviewFingerprint = (ImageView)mLayoutElector.findViewById(R.id.imgFingerprint4Elector)) != null) {
			mIvPreviewFingerprint.setImageBitmap(null);
		}

		if (mIvIDCard != null || (mIvIDCard = (ImageView)findViewById(R.id.imgIDCard)) != null) {
			if(juminNo!=null&&mRecorgnizedNo != null&&mFactory.getProperty(ResourceFactory.CONFIG_RECOGNIZE).equals(ResourceFactory.RECOGNIZE_left))
				juminNo = "%"+juminNo.substring(0, 6);
			else if(juminNo!=null&&mRecorgnizedNo != null&&mFactory.getProperty(ResourceFactory.CONFIG_RECOGNIZE).equals(ResourceFactory.RECOGNIZE_right))
				juminNo = juminNo.substring(6)+"%";
			if (mRecorgnizedBitmap != null && elector != null && ((mRecorgnizedNo != null && juminNo.equals(mRecorgnizedNo)) || (mRecorgnizedName != null && elector[2].equals(mRecorgnizedName))))
				mIvIDCard.setImageBitmap(mRecorgnizedBitmap);
			else
				mIvIDCard.setImageResource(R.drawable.img_idcard_default_01);
		}

		if (NotIssueable) {
			if (mTvIssueDate != null || (mTvIssueDate = (TextView)findViewById(R.id.infoIssuedDate)) != null) {
				mTvIssueDate.setText(elector == null || elector[7] == null? null : new SimpleDateFormat("yyyy.MM.dd  a hh:mm", getResources().getConfiguration().locale).format(elector[7]));
			}
			if (mTvIssuePlace != null || (mTvIssuePlace = (TextView)findViewById(R.id.infoIssuedPlace)) != null) {
				mTvIssuePlace.setText(elector == null || elector[8] == null? null : elector[8].toString());
			}
			if(ResourceFactory.UI_TEST){
				if (mFactory.dbQuery(QueryID.Q72)) mNotChangeable = true;
			}else
				if (mFactory.dbQuery(QueryID.Q32, QueryID.Q14)) mNotChangeable = true;
			elector[11]=false;
		}

		if (elector != null) {
			setElectorInfo(getString(R.string.format_elector_info, elector[2], juminNo.substring(0, 2), juminNo.substring(2, 4), juminNo.substring(4, 6)),
					juminNo.length() > 6 ? ((juminNo.charAt(6)-'0')%2):2, // 0 - MALE, 1 - FEMALE
							NotIssueable? ELECTOR_VOTE_COMPLETED : ELECTOR_VOTE_READY);
		}
	};

	@Override
    public synchronized void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		mPosition = position;
	}

	private void showActionBar(boolean show) {
			findViewById(R.id.btnAccept).setVisibility(show? View.GONE : View.VISIBLE);
			findViewById(R.id.btnClear).setVisibility(show? View.GONE : View.VISIBLE);
			findViewById(R.id.infoElectorAlreadyIssued).setVisibility(!show? View.GONE : View.VISIBLE);
	}

	public int getPosition() {
		return mPosition;
	}

	public void setPosition(int position) {
		if(mFragment !=null&&mFragment.getClass().equals(IssuedTicketFragment.class))
			((IssuedTicketFragment)mFragment).setPosition(position);
		this.mPosition = position;
	}

	private class NetworkInfo extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			try {
				Intent intent2 = new Intent(
						"net.innodigital.ItmeSettingService.GETIPADDR");
				sendBroadcast(intent2);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				}
				FileReader f = new FileReader(mFactory.getNetworkPath());
				BufferedReader br = new BufferedReader(f);
				String s = br.readLine();
				if (s != null){
					mFactory.setLocalIP(s);
					Log.d(TAG, "IP local : "+mFactory.getLocalIP());
				}else
					mFactory.setLocalIP("0.0.0.0");
				return mFactory.getLocalIP();
			} catch (IOException e) {
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}
			return null;
		}

	}
	@Override
	public void onEvent(int qn, String arg0) {
		mElectors=null;
		JSONObject res;
		JSONArray arr;
		if(qn==48){
			try {
				arr = new JSONArray(arg0);
			for (int i = 0; i < arr.length(); i++) {
				res = arr.getJSONObject(i);
					if(mClosingFactors !=null)
						mClosingFactors.clear();
					else
						mClosingFactors = new ArrayList<Object>();
					mClosingFactors.add(res.getString("STATUS"));
					mClosingFactors.add(new SimpleDateFormat("hhmmss").parse(res.getString("START_TIME")));
					mClosingFactors.add(new SimpleDateFormat("hhmmss").parse(res.getString("CLOSE_TIME")));
					mClosingFactors.add(res.getString("BK_MCNT"));
					mClosingFactors.add(res.getString("VT_MCNT"));
					mClosingFactors.add(res.getString("VT_XCNT"));
					mClosingFactors.add(res.getString("VT_PCNT"));
					mClosingFactors.add(res.getString("VT_CCNT"));
					mClosingFactors.add(res.getString("VT_ECNT"));
					mClosingFactors.add(res.getString("NVT_CCNT"));
					mClosingFactors.add(res.getString("VTR_ALL"));
					mClosingFactors.add(res.getString("VTR_ABS"));
				}
			mFactory.dbQuery(QueryID.Q49);
			} catch (Exception e) {
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			} 
			
		}else if(qn==49){
			try {
				arr = new JSONArray(arg0);
				for (int i = 0; i < arr.length(); i++) {
					res = arr.getJSONObject(i);
					mClosingFactors.set(7, res.getString("VT_CCNT"));
				}
				mFactory.dbQuery(QueryID.Q50);
			} catch (Exception e) {
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}
		}else if(qn==50){
			try {
				arr = new JSONArray(arg0);
				for (int i = 0; i < arr.length(); i++) {
					res = arr.getJSONObject(i);
				mClosingFactors.set(9,res.getString("NVT_CCNT"));
				mHandler.sendEmptyMessage(MSG_UPDATE_CLOSING_INFO);
				}
			} catch (Exception e) {
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}
		}else if (mFragment != null)
			((BasicFragment)mFragment).onEvent(qn, arg0);
		else if(qn==19){
			try {
				arr = new JSONArray(arg0);
			for (int j = 0; j < arr.length(); j++) {
				res = arr.getJSONObject(j);
			mPlaceCodes.remove(res.getString("TPG_ID"));
			mPlaceCodes.add(res.getString("TPG_ID"));}
			} catch (JSONException e) {
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}
		}
		else if(qn==14 || qn==72){
			try {
					arr = new JSONArray(arg0);
					for (int j = 0; j < arr.length(); j++) {
						res = arr.getJSONObject(j);
						byte[] image;
						String kindOf;
						SingleChoiceAdapter adapter = (SingleChoiceAdapter)mListView.getAdapter();
						if (adapter.getSelectedIndex() < 0)
							Log.d(TAG, "has SEAL but loses owner");
						else if((kindOf = res.getString("SEAL_CD")) != null && // 'S'ign, 'F'ingerprint
								(image = ResourceFactory.UI_TEST?Base64Util.decode(res.getString("SEAL_IMG")):
//										Base64Util.decode(res.getString("SEAL_IMG")))!=null){
									SimpleCrypto.Decrypt(
											Base64Util.decode(res.getString("SEAL_IMG")
													), mFactory.getTcardKey()))
								!= null) {
							Object[] elector = adapter.getItem(adapter.getSelectedIndex());

							if ("F".equals(elector[9] = kindOf))
								elector[10] = mFactory.loadImageFromBuffer(image);
							else
								elector[10] = BitmapFactory.decodeByteArray(image, 0, image.length);

							mHandler.sendMessage(mHandler.obtainMessage(MSG_UPDATE_SEAL, 0, 1, elector));
						}
					}
				}catch (Exception e) {
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				}	
			mNotChangeable=false;
		}
		else if(qn==23){
			try {
			arr = new JSONArray(arg0);
			byte[] SGIG = "".getBytes();
				for (int j = 0; j < arr.length(); j++) {
					res = arr.getJSONObject(j);
					SGIG = res.getString("SGIG_ID").getBytes();
					}
				if(SGIG.length==2)
					mSGIG=SGIG;
				else
					mSGIG="".getBytes();
			}catch (JSONException e) {
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}
		}
		else if(qn==13 || qn==70){
			try {
				String str = null;
				boolean isTPGIDYn= false;
				mElectors = new ArrayList<Object[]>();
				arr = new JSONArray(arg0);
				for (int j = 0; j < arr.length(); j++) {
					res = arr.getJSONObject(j);
					boolean isTpgId = false;
					if(!ResourceFactory.UI_TEST)
					for (String i : mPlaceCodes) {
						if(i.equals(res.getString("TPG_ID")))
							isTpgId=true;
						Log.i(TAG, "TPG_ID : "+i);
					}
					Object[] elector = new Object[15];
					if(ResourceFactory.UI_TEST || (isTpgId
							&&mFactory.getProperty(ResourceFactory.CONFIG_TPGID).equals(
							res.getString("TPG_ID_YN").equals("Y")?res.getString("VT_TPG_ID"):
								mFactory.getProperty(ResourceFactory.CONFIG_TPGID)))
								){
					elector[13] = res.getString("VT_TPG_ID").equals("")?"":res.getString("VT_TPG_ID");
					elector[ 0] = res.getString("DJ_NO");
					elector[ 1] = res.getString(ResourceFactory.UI_TEST?"REMARK":"ADDR");
					elector[ 2] = res.getString("NAME");
					elector[ 6] = res.getString("JUMIN_NO");
					elector[ 5] = res.getString("ETC");
					elector[ 4] = res.getString("VT_YN"); // 'Y'발급가능, 'N'발급불가
					if(!ResourceFactory.UI_TEST){
						elector[ 3] = res.getString("BIRTH_DATE");
						elector[14] = res.getString("ISSUED_LOCATION");
						elector[12] = res.getString("TPG_ID");
					}
					try {
						elector[7] = res.getString("VT_DATE").equals("") ? null
								: new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS",getResources().getConfiguration().locale).parse(res.getString("VT_DATE"));
					} catch (ParseException e) {
						Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
						elector[7] = null;
					} // 발급일자
					elector[ 8] = ResourceFactory.UI_TEST?"":res.getString("TPG_NAME"); // 발급장소
					elector[11] = true;
					mElectors.add(elector);}
					else{
						isTPGIDYn=res.getString("TPG_ID_YN").equals("Y");//지정여부
						str = res.getString("TPG_NAME");
					}
				}
				if (mElectors.size()==0) {
					if(isTPGIDYn)
						showDefaultMessage(getString(R.string.error_TPS_elector,str));//지정일경우에 해당 투표소 안내
					else
						showDefaultMessage(R.string.err_no_search_result);
					mHandler.sendEmptyMessage(MSG_ERROR);
				}
				mIsEnter = true;
			} catch (JSONException e) {
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}
		}else if(qn==32){
			try {
				arr = new JSONArray(arg0);
				for (int i = 0; i < arr.length(); i++) {
					res = arr.getJSONObject(i);
					if(res.has("STATUS")&&(ResourceFactory.UI_TEST||res.getString("STATUS").equals(ResourceFactory.VOTE_working)))
						mIssueDate =  new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
					else{
						mIsEnter = true;
						showDefaultMessage(R.string.already_vote_finished);
						mElectors = new ArrayList<Object[]>();
					}
				}
			} catch (JSONException e) {
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				mIsEnter = true;
			}
		}else if(qn==62){
			try {
				arr = new JSONArray(arg0);
				for (int i = 0; i < arr.length(); i++) {
					res = arr.getJSONObject(i);
					SGGInfoObject sggInfo = new SGGInfoObject();
					sggInfo.setSggId(res.getString("SGG_ID"));
					PrintIntentObject pio = new PrintIntentObject();
					pio.setTitleName(res.getString("SGG_NAME"));
					pio.setSubTitleName(res.getString("SGG_BPS_NAME"));
					if(res.getString("SGG_BPS_STAMP").length()>0)
						try {
							pio.setStamp(Base64Util.decode(res.getString("SGG_BPS_STAMP")));
						} catch (Exception e) {
							Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
						}
					else
						pio.setStamp(null);
					if(mFactory.getAdminStamp()==null || mFactory.getAdminStamp().length>0)
						pio.setAdminstamp(mFactory.getAdminStamp());
					else
						pio.setAdminstamp(null);
					mSggInfos.add(i, sggInfo);
					mPios.add(i, pio);
				}
					mFactory.dbQuery(QueryID.Q61);
			} catch (JSONException e) {
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				mIsEnter = true;
			}
		}else if(qn==58){
			try {
				arr = new JSONArray(arg0);
				for (int i = 0; i < arr.length(); i++) {
					res = arr.getJSONObject(i);
					mPrintSN = res.getInt("PRT_SN");
					mPios.get(mPioNo).setStartSerialNum(mPrintSN+1);
					mPios.get(mPioNo).setEndSerialNum(mPrintSN+1);
					mPios.get(mPioNo).setBallotColor(ResourceFactory.BOX_COLOR2);
					mPios.get(mPioNo).setFillType(ResourceFactory.FILL_FULL);
					mPios.get(mPioNo).setSerial(true);
					if(!ResourceFactory.UI_TEST){
						mFactory.dbQuery(QueryID.Q60);
					}else{
						mHandler.sendEmptyMessage(MSG_PRINT);
					}
				}
			} catch (Exception e) {
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}
		}else if(qn==61){
			try {
				arr = new JSONArray(arg0);
				mCandidates.clear();
				for (int i = 0; i < arr.length(); i++) {
					res = arr.getJSONObject(i);
					Candidate paper = new Candidate(res.getString("HBJ_GIHO"),
								res.getString("jd_name").equals("")?res.getString("HBJ_NAME"):res.getString("jd_name"),
								!res.getString("HBJ_NAME").equals("")?res.getString("HBJ_NAME"):res.getString("jd_name"),
							"", "", res.getString("SGG_ID"));
					
					mCandidates.add(paper);
				}
				for (int j = 0; j < mPios.size(); j++) {
					ArrayList<Candidate> candidates = new ArrayList<Candidate>();
					for (int i = 0; i < mCandidates.size(); i++) {
						Candidate paper = mCandidates.get(i);
						if(mSggInfos.get(j).getSggId().equals(paper.getId())){
							Log.d(TAG, "giho : "+paper.getGiho()+" hbjname : "+paper.getHbjName());
							if (!paper.getHbjName().equals("")
									&& !paper.getJdName().equals("")
									&& paper.getJdName().equals(paper.getHbjName())
									)
								mPios.get(j).setBallotType(ResourceFactory.BALLOT_TYPE1);
							else if (!paper.getHbjName().equals("")
									&& !paper.getJdName().equals("")
									&& !paper.getJdName().equals(paper.getHbjName()))
								mPios.get(j).setBallotType(ResourceFactory.BALLOT_TYPE0);
							else
								mPios.get(j).setBallotType(ResourceFactory.BALLOT_TYPE4);
							candidates.add(paper);
						}
						mPios.get(j).setCandidate(candidates);
						mPios.get(j).setCandidateNum(candidates.size());
					}
				}
				mFactory.dbQuery(QueryID.Q58);
			} catch (Exception e) {
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}
			}
		else if(qn==69){
			try {
				arr = new JSONArray(arg0);
			for (int i = 0; i < arr.length(); i++) {
				res = arr.getJSONObject(i);
				if(mFactory.getAdminStamp() == null || mFactory.getAdminStamp().length <=0)
					if(res.has("IMG_MGR_STAMP")&&!res.getString("IMG_MGR_STAMP").isEmpty())
					mFactory.setAdminStamp(Base64Util.decode(res.getString("IMG_MGR_STAMP")));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			} 
		}
		if (mElectors != null&&mIssueStep<1&&!mElectors.isEmpty()) {
			Log.d(TAG, "id card setting : not null");
			mHandler.sendMessageAtFrontOfQueue(mHandler.obtainMessage(MSG_REBUILD_LISTVIEW, mElectors));
		}
		return;
	}

	@Override
	public void onEvent(int qn, int updateCount) {
		if(qn==51){
			mHandler.sendEmptyMessage(MSG_CLOSING_STEP_NEXT);
		}else if (mFragment != null)
			((BasicFragment)mFragment).onEvent(qn, updateCount);
		else if (qn==38 || qn==71) {
			mPios.clear();
			mSggInfos.clear();
			closeTimer();
			if (updateCount < 1) {
				mHandler.sendMessageAtFrontOfQueue(mHandler.obtainMessage(
						MSG_UPDATE_SEAL, 1, 0, null));
				if (ResourceFactory.UI_TEST)
					Log.d(TAG, "[issue] "
							+ getString(R.string.err_nores_writer));
				else
					showDefaultMessage(R.string.err_nores_writer);
				Log.i(TAG, "update error....");
			} else {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						((Button)findViewById(R.id.imgStep04)).setText(getResources().getString(R.string.menu_issue_01_step_04));
					}
				});
				mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT); // 카드발급 완료
				try {
					if (mMediaPlayer != null) {
						if (mMediaPlayer.isPlaying())
							mMediaPlayer.stop();
						setSoundFile(mMediaFileName[mMediaFileName.length - 1]);
						mMediaPlayer.seekTo(0);
						mMediaPlayer.start();
					}
				} catch (IllegalStateException ise) {
					Log.e(TAG, "[issue] Can't play sound: " + ise.getMessage());
					mMediaPlayer.release();
					mMediaPlayer = null;
				}
				if(!mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_card)){
					try {Thread.sleep(2000);} catch (InterruptedException e) {Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();}
					mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT); //화면 삭제
				}
			}
		} else if (qn==60) {
			if(++mPioNo<mPios.size())
				mFactory.dbQuery(QueryID.Q58);
			else
				mHandler.sendEmptyMessage(MSG_PRINT);
		}
		return;
		
	}

	@Override
	public void onPrepareUpdate(SQLNO qn, String[] jsonParam) {
		if(qn.equals(SQLNO.Q51)){
			if (mClosingDialog != null && writeClosingInSCard(false)) {
				mFactory.jsonCommit(qn, jsonParam);
			}
			else {
				mClosingDialog.dismiss();
				mClosingDialog=null;
			}
		}else if(mFragment != null)
			((BasicFragment)mFragment).onPrepareUpdate(qn, jsonParam);
		else if(qn.equals(SQLNO.Q38) || qn.equals(SQLNO.Q71)){
			if(mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_batch)){
				mFactory.jsonCommit(qn, jsonParam);
			}else if(mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_paper)){
				mFactory.jsonCommit(qn, jsonParam);
			}else {	
				mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT);
				runOnUiThread(new Runnable() {
					public void run() {
						if(mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_card)){
							((Button)findViewById(R.id.imgStep03)).setText(getResources().getText(R.string.menu_issue_01_step_03));
						}
					}
				});
				MiruSmartCard card = mFactory.getSmartCard(0); // 자동발급 카드장치
	
				synchronized (ResourceFactory.UI_TEST ? mFactory : card) {
					if (ResourceFactory.UI_TEST || writeVotingCard(card)) // 카드발급 처리
						mFactory.jsonCommit(qn, jsonParam);
					else{
						mHandler.sendEmptyMessage(MSG_ERROR);
						if (ResourceFactory.UI_TEST)
							Log.d(TAG, "[issue] "
									+ getString(R.string.err_nores_writer));
						else
							showDefaultMessage(R.string.err_nores_writer);
						}
					
//					mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT);
				}
			}
		}else if(qn.equals(SQLNO.Q17)){
			mFactory.jsonCommit(qn, jsonParam);
		}else if(qn.equals(SQLNO.Q18)){
			dismissedMessageBox();
			mFactory.jsonCommit(qn, jsonParam);
			issueCancle();
		}else if(qn.equals(SQLNO.Q60)){
			mFactory.jsonCommit(qn, jsonParam);
		}else if(qn.equals(SQLNO.Q71)){
			mFactory.jsonCommit(qn, jsonParam);
		}
		
		return;
	}
	
	private void issueCancle(){
		mIssueStep = ISSUE_STEP_READY;
		final int managerCaller = mManagerCaller;
		if(mManagerCaller != R.id.btnReissue){
			mIsCompleted = true;
			mIsEnter = true;
			mHandler.sendEmptyMessage(MSG_ERROR);}
		if(mReqDialog!=null && mReqDialog.isShowing())
			mReqDialog.dismiss();
		runOnUiThread(
				new Runnable() {
			@Override
			public void run() {
				mNotChangeable = false;

				if (managerCaller == R.id.btnIssueCancel) {
					Object[] elector = mElector;
					elector[ 4] = "Y"; // VT_YN
					elector[ 5] = mStrReqDesc; // ETC
					elector[ 7] = null; // VT_DATE
					elector[ 9] = null; // SEAL_CD
					elector[10] = null; // SEAL_IMG
					mListView.performItemClick(null, -1, 0); // un-select and clear detail info.
				}
				else if(managerCaller == R.id.btnReissue){ // Re-issue
				    mSearchMethods[0].setText(mElector[0].toString());
				    mSearchMethods[1].setText(null);
				    mSearchMethods[2].setText(null);
				    findViewById(R.id.btnIssueSearch).performClick();
				}
			}
		});
	}
	private class DoubleTap extends GestureDetector.SimpleOnGestureListener{
		@Override
		public boolean onDown(MotionEvent e) {
			return true;
		}
		@Override
		public boolean onDoubleTap(MotionEvent e) {
	        Intent intent = new Intent(MainActivity.this, PickerActivity.class);
			startActivity(intent);			
			return true;
		}
	}
	private class DoubleItemTap extends GestureDetector.SimpleOnGestureListener{
		@Override
		public boolean onDown(MotionEvent e) {
			return true;
		}
		
		@Override
		public boolean onDoubleTap(MotionEvent e) {
			selectedItem();
			return true;
		}
	}
	private void selectedItem(){
		Log.d(TAG, "Issue Step = " + mIssueStep + " mIsEnter " + mIsEnter);
		if (!mIsEnter && mPosition != -1) {
			showDefaultMessage(R.string.error_sgi_search);
		} else if (mIssueStep < 1) {
			mIssueStep = ISSUE_STEP_READY;
			SingleChoiceAdapter adapter = (SingleChoiceAdapter) mListView
					.getAdapter();
			if (mNotChangeable == false && adapter != null) {
				final Object[] elector = mPosition == -1 ? null : adapter
						.getItem(mPosition);
				final boolean showActionBar = elector != null
						&& "N".equals(elector[4]);

				setmElector(elector);
				displayDetail(elector, showActionBar);
				adapter.setSelectedIndex(mPosition);
				showActionBar(showActionBar);
				mIsShowActionBar = showActionBar;
				if (!showActionBar && mIssueStep < 3 && mPosition != -1
						&& mMediaPlayer != null
						) {
					try {
						if (mMediaPlayer != null) {
							if (mMediaPlayer.isPlaying())
								mMediaPlayer.stop();
							setSoundFile(mMediaFileName[0]);
							mMediaPlayer.seekTo(0);
							mMediaPlayer.start();
						}
					} catch (IllegalStateException ise) {
						Log.e(TAG, "[issue] Can't play sound: " + ise.getMessage());
						mMediaPlayer.release();
						mMediaPlayer = null;
					}
				} else if (mMediaPlayer == null) {
					mMediaPlayer = new MediaPlayer();
					mMediaPlayer
							.setAudioStreamType(AudioManager.STREAM_MUSIC);
					setSoundFile(mMediaFileName[0]);
					mMediaPlayer.seekTo(0);
					mMediaPlayer.start();
				}
//				if (mIssueDetail != null
//						|| (mIssueDetail = (ViewGroup) findViewById(R.id.layoutElectorInfo)) != null) {
//					mIssueDetail.setVisibility(
//							elector != null ? View.VISIBLE: View.GONE);
//				}

				if (mLayoutElector != null) {
					// 기발급인 경우 서명 입력 불가
					mLayoutElector.findViewById(R.id.btnAccept).setEnabled(
							showActionBar == false);
				}
				

				// 지문 및 사인 이미지 표시영역 초기화한 후 발급 시 등록된 유권자 지문 또는 사인 이미지 표시
				mHandler.sendMessageAtFrontOfQueue(mHandler.obtainMessage(
						MSG_UPDATE_SEAL,
						elector == null || showActionBar ? 0 : 1, 0,
						elector));

				if (mSearchMethods != null)
					if (mPosition == -1) {
						for (EditText et : mSearchMethods){
							et.setText("");
							et.setFocusableInTouchMode(true);
							et.setEnabled(true);
						}
					} else {
						mSearchMethods[0].setText((String)elector[0]);
						mSearchMethods[1].setText((String)elector[2]);
						mSearchMethods[2].setText((String)elector[6]);
						mSearchMethods[0].setFocusable(false);
						mSearchMethods[1].setFocusable(false);
						mSearchMethods[2].setFocusable(false);
						mSearchMethods[0].setEnabled(false);
						mSearchMethods[1].setEnabled(false);
						mSearchMethods[2].setEnabled(false);
					}
				activeElectorLayout(elector != null);
			}
		} else
			showDefaultMessage(R.string.error_sgi_issue);
	}
	
	public void setmElector(Object[] elector){
		mElector = elector;
	}
	
	public String[] getSearchData() {
		return mSearchData;
	}
	
	public String getRecorgnizedNo() {
		return mRecorgnizedNo;
	}
	
	
	
	public boolean isEnter() {
		return mIsEnter;
	}

	public void setIsEnter(boolean isEnter) {
		this.mIsEnter = isEnter;
	}

    private class USBChecerTask extends TimerTask{

		@Override
		public void run() {
			if(usbChecker())
				mHandler.sendEmptyMessage(MSG_CLOSING_STEP_NEXT);
		}
   	 
    }
	private class PrintTask extends TimerTask{

		@Override
		public void run() {
			if(!mFactory.getmBallotPrint().isOpen()){
					mFactory.dbQuery(ResourceFactory.UI_TEST?QueryID.Q71:QueryID.Q38);
					mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT);
					closeTimer();
			}else{
//				if(mFactory.getmBallotPrint().getPrintStatus() == ResourceFactory.PRINT_READY){
//				mFactory.dbQuery(ResourceFactory.UI_TEST?QueryID.Q71:QueryID.Q38);
//				mHandler.sendEmptyMessage(MSG_ISSUE_STEP_NEXT);
//				closeTimer();
//				}
			}
		}
	}
	
	private void closeTimer() {
		if(mTimer!=null){
			mTimer.cancel();
			mTimer.purge();
			mTimer = null;
		}
	}
	private void closingMakeFile(File f, File f1, String dn){
		FileWriter fos = null;
		XmlParsing closeXml = new XmlParsing();
		try {
			closeXml.closingXml(f1.getAbsolutePath(),dn,mFactory.getProperty(ResourceFactory.CONFIG_TPGID),
					new SimpleDateFormat("yyyyMMdd").format(new Date(mFactory.getVtDate())),ResourceFactory.VOTE_finished,new SimpleDateFormat("hhmm").format(mClosingFactors.get(1)),
					new SimpleDateFormat("hhmm").format(mClosingFactors.get(2)),mClosingFactors.get(10).toString(),mClosingFactors.get(11).toString(),mClosingFactors.get(3).toString(),
					mClosingFactors.get(4).toString(),mClosingFactors.get(5).toString(),mClosingFactors.get(6).toString(),mClosingFactors.get(7).toString(),
					mClosingFactors.get(12).toString(),mClosingFactors.get(8).toString(),mClosingFactors.get(9).toString());
			FileInputStream fis = new FileInputStream(f1);
			byte[] buffer = new byte[fis.available()];
			fis.read(buffer);
			Log.i(TAG, "File Buffer = "+Base64Util.encode(buffer));
			Log.i(TAG, "vt_tpg_a.nec Size : "+buffer.length);
			byte[] sign = Base64.encode(mFactory.getSign().digitalSign(buffer, mFactory.getTcardPriKey()));
			fos = new FileWriter(f);
			fos.write(new String(sign, "utf-8"));
			fos.flush();
			try {//파일 작성 시간 기다림.
				new Thread().sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			fis.close();
			Log.i(TAG, "vt_end.nec Size : "+f.length());
		} catch (IOException e) {
			e.printStackTrace();
			return;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(fos!=null)
			try {fos.close();} catch (IOException e) {e.printStackTrace();}
		}
	}
	
	Thread closeFileMake = new Thread(){
		public void run() {
			File[] directory = new File(getUsbDirectory()).listFiles();
			for(int i=0;i<directory.length;i++){
				directory[i].delete();
			}
			File f = new File(getUsbDirectory()+"/vt_end.nec");
			File f1 = new File(getUsbDirectory()+"/vt_tpg_a.nec");
				try {
					if(!f.exists())
						f.createNewFile();
					if(!f1.exists())
						f1.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			String dn = mFactory.getSign().certDn(mFactory.getTcardCert());
			closingMakeFile(f,f1,dn);
			FileInputStream fis1, fis2;
			try {
				fis1= new FileInputStream(f);
				fis2= new FileInputStream(f1);
				Log.i(TAG, f.getAbsolutePath()+" : "+fis1.available()+" "+f1.getAbsolutePath()+" : "+fis2.available());
				if(fis1.available()<=0||fis2.available()<=0){
					closingMakeFile(f,f1,dn);
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			mHandler.sendEmptyMessage(MSG_CLOSING_STEP_NEXT);
		};
	};

	protected boolean ibScanClose(){
		mFactory.ibscannerClose();
		return mIDScanner.fingerOn(true);
	}
};