package com.mirusys.electors;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.mirusys.electors.ResourceFactory.Log;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.innoapi.InnoApi;
import android.innoapi.InnoControlApi;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

public class PickerActivity extends Activity implements OnTouchListener {

	static final int DIALOG_DATETIME = 0;

    private View mDateTimeLayout = null;
    private Dialog mDateTimeDialog = null;
    private Dialog dialog = null;

    private int mYear = 2014;
    private int mMonth = 7;
    private int mDate = 3;
    private int mHour = 7;
    private int mMin = 0;
    
    private static InnoControlApi mInnoCtrlApi;
    private static InnoApi mInnoApi;
    static final SimpleDateFormat mReportFormat = new SimpleDateFormat("HH:mm:ss, dd-MM-yyyy\r\n");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		showDateTimeDialog();
	}

	private void setDateTime() {
		Intent intent = new Intent("net.innodigital.ItmeSettingService.SETTIME");
		Calendar   now = Calendar.getInstance();

//		mMonth += 1;
		Log.i("DATETIME",  mYear+"-"+ (mMonth+1) +"-"+ mDate +", "+ mHour +":"+mMin+":"+Calendar.SECOND);
		
		intent.putExtra("year" , mYear);
		intent.putExtra("month" , mMonth+1);
		intent.putExtra("day" , mDate);
       	intent.putExtra("hour" , mHour);
       	intent.putExtra("minute" , mMin);
       	intent.putExtra("second" , Calendar.SECOND);
       	
       	sendBroadcast(intent);
//		mMonth -= 1;
//		
		now.set(mYear, mMonth, mDate, mHour, mMin);
		now.set(Calendar.MILLISECOND, 0);
		now.getTime();
		updateCurrentDateTime();
	}
	
	private void updateCurrentDateTime()
	{
		Date dt = new Date(System.currentTimeMillis());
		Calendar now = Calendar.getInstance();
		
		now.setTime(dt);
		
		mYear = now.get(Calendar.YEAR);
		mMonth = now.get(Calendar.MONTH)+1;
		mDate = now.get(Calendar.DAY_OF_MONTH);
		mHour = now.get(Calendar.HOUR_OF_DAY);
		mMin = now.get(Calendar.MINUTE);

		Log.i("DATETIME2",  mYear+"-"+ (mMonth) +"-"+ mDate +", "+ mHour +":"+mMin+":"+now.get(Calendar.SECOND));

		finish();
	}
	
	@SuppressWarnings("deprecation")
	private void showDateTimeDialog() {
		DatePicker pickerDate;
		TimePicker pickerTime;
		Calendar   cal = Calendar.getInstance();
		Log.i("DATETIME2",  cal.get(Calendar.YEAR)+"-"+ (cal.get(Calendar.MONTH)) +"-"
				+ (cal.get(Calendar.DAY_OF_MONTH)) +", "+ cal.get(Calendar.HOUR_OF_DAY)
				+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND));
//		Date dt = new Date(now.getTimeInMillis());
//		
//		now.setTime(dt);
		
		if (mDateTimeLayout == null)
		{
			LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
			mDateTimeLayout = inflater.inflate(R.layout.datetime, (ViewGroup)findViewById(R.id.datetime_layout));
			 ((Button)mDateTimeLayout.findViewById(R.id.btnEnterDate)).setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						setDateTime();
						updateCurrentDateTime();
						mDateTimeDialog.dismiss();
						finish();
					}
				});
				((Button)mDateTimeLayout.findViewById(R.id.btnCancelDate)).setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
							mDateTimeDialog.dismiss();
							finish();
					}
				});

			pickerDate = (DatePicker)mDateTimeLayout.findViewById(R.id.datePicker1);
			pickerTime = (TimePicker)mDateTimeLayout.findViewById(R.id.timePicker1);
//	    	((TextView)mPrintLayout.findViewById(R.id.textReport)).setTextSize(TypedValue.COMPLEX_UNIT_DIP, 25);
	        
			mYear = cal.get(Calendar.YEAR);
			mMonth = cal.get(Calendar.MONTH);
			mDate = cal.get(Calendar.DAY_OF_MONTH);
			mHour = cal.get(Calendar.HOUR_OF_DAY);
			mMin = cal.get(Calendar.MINUTE);
			
			pickerDate.init(cal.get(Calendar.YEAR),  cal.get(Calendar.MONTH),  cal.get(Calendar.DAY_OF_MONTH), new OnDateChangedListener(){
				@Override
				public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) 
				{
					mYear = year;
					mMonth = monthOfYear;
					mDate = dayOfMonth;
				}
			});
			
			pickerTime.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
			pickerTime.setCurrentMinute(cal.get(Calendar.MINUTE));
			pickerTime.setOnTimeChangedListener(new OnTimeChangedListener() {
				@Override
				public void onTimeChanged(TimePicker view, int hourOfDay, int minute) 
				{
					mHour = hourOfDay;
					mMin = minute;
				}	
			});
		}
        showDialog(DIALOG_DATETIME);
        
	}

	@SuppressWarnings("deprecation")
	protected void onPrepareDialog(int id, Dialog dlg){
		dlg.getWindow().setLayout(650, 300);
		super.onPrepareDialog(id, dlg);
	}

	@SuppressWarnings("deprecation")
	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dlg = null;
		
		mDateTimeDialog = new AlertDialog.Builder(this)
			.setCancelable(false)
			.setView(mDateTimeLayout)
			.create();
		dlg = mDateTimeDialog;
		return dlg;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		InputMethodManager imm = (InputMethodManager) ResourceFactory.getInstance().getSystemService(INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromInputMethod(v.getWindowToken(), 0);
		return false;
	}
}
