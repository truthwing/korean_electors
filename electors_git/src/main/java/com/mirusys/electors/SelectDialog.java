package com.mirusys.electors;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

public class SelectDialog
extends Dialog
implements View.OnClickListener, OnItemClickListener {
	private Activity mActivity;
	private ListView mListView;
	private String mTitle;
	private Object[] mItems;
	private int mSelected, mOrgSelected;

	public SelectDialog(Activity a, int titleResId, int itemArrayId, int selected) {
		this(a, a.getString(titleResId), a.getResources().getStringArray(itemArrayId), selected);
	}

	public SelectDialog(Activity a, String title, Object[] items, int selected) {
		super(a);
		mActivity = a;
		mTitle = title;
		mItems = items;
		mSelected = mItems != null && selected < mItems.length? selected : -1;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(LayoutInflater.from(mActivity).inflate(R.layout.popup_setting, null));

		findViewById(R.id.btnCancel).setOnClickListener(this);
		((TextView)findViewById(R.id.setting_title)).setText(mTitle);

		mListView = (ListView)findViewById(R.id.popup_setting_list);
		mListView.setOnItemClickListener(this);
		mListView.setAdapter(new BaseAdapter() {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				RadioButton r;

				if (convertView != null)
					r = (RadioButton)convertView.getTag();
				else {
					convertView = LayoutInflater.from(mActivity).inflate(R.layout.cell_popup_setting, null);
					r = (RadioButton)convertView.findViewById(R.id.radio0);
					convertView.setTag(r);;
				}

				r.setText(mItems[position].toString());
				r.setSelected(mSelected == position);
				r.setChecked(mSelected==position);
				return convertView;
			}

			@Override
			public long getItemId(int position) {
				return position;
			}

			@Override
			public Object getItem(int position) {
				return mItems[position];
			}

			@Override
			public int getCount() {
				return mItems == null? 0 : mItems.length;
			}
		});

		setOnShowListener(new OnShowListener() {
			@Override
			public void onShow(DialogInterface dialog) {
				mOrgSelected = mSelected;
				getWindow().setLayout(LayoutParams.WRAP_CONTENT, 72+48+24+16 + (64*mItems.length + mItems.length-1));
			}
		});
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnCancel) {
			mSelected = mOrgSelected;
			dismiss();
		}
	}

	@Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		if (mSelected != position) {
			if (mSelected > -1) ((RadioButton)parent.getChildAt(mSelected).getTag()).setChecked(false);
			((RadioButton)v.getTag()).setChecked(true);
			mSelected = position;
		}
		dismiss();
    }

	public int getSelectedIndex() {
		return mSelected;
	}

	public Object getSelectedItem() {
		return mItems == null || mSelected >= mItems.length? null : mItems[mSelected];
	}
}