package com.mirusys.electors;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.haesinit.common.Base64Util;
import com.haesinit.ultralite.database.SQLNO;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.electors.ResourceFactory.OnSQListener;
import com.mirusys.electors.ResourceFactory.QueryID;
import com.mirusys.electors.ResourceFactory.SQLState;
import com.mirusys.voting.MiruSmartCard;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class BeforeActivity extends BasicActivity implements OnSQListener, OnClickListener, OnDismissListener{
	private String TAG = "BeforeActivity";
	private long mOpeningDate;
	private int mIssuedCount;
	
	private boolean mIsCardCheck = false;
	private int mBkCount,mVtCount; 
	private Dialog mConfirmDialog;
	private SelectDialogFile mFileSelector = null;
	private byte[] mStampImage;
	private final int ACTIVITY_FLAG =0;
	private final static int CHANGE_STATUS =1;
	
	@Override
	protected void onStart() {
		super.onStart();
		mFactory.setOnSQListener(this);
		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.before);
		((Button)findViewById(R.id.btnBeforeTPG)).setText(mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME));
		findViewById(R.id.btnBeforeShutDown).setEnabled(true);
	}

	public void onMenu(View v) {
		v.setClickable(false);
		switch (v.getId()) {
		case R.id.btnBeforeShutDown:
			finish();
			break;
		case R.id.btnBeforeMain:
			startActivityForResult(new Intent(BeforeActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP),ACTIVITY_FLAG);
			break;
		case R.id.btnBeforeStart:
			if(!mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER).CardGetType().equals("T"))
				showDefaultMessage(getString(R.string.error_password_miscard, mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME)));
			else
				mIsCardCheck = true;
			break;
		case R.id.btnBeforeStamp:
			mPopupHandler.sendEmptyMessage(0);
			break;
		default:
			if (v.getTag() != null) {
				Bundle bundle = new Bundle();
				bundle.putString("tag", v.getTag().toString());
				bundle.putString("name", ((Button)v).getText().toString());
				Intent intent = new Intent(BeforeActivity.this, MainActivity.class)
				.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtras(bundle);
				startActivityForResult(intent,ACTIVITY_FLAG);
			}
			break;
		}
		v.setClickable(true);
	}
	@Override
	protected void onResume() {
		super.onResume();

		open();	
		//체험용 인지 아닌지..
		if(!ResourceFactory.UI_TEST
				&&!mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_card)) 
			mFactory.dbQuery(QueryID.Q69);
	}
	
	private void open(){
		Log.i(TAG, "open start...");
		try{
			findViewById(R.id.btnBeforeStart).setEnabled(ResourceFactory.UI_TEST?false:mFactory.getVtStatus().equals(ResourceFactory.VOTE_ready));
			findViewById(R.id.btnBeforeMain).setEnabled(ResourceFactory.UI_TEST?true:mFactory.getVtStatus().equals(ResourceFactory.VOTE_working));
			findViewById(R.id.btnBeforeClose).setEnabled(ResourceFactory.UI_TEST?false:mFactory.getVtStatus().equals(ResourceFactory.VOTE_working));
			findViewById(R.id.btnBeforeStatistics).setEnabled(mFactory.getVtStatus().equals(ResourceFactory.VOTE_working));
			findViewById(R.id.btnBeforeIssued).setEnabled(mFactory.getVtStatus().equals(ResourceFactory.VOTE_working));
			findViewById(R.id.btnBeforeVote).setEnabled(mFactory.getVtStatus().equals(ResourceFactory.VOTE_working));
			findViewById(R.id.btnBeforeStatus).setEnabled(mFactory.getVtStatus().equals(ResourceFactory.VOTE_working));
			findViewById(R.id.btnBeforeShutDown).setEnabled(true);
			if(mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD).equals(ResourceFactory.ISSUE_paper))
				findViewById(R.id.btnBeforeStamp).setEnabled(true);
		}
		catch (Exception e){
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
		}
		Log.i(TAG, "open finish...");
	}

	@Override
	public void onSqlStatus(SQLState state) {
		if(!state.equals(SQLState.SQL_CONNECTED))
			dismissedMessageBox();
		if(state.equals(SQLState.SQL_DISCONNECTED))
			showDefaultMessage(R.string.error_database_failed_connection);
		else if(state.equals(SQLState.SQL_UNKNOWN))
			showDefaultMessage(R.string.error_database_failed_auth);
	}

	@Override
	public Object[] getParameters(QueryID id) {
		// TODO Auto-generated method stub
		String strTpgId = mFactory.getProperty(ResourceFactory.CONFIG_TPGID);

		switch (id) {
			case Q32:
				return new Object[] {
					strTpgId,
					mFactory.isJson()?new SimpleDateFormat("yy-MM-dd").format(new Date(mFactory.getVtDate())):new java.sql.Date(mFactory.getVtDate())
				};
			case Q53:
				return new Object[] {
					strTpgId	
				};
			case Q54:
				if(mFactory.isJson())
				return new Object[] { new SimpleDateFormat("yyyyMMddhhmmss").format(mOpeningDate),
						Integer.toString(mBkCount), Integer.toString(mVtCount), strTpgId,
						new SimpleDateFormat("yy-MM-dd").format(new Date(mFactory.getVtDate()))
				};
				else
				return new Object[] { mBkCount, mVtCount, strTpgId, new java.sql.Date(mFactory.getVtDate())
				};
			case Q68:
				if(mFactory.isJson()){
					return new Object[]{
						Base64Util.encode(mStampImage),	
						mFactory.getProperty(ResourceFactory.CONFIG_TPGID)
					};
				}
				return new Object[]{
						mStampImage,
						mFactory.getProperty(ResourceFactory.CONFIG_TPGID)
				};
			case Q69:
				return new Object[]{
						mFactory.getProperty(ResourceFactory.CONFIG_TPGID)
				};
		}
		return null;
	}
	
	@Override
	public boolean onResultSet(ResultSet rs, QueryID id) throws SQLException {
		// TODO Auto-generated method stub
		boolean moreQuery = true;
		if(rs.next())
		switch (id) {
		case Q32:
			String status = rs.getString("STATUS");
			// 이미 개시/마감 상태인 경우 (DB기준) 오류 팝업
			if (!ResourceFactory.VOTE_ready.equals(status)){ 
				showDefaultMessage(R.string.error_password_open);
				moreQuery = false;
			}
			break;
		case Q53:
			try {
				String date = new SimpleDateFormat("yyyy.MM.dd ").format(rs.getDate("NOW"))+new SimpleDateFormat("HH:mm:ss").format(rs.getTime("NOW"));
				mOpeningDate = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").parse(date).getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			mIssuedCount = rs.getInt("VT_CCNT");
			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					if (mConfirmDialog != null)
						mConfirmDialog.findViewById(R.id.btnOkay).setClickable(true);
					else {
					    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd a hh:mm:ss", getResources().getConfiguration().locale);

					    mConfirmDialog = new Dialog(BeforeActivity.this);
						mConfirmDialog.setContentView(R.layout.popup_opening_confirm);
						mConfirmDialog.findViewById(R.id.btnOkay).setOnClickListener(BeforeActivity.this);
						mConfirmDialog.findViewById(R.id.btnCancel).setOnClickListener(BeforeActivity.this);

						((TextView)mConfirmDialog.findViewById(R.id.textTitleVote)).setText(mFactory.getProperty(ResourceFactory.CONFIG_VTTITLE));
						((TextView)mConfirmDialog.findViewById(R.id.textTitlePolls)).setText(mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME));
						((TextView)mConfirmDialog.findViewById(R.id.infoVoteDate)).setText(dateFormat.format(new Date(mOpeningDate)));
					}
					((TextView)mConfirmDialog.findViewById(R.id.infoVoteElectors)).setText(getString(R.string.format_issued_count, mIssuedCount));
					mConfirmDialog.show();
				}
			});
			break;
		case Q69:
			mFactory.setAdminStamp(rs.getBytes("IMG_MGR_STAMP"));
			break;
		default:
			break;
		}
		return moreQuery;
	}

	@Override
	public boolean onResultSet(int rows, QueryID id) throws SQLException {
		// TODO Auto-generated method stub
		switch (id) {
		case Q54:
			MiruSmartCard card = mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER);
			synchronized (ResourceFactory.UI_TEST ? mFactory : card) {
				// 관리카드의 개시 정보를 업데이트하고
				byte[] bytesOpeningState = new byte[13];
				String strOpeningValue = String.format("%010d",(long) (Math.random() * 9999999999L));
				String strOpeningTime = new SimpleDateFormat("HHmmss").format(new java.util.Date(mOpeningDate));
				bytesOpeningState[0] = (byte) ResourceFactory.VOTE_working.charAt(0);
				System.arraycopy(strOpeningTime.getBytes(), 0, bytesOpeningState, 1, 6);
				
				if (ResourceFactory.UI_TEST || 
						(card.TCardWriteVoteRightRandom(strOpeningValue.getBytes()) == MiruSmartCard.SUCCESS 
						&& card.TCardWriteTPGData(bytesOpeningState) == MiruSmartCard.SUCCESS)) {
					mFactory.setVtStatus(ResourceFactory.VOTE_working);
					mFactory.setVtOpeningValue(strOpeningValue);
					mFactory.transaction(ResourceFactory.TRANSACTION_COMMIT);
				}else{
					showDefaultMessage(getString(R.string.error_write_card) + '\n' + getString(R.string.error_vote_opening));
					mFactory.transaction(ResourceFactory.TRANSACTION_ROLLBACK);
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						if(mConfirmDialog!=null&&mConfirmDialog.isShowing())
							{mConfirmDialog.dismiss();mConfirmDialog=null;}
						mHandler.sendEmptyMessage(CHANGE_STATUS);
					}
				});
			}

			break;
		case Q68:
//			mFactory.setAdminStamp(BitmapFactory.decodeByteArray(mStampImage, 0, mStampImage.length));
			dismissedMessageBox();
			showDefaultMessage(R.string.mcard_init_complete);
			mFactory.transaction(ResourceFactory.TRANSACTION_COMMIT);
			break;
		default:
			break;
		}
		return false;
	}

	@Override
	public void onEvent(int qn, String arg0) {
		try {
			JSONArray arr = new JSONArray(arg0);
			JSONObject rs;
		// TODO Auto-generated method stub
		switch (qn) {
		case 32:
				for (int i = 0; i < arr.length(); i++) {
					rs = arr.getJSONObject(i);
					String status = rs.getString("STATUS");
					if (ResourceFactory.VOTE_ready.equals(status) == false) {
						showDefaultMessage(R.string.error_password_open);
					} else
						mFactory.dbQuery(QueryID.Q53);
				}
			break;
		case 53:
				for (int i = 0; i < arr.length(); i++) {
					rs = arr.getJSONObject(i);
					String openDate = rs.getString("NOW");
					try {
						mOpeningDate = new SimpleDateFormat(
								"yyyy-MM-dd hh:mm:ss").parse(openDate)
								.getTime();
					} catch (ParseException e) {
						Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
					}
					mIssuedCount = rs.getInt("VT_CCNT");

					runOnUiThread(new Runnable() {
						@Override
						public void run() {

							if (mConfirmDialog != null)
								mConfirmDialog.findViewById(R.id.btnOkay)
										.setClickable(true);
							else {
								SimpleDateFormat dateFormat = 
										new SimpleDateFormat("yyyy.MM.dd  a hh:mm", getResources().getConfiguration().locale);

								mConfirmDialog = new Dialog(BeforeActivity.this);
								mConfirmDialog.setContentView(R.layout.popup_opening_confirm);
								mConfirmDialog.findViewById(R.id.btnOkay).setOnClickListener(BeforeActivity.this);
								mConfirmDialog.findViewById(R.id.btnCancel).setOnClickListener(BeforeActivity.this);

								((TextView) mConfirmDialog.findViewById(R.id.textTitleVote)).setText(mFactory.getProperty(ResourceFactory.CONFIG_VTTITLE));
								((TextView) mConfirmDialog.findViewById(R.id.textTitlePolls)).setText(mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME));
								((TextView) mConfirmDialog.findViewById(R.id.infoVoteDate)).setText(dateFormat.format(new java.util.Date(mOpeningDate)));
							}

							((TextView) mConfirmDialog.findViewById(R.id.infoVoteElectors)).setText(getString(R.string.format_issued_count,mIssuedCount));
							mConfirmDialog.show();
						}
					});
				}
				break;
		case 69:
			for (int i = 0; i < arr.length(); i++) {
				rs = arr.getJSONObject(i);
				try {
					if(rs.has("IMG_MGR_STAMP")&&!rs.getString("IMG_MGR_STAMP").isEmpty())
						mFactory.setAdminStamp(Base64Util.decode(rs.getString("IMG_MGR_STAMP")));
				} catch (Exception e) {
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				}
			}
			break;
			default:
				break;
			}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	@Override
	public void onEvent(int qn, int int1) {
		// TODO Auto-generated method stub
		switch (qn) {
		case 54:
			if(mConfirmDialog!=null && mConfirmDialog.isShowing()){
				if(int1!=0){
					{mConfirmDialog.dismiss();mConfirmDialog=null;}
					mHandler.sendEmptyMessage(CHANGE_STATUS);
					}
				else
					showDefaultMessage(R.string.error_database_failed_auth);
				}
			break;
		case 68:
			dismissedMessageBox();
			showDefaultMessage(R.string.mcard_init_complete);
			break;
		default:
			break;
		}
	}

	@Override
	public void onPrepareUpdate(SQLNO qn, String[] jsonParam) {
		switch (qn) {
		case Q54:
			MiruSmartCard card = mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER);
			synchronized (ResourceFactory.UI_TEST ? mFactory : card) {
				// 관리카드의 개시 정보를 업데이트하고
				byte[] bytesOpeningState = new byte[13];
				String strOpeningValue = String.format("%010d",(long) (Math.random() * 9999999999L));
				String strOpeningTime = new SimpleDateFormat("HHmmss").format(new java.util.Date(mOpeningDate));
				bytesOpeningState[0] = (byte) ResourceFactory.VOTE_working.charAt(0);
				System.arraycopy(strOpeningTime.getBytes(), 0, bytesOpeningState, 1, 6);
				
				if (ResourceFactory.UI_TEST || 
						(card.TCardWriteVoteRightRandom(strOpeningValue.getBytes()) == MiruSmartCard.SUCCESS 
						&& card.TCardWriteTPGData(bytesOpeningState) == MiruSmartCard.SUCCESS)) {
					mFactory.setVtStatus(ResourceFactory.VOTE_working);
					mFactory.setVtOpeningValue(strOpeningValue);
					mFactory.jsonCommit(qn, jsonParam);
				}else{
					showDefaultMessage(getString(R.string.error_write_card) + '\n' + getString(R.string.error_vote_opening));	
				}
			}
			break;
		case Q68:
//			mFactory.setAdminStamp(BitmapFactory.decodeByteArray(mStampImage, 0, mStampImage.length));
			mFactory.jsonCommit(qn, jsonParam);
			break;
		default:
			break;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		v.setClickable(false);
		switch (v.getId()) {
		case R.id.btnOkay:
			if (mConfirmDialog != null && mConfirmDialog.isShowing()) {
				mConfirmDialog.findViewById(R.id.btnCancel).setClickable(false);
				if (((EditText) mConfirmDialog.findViewById(R.id.editElectorDeivceCount)).getText().length() == 0)
					showDefaultMessage(R.string.error_empty_bkcount);
				else if (((EditText) mConfirmDialog.findViewById(R.id.editVotingDeivceCount)).getText().length() == 0)
					showDefaultMessage(R.string.error_empty_vtcount);
				else if (mIssuedCount != 0)
					showQuestionMessage(R.string.ask_forcely_opening, v);
				else {
					mBkCount = Integer.parseInt(((EditText) mConfirmDialog.findViewById(R.id.editElectorDeivceCount)).getText().toString());
					mVtCount = Integer.parseInt(((EditText) mConfirmDialog.findViewById(R.id.editVotingDeivceCount)).getText().toString());
					if(mFactory.dbQuery(QueryID.Q54))
						return;
				}
				mConfirmDialog.findViewById(R.id.btnCancel).setClickable(true);
			}
			break;
		case R.id.btnCancel:
			if (mConfirmDialog != null && mConfirmDialog.isShowing()) 
				{mConfirmDialog.dismiss();mConfirmDialog=null;}
			break;

		default:
			break;
		}
		v.setClickable(true);
	}
	
	@Override
	public void onAttached(MiruSmartCard card) {
		// TODO Auto-generated method stub
		super.onAttached(card);
		if(mIsCardCheck){
			mIsCardCheck = false;
			dismissedMessageBox();
			byte[] bytesBSection = new byte[51];
			if (ResourceFactory.UI_TEST == false && (
					card.CardVerify(mFactory.getManagerKey()) != MiruSmartCard.SUCCESS ||
					card.TCardReadBSection(bytesBSection) != MiruSmartCard.SUCCESS ||
					mFactory.getProperty(ResourceFactory.CONFIG_TPGID).equals(ResourceFactory.bytesToString(bytesBSection, 0, 10)) == false))
				{
					mIsCardCheck = true;
				}
				else if(ResourceFactory.UI_TEST == false && (char)bytesBSection[18] != ResourceFactory.VOTE_ready.charAt(0) && (char)bytesBSection[18] != 0) {
					// 이미 개시/마감 상태인 경우 (Card기준) 오류 팝업
					showDefaultMessage((char)bytesBSection[18] == ResourceFactory.VOTE_working.charAt(0)? R.string.error_password_open : R.string.already_vote_finished);
				}
				else {
					if(mFactory.isJson())
						mFactory.dbQuery(QueryID.Q32);
					else
						mFactory.dbQuery(QueryID.Q32, QueryID.Q53);
				}
		}
	}
	
	@Override
	public void onDetached(MiruSmartCard card) {
		// TODO Auto-generated method stub
		super.onDetached(card);
	}
	
	@Override
	public void onIdle(MiruSmartCard card) {
		// TODO Auto-generated method stub
		if(mIsCardCheck && mFactory.isCardAttached())
			onAttached(card);
		else
			super.onIdle(card);
	}
	Handler mHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case CHANGE_STATUS:
				open();
				break;

			default:
				break;
			}
			
		};
	};
	private Handler mPopupHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch(msg.what) {
			case 0:
				if (mFileSelector == null) {
					mFileSelector = new SelectDialogFile(BeforeActivity.this,
							R.string.setup_basic_stamp);
					mFileSelector.setOnDismissListener(BeforeActivity.this);
				}

				mFileSelector.show();
				break;
			case 1:
				//				byte[] image = SeedCrypto.decrypt(mStampImage, mFactory.getTcardKey());
//				byte[] image = mStampImage;
//				((ImageView)findViewById(R.id.img_stamp)).setImageBitmap(BitmapFactory.decodeByteArray(image, 0, image.length));
				break;
			}
		}
	};

	@Override
	public void onDismiss(DialogInterface dialog) {
		if (dialog.equals(mFileSelector)) {
			String path = mFileSelector.getSelectedItem();
			if (path != null) {
				// 선택한 파일을 DB에 입력
				Log.d(TAG, "path="+path);
				mStampImage = null;
				try {
					FileInputStream fis = new FileInputStream(new File(path));
					ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
					byte[] buffer = new byte[1024];
					int read = -1;
					while( (read = fis.read(buffer) ) != -1 ) {
						baos.write(buffer, 0, read);
					}
					baos.close();
					fis.close();

					mStampImage = baos.toByteArray();
					mFactory.dbQuery(QueryID.Q68);
					showWaitMessage(R.string.download_path_wait_save);
				} catch (FileNotFoundException e) {
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				} catch (IOException e) {
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				}
			}
			else {
				mStampImage = null;
			}
		}
	}
}
