package com.mirusys.electors;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.innoapi.InnoControlApi;
import android.util.SparseArray;

import com.haesin.manager.IBScanCommander;
import com.haesinit.common.Base64Util;
import com.haesinit.ultralite.database.SQLNO;
import com.haesinit.ultralite.socket.OnEventListener;
import com.haesinit.ultralite.socket.SocketClient;
import com.haesinit.ultralite.socket.SocketServer;
import com.integratedbiometrics.ibscancommon.IBCommon.ImageDataExt;
import com.integratedbiometrics.ibscanmatcher.IBMatcher;
import com.integratedbiometrics.ibscanmatcher.IBMatcher.Template;
import com.integratedbiometrics.ibscanmatcher.IBMatcherException;
import com.mirusys.epson.BallotPrint;
import com.mirusys.sign.Common;
import com.mirusys.sign.ElectronSign;
import com.mirusys.voting.MiruSmartCard;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

public final class ResourceFactory extends Application implements OnEventListener {
	public static boolean UI_TEST = false; // UI테스트 시에만 사용할 것 (단말기 설치용으로 빌드 시 false로 할 것)
	public static boolean IB_LOG = true; // 지문인식 로그 남기는 작업

	private static String TAG = "ResourceFactory";

	private static ResourceFactory mSingleton;
	private final String HashValue = "YQxYx768alR+NTjlfOA6zrT8Og4=";
	private final String NetworkPath = "/var/ethernet";
	private String mLocalIP;
	private String mManagerKey; // manager password
	private String mVtOpeningValue; // random value
	private String mVtStatus;
	private long mVtDate;
	private byte[] mTcardCert;
	private byte[] mVcardCert;
	private byte[] mVcardDn;
	private byte[] mTcardPriKey;
	private byte[] mTcardCenterCert;
	private byte[] mTcardKey;
	private ElectronSign sign = new ElectronSign();
	private IBScanCommander mIBScanner = null;
	private IBMatcher mIBMatcher = null;
	private OnAttachStateListener mCardListener = null;
	private MiruSmartCard mSmartCard = null;
	private boolean mCardAttached = false;
	private int mSmartDeviceIndex;
	private Timer mSCardTimer = null;
	private static FileWriter fw;
	private static boolean mJdbcInited = false;
	private static boolean mJdbcUsable = false;
	private OnSQListener mSqlListener = null;
	private Connection mConnection = null;
	private SqlTask mSqlTask = null;
	private Timer mSqlTimer = null;
	private String mDatabaseVer = null;
	
	private SparseArray<String> mQueries = new SparseArray<String>();
	private String mSqlUserName = "dba";
	private String mSqlUserPassword = "sql";
	private String mSqlSourceAddress;
	private String mSqlSourcePort = "2368";//SQLAnywhere 10 버젼 사용시 포트
//	private String mSqlSourcePort = "2638";//SQLAnywhere 16 버젼 사용시 포트
	
	private int mUltraPort = 8080;
	private SocketServer mServer = null;
	private SocketClient mClient = null;
	private boolean mIsJson = false;
	private boolean mIsServer = false;
	private int mSoTimeout = 30000;
	private int mConnectionTimeout = 15000;
	
	private InnoControlApi mInnoCtrAPI;
	
	private Properties mProperty = new Properties();

	public static String UDB_PATH;
	
	private byte[] mAdminStamp;
	
	private BallotPrint mBallotPrint = null;
	private Timer mTimer = null;
	
	private final static String CONFIG_FILE = "mbk.dat";
	public final static String CONFIG_VTTITLE = "VtTitle";
	public final static String CONFIG_TPGID = "TpgId";
	public final static String CONFIG_TPGNAME = "TpgName";
	public final static String CONFIG_ISSUE_METHOD = "IssueMethod";
	public final static String CONFIG_SERVER_ADDRESS = "ServerAddress";
	public final static String CONFIG_SERVER_MODE = "ServerMode";
	public final static String CONFIG_RECOGNIZE = "Recognize";
	
	public final static String ISSUE_card = "card";
	public final static String ISSUE_paper = "paper";
	public final static String ISSUE_batch = "batch";

	public final static String MODE_server = "server";
	public final static String MODE_both = "both";
	public final static String MODE_client = "client";
	
	public final static String RECOGNIZE_full = "full";
	public final static String RECOGNIZE_left = "left";
	public final static String RECOGNIZE_right = "right";
	
	public final static String VOTE_ready = "A";
	public final static String VOTE_working = "B";
	public final static String VOTE_finished = "C";

	private final static int DB_CONNECTION_CHECK_TIME = 60 * 1000; // 1분마다 연결 유지
																	// 여부 확인

	public final static int TRANSACTION_COMMIT = 1;
	public final static int TRANSACTION_ROLLBACK = 2;

	public final static int SCARD_FOR_MANAGER = 0; // 관리자카드 처리용으로 사용될 디바이스 노드
	public static String PATH_TRAINING;

	public static final int BALLOT_TYPE0 = 0;
	public static final int BALLOT_TYPE1 = 1;
	public static final int BALLOT_TYPE4 = 2;
	public static final int BALLOT_TYPE12 = 3;
	public static final int BALLOT_TYPE_LABEL = 4;

	public static final int BOX_COLOR1 = Color.rgb(255, 255, 255);	//백색
	public static final int BOX_COLOR2 = Color.rgb(230, 250, 230);	//연두색
	public static final int BOX_COLOR3 = Color.rgb(230, 230, 230);	//청회색
	public static final int BOX_COLOR4 = Color.rgb(230, 250, 250);	//하늘색
	public static final int BOX_COLOR5 = Color.rgb(250, 230, 200);	//계란색
	public static final int BOX_COLOR6 = Color.rgb(250, 250, 230);	//연미색

	public static final int FILL_TOP = 111;
	public static final int FILL_FULL = 211;

	public static final int BATANG = 0;
	public static final int MALGUN = 1;
	
	public static final int PRINT_BUSY = 0;
	public static final int PRINT_READY = 1;
	public ResourceFactory() {
		mSingleton = this;
	}
	
	private static File LogFile;
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	public void timeSetting(String s){
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(new SimpleDateFormat("yyyy-MM-ddHH:mm:ss").parse(s));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Intent intent = new Intent("net.innodigital.ItmeSettingService.SETTIME");
		intent.putExtra("year" , cal.get(Calendar.YEAR));
		intent.putExtra("month" , cal.get(Calendar.MONTH)+1);
		intent.putExtra("day" , cal.get(Calendar.DAY_OF_MONTH));
       	intent.putExtra("hour" ,  cal.get(Calendar.HOUR_OF_DAY));
       	intent.putExtra("minute" , cal.get(Calendar.MINUTE));
       	intent.putExtra("second" , cal.get(Calendar.SECOND));
       	sendBroadcast(intent);
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		SimpleDateFormat df = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss_");
		String date = df.format(new Date());
		LogFile = new File(getFilesDir().getAbsolutePath()+"/"+date+"Log.log");
		if(!LogFile.exists()){
			try {
				LogFile.createNewFile();
			} catch (IOException e) {
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}
		}

		try {
			fw = new FileWriter(LogFile);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		mInnoCtrAPI = new InnoControlApi(getApplicationContext());
		generateTrainData();
		generateQueryData();
		try {
			mProperty.load(openFileInput(CONFIG_FILE));
		} catch (IOException e) {
		}
		UDB_PATH= mSingleton.getFilesDir().getAbsolutePath();
	}
	
	
	public void shutDown() {
		mInnoCtrAPI.shutdown();
	}

	public void forcedShutDown() {
		mInnoCtrAPI.forcedShutdown();
	}
	@Override
	public void onTerminate() {
		release();
		super.onTerminate();
	}
	public void ibscannerClose(){
		if (mIBScanner != null){
			mIBScanner.setOnStateListener(null);
			mIBScanner.close();}
		mIBScanner=null;
	}
	public void release() {
		setOnAttachStateListener(null);
		releaseSmartCard();
		if (mIBScanner != null) {
			mIBScanner.setOnStateListener(null);
			mIBScanner.close();
		}

		if (mConnection != null) {
			disconnect();
		}
		try {
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getHashValue() {
		return HashValue;
	}

	public void disconnect() {
		if (mSqlTimer != null) {
			mSqlTimer.cancel();
			mSqlTimer.purge();

			synchronized (mSqlTask) {
				try {
					mConnection.close();
					Log.i(TAG, "Disconnection successful !");
				} catch (Exception e) {
				}

				mConnection = null;
				mSqlTimer = null;
			}
		}
	}
	public String getProperty(String name) {
		return getProperty(name, null);
	}

	public String getProperty(String name, String defaultValue) {
		return mProperty.getProperty(name, defaultValue);
	}

	public Object setProperty(String name, String value) {
		return mProperty.setProperty(name, value);
	}

	public boolean store() {
		try {
			mProperty.store(openFileOutput(CONFIG_FILE, MODE_PRIVATE), null);
			return true;
		} catch (IOException e) {
		}

		return false;
	}

	public static String bytesToString(byte[] src, int start, int length) {
		while (--length >= 0 && src[start + length] == 0)
			; // trim right
		return length < 0 ? "" : new String(src, start, length + 1);
	}

	public static String bytesToString(byte[] src) {
		return bytesToString(src, 0, src.length);
	}

	private void generateTrainData() {
		byte[] buffer = null;
		String files[] = { "asc.wgt", "hang.rec" };
		File cache = new File(PATH_TRAINING = getCacheDir().getAbsolutePath()
				+ "/training/");
		if (!cache.exists()) {
			cache.mkdirs();
		}

		for (String filename : files) {
			File file = new File(PATH_TRAINING + filename);
			if (file.exists() == false) {
				InputStream in = null;
				OutputStream out = null;

				try {
					in = getAssets().open(filename);
					out = new FileOutputStream(file);

					if (buffer != null || (buffer = new byte[4096]) != null) {
						int len;
						while ((len = in.read(buffer)) > 0)
							out.write(buffer, 0, len);
					}

					Log.i("OCR", "Copied " + filename + " in cachedir");
				} catch (IOException e) {
					Log.e("OCR",
							"Unable copy " + filename + ": " + e.getMessage());
				} finally {
					try {
						in.close();
					} catch (Exception e) {
					}
					try {
						out.close();
					} catch (Exception e) {
					}
				}
			}
		}
	}

	public boolean isJson() {
		return mIsJson;
	}
	
	public boolean setIsJson(boolean isJson) {
		return mIsJson = isJson;
	}
	
	public String getManagerKey() {
		return mManagerKey;
	}

	public void setManagerKey(String key) {
		mManagerKey = key;
	}

	public String getVtStatus() {
		return mVtStatus;
	}

	public void setVtStatus(String status) {
		mVtStatus = status;
	}

	public String getVtOpeningValue() {
		return mVtOpeningValue;
	}

	public void setVtOpeningValue(String value) {
		mVtOpeningValue = value;
	}

	public long getVtDate() {
		return mVtDate;
	}

	public void setVtDate(long vtDate) {
		mVtDate = vtDate;
	}

	public boolean isUsableSmartcard() {
		return UI_TEST || mSmartCard != null;
	}

	public boolean isCardAttached() {
		return mCardAttached;
	}

	public String getmSqlSourceAddress() {
		if (mSqlSourceAddress == null)
			mSqlSourceAddress = getProperty(ResourceFactory.CONFIG_SERVER_ADDRESS, "0.0.0.0");
		return mSqlSourceAddress;
	}

	public void setmSqlSourceAddress(String mSqlSourceAddress) {
		this.mSqlSourceAddress = mSqlSourceAddress;
		setProperty(ResourceFactory.CONFIG_SERVER_ADDRESS, mSqlSourceAddress);
		store();
	}

	public int getSmartDeviceIndex() {
		return mSmartDeviceIndex;
	}

	public MiruSmartCard getSmartCard(int deviceIndex) {
		if (mSmartCard != null && deviceIndex != mSmartDeviceIndex) {
			releaseSmartCard();
		}
		
		if (mSmartCard != null && (mSmartCard.CheckCardInOut()==MiruSmartCard.SUCCESS))
			return mSmartCard;
		else
			try {
				if ((mSmartCard = new MiruSmartCard()) == null || 
						mSmartCard.CardOpenSession(deviceIndex) != MiruSmartCard.SUCCESS)
					Log.e(TAG, "Can't open to smartcard");
				else {
					if(mSCardTimer==null)
						mSCardTimer = new Timer();
					else{
						mSCardTimer.cancel();
						mSCardTimer.purge();
						mSCardTimer = new Timer();
						}
					mSCardTimer.schedule(new SCardTask(), 1000, 1000);
					mSmartDeviceIndex = deviceIndex;
					Log.i(TAG, "Successfully open to smartcard #" + deviceIndex);
					return mSmartCard;
				}
			} catch (UnsatisfiedLinkError e) {
				Log.e(TAG, "UnsatisfiedLinkError: " + e.getMessage());
			} catch (ExceptionInInitializerError e) {
			} catch (NoClassDefFoundError e) {
			} catch (Exception e) {
				Log.e(TAG, "Exception: " + e.getMessage());
			}

		releaseSmartCard();
		return null;
	}

	public void releaseSmartCard() {
		if (mSCardTimer != null) {
			synchronized (mSCardTimer) {
				mSCardTimer.cancel();
				mSCardTimer.purge();
				mSCardTimer = null;
			}
		}

		mCardAttached = false;

		if (mSmartCard != null) {
			synchronized (mSmartCard) {
				mSmartCard.CardCloseSession(mSmartDeviceIndex);
				mSmartCard = null;

				Log.i(TAG, "Closed Smartcard");
			}
		}
	}
	
	public boolean forceOpenIBScanner (IBScanCommander.OnStateListener l){
		if (mIBScanner != null)
			mIBScanner.setOnStateListener(l);
		else
			try {
				mIBScanner = new IBScanCommander();
				mIBScanner.setOnStateListener(l);
				mIBScanner.forceOpen();
			} catch (Exception e) {
				Log.e(TAG, "Exception: " + e.getMessage());
				
				if (mIBScanner != null) {
					mIBScanner.close();
					mIBScanner = null;
				}
			}

		return mIBScanner != null;
	}
	
	public boolean openIBScanner(IBScanCommander.OnStateListener l) {
		if (mIBScanner != null)
			mIBScanner.setOnStateListener(l);
		else
			try {
				mIBScanner = new IBScanCommander();
				mIBScanner.setOnStateListener(l);
//				mIBScanner.open();
			} catch (Exception e) {
				Log.e(TAG, "Exception: " + e.getMessage());
				
				if (mIBScanner != null) {
					mIBScanner.close();
					mIBScanner = null;
				}
			}

		return mIBScanner != null;
	}

	public boolean isPreparing() {
		return mIBScanner != null && mIBScanner.isPreparing();
	}

	public boolean isCapturable() {
		return mIBScanner != null && mIBScanner.isOpened();
	}

	public boolean startCapture() {
		return mIBScanner != null && mIBScanner.startCapture();
	}

	public boolean stopCapture() {
		return mIBScanner != null && mIBScanner.stopCapture();
	}

	
	public String getNetworkPath() {
		return NetworkPath;
	}
	
	public byte[] getVcardCert() {
		return mVcardCert;
	}

	public boolean setVcardCert(byte[] mVcardCert) {
		this.mVcardCert = setEncodeSHA(mVcardCert);
		return this.mVcardCert!=null;
	}

	public byte[] getVcardDn() {
		return mVcardDn;
	}

	public boolean setVcardDn(byte[] mVcardDn) {
		String strDn;
		try {
			strDn = new String (mVcardDn, "MS949");
		this.mVcardDn = Base64Util.decode(strDn);
		Log.d(TAG, "DN sign: "+ strDn);
		} catch (UnsupportedEncodingException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return false;
		} catch (Exception e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return false;
		}
		return this.mVcardDn!=null;
	}

	public ElectronSign getSign() {
		return sign;
	}

	public void setSign(ElectronSign sign) {
		this.sign = sign;
	}

	public byte[] getTcardCenterCert() {
		return mTcardCenterCert;
	}
	public boolean setTcardCenterCert(byte[] mTcardCenterCert) {
		this.mTcardCenterCert = setEncodeSHA(mTcardCenterCert);
		return this.mTcardCenterCert!=null;
	}
	
	public byte[] getTcardCert() {
		return mTcardCert;
	}
	
	public boolean setTcardCert(byte[] mTcardCert) {
		this.mTcardCert = setEncodeSHA(mTcardCert);
		return this.mTcardCert!=null;
	}
	public boolean setTcardCertDB(BasicActivity activity, byte[] mTcardCert) {
		this.mTcardCert = mTcardCert;
		activity.showWaitMessage(R.string.wait_cert_processing);
		return this.mTcardCert!=null;
	}
	
	public byte[] getTcardPriKey() {
		return mTcardPriKey;
	}
	
	public boolean setTcardPriKey(byte[] mTcardPriKey) {
		this.mTcardPriKey = setEncodeSHA(mTcardPriKey);
		return this.mTcardPriKey!=null;
	}
	public byte[] getTcardKey() {
		return mTcardKey;
	}

	public boolean setTcardKey(BasicActivity activity, byte[] mTcardKey) {
		this.mTcardKey = mTcardKey;
		activity.showWaitMessage(R.string.wait_vote_date_processing);
		return this.mTcardKey!=null;
	}
	
	private byte[] setEncodeSHA(byte[] prikey){
		byte[] tmc_len = new byte[2];
		tmc_len[0] = prikey[1300];
		tmc_len[1] = prikey[1301];
		int len = Common.byteArrayToInt(tmc_len, 2);
		byte[] realdata = Common.subByteArray(prikey, len);
		return realdata;
	}

	public String getLocalIP() {
		return mLocalIP;
	}

	public void setLocalIP(String mLocalIP) {
		this.mLocalIP = mLocalIP;
	}

	private IBMatcher getMatcher() {
		try {
			return mIBMatcher != null ? mIBMatcher : (mIBMatcher = IBMatcher
					.getInstance());
		} catch (UnsatisfiedLinkError e) {
			Log.e(TAG, "UnsatisfiedLinkError(getMatcher): " + e.getMessage());
		} catch (ExceptionInInitializerError e) {
			Log.e(TAG,
					"ExceptionInInitializerError(getMatcher): "
							+ e.getMessage());
		} catch (NoClassDefFoundError e) {
			Log.e(TAG, "NoClassDefFoundError(getMatcher): " + e.getMessage());
		}

		return null;
	}

	public synchronized void saveImageToBuffer(ImageDataExt image,
			ByteArrayOutputStream output) {
		IBMatcher matcher;
		if (image != null && (matcher = getMatcher()) != null) {
			File f = new File(getFilesDir(), "saveEx.ibsm_imagedata");
			FileInputStream fis = null;
			if (f.exists())
				f.delete();

			try {
				matcher.saveImage(image, f.getAbsolutePath());

				byte[] buffer = new byte[4096];
				fis = new FileInputStream(f);

				while (true) {
					int readBytes = fis.read(buffer);
					if (readBytes < 0)
						break; // EOF
					if (readBytes > 0)
						output.write(buffer, 0, readBytes);
				}
			} catch (Exception e) {
				Log.e(TAG, "saveImageToBuffer: " + e.getMessage());
				output.reset();
			} finally {
				if (fis != null)
					try {
						fis.close();
					} catch (Exception e) {
					}
			}
		}
	}

	public synchronized ImageDataExt loadImageFromBuffer(byte[] bytes) {
		IBMatcher matcher;
		if (bytes != null && bytes.length > 0
				&& (matcher = getMatcher()) != null) {
			File f = new File(getFilesDir(), "loadEx.ibsm_imagedata");
			FileOutputStream fos = null;
			if (f.exists())
				f.delete();

			try {
				fos = new FileOutputStream(f);
				fos.write(bytes);
				fos.flush();
				fos.close();
				fos = null;

				return matcher.loadImage(f.getAbsolutePath());
			} catch (IBMatcherException e) {
				Log.e(TAG, "loadImageFromBuffer: " + e.getMessage());
			}catch(IOException e1){
				Log.e(TAG, "IOexception : "+e1.getMessage());
			}finally {
				if (fos != null)
					try {
						fos.close();
					} catch (Exception e) {
					}
			}
		}

		return null;
	}

	public Bitmap convertImageToBitmap(ImageDataExt image) {
		IBMatcher matcher;
		Log.d(TAG, "convertImageToBitmap... from IBMatcher");
		return image != null && (matcher = getMatcher()) != null ? matcher
				.convertImageToBitmap(image) : null;
	}

	/**
	 * 두 지문을 비교하여 일치하는 점수(score, point)를 반환한다.
	 * 
	 * @param src
	 *            비교할 원본 지문
	 * @param dst
	 *            비교할 대상 지문
	 * @return 일치하는 점수
	 */
	public int match(ImageDataExt src, ImageDataExt dst) {
		IBMatcher matcher;
		Log.d(TAG, "getMatch Start...");
		if (src != null && dst != null && (matcher = getMatcher()) != null) {
			try {
				Log.d(TAG, "IBSCAN : extractTemplate src Start...");
				Template srcTem = matcher.extractTemplate(src);
//				Log.d(TAG, "IBSCAN : save src template...");
//				matcher.saveTemplate(srcTem, getFilesDir().getAbsolutePath()+"/srctemplate");
				Log.d(TAG, "IBSCAN : extractTemplate dst Start...");
				Template dstTem = matcher.extractTemplate(dst);
//				Log.d(TAG, "IBSCAN : save dst template...");
//				matcher.saveTemplate(dstTem, getFilesDir().getAbsolutePath()+"/dsttemplate");
				Log.d(TAG, "IBSCAN : matchTemplates Start...");
				return matcher.matchTemplates(srcTem,dstTem);
			} catch (IBMatcherException e) {
				Log.e(TAG, "IBMatcherException: " + e.getMessage());
			}
		}

		return 0;
	}

	public static ResourceFactory getInstance() {
		return mSingleton;
	}

	public void setOnAttachStateListener(OnAttachStateListener l) {
		mCardListener = l;
	}

	private class SCardTask extends TimerTask {
		@Override
		public void run() {
			if(mSCardTimer!=null)
			synchronized (mSCardTimer) {
				if (UI_TEST == false && mSmartCard == null){
					if(mSCardTimer!=null)
					mSCardTimer.cancel();
				}else
					try {
						synchronized (mSmartCard) {
							boolean attached = mSmartCard.CheckCardInOut() == MiruSmartCard.SUCCESS;

							if (attached == mCardAttached){
								mCardListener.onIdle(mSmartCard);
							}else {
								mCardAttached = attached;

								if (attached){
									mCardListener.onAttached(mSmartCard);
								}else{
									mCardListener.onDetached(mSmartCard);
								}
							}
						}
					} catch (Exception e) {
						Log.e(TAG, "ScanTask: " + e.getMessage());
						e.printStackTrace();
					}
			}
		}
	}

	
	@SuppressLint("SdCardPath")
	public void makeFont(Context mContext) {
			String[] path = { "NanumBrush.ttf",
					"NanumGothic.ttf", "NanumGothicBold.ttf",
					"NanumGothicExtraBold.ttf",
					"NanumMyeongjo.ttf", "NanumMyeongjoBold.ttf",
			"NanumMyeongjoExtraBold.ttf" };
			File dir = new File("/sdcard/cache/font/");
			if (!dir.isDirectory()) 
				dir.mkdirs();
			for (int i =0;i<path.length;i++) {
				File file = new File("/sdcard/cache/font/" + path[i]);
				FileOutputStream outStream = null; 
				try {
					if (!file.exists()){
						file.createNewFile();
					InputStream is = mContext.getAssets().open("font/"+path[i]);
					byte[] buffer = new byte[is.available()];
					is.read(buffer);
					outStream = new FileOutputStream(file);
					outStream.write(buffer);
					is.close();
					outStream.close();}
				} catch (FileNotFoundException e) {
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				} catch (IOException e) {
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				}
			}
	}
	
	private void generateQueryData() {
		String[] queries = getResources().getStringArray(R.array.sql_queries);
		// QueryID[] id = QueryID.values();
		int index = 0;
		for (String q : queries) {
			mQueries.append(++index, q);
			// Log.d(TAG, id[index] + " : " + q);
		}
	}

	/**
	 * 연결된 데이터베이스(Sybase)의 버전 정보를 알려준다.
	 *
	 * @return 한번도 데이터베이스에 연결되지 않았다면 null을 반환한다.
	 */
	public String getDatabaseVersion() {
		return mDatabaseVer;
	}

	public void setOnSQListener(OnSQListener l) {
		mSqlListener = l;
	}

	public byte[] getAdminStamp() {
		return mAdminStamp;
	}

	public void setAdminStamp(byte[] bs) {
		this.mAdminStamp = bs;
	}
	
	/**
	 * 지정된 데이터베이스에 연결하여 하나 또는 그 이상의 SQL문을 실행하고 그에 대한 결과를
	 * Callback(OnSQListener.onResultSet)으로 전달한다. 하나 이상의 QueryID 실행 시 그 수만큼
	 * Callback(onResultSet)이 호출된다.
	 * 
	 * @param sql
	 *            실행하고자 하는 하나 또는 그 이상의 QueryID
	 * @return 요청(백그라운드 쓰레드 시작)에 대한 성공 또는 실패 (실패 시 onSqlStatus callback으로 원인을
	 *         통지한다.)
	 */
	public boolean dbQuery(QueryID... sql) {
		if ((mJdbcInited == false || mJdbcUsable)
				&& (mSqlTask != null || (mSqlTask = new SqlTask()) != null)) {
			try {
				mSqlTask.setQueries(sql);
				new Thread(mSqlTask).start();
				return true;
			} catch (IllegalThreadStateException e) {
				Log.e(TAG,
						"IllegalThreadStateException(dbQuery): "
								+ e.getMessage());
			} catch (Exception e) {
				// TODO: handle exception
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}
		}

		mSqlListener
				.onSqlStatus(mJdbcInited && mJdbcUsable == false ? SQLState.SQL_NODRIVER
						: SQLState.SQL_UNKNOWN);
		return false;
	}
	
	public void connectionDBTest(){
		disconnect();
		try {
			new Thread().sleep(1000);
		} catch (InterruptedException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
		}
		mIsJson = false;
		mJdbcInited = false;
		mJdbcUsable = false;
		new Thread(mSqlTask).start();
	}
	
	private void closeTimer() {
		mTimer.cancel();
		mTimer.purge();
		mTimer = null;
	}
	
	public interface OnPrintListener{

		public void onPrintReady();
	}

	private class SqlTask implements Runnable {
		private QueryID[] mRequests = null;
		private SQLNO[] mRequestsJson = null;

		public void setQueries(QueryID... requests) {
 			synchronized (this){ 
			mRequestsJson = new SQLNO[requests.length];
			for (int i = 0; i < requests.length; i++) {
				mRequestsJson[i]=SQLNO.valueOf(requests[i].name());
				Log.d(TAG, "name : "+requests[i].name()+" " +mRequestsJson[i].name());
				}
				mRequests = requests;
			}
		}

		@Override
		public void run() {
			if (mJdbcInited == false && !mIsJson) {
				mJdbcInited = true;

				try {
					Class.forName("com.sybase.jdbc4.jdbc.SybDriver")
							.newInstance();
					mJdbcUsable = true;
				} catch (ClassNotFoundException e) {
					Log.e(TAG, "ClassNotFoundEx: " + e.getMessage());
				} catch (IllegalAccessException e) {
					Log.e(TAG, "IllegalAccessEx: " + e.getMessage());
				} catch (InstantiationException e) {
					Log.e(TAG, "InstantiationEx: " + e.getMessage());
				}
			}

			if (mJdbcUsable == false && !mIsJson)
				mSqlListener.onSqlStatus(SQLState.SQL_NODRIVER);
			else
				try {
					if (mRequests == null || mRequests.length == 0)
						mSqlListener.onResultSet(null, QueryID.Q4);
					else {
						if (mConnection == null&&!mIsJson) {
							Properties props = new Properties();

							props.put("CHARSET", "eucksc");
							props.put("DYNAMIC_PREPARE", "true");
							props.put("REPEAT_READ", "true");
							props.put("USER", mSqlUserName);
							props.put("PASSWORD", mSqlUserPassword);

							DriverManager.setLoginTimeout(30);
							mConnection = DriverManager.getConnection(
									"jdbc:sybase:Tds:" + getmSqlSourceAddress()
											+ ":" + mSqlSourcePort, props);
							mDatabaseVer = mConnection.getMetaData()
									.getDatabaseProductVersion();
							mConnection.setAutoCommit(false);
							

							mSqlTimer = new Timer();
							mSqlTimer.schedule(new SqlVerifyTask(),
									DB_CONNECTION_CHECK_TIME,
									DB_CONNECTION_CHECK_TIME);

							Log.i(TAG, "Connection successful !");
							mSqlListener.onSqlStatus(SQLState.SQL_CONNECTED);
//							setProperty(CONFIG_SERVER_MODE, MODE_client);
						}
						synchronized (this) {
							if(!mIsJson)
							for (QueryID id : mRequests) {
								String strQuery = mQueries.get(id.ordinal())
										.toUpperCase(Locale.KOREA);
								Log.i(TAG, strQuery);
								PreparedStatement ps = mConnection
										.prepareStatement(strQuery);
								ps.setQueryTimeout(10);
								Object[] params = mSqlListener
										.getParameters(id);
								if (params != null) {
									int index = 0;
									for (Object p : params) {
										if (p == null) {
											ps.setNull(++index, java.sql.Types.INTEGER);
											Log.i(TAG, "params = null");
										}
										else {
											ps.setObject(++index, p);
											Log.i(TAG, "params = " + p.toString());
										}
									}
								}
								try {
									if (strQuery.startsWith("SELECT")
											|| strQuery.startsWith("IF")) {
										ps.setQueryTimeout(10);
										ResultSet rs = ps.executeQuery();
										try {
											if (mSqlListener
													.onResultSet(rs, id) == false)
												break;
										} catch(Exception e){
											Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
										} finally {
											rs.close();
										}
									} else if (mSqlListener.onResultSet(
											ps.executeUpdate(), id) == false) {
										break;
									}
								} catch(Exception e){
									Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
									mSqlListener.onSqlStatus(SQLState.SQL_UNKNOWN);
								}finally {
									ps.close();
								}
							}
							else{//json을 통해서 조치
								for (SQLNO b : mRequestsJson) {
									Object[] params = mSqlListener.getParameters(QueryID.valueOf(b.name()));
									String query = mQueries.get(QueryID.valueOf(b.name()).ordinal()).toUpperCase(getResources().getConfiguration().locale);
//									파라미터 설정.
										if((mClient !=null||(mClient = new SocketClient(getmSqlSourceAddress(), mUltraPort, mConnectionTimeout, mSoTimeout, ResourceFactory.this))!=null)&&params!=null){
											String[] jsonParam = new String[params.length];
											for (int i=0; i<params.length;i++) {
												if(params[i]!=null && ((String)params[i]).length() > 0)
												jsonParam[i] = (String) params[i];
												else
													jsonParam[i] = new String();
											}
											if (query.startsWith("SELECT")
													|| query.startsWith("IF")) {
												mClient.executeQuery(b, jsonParam, 0);
											}else{//업데이트시 조치사항
												mSqlListener.onPrepareUpdate(b, jsonParam);
											}
										}else{
											mClient.executeQuery(b, new String[0], 0);
										}
								}
							}
						}
					}
				} catch (SQLException e) {
					disconnect();
					if(!mIsJson){
							mClient = new SocketClient(getmSqlSourceAddress(), mUltraPort, mConnectionTimeout, mSoTimeout, ResourceFactory.this);
							mClient.executeQuery(SQLNO.Q0, new String[0], 0);
					}else
					Log.e(TAG, "Unexpected exception: " + e.toString() + ", sqlstate = " + e.getSQLState());
					
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				} catch (Exception e) {
					mSqlListener.onSqlStatus(SQLState.SQL_DISCONNECTED);
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				}
		}
	}

	/**
	 * 데이터베이스 변경 사항에 대해 commit/또는 rollback을 수행한다. 반드시, onResultSet(int rows,
	 * QueryID id) callback 내에서만 수행되어야 한다.
	 * 
	 * @param mode
	 *            수행하고자 하는 트랜잭션 모드 (TRANSACTION_ 정의 참조)
	 * @throws SQLException
	 */
	public void transaction(int mode) throws SQLException {
			if (mode == TRANSACTION_COMMIT)
				mConnection.commit();
			else if (mode == TRANSACTION_ROLLBACK)
				mConnection.rollback();
	}

	public void jsonCommit(SQLNO b, String[] jsonParam){
		mClient.executeQuery(b, jsonParam, 1);
	}
	
	private class SqlVerifyTask extends TimerTask {
		@Override
		public void run() {
			try {
				if (mConnection == null && mSqlTimer!=null) {
					mSqlTimer.cancel();
					mSqlTimer.purge();
					mSqlTimer = null;
				} else
					synchronized (mSqlTask) {
						// Verify connection
						if (mConnection==null || mConnection.isClosed()
								|| mConnection.isValid(10) == false) {
							throw new SQLException();
						}
					}
			} catch (SQLException e) {
				disconnect();
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				Log.i(TAG, "Disconnected in verify !");
				mSqlListener.onSqlStatus(SQLState.SQL_DISCONNECTED);
			}
		}
	}

	public interface OnAttachStateListener {
		// 함수 내에서 MiruSmartCard 인스턴스는 thread safety함
		public void onAttached(MiruSmartCard card);

		public void onDetached(MiruSmartCard card);

		public void onIdle(MiruSmartCard card);
	}

	public enum SQLState {
		SQL_NODRIVER, // JDBC 드라이버가 활성화되지 않았음 (최초 한번만 발생)
		SQL_CONNECTED, // 데이터베이스와 정상 연결되었음
		SQL_DISCONNECTED, // 데이터베이스와 연결이 끊어졌음
		SQL_UNKNOWN; // 처리중 알 수 없는 오류가 발생하였음
	}
	
	public interface OnSQListener{
		public void onSqlStatus(SQLState state);

		/**
		 * 호출한 곳에서 요청한 SQL에 필요한 파라메터(?)를 반환해주어야 한다.
		 * 
		 * @param id
		 *            요청한 QueryID
		 * @return 해당 SQL을 수행하는데 필요한 파라메터 값 (null이거나 하나 이상일 수 있다.)
		 */
		public Object[] getParameters(QueryID id);

		/**
		 * 요청한 SQL에 대한 결과(ResultSet)를 통보한다.
		 *
		 * @param rs
		 *            처리된 결과 (ResultSet)
		 * @param id
		 *            수행된 QueryID
		 * @return 다수의 Query를 요청한 경우 다음 Query를 처리할지 여부를 반환해야 한다. (false - 나머지
		 *         Query 무시)
		 * @throws SQLException
		 */
		public boolean onResultSet(ResultSet rs, QueryID id)
				throws SQLException;

		/**
		 * 요청한 SQL에 대한 결과(rows)를 통보한다.
		 *
		 * @param rows
		 *            요청한 SQL에 의해 갱신된 레코드 수
		 * @param id
		 *            수행된 QueryID
		 * @return 다수의 Query를 요청한 경우 다음 Query를 처리할지 여부를 반환해야 한다. (false - 나머지
		 *         Query 무시)
		 * @throws SQLException
		 */
		public boolean onResultSet(int rows, QueryID id) throws SQLException;
		/**
		 * 요청한 SQL에 대한 결과(arg0)를 통보한다.
		 *
		 * @param qn
		 *            요청한 SQL의 쿼리 번호 
		 * @param arg0
		 *            요청한 SQL에 의해 json 데이터 수신 
		 */
		public void onEvent(int qn, String arg0);
		
		
		/**
		 * 요청한 SQL에 대한 결과(arg0)를 통보한다.
		 *
		 * @param qn
		 *            요청한 SQL의 쿼리 번호 
		 * @param int1
		 *           	 		update시 요청한 쿼리 갯수 
		 */
		public void onEvent(int qn, int int1);
		
		/**
		 * 요청한 SQL에 대한 결과(arg0)를 통보한다.
		 *
		 * @param qn
		 *            요청한 SQLNO 
		 * @param jsonParam
		 *            보내질 파리미터 값들 
		 */
		public void onPrepareUpdate(SQLNO qn, String[] jsonParam);

	}

	public boolean isServer(){
		return mIsServer;
	}
	
	public boolean onServer(final boolean isStart){

		try {
			// TODO: handle exception
				if(mServer==null)
					mServer = new SocketServer(getApplicationContext(), mUltraPort, new OnEventListener() {
						@Override
						public void onEvent(String arg0) {
							// TODO Auto-generated method stub
							Log.i(TAG, "Server : "+arg0);
						}
					});
				mIsServer = false;
				if(isStart){
					mServer.startServer(ResourceFactory.UDB_PATH+"/tsvs16.udb");
					setmSqlSourceAddress(mServer.getLocalIP());
					mIsServer =  mServer.getStatus()==0;
				}else
					mServer.stopServer();
		return mIsServer;
		} catch (Exception e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			mIsServer = false;
			return false;
		}
	}
	
	@Override
	public void onEvent(String arg0) {
		Log.i(TAG, arg0);
		JSONObject res;
		int qn;
		try {
			res = new JSONObject(arg0);
			if (res.has("qn")) {
				qn = res.getInt("qn");
				if(qn==0)
				{
					if(res.has("r")){
						mSqlListener.onSqlStatus(res.getString("r").equals("0")?SQLState.SQL_CONNECTED:SQLState.SQL_DISCONNECTED);
						mIsJson = res.getString("r").equals("0");
						}
				}else if(qn==3){
					if(res.has("r")){
						JSONObject r = new JSONObject(res.getString("r"));
						JSONArray arr = new JSONArray(r.getString("rec"));
						for (int i = 0; i < arr.length(); i++) {
							JSONObject rec = arr.getJSONObject(i);
							mDatabaseVer = rec.getString("DB_VERSION");
						}
					}
				}
				if (res.has("r")) {
					String r = res.getString("r");
					String c = res.getString("c");
					if (r.contains("{")) {
						JSONObject result = new JSONObject(r);
						if (result.isNull("uc")) {
							// String et = result.getString("et");
							// String rc = result.getString("rc");
							// String cc = result.getString("cc");
							mSqlListener.onEvent(qn, result.getString("rec"));
						} else if (result.getInt("uc") > 0) {
							mSqlListener.onEvent(qn, result.getInt("uc"));
							mSqlListener.onSqlStatus(SQLState.SQL_CONNECTED);
						} else {
							Log.i(TAG, "Android Server Update error...");// updqte 오류
							mSqlListener.onEvent(qn, result.getInt("uc"));
						}
					}else if(c.equals("-1")){
						mSqlListener.onSqlStatus(SQLState.SQL_UNKNOWN);
					}
				}else if(res.has("e")){
					mSqlListener.onSqlStatus(SQLState.SQL_DISCONNECTED);
				}
			}else if (res.has("r")){
				String r = res.getString("r");
				if(r.equals("-9999")){
				Log.d(TAG, "Responce Error from Server to Client...");
				mIsJson = false;
				mSqlListener.onSqlStatus(SQLState.SQL_UNKNOWN);
				}else if (r.equals("-8888")){
				Log.d(TAG, "Responce Error from Server to Client...");
				mIsJson = false;
				mSqlListener.onSqlStatus(SQLState.SQL_UNKNOWN);
				}
				else{
					mIsJson = false;
				mSqlListener.onSqlStatus(SQLState.SQL_DISCONNECTED);}
			}
		}
		catch(JSONException e){
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			mSqlListener.onSqlStatus(SQLState.SQL_UNKNOWN);
		}
	}
	
	public static class Log{
		public Log() throws IOException {
			
		}
		public static void d(String tag, String msg){
			 StringBuffer bufLogMsg = new StringBuffer();
			 SimpleDateFormat df = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss_");
			 bufLogMsg.append(df.format(new Date()));
			 bufLogMsg.append("[Debug : ");
	         bufLogMsg.append(tag);
             bufLogMsg.append("]");
             bufLogMsg.append(msg+"\r\n");
			try {
				fw = new FileWriter(LogFile,true);
				fw.write(bufLogMsg.toString());
				fw.flush();
			} catch (IOException e) {
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}finally {
				try {fw.close();} catch (IOException e) {e.printStackTrace();}
			}
			android.util.Log.d(tag, msg);
		}
		public static void e(String tag, String msg){
			StringBuffer bufLogMsg = new StringBuffer();
			SimpleDateFormat df = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss_");
			bufLogMsg.append(df.format(new Date()));
			bufLogMsg.append("[Error : ");
			bufLogMsg.append(tag);
			bufLogMsg.append("]");             
			bufLogMsg.append(msg+"\r\n");
			try {
				fw = new FileWriter(LogFile, true);
				fw.write(bufLogMsg.toString());
				fw.flush();
			} catch (IOException e) {
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}finally{
				try {fw.close();} catch (IOException e) {e.printStackTrace();}
			}
			android.util.Log.e(tag, msg);
		}
		public static void i(String tag, String msg){
			StringBuffer bufLogMsg = new StringBuffer();
			SimpleDateFormat df = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss_");
			bufLogMsg.append(df.format(new Date()));
			bufLogMsg.append("[Info : ");
			bufLogMsg.append(tag);
			bufLogMsg.append("]");             
			bufLogMsg.append(msg+"\r\n");
			try {
				fw = new FileWriter(LogFile, true);
				fw.write(bufLogMsg.toString());
				fw.flush();
			} catch (IOException e) {
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}finally{
				try {fw.close();} catch (IOException e) {e.printStackTrace();}
			}
			android.util.Log.i(tag, msg);
		}
	};
	
	public BallotPrint getmBallotPrint() {
		return mBallotPrint;
	}

	public void setmBallotPrint(BallotPrint mBallotPrint) {
		this.mBallotPrint = mBallotPrint;
	}
	public enum QueryID {
		Q0, Q1 /* 인증서 DB 가져오기 */, Q2 /* 투표마감여부 지정값을 EN_S Table애서 구함(END_GUBN) */, Q3 /* 데이터베이스 마지막 변경정보 조회*/, 
		Q4 /* 선거환경 최종검증일자를 조회 */, Q5 /* 투표소ID 상태 점검 */, Q6 /* 선거인 그룹 점검 */, Q7 /* 투표소 정보 점검*/, Q8/*초기화 DB*/, Q9/*초기화 DB*/, Q10 /* 투표소별 진행상황을 조회*/, 
		Q11 /* 투표정보를 표시한다. - 카드발급자수, 현재일시*/, Q12 /* 명부기당 최대 카드발급수 */, Q13 /* 검색 (Q15 동일) */, Q14 /* 이미지 데이터 조회 (Q29 통합)*/,
		Q16 /* 등재번호로 선거인 조회 */,Q17 /* 재발급 및 발급취소 로그 */, Q18 /* 재발급, 발급취소 처리 */, Q19 /* 투표구 조회 [가상='N', 실제='Y'] (Q21 통합)*/, Q20 /* 투표구 수 조회 [가상='N', 실제='Y'](Q22 통합)*/,
		Q23 /* 선거인 그룹 조회 */, Q24 /* 선거인 그룹 조회 */, Q25 /* 선거인 중복여부 검사 */, Q26 /* 선거인 추가 */, Q28 /* 새로운 등재번호를 구한다. */, 
		Q31 /* 등재번호를 이용하여 명부중복자 조회 */, Q32 /* 투표소 상태 조회 (Q47, Q52 동일) */, Q33 /* 등재번호로 선거인정보 조회 */, Q34 /* 등재번호로 선거인정보 조회 */, Q35 /* 주민등록번호 / 선거인명 조회 */, 
		Q38 /* 기발급처리 */, Q41 /* 등재번호로 선거인pdf 조회 */, Q42 /* 투표정보 조회 */, Q43 /* pdf 선거인 100 분율 */, Q44 /* 연령대별 투표자 수 (pdf) */, 
		Q45 /* 시간대별 (pdf) 조회 */, 
		Q46 /* 투표구 이름 조회 */, Q48 /* 투표소 정보를 조회한다. */, Q49 /* 투표권카드 발급자수 */, Q50 /* 미투표자수 */, Q51 /* 투표소 마감정보 데이터베이스 반영 */, 
		Q53 /* 투표소 ZERO OUT 정보 조회 */, Q54 /* 투표소 개시정보 데이터베이스 반영 */, Q56/* 선거 정보 조회 */, Q57/* 후보자 정보 조회 */, Q58/* 발급 일련번호 조회 */, 
		Q60 /* 시리얼 번호 입력 */, Q61 /* 후보자 정보 조회 */, Q62/* 선거구 정보 조회 */, Q68/* 직인이미지 입력 */, Q69/* 직인 이미지 쿼리 */, Q70 /*체험판 select*/, Q71/*체험판 발금 */, Q72 /*체험판 발급된 지문 사인 이미지 조*/,
		Q73 /* 체험용 선거정보 */, Q74 /*체험판 등재번호 조*/, Q75 /*체험판 추가등재*/, Q76 /*체험판 중복확*/, Q77 /* 체험판 발급 취소 */, Q78/* 체험판 선거구 ID 선거 이름 가져오기 */
	}
	
}
