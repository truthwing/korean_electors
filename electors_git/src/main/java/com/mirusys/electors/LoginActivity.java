package com.mirusys.electors;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.haesin.manager.IBScanCommander.OnStateListener;
import com.haesin.manager.IBScanCommander.State;
import com.haesin.util.IPSaveSQLite;
import com.haesinit.common.Base64Util;
import com.haesinit.ultralite.database.SQLNO;
import com.integratedbiometrics.ibscancommon.IBCommon.ImageDataExt;
import com.mirusys.electors.ResourceFactory.Log;
import com.mirusys.electors.ResourceFactory.QueryID;
import com.mirusys.sign.Common;
import com.mirusys.voting.MiruSmartCard;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity
extends BasicActivity
implements OnClickListener, ResourceFactory.OnSQListener, OnStateListener
{
    public final static String TAG = "Login";
    public final static int ID_SETUP = 0;
    public final static int ID_LOGIN = 1;

    private View mBtnSetup;
    private View mBtnLogin;
    private EditText mEditSecureCode;
    private boolean mInstallable = true;
    private boolean mCardCheckable = false;
    private boolean mCardAttachable = false;
    private int mRetryCount = 0;

    private String mCardTpgId = null;

    private String mDbVtTitle;
    private String mDbTpgName;
    private String mDbTpgStatus;

    private Object mDbSync = new Object();
    private int mDbResult = DB_RESULT_READY;
    private boolean mIsSQLCheck = true;

    private static final String SHUT_DOWN="SHUTDOWN";

    private static final int DB_RESULT_CERT_ERROR = -3;
    private static final int DB_RESULT_0 = -2;
    private static final int DB_RESULT_ERROR = -1;
    private static final int DB_RESULT_READY = 0;
    private static final int DB_RESULT_SUCCESS = 1;
    private static final int DB_DEMO = 2;	//체험용


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFactory.openIBScanner(this);
		if(mFactory.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD)==null)
        mFactory.setProperty(ResourceFactory.CONFIG_ISSUE_METHOD, ResourceFactory.ISSUE_card);
        if(mFactory.getProperty(ResourceFactory.CONFIG_SERVER_MODE)==null)
        	mFactory.setProperty(ResourceFactory.CONFIG_SERVER_MODE, ResourceFactory.UI_TEST?ResourceFactory.MODE_both:ResourceFactory.MODE_client);
        if(mFactory.getProperty(ResourceFactory.CONFIG_RECOGNIZE)==null)
        	mFactory.setProperty(ResourceFactory.CONFIG_RECOGNIZE, ResourceFactory.RECOGNIZE_full);
        setContentView(R.layout.login);
        IPSAVE = new IPSaveSQLite(LoginActivity.this, "ipsetting.db", null, 1);
        mBtnSetup = findViewById(R.id.btnSetup);
        mBtnLogin = findViewById(R.id.btnLogin);
        mEditSecureCode = (EditText)findViewById(R.id.editSecureCode);
        mEditSecureCode.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (event.getAction() == KeyEvent.ACTION_DOWN){
					if(keyCode==KeyEvent.KEYCODE_ENTER){
					((Button)findViewById(R.id.btnLogin)).performClick();
					return true;
					}
				}
				return false;
			}
		});

        String strApkVer;

        try {
        	PackageInfo pi = getPackageManager().getPackageInfo(getPackageName(), 0);
        	strApkVer = String.valueOf(pi.versionCode) + "." + pi.versionName;
        } catch(NameNotFoundException e) {
            Log.d(TAG, e.getMessage());
        	strApkVer = "Unknown";
        }

        ((TextView)findViewById(R.id.textApkVer)).setText("BK " + strApkVer);
        ((TextView)findViewById(R.id.textTitlePolls)).setText(mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME));
        setVoteTitle(mFactory.getProperty(ResourceFactory.CONFIG_VTTITLE));

//        DisplayMetrics dm = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(dm);
//        Log.i(TAG, "Resolution is " + String.valueOf(dm.widthPixels) + " X " + String.valueOf(dm.heightPixels) + " (" + String.valueOf(dm.densityDpi) + " dpi)");

        // 런처용 앱목록 기능 추가
		pm = getPackageManager();
		layoutGrid = (LinearLayout)findViewById(R.id.layoutGrid);
		adapterGrid = new gridAdapter(this, R.layout.cell_main_grid, allList);
		gridView = (GridView)findViewById(R.id.gridView);
		gridView.setAdapter(adapterGrid);
		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				final ResolveInfo info = allList.get(arg2);

				Intent intent = new Intent(Intent.ACTION_RUN);
				intent.setComponent(new ComponentName( info.activityInfo.packageName, info.activityInfo.name));
				startActivity(intent);

				layoutGrid.setVisibility(View.GONE);
			}
		});

		findViewById(R.id.infoDbState).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				// 추가 앱이 있을 경우(array 마지막 아이템으로 더보기를 넣었을 경우, 탭했을때 더보기 창을 visible)
				if (layoutGrid.getVisibility() == View.GONE) {
					layoutGrid.setVisibility(View.VISIBLE);
				}
				else {
					layoutGrid.setVisibility(View.GONE);
				}
			}});

    }

	@Override
	protected void onResume() {
		super.onResume();
		if(mIsSQLCheck)
			mIsSQLCheck = mFactory.dbQuery(QueryID.Q4);
		// DB Listener 연결
        mFactory.setOnSQListener(this);
		// 관리자용 카드장치 오픈
        new Thread(){public void run() {mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER);
        if (mFactory.isUsableSmartcard() == false) {
        	// 관리자용 카드장치 오픈이 안될 경우 로그인 불가 (및 USB매체 설치 불가) 처리
        	runOnUiThread(new Runnable() {
				public void run() {
					mEditSecureCode.setEnabled(false);
					mBtnLogin.setClickable(false);
				}
			});
        }
        else if(mInstallable) {
        	mInstallable = false;
            upgrade(false);
        }
        };}.start();
        mDbResult = DB_RESULT_READY;
        // 런처용 앱목록 기능 추가
		Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		allList.clear();
		allList.addAll(pm.queryIntentActivities(mainIntent, 0));
		adapterGrid.notifyDataSetChanged();

    }

    @Override
    protected void onPause() {
		mDbResult = DB_RESULT_ERROR;
		synchronized(mDbSync) {
			mDbSync.notify();
		}


		// 관리자카드 삽입 전에 서브 화면으로 진입할 경우도 있을 수 있어 예외 처리
		if (mCardCheckable && mRetryCount < 5) {
			mEditSecureCode.setEnabled(true);
        	mBtnLogin.setClickable(true);
		}

		mEditSecureCode.setText("");
		mCardCheckable = false;
		super.onPause();
    }

    @Override
    protected void onDestroy() {

    	mFactory.release();
    	Log.d(TAG, "Login Destroy");
    	super.onDestroy();
    	android.os.Process.killProcess(android.os.Process.myPid());

    }

    @Override
    protected void setVoteTitle(String title) {
    	super.setVoteTitle(title);
    }

    @Override
    public void onAttached(MiruSmartCard card) {
    	if (mCardCheckable == false)
    		super.onAttached(card);
    	else {
    		mCardCheckable = false;
			dismissedMessageBox();

			byte[] cType = new byte[2];

			if (card.ProcGetCardType(cType) == MiruSmartCard.SUCCESS && cType[0] == 'T')	//체험용 추가로 모든 경우에 카드 타입 확
				new LoginTask().execute(card);
			else {
				mCardAttachable = true;

				if (mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME) != null)
					showDefaultMessage(getString(R.string.error_password_miscard, mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME)));
				else
					showDefaultMessage(R.string.error_password_nomanager);
			}
    	}
    }

    @Override
	public void onDetached(MiruSmartCard card) {
    	mRetryCount = 0;
		if (mCardAttachable) {
			if(!mEditSecureCode.getText().toString().isEmpty())
				mCardCheckable = true;
			mCardAttachable = false;
		}
    }

    @Override
    public void onIdle(MiruSmartCard card) {
    	if (mCardCheckable && mFactory.isCardAttached())
			onAttached(card);
    	else
    		super.onIdle(card);
    }

	@Override
    public void onClick(View v) {
    	v.setClickable(false);

        if (v.equals(mBtnSetup)) {
        	synchronized (mDbSync){mDbSync.notify();}
        	mCardAttachable = true;
        	startActivityForResult(new Intent(this, SetupActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP), ID_SETUP);
        }else if(v.getId()==R.id.btnShutDown)
        	showQuestionMessage(R.string.menu_shutdown, SHUT_DOWN);
        else if(mEditSecureCode.getText().toString().length() == 0){
        	mIsSQLCheck=false;
        	showDefaultMessage(R.string.error_password_empty);
        }else {
			mIsSQLCheck=false;
			mCardCheckable = true;
			mEditSecureCode.setEnabled(false);
			if (mFactory.isCardAttached() == false) {
				showDefaultMessage(R.string.error_password_nomanager);
			}else
			onAttached(mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER));
			return;
        }
        v.setClickable(true);
    }

    @Override
    public void onBackPressed() {
		moveTaskToBack(true);
    	finish();
    }

	@Override
    public void onSqlStatus(final ResourceFactory.SQLState state) {
		if (state == ResourceFactory.SQLState.SQL_CONNECTED ||
			state == ResourceFactory.SQLState.SQL_DISCONNECTED) {
			if(!mIsSQLCheck)
				mIsSQLCheck=true;
			else
    		runOnUiThread(new Runnable() {
				@Override
				public void run() {
					TextView v = (TextView)findViewById(R.id.infoDbState);
					v.setCompoundDrawablesWithIntrinsicBounds(
							getResources().getDrawable(state == ResourceFactory.SQLState.SQL_CONNECTED? R.drawable.ic_db_01_on : R.drawable.ic_db_01_off),
							null, null, null);
					v.setText(state == ResourceFactory.SQLState.SQL_CONNECTED? R.string.db_on : R.string.db_off);
				}
			});
		}

		if(state == ResourceFactory.SQLState.SQL_UNKNOWN) {
			mDbResult = DB_RESULT_READY;

			synchronized(mDbSync) {
				mDbSync.notify();
			}
    	}else if(state != ResourceFactory.SQLState.SQL_CONNECTED){
    		mDbResult = DB_RESULT_ERROR;

    		synchronized(mDbSync) {
    			mDbSync.notify();
    		}
    	}
    }

	@Override
    public Object[] getParameters(QueryID id) {
		if (id == QueryID.Q7)
			if(mFactory.isJson())
				return null;
			else
				return new Object[] {ResourceFactory.UI_TEST? "N" : "Y"};
		else if(id == QueryID.Q2)
    		return new Object[] {
    				mCardTpgId,
    				mFactory.isJson()?new SimpleDateFormat("yy-MM-dd").format(new Date(mFactory.getVtDate())):new java.sql.Date(mFactory.getVtDate())
    		};
		else if(id==QueryID.Q1)
			return new Object[]{
				mFactory.isJson()?new SimpleDateFormat("yy-MM-dd").format(new Date(mFactory.getVtDate())):new java.sql.Date(mFactory.getVtDate()),
				mCardTpgId
			};
		else
			return null; // Query에 요구되는 파라메터가 없을 경우 null을 반환한다.
    }

	@Override
    @SuppressWarnings("incomplete-switch")
    public boolean onResultSet(ResultSet rs, QueryID id) throws SQLException {
		boolean noMoreQuery = true;

    	if (rs.next() == false){
    		switch (id) {
			case Q4:
				mDbResult = DB_RESULT_READY;
				break;
			default:
				mDbResult = DB_RESULT_ERROR;
			}
    	}
    	else {
    		mDbResult = ResourceFactory.UI_TEST? DB_DEMO:DB_RESULT_SUCCESS;

    		Log.d(TAG, "onResultSet for " + id);

    		switch (id) {
    			case Q1:
				try {
					String cert = rs.getString("CERT").split("-----")[2].substring(1);
					Log.i(TAG, "cert : "+cert);
					byte[] bytesCert = Base64Util.decode(cert);
					mDbResult = (noMoreQuery = !mFactory.getSign().verifySign(
						mFactory.getSign().digitalSign(
							"1234567823456789".getBytes(), mFactory.getTcardPriKey()),
						bytesCert, "1234567823456789".getBytes()))?
										DB_RESULT_CERT_ERROR:mDbResult;
					mFactory.setTcardCertDB(LoginActivity.this, bytesCert);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    			break;
    			case Q5:
    				noMoreQuery = false;
    				break;
    			case Q6: // EN_S라는 선거정보가 없는 경우 오류
       				if (ResourceFactory.UI_TEST)
       					noMoreQuery = false;
       				else if(noMoreQuery = rs.getInt(1) != 0) {
       		    		mDbResult = DB_RESULT_ERROR;
       				}
    				break;
    			case Q7: // 투표소 비지정 갯수가 하나라도 있으면 오류
       				if (noMoreQuery = rs.getInt(1) > 0) {
       		    		mDbResult = DB_RESULT_0;
       				}
    				break;
    			case Q2:
    				mDbVtTitle = rs.getString("S_NAME"); // 선거 대표명
    				//rs.getString("END_GUBN"); // 마감구분 (Y or N)
    				mDbTpgStatus = rs.getString("STATUS"); // 투표구상태 (A:준비,B:개시,C:마감)
    				mDbTpgName = rs.getString("TPG_NAME"); // 투표구명
    				String s = new SimpleDateFormat("yyyy-MM-dd").format(rs.getDate("SERVER_DATE"))+new SimpleDateFormat("HH:mm:ss").format(rs.getTime("SERVER_TIME"));
    				mFactory.timeSetting(s);//서버시간에 맞춰서 설정.
    				mFactory.setProperty(ResourceFactory.CONFIG_TPGNAME, mDbTpgName);
    				Log.d(TAG, "S_NAME=" + mDbVtTitle + ", STATUS=" + mDbTpgStatus + ", mDbTpgName=" + mDbTpgName);
    				break;
    			case Q73:
    				mDbTpgName = rs.getString("SG_PLACE");
    				mDbVtTitle = rs.getString("SG_NAME");
    				mCardTpgId = rs.getString("SG_DID");
    				synchronized (mDbSync) {
    					mDbSync.notify();
					}
    				break;
    		}
    	}

    	if (noMoreQuery) {
			synchronized(mDbSync) {
				mDbSync.notify();
			}

			return false;
    	}

		return true;
    }

	@Override
    public boolean onResultSet(int rows, QueryID id) throws SQLException {
		return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_CANCELED)
            Log.d(TAG, "Result canceled from " + String.valueOf(requestCode));
        else if(requestCode == ID_LOGIN && ResourceFactory.VOTE_working.equals(mFactory.getVtStatus()) == false)
           	mInstallable = true;
    }

    @Override
    protected void onMessageResult(View v, Object tag) {
    	// TODO Auto-generated method stub
    	if(v.getId()==android.R.id.button1&&tag==TAG){
    		mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER);
    		mFactory.setProperty(ResourceFactory.CONFIG_TPGID, mCardTpgId);
    		mFactory.setProperty(ResourceFactory.CONFIG_TPGNAME, mDbTpgName);
    		new LoginTask().execute(mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER));
    	}else if(v.getId()==android.R.id.button1&&tag==null){
    		mCardCheckable = false;
    		mEditSecureCode.setEnabled(true);
    		mBtnLogin.setClickable(true);
    	}else if(SHUT_DOWN.equals(tag)&&v.getId()==android.R.id.button1){
    		mFactory.shutDown();
			try {Thread.sleep(3000);} catch (InterruptedException e) {Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();}
			mFactory.forcedShutDown();
		}
    	super.onMessageResult(v, tag);
    }

	private class LoginTask extends AsyncTask<MiruSmartCard, Void, Integer> {
		String mSecureCode = "";
		long mEstimateTime;

		final int ALLOWED_RETRY_COUNT = 5;
		final int WAIT_DISPOSE_MILLIS = 2000;

		@Override
		protected void onPreExecute() {
			// 보안처리를 위한 진행팝업 활성화
			showWaitMessage(R.string.wait_verify_password);
			mEstimateTime = System.currentTimeMillis();

			mSecureCode = mEditSecureCode.getText().toString();
		}

		@Override
        @SuppressLint("SimpleDateFormat")
        protected Integer doInBackground(MiruSmartCard... cards) {
        	int resultMsgId = 0;

			synchronized(cards[0]) {
				if (cards[0].CardVerify(mSecureCode) == MiruSmartCard.SUCCESS) {	//체험용 추가로 암호만 맞으면 OK
					long now = System.currentTimeMillis();
					if ((now - mEstimateTime) < WAIT_DISPOSE_MILLIS) {
						try { Thread.sleep(WAIT_DISPOSE_MILLIS - (now - mEstimateTime)); } catch(InterruptedException ie) {}
					}

					showWaitMessage(R.string.wait_secure_processing);

					byte[] bytesPKey = new byte[16];
					byte[] bytesDbUserId = new byte[50];
					byte[] bytesDbUserPasswd = new byte[50];
					byte[] bytesDn = new byte[1302];
					byte[] bytesCert = new byte[1302];
					byte[] bytesCtrCert = new byte[1302];
					byte[] bytesBSection = new byte[51];
					int ret = 0;
					if (ResourceFactory.UI_TEST == false && (
						// 관리자 인증서, 관리자 키, 중앙선관위 인증서
						(mFactory.isJson()&&((ret = cards[0].ProcReadTMCPKIDatainBK(Common.string2Bytearray(mSecureCode,mSecureCode.length()),Common.string2Bytearray(mFactory.getHashValue(), 44), (byte)0x01, bytesCert)) != MiruSmartCard.SUCCESS
						|| !mFactory.setTcardCert(bytesCert)))||
						(ret = cards[0].ProcReadTMCPKIDatainBK(Common.string2Bytearray(mSecureCode,mSecureCode.length()),Common.string2Bytearray(mFactory.getHashValue(), 44), (byte)0x02, bytesDn)) != MiruSmartCard.SUCCESS
						|| !mFactory.setTcardPriKey(bytesDn)
						|| (ret = cards[0].ProcReadTMCPKIDatainBK(Common.string2Bytearray(mSecureCode,mSecureCode.length()),Common.string2Bytearray(mFactory.getHashValue(), 44), (byte)0x03, bytesCtrCert)) != MiruSmartCard.SUCCESS
						|| !mFactory.setTcardCenterCert(bytesCtrCert)
						||
						mFactory.isJson()&&(!mFactory.getSign().verifySign(mFactory.getSign().digitalSign("1234567823456789".getBytes(),
								mFactory.getTcardPriKey()),
								mFactory.getTcardCert(), "1234567823456789".getBytes()))
						||// 인증서 및 DB ID/PASSWORD 획득
								cards[0].TCardReadKeyDBData(bytesPKey, bytesDbUserId, bytesDbUserPasswd) != MiruSmartCard.SUCCESS ||
								!mFactory.setTcardKey(LoginActivity.this, bytesPKey)||
								// 투표소ID, 투표일자, 투표소상태 획득
								cards[0].TCardReadBSection(bytesBSection) != MiruSmartCard.SUCCESS
						)){
						Log.d(TAG, "Login ret : "+ret);
						resultMsgId = R.string.error_password_miscard;}
					else {
						String sysTpgId = null;
						showWaitMessage(R.string.wait_reading_processing);
						if (ResourceFactory.UI_TEST == false)
							mCardTpgId = ResourceFactory.bytesToString(bytesBSection, 0, 10);
						else {
							bytesBSection[18] = (byte)(mDbTpgStatus = ResourceFactory.VOTE_working).charAt(0);

							System.arraycopy("20150407".getBytes(), 0, bytesBSection, 10, 8);
						}

						try {
							// 투표일자를 보관한다. (선거정보 조회 시 파라메터로 사용되는 중요 변수)
							mFactory.setVtDate(new SimpleDateFormat("yyyyMMdd").parse(new String(bytesBSection, 10, 8)).getTime());
							Log.d(TAG, "card date = "+ ResourceFactory.bytesToString(bytesBSection, 10, 8));
							Log.d(TAG, "card tpgID = "+ ResourceFactory.bytesToString(bytesBSection, 0, 10));
							Log.d(TAG, "card status = "+ ResourceFactory.bytesToString(bytesBSection, 18, 1));
							if(ResourceFactory.UI_TEST){	//체험용
								mDbResult = DB_DEMO;
								mFactory.dbQuery(QueryID.Q73);
								synchronized(mDbSync) {
									try { mDbSync.wait(); } catch(InterruptedException ie) {}
								}
							}else{
								mDbResult = DB_RESULT_READY;
								// 순차적으로 Query가 처리되며 각 단계에서 조건을 만족하지 못할 시 다음 단계는 실행되지 않는다.
								// Query에 필요한 파라메터는 getParameters callback을 통해 DB handler에 전달한다.
								if (mFactory.dbQuery(QueryID.Q1, QueryID.Q5, QueryID.Q6, QueryID.Q7, QueryID.Q2)) {
									synchronized(mDbSync) {
										try { mDbSync.wait(); } catch(InterruptedException ie) {}
									}
								}
							}

						} catch(ParseException e) {
							Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
							mDbResult = DB_RESULT_ERROR;
						}

						/* 체험용 */
						if(mDbResult == DB_DEMO){
							mDbTpgStatus = ResourceFactory.VOTE_working;
							mFactory.store();
							resultMsgId = 0;
							File f = new File(ResourceFactory.UDB_PATH+"/tsvs16.udb");
							if(f.exists()&&mFactory.onServer(true));
							else{
								mFactory.onServer(false);
								resultMsgId = R.string.error_no_database;
							}
							dismissedMessageBox();
						}
						/**/

						if(mDbResult == DB_RESULT_READY)
							resultMsgId = R.string.error_database_failed_connection;
						else if (mDbResult==DB_RESULT_ERROR)
							resultMsgId = R.string.error_database_failed_auth;
						else if(mDbResult == DB_RESULT_0)
							resultMsgId = R.string.error_empty_basicinfo;
						else if(mDbResult == DB_RESULT_CERT_ERROR)
							resultMsgId = R.string.error_card_failed_auth;
						else if(ResourceFactory.UI_TEST == false &&
								(bytesBSection[18]==0?'A':bytesBSection[18]) !=
								(mFactory.getProperty(ResourceFactory.CONFIG_SERVER_MODE).equals(ResourceFactory.MODE_server)?
										(bytesBSection[18]==0?'A':bytesBSection[18]):mDbTpgStatus.charAt(0))){ // 카드와 DB의 개시 상태가 불일치하면 이전 발급카드로 간주하여 로그인 불가 처리
								resultMsgId = R.string.error_password_open;
							}
						else if (ResourceFactory.UI_TEST == false &&
								(sysTpgId = mFactory.getProperty(ResourceFactory.CONFIG_TPGID)) != null &&
								(sysTpgId.equals(mCardTpgId) == false && ResourceFactory.VOTE_working.equals(mDbTpgStatus)))
							// 개시 상태이면서 투표소의 관리자카드가 아닌 경우 접속 불허
							resultMsgId = R.string.error_password_change;
						else {
							mFactory.setProperty(ResourceFactory.CONFIG_TPGID, mCardTpgId);
							mFactory.setProperty(ResourceFactory.CONFIG_TPGNAME, mDbTpgName);

							mFactory.setProperty(ResourceFactory.CONFIG_VTTITLE, mDbVtTitle);
							mFactory.store();

							mFactory.setVtStatus(mDbTpgStatus);
							mFactory.setVtOpeningValue(new String(bytesBSection, 41, 10)); // 개시값
							mFactory.setManagerKey(mSecureCode); // 관리자카드 비밀번호 백업 (카드 access를 위해 필요)
							if(mDbResult == DB_DEMO && resultMsgId == 0 || mDbResult == DB_RESULT_SUCCESS){
							startActivityForResult(new Intent(LoginActivity.this,
									mFactory.getProperty(ResourceFactory.CONFIG_SERVER_MODE).equals(ResourceFactory.MODE_server)?
									MainActivity.class:BeforeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP), ID_SETUP);
							resultMsgId = 0;
							}
						}
					}
				}
				else if(++mRetryCount < ALLOWED_RETRY_COUNT)
					resultMsgId = R.string.error_password_wrong;
				else
					resultMsgId = R.string.error_password_locked;
			}

			dismissedMessageBox();
			return resultMsgId;
        }

        @Override
        protected void onPostExecute(Integer resultMsgId) {
        	boolean result = resultMsgId == 0;

        	if (result) {
				setVoteTitle(mDbVtTitle);
				runOnUiThread(new Runnable() {
					public void run() {
						((TextView)findViewById(R.id.textTitlePolls)).setText(mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME));
					}
				});
        	}
        	else {
        		if (resultMsgId != R.string.error_password_locked) {
					result = true;
				}
        		try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				}
        		dismissedMessageBox();
				if (resultMsgId == R.string.error_password_wrong) {
					runOnUiThread(new Runnable() {
						public void run() {
							mEditSecureCode.setText("");
						}
					});
					showDefaultMessage(getString(resultMsgId,mRetryCount));
				}else if(resultMsgId == R.string.error_password_change){
        			showQuestionMessage(R.string.error_password_change, TAG);
        			return;
        		}else if (resultMsgId != R.string.error_password_miscard)
        			showDefaultMessage(resultMsgId);
        		else if(mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME) != null)
					showDefaultMessage(getString(resultMsgId, mFactory.getProperty(ResourceFactory.CONFIG_TPGNAME)));
        		else
        			showDefaultMessage(R.string.error_password_nomanager);
        	}

    		if (result) {
	        	mEditSecureCode.setEnabled(true);
	        	mBtnLogin.setClickable(true);
    		}
    		mIsSQLCheck = true;
    		mSecureCode = "";
    		onPause();
        }
	}

	private PackageManager pm;
	private LinearLayout layoutGrid;
	private GridView gridView;
	private static gridAdapter adapterGrid;
	private List<ResolveInfo> allList = new ArrayList<ResolveInfo>();

	public class gridAdapter extends ArrayAdapter<ResolveInfo> {
		LayoutInflater inflater;
		int mResource;
		List<ResolveInfo> apps;

		public gridAdapter(Context context, int resource, List<ResolveInfo> objects) {
			super(context, resource, objects);
			inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			mResource = resource;
			apps = objects;
		}

		@Override
		public View getView(int position, View view, ViewGroup arg2) {

			if (view == null) {
				view = inflater.inflate(mResource, arg2, false);
			}
			final ResolveInfo info = apps.get(position);

			ImageView imgTitle = (ImageView)view.findViewById(R.id.imgTitle);
			TextView tvTitle = (TextView)view.findViewById(R.id.tvTitle);

			imgTitle.setImageDrawable(info.activityInfo.loadIcon(pm));
			tvTitle.setText(info.activityInfo.loadLabel(pm).toString());

			return view;
		}
	}

	@Override
	public void onEvent(int qn, String arg0) {
		JSONArray res;
		try {
			res = new JSONArray(arg0);
				if (qn==7)
					// Q7
					for(int i=0;i<res.length();i++){
						JSONObject obj = res.getJSONObject(i);
						mDbResult = obj.getString("CNT").equals("0") ? DB_RESULT_SUCCESS
							: DB_RESULT_0;
					}
				else if (qn==2) {
					if(res.length()<1){
						mDbResult=DB_RESULT_ERROR;
					}else
					for(int i=0;i<res.length();i++){
						JSONObject obj = res.getJSONObject(i);
					// Q2
					mDbTpgName = obj.getString("TPG_NAME");
					mDbVtTitle = obj.getString("S_NAME");
					mDbTpgStatus = obj.getString("STATUS");
					String s = obj.getString("SERVER_DATE")+obj.getString("SERVER_TIME");
    				mFactory.timeSetting(s);//서버시간에 맞춰서 설정.
					mDbResult = DB_RESULT_SUCCESS;
					mFactory.setProperty(ResourceFactory.CONFIG_TPGNAME, mDbTpgName);
					}
					synchronized (mDbSync) {
						mDbSync.notify();
					}
				}else if(qn==73){
					if(res.length()<1){
						mDbResult=DB_RESULT_ERROR;
					}else
					for(int i=0;i<res.length();i++){
						JSONObject obj = res.getJSONObject(i);
		  				mDbTpgName = obj.getString("SG_PLACE");
	    				mDbVtTitle = obj.getString("SG_NAME");
	    				mCardTpgId = obj.getString("SG_DID");
	    				synchronized (mDbSync) {
	    					mDbSync.notify();
						}
					}
				}
					else if (qn==5)
					// Q5
					for(int i=0;i<res.length();i++){
						JSONObject obj = res.getJSONObject(i);
					mDbResult = obj.getString("CNT").equals("0") ? DB_RESULT_SUCCESS
							: DB_RESULT_0;}
		} catch (JSONException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			synchronized(mDbSync) {
				mDbSync.notify();
			}
			mDbResult = DB_RESULT_READY;
		}
	}

	@Override
	public void onEvent(int qn, int updateCount) {}

	@Override
	public void onPrepareUpdate(SQLNO qn, String[] jsonParam) {}

	@Override
	public void onStatus(State state) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPreview(Bitmap bitmap) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCompleted(ImageDataExt image) {
		// TODO Auto-generated method stub

	}
}
