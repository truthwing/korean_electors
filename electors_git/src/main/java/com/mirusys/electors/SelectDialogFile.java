package com.mirusys.electors;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class SelectDialogFile
extends Dialog
implements View.OnClickListener, OnItemClickListener {

	private static String USB_ROOT = "/mnt";

	private Activity mActivity;
	private TextView tvRoute;
	private ListView mListView;
	private String mTitle;

	private String mChoice = ResourceFactory.getInstance().getString(R.string.download_path_choice);
	private String mBack = ResourceFactory.getInstance().getString(R.string.download_path_back);
	private String mRoot = ResourceFactory.getInstance().getString(R.string.download_path_root);
	private String mUSB = ResourceFactory.getInstance().getString(R.string.download_path_usb_storage);
	private String mFirst = ResourceFactory.getInstance().getString(R.string.download_path_first_menu);
	
	private ArrayList<String> item = new ArrayList<String>();
	private ArrayList<String> path = new ArrayList<String>();
	private int selectPositionList = -1;


	public SelectDialogFile(Activity a, int titleResId) {
		this(a, a.getString(titleResId));
	}

	public SelectDialogFile(Activity a, String title) {
		super(a);
		mActivity = a;
		mTitle = title;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(LayoutInflater.from(mActivity).inflate(R.layout.popup_setting_file, null));

		findViewById(R.id.btnCancel).setOnClickListener(this);
		((TextView)findViewById(R.id.setting_title)).setText(mTitle);

		mListView = (ListView)findViewById(R.id.popup_setting_list);
		mListView.setOnItemClickListener(this);
		mListView.setAdapter(new BaseAdapter() {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				if (convertView == null) {
					convertView = LayoutInflater.from(mActivity).inflate(R.layout.cell_popup_setting_file, null);
				}

				TextView tvTitle = (TextView)convertView.findViewById(R.id.tvTitle);

				String e = item.get(position);
				if (e != null) 
				{
					tvTitle.setText(e);

					// 처음메뉴, 뒤로면 뒤로폴더 이미지 표시
					if (e.equals(mFirst) || e.equals(mBack)) {
						tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_folder_back, 0, 0, 0);
					} else {
						// 폴더면 폴더이미지 표시, 파일이면 파일이미지 표시
						File file = new File(path.get(position));
						if (file.isDirectory() || e.equals(mUSB))
						{
							tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_folder, 0, 0, 0);
						}
						else{
							if (file.getAbsolutePath().contains(".bmp")) {
								tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_filetype_img, 0, 0, 0);
							} else if (file.getAbsolutePath().contains(".jpg")) {
								tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_filetype_img, 0, 0, 0);
							} else if (file.getAbsolutePath().contains(".png")) {
								tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_filetype_img, 0, 0, 0);
							} else if (file.getAbsolutePath().contains(".gif")) {
								tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_filetype_img, 0, 0, 0);
							} else {
								tvTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
							}
						}
					}

					tvTitle.setSelected((selectPositionList == position));
				}
				return convertView;
			}

			@Override
			public long getItemId(int position) {
				return position;
			}

			@Override
			public Object getItem(int position) {
				return item.get(position);
			}

			@Override
			public int getCount() {
				return item.size();
			}
		});

		setOnShowListener(new OnShowListener() {
			@Override
			public void onShow(DialogInterface dialog) {
				getWindow().setLayout(LayoutParams.WRAP_CONTENT, 400);
			}
		});

		tvRoute = (TextView)findViewById(R.id.tvRoute);		
		setRootDir();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onAttachedToWindow() {
		// TODO Auto-generated method stub
		super.onAttachedToWindow();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
		intentFilter.addAction(Intent.ACTION_MEDIA_EJECT);
		intentFilter.addAction(Intent.ACTION_UMS_CONNECTED);
		intentFilter.addAction(Intent.ACTION_UMS_DISCONNECTED);
		intentFilter.addAction(Intent.ACTION_MEDIA_SCANNER_STARTED);
		intentFilter.addAction(Intent.ACTION_MEDIA_SCANNER_FINISHED);
		intentFilter.addDataScheme("file");
		mActivity.registerReceiver(mMediaScannerReceiver, intentFilter);
	}

	@Override
	public void onDetachedFromWindow() {
		// TODO Auto-generated method stub
		super.onDetachedFromWindow();
		mActivity.unregisterReceiver(mMediaScannerReceiver);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btnCancel) {
			dismiss();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		selectPositionList = -1;

		if (path.get(position).equals(mRoot)) {
			setRootDir();
		} else {
			File file = new File(path.get(position));
			if (file.isDirectory())
			{
				if(file.canRead())
					getDir(path.get(position));
				else{
					//					ErrorDialog.showPopupError(getFragmentManager(), "[" + file.getName() + "] folder can't be read!");
				}
			}
			else{
				selectPositionList = position;
				dismiss();
			}
		}
		((BaseAdapter)mListView.getAdapter()).notifyDataSetChanged();
	}
    private Pattern psNext = Pattern.compile("^/mnt/usb[0-9]");// \uFE55
    private Pattern psPrev = Pattern.compile("^/mnt/usb[0-9]/sda");// \uFE55

	private void getDir(String dirPath)
	{
	    if (psNext.matcher(dirPath).matches()) {
	    	dirPath = dirPath + "/sda/sda1";
	    }
	    else if (psPrev.matcher(dirPath).matches()) {
			setRootDir();
			return;
	    }

	    File f = new File(dirPath);
		if (!f.exists() && !dirPath.equals(USB_ROOT)){
			tvRoute.setText(f.getParent());
			getDir(f.getParent());
			return;
		}

		selectPositionList = -1;

		tvRoute.setText(dirPath);

		item.clear();
		path.clear();

		File[] files = f.listFiles();

		if(!dirPath.equals(USB_ROOT)){
			//			item.add("처음메뉴");
			//			path.add("루트");

			if (!dirPath.equals("/")) {
				if (dirPath.equals(USB_ROOT)) {
					item.add(mBack);
					path.add(mRoot);
				} else {
					item.add(mBack);
					path.add(f.getParent());
				}
			}

			for(int i=0; i < files.length; i++){
				File file = files[i];

				if (file.isDirectory() && !file.isHidden()) {
					path.add(file.getPath());
					item.add(file.getName());
				}
				else if (file.getAbsolutePath().contains(".bmp") || file.getAbsolutePath().contains(".jpg")||
						file.getAbsolutePath().contains(".gif") || file.getAbsolutePath().contains(".png")) {
					path.add(file.getPath());
					item.add(file.getName());
				}
			}
		}
		else {
			for(int i=0; i < files.length; i++){
				File file = files[i];

				if (file.isDirectory() && !file.isHidden() && file.getAbsolutePath().startsWith(USB_ROOT+"/usb")) {
					path.add(file.getPath());
					item.add(file.getName());
				}
				else if (file.getAbsolutePath().contains(".bmp") || file.getAbsolutePath().contains(".jpg")||
						file.getAbsolutePath().contains(".gif") || file.getAbsolutePath().contains(".png")) {
					path.add(file.getPath());
					item.add(file.getName());
				}
			}
		}


		((BaseAdapter)mListView.getAdapter()).notifyDataSetChanged();
	}

	private void setRootDir() {
		tvRoute.setText(mChoice);

		item.clear();
		path.clear();

		getDir(USB_ROOT);
	}


	private final BroadcastReceiver mMediaScannerReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			final String currentPath = tvRoute.getText().toString();
			final String scannedPath = intent.getData().toString().replace("file:///", "/");

			if (currentPath.contains(scannedPath)) {//USB하위폴더 경우
				mListView.postDelayed(new Runnable(){
					public void run() {
						try {
							getDir(scannedPath);
						}
						catch (Exception e){
							setRootDir();
						}
					}
				}, 1000);
			}
			else if (scannedPath.contains(currentPath)) {//상위폴더 경우
				mListView.postDelayed(new Runnable(){
					public void run() {
						try {
							getDir(currentPath);
						}
						catch (Exception e){
							setRootDir();
						}
					}
				}, 1000);
			}
		}
	};
	

	public String getSelectedItem() {
		return selectPositionList>-1?path.get(selectPositionList):null;
	}
}