package com.mirusys.electors;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.haesin.entities.IPAddressObject;
import com.haesin.entities.PrintIntentObject;
import com.haesin.manager.IBScanCommander;
import com.haesin.manager.IDScanCommander;
import com.haesin.util.EditTextWithClearButton;
import com.haesin.util.PlaySound;
import com.haesin.util.SignatureMirrorView;
import com.haesin.util.SignatureView;
import com.haesinit.ultralite.database.SQLNO;
import com.integratedbiometrics.ibscancommon.IBCommon.ImageDataExt;
import com.mirusys.electors.ResourceFactory.OnSQListener;
import com.mirusys.electors.ResourceFactory.QueryID;
import com.mirusys.electors.ResourceFactory.SQLState;
import com.mirusys.epson.BallotPrint;
import com.mirusys.epson.dto.Candidate;
import com.mirusys.voting.MiruSmartCard;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import com.mirusys.electors.ResourceFactory.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

public class SetupActivity extends BasicActivity implements
		MediaPlayer.OnCompletionListener, DialogInterface.OnDismissListener,
		IDScanCommander.OnStateListener, IBScanCommander.OnStateListener,
		OnClickListener, OnSQListener, OnItemClickListener {
	public final static String TAG = "Setup";
	private Button mBtnVerifyTestPrint;
	private PlaySound mBeeper = new PlaySound();

	// ------------------- IDSCAN INTERFACE -----------------------

	private IDScanCommander mIDScanner = new IDScanCommander(this);
	private boolean midCapturing = false;

	// ------------------- IBSCAN INTERFACE -----------------------

	private boolean mibCapturing = false;

	// ------------------ SMARTCARD INTERFACE ---------------------

	private int mCardIndex = 0;

	// ------------------------------------------------------------

	// The device state.
	private final static int MSG_IBSCAN_CAPTURE = 0;
	private final static int MSG_IBSCAN_CLOSING = 1;
	private final static int MSG_IDSCAN_CLOSING = 2;
	private final static int MSG_SMCARD_CLOSING = 3;
	private final static int MSG_SQL_CONNECT = 4;
	private final static int MSG_SETUP_DIALOG = 5;

	private final static int NORMAL = 0;
	private final static int ABNORMAL = 1;

	private final static int SERVER = 0;
	private final static int BOTH = 1;
	private final static int CLIENT = 2;

	private AudioManager mAudioManager;
	private MediaPlayer mMediaPlayer;

	private ViewGroup mTabBasic, mTabVerify;
	private Button mBtnBasicTab, mBtnVerifyTab, mBtnSoundguide;
	private ImageView mImgFingerprint = null;

	private SelectDialog mModeSelector = null;
	private TextView mDeviceMode, mTextPowerStatus;
	private int mSelectedMode = -1;

	private SelectDialog mMethodSelector = null;
	private TextView mOutputMethod;
	private int mSelectedMethod = -1;

	private SelectDialog mRecognizeSelector = null;
	private TextView mRecognizeText;
	private int mSelectedRecognize = -1;
	
	private Button mDBStart = null;
	private Button mDBStop = null;
	private boolean mIsDBOn;
	
	private ListView mIPSavelist;
	private IPSetAdapter mIpsetAdapter;
	
	private List<IPAddressObject> mSavedIPAddresses;

	private EditTextWithClearButton mServerIp, mNetIp, mNetSubnet, mNetGateway,
			mNetDns, mNetRemark;
	private TextView mBtnSave, mBtnSubmit;
	private AsyncTask<Void, Void, IPAddressObject> nwi;

	private IPAddressObject mIPObj = new IPAddressObject();
	private TextView mDefaultTextView;
	private Dialog mMessageDialog;
	private String mMsgText;
	private final String MsgTag = "DeviceCheck";
	private boolean mIsScan=false;
	private boolean mIsFinger=false;
	private boolean mIsSign=false;
	private boolean mIsWriter=false;

	private void setUI(IPAddressObject networkInfo) {
		mNetIp.setText(networkInfo.getIpaddress());
		mNetGateway.setText(networkInfo.getGateway());
		mNetDns.setText(networkInfo.getDns());
		mNetSubnet.setText(networkInfo.getSubnet());
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setup);
		mIDScanner.open();
		mIDScanner.fingerOn(false);
		mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		mBtnVerifyTestPrint = (Button) findViewById(R.id.btnVerifyTestPrint);
		mTabBasic = (ViewGroup) findViewById(R.id.layoutBasicTab);
		mTabVerify = (ViewGroup) findViewById(R.id.layoutVerifyTab);
		mBtnBasicTab = (Button) findViewById(R.id.btnToggleBasic);
		mBtnVerifyTab = (Button) findViewById(R.id.btnToggleVerify);

		mDBStart = (Button) findViewById(R.id.btn_db_start);
		mDBStop = (Button) findViewById(R.id.btn_db_stop);
		mDBStart.setOnClickListener(this);
		mDBStop.setOnClickListener(this);
		
		mBtnBasicTab.setSelected(true);

		mBtnSoundguide = (Button) findViewById(R.id.btnVerifySoundguide);

		mTextPowerStatus = (TextView) findViewById(R.id.textDevStatePower);

		mIPSavelist = (ListView) findViewById(R.id.lv_save_ip);
		mIPSavelist.setOnItemClickListener(this);
		mSavedIPAddresses = IPSAVE.select();
		mIpsetAdapter = new IPSetAdapter();
		
		mNetIp = (EditTextWithClearButton) findViewById(R.id.et_basic_network_ip);
		mNetSubnet = (EditTextWithClearButton) findViewById(R.id.et_basic_network_subnet);
		mNetGateway = (EditTextWithClearButton) findViewById(R.id.et_basic_network_gateway);
		mNetDns = (EditTextWithClearButton) findViewById(R.id.et_basic_network_dns);
		mNetRemark = (EditTextWithClearButton) findViewById(R.id.et_basic_network_remark);

		mBtnSave = (TextView) findViewById(R.id.btn_network_save);
		mBtnSave.setClickable(true);
		mBtnSave.setOnClickListener(this);
		mBtnSubmit = (TextView) findViewById(R.id.btn_network_submit);
		mBtnSubmit.setClickable(true);
		mBtnSubmit.setOnClickListener(this);

		// 서버 연결 여부 체크 정의 pjw
		mServerIp = (EditTextWithClearButton) findViewById(R.id.basic_server);
		mServerIp.setText(mFactory.getmSqlSourceAddress());

		findViewById(R.id.btn_connect_server_test).setOnClickListener(this);
		mBtnVerifyTestPrint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PrintIntentObject pio = new PrintIntentObject();
				pio.setBallotType(ResourceFactory.BALLOT_TYPE0);
				pio.setCandidateNum(2);
				pio.setTitleName("테스트 선거");
				pio.setSubTitleName("테스트 선거구");
				ArrayList<Candidate> mCandidates = new ArrayList<Candidate>();
				mCandidates.add(new Candidate("1", "정당", "후보자1", "", "", ""));
				mCandidates.add(new Candidate("2", "정당", "후보자1", "", "", ""));
				pio.setCandidate(mCandidates);
				pio.setBallotColor(ResourceFactory.BOX_COLOR2);
				pio.setFillType(ResourceFactory.FILL_FULL);
				pio.setSerial(true);
				pio.setStartSerialNum(1);
				pio.setEndSerialNum(1);
				pio.setStamp(null);
				pio.setAdminstamp(null);
				ArrayList<PrintIntentObject> pios = new ArrayList<PrintIntentObject>();
				pios.add(pio);
				if(mFactory.getmBallotPrint()==null){
					mFactory.setmBallotPrint(new BallotPrint(mFactory));
				}
				if( mFactory.getmBallotPrint().getPrintStatus() == ResourceFactory.PRINT_READY)
					mFactory.getmBallotPrint().printBallot(pios);
				else
					showDefaultMessage(R.string.printBusy);
			}
		});
		
		((Button)findViewById(R.id.btnVerifyDateTime)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SetupActivity.this, PickerActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(intent);
			}});
		SeekBar sb = (SeekBar) findViewById(R.id.seekBarSoudVolume);
		sb.setMax(mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
		sb.setProgress(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
		sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
						progress, AudioManager.FLAG_PLAY_SOUND);
			}
		});

		mDeviceMode = (TextView) findViewById(R.id.status_setting);
		mOutputMethod = (TextView) findViewById(R.id.tv_basic_print_set);
		
		if (mFactory.isUsableSmartcard() == false) {
			// 관리자용 카드장치 오픈이 안될 경우 USB매체 설치 불가 처리
			findViewById(R.id.obj_install).setEnabled(false);
			// 카드장치 오픈이 안될 경우 카드장치 점검 불가 처리
			findViewById(R.id.btnVerifyCardwriter).setEnabled(false);
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		mIPSavelist.setAdapter(mIpsetAdapter);
		super.onResume();
		mFactory.setOnSQListener(this);
		nwi = new NetworkInfo();
		nwi.execute();
		mIsDBOn = mFactory.isServer();
		mMenuChangeHandler.sendEmptyMessage(1);
	}

	private class NetworkInfo extends AsyncTask<Void, Void, IPAddressObject> {

		@Override
		protected IPAddressObject doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				showWaitMessage(R.string.ip_setting);
				Intent intent2 = new Intent(
						"net.innodigital.ItmeSettingService.GETIPADDR");
				sendBroadcast(intent2);
				try {
					new Thread().sleep(1000);
				} catch (InterruptedException e) {
					Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
				}
				FileReader f = new FileReader(mFactory.getNetworkPath());

				BufferedReader br = new BufferedReader(f);
				String s[] = new String[4];
				for (int i = 0; i < 4; i++)
					s[i] = br.readLine();
				if (s[0] != null && s[1] != null && s[2] != null
						&& s[3] != null) {
					mIPObj.setIpaddress(s[0]);
					mIPObj.setGateway(s[1]);
					mIPObj.setSubnet(s[2]);
					mIPObj.setDns(s[3]);
					return mIPObj;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(IPAddressObject result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dismissedMessageBox();
			if (result != null) {
				setUI(mIPObj);
			} else
				showDefaultMessage(R.string.err_ip_set);
		}

	}

	private void setNetworkInfo(IPAddressObject ip) {
		Intent intent = new Intent(
				"net.innodigital.ItmeSettingService.SETIPADDR");
		intent.putExtra("ipaddr", ip.getIpaddress());
		intent.putExtra("gateway", ip.getGateway());
		intent.putExtra("netmask", ip.getSubnet());
		intent.putExtra("dns", ip.getDns());
		intent.putExtra("mode", "static");
		sendBroadcast(intent);
	}
	
	

	@Override
	protected void onStart() {
		super.onStart();
		
		updateDeviceStatus(registerReceiver(mReceiver, new IntentFilter(
				Intent.ACTION_BATTERY_CHANGED)));

		mMenuChangeHandler.sendEmptyMessage(0);
		
		mFactory.setmSqlSourceAddress(mServerIp.getText().toString());
		DriverManager.setLoginTimeout(2);
		mFactory.disconnect();
		
	}

	@Override
	protected void onPause() {
		super.onPause();
		((SignatureView) mLayoutElector.findViewById(R.id.viewSignpad)).stopThread();
		mDeviceHandler.sendEmptyMessage(MSG_IDSCAN_CLOSING);
		mDeviceHandler.sendEmptyMessage(MSG_IBSCAN_CLOSING);
		mFactory.ibscannerClose();
	}

	@Override
	protected void onStop() {
		unregisterReceiver(mReceiver);

		if (mMediaPlayer != null) {
			mMediaPlayer.release();
			mMediaPlayer = null;
		}

		super.onStop();
	}

	@Override
	public void onAttached(MiruSmartCard card) {
		if (mBtnVerifyTab.isClickable())
			super.onAttached(card);
		else {
			byte[] cType = { 'T', 0 };
			int ret = ResourceFactory.UI_TEST ? MiruSmartCard.SUCCESS : card
					.ProcGetCardType(cType);

			if (ret != MiruSmartCard.SUCCESS) {
				Log.e(TAG, "Unknown CARD: " + ret);
				ret = R.string.err_state_verify;
			} else {
				Log.i(TAG, "CARD Type is " + (char) cType[0]);
				ret = R.string.setup_verify_status_fine;
				Log.i(TAG, "Authtication : "+card.CardAuthentication());
			}
			if(mIsWriter){
			mDeviceHandler.sendMessage(mDeviceHandler.obtainMessage(
					MSG_SMCARD_CLOSING, ret, 0));
			mIsWriter = false;
			}
		}
		if(mMessageDialog!=null&&mMessageDialog.isShowing()){
			mMessageDialog.dismiss();}
	}

	@Override
	@SuppressWarnings("incomplete-switch")
	public void onError(IDScanCommander.State status) {
		if(mMessageDialog!=null&&mMessageDialog.isShowing())
			mMessageDialog.dismiss();
		int reason = 0;

		switch (status) {
		case IDSCAN_COVEROPEN:
			reason = R.string.err_cover;
			break;
		case IDSCAN_PAPERJAM:
			reason = R.string.err_jam;
			break;
		case IDSCAN_IO_ERROR:
		case IDSCAN_OCR_ERROR:
			reason = R.string.err_nores_scanner;
			break;
		}

		if (reason == 0) {
			// busy (scanning)
		} else {
			Log.e(TAG, getResources().getString(reason));
			mDeviceHandler.sendMessage(mDeviceHandler.obtainMessage(
					MSG_IDSCAN_CLOSING, ABNORMAL, 0));
		}
	}

	@Override
	public void onCompleted(Bitmap bitmap, String socialNo, String peopleName) {
		if(mMessageDialog!=null&&mMessageDialog.isShowing()){
			mIsScan = false;
		mMessageDialog.dismiss();
		}
		mBeeper.playSound();
		mDeviceHandler.sendEmptyMessage(MSG_IDSCAN_CLOSING);

		((ImageView) findViewById(R.id.imgVerifyScanner))
				.setImageBitmap(bitmap);
		((TextView) findViewById(R.id.textVerifySocialNumber))
				.setText(socialNo);
	}

	@Override
	public void onStatus(IBScanCommander.State state) {
		if (state != IBScanCommander.State.IBSCAN_CONNECTED)
			mDeviceHandler.sendMessage(mDeviceHandler.obtainMessage(
					MSG_IBSCAN_CLOSING, ABNORMAL, 0));
	}

	@Override
	public void onPreview(final Bitmap bitmap) {
		if (mImgFingerprint != null
				|| mLayoutElector != null
				&& (mImgFingerprint = (ImageView) mLayoutElector
						.findViewById(R.id.imgFingerprint4Elector)) != null) {
			// Make sure this occurs on UI thread.
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mImgFingerprint.setImageBitmap(bitmap);
				}
			});
		}
	}

	@Override
	public void onCompleted(ImageDataExt image) {
		if(mMessageDialog!=null&&mMessageDialog.isShowing()){
			mIsFinger = false;
			mMessageDialog.dismiss();
			}
		final Bitmap bitmap = mFactory.convertImageToBitmap(image);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				((ImageView) findViewById(R.id.imgVerifyFingerprint))
						.setImageBitmap(bitmap);
			}
		});

		mBeeper.playSound();
		dismissedMessageBox();
	}

	@Override
	public void onCompletion(MediaPlayer player) {
		if (mMediaPlayer != null) {
			mMediaPlayer.stop();
			mMediaPlayer.release();
			mMediaPlayer = null;
		}

		mBtnSoundguide.setText(R.string.setup_verify_button_play);
		mBtnSoundguide.setClickable(true);
	}

	void updateDeviceStatus(Intent batteryIntent) {
		final int plugged = batteryIntent.getIntExtra(
				BatteryManager.EXTRA_PLUGGED, -1);

		mTextPowerStatus
				.setTextColor(getResources()
						.getColor(
								plugged == BatteryManager.BATTERY_PLUGGED_AC
										|| plugged == BatteryManager.BATTERY_PLUGGED_USB ? R.color.article_active
										: R.color.article_inactive));
	}

	public void onClick(View v) {
		v.setClickable(false);

		switch (v.getId()) {
		case R.id.btn_db_start:
		case R.id.btn_db_stop:
			showWaitMessage(R.string.check_DB);
			new DBOnOffTask().execute();
			break;
		// 연결 테스트 버튼 클릭시 연결 시도 후 연결 종료.pjw
		case R.id.btn_connect_server_test:
			showWaitMessage(R.string.wating_pdf_1);
			mFactory.setmSqlSourceAddress(mServerIp.getText().toString());
			mFactory.dbQuery(QueryID.Q4);
			mFactory.connectionDBTest();
			break;
		case R.id.btn_network_save:
			mIPObj.setIpaddress(mNetIp.getText().toString());
			mIPObj.setDns(mNetDns.getText().toString());
			mIPObj.setGateway(mNetGateway.getText().toString());
			mIPObj.setRemark(mNetRemark.getText().toString());
			mIPObj.setSubnet(mNetSubnet.getText().toString());
			mIPObj.setServer((mSelectedMode == 1 || mSelectedMode == 0));
			LoginActivity.IPSAVE.insert(mIPObj);
			setNetworkInfo(mIPObj);
			nwi = new NetworkInfo();
			nwi.execute();
			mSavedIPAddresses.clear();
			mSavedIPAddresses = LoginActivity.IPSAVE.select();
			mIpsetAdapter.notifyDataSetChanged();
			mFactory.store();
			break;
		case R.id.btn_network_submit:
		case R.id.btnBack:
			setResult(RESULT_CANCELED);
			mFactory.releaseSmartCard();
			finish();
			break;
		case R.id.tv_basic_scan_no:
			if (mRecognizeSelector == null) {
				mRecognizeSelector = new SelectDialog(this,
						R.string.setting_title_03, R.array.listSetting04,
						mSelectedRecognize);
				mRecognizeSelector.setOnDismissListener(this);
			}

			mRecognizeSelector.show();
			break;
		case R.id.btnVerifyScanner:
			if (midCapturing) {
				midCapturing = false;
				mDeviceHandler.sendEmptyMessage(MSG_IDSCAN_CLOSING);

				((TextView) findViewById(R.id.textVerifySocialNumber))
						.setText("");
				((ImageView) findViewById(R.id.imgVerifyScanner))
						.setImageDrawable(null);
				((Button) v).setText(R.string.setup_verify_button_start);
			} else if (mIDScanner.open() == false)
				showDefaultMessage(R.string.err_nodev_scanner);
			else {
				midCapturing = mIDScanner.setScannable(true); // one shot
				showQuestionMessage(R.string.wait4scanning, MsgTag);
				mIsScan = true;
				((Button) v).setText(R.string.setup_verify_button_finish);
			}
			break;

		case R.id.btnVerifyFingerprint:
//			if(((Button) findViewById(R.id.btnVerifySignature)).getText().equals(getString(R.string.setup_verify_button_finish)))
//				((Button) findViewById(R.id.btnVerifySignature)).performClick();
			if (mFactory.isPreparing())
				showToastMessage(R.string.conn4fingerprint, TOAST_TIMEOUT);
			else if (!ResourceFactory.UI_TEST
					&& mFactory.isCapturable() == false)
				showDefaultMessage(R.string.err_nodev_fingerprint);
			else {
				if (mibCapturing) {
					((Button) v).setText(R.string.setup_verify_button_start);
					((ImageView) findViewById(R.id.imgVerifyFingerprint))
							.setImageDrawable(null);
					mDeviceHandler.sendMessage(mDeviceHandler.obtainMessage(
							MSG_IBSCAN_CLOSING, 0, 0));
				} else {
					showQuestionMessage(R.string.wait4fingerprint, MsgTag);
					((Button) v).setText(R.string.setup_verify_button_finish);
					new Thread(){public void run() {mIsFinger=mFactory.startCapture();};}.start();
				}

				mibCapturing = !mibCapturing;
				activeElectorLayout(mibCapturing);
				if (mibCapturing)
					activeFingerprint();
			}
			break;

		case R.id.btnVerifySignature:
			if (canDisplayElector() == false)
				showDefaultMessage(R.string.err_nodev_elector);
			else {
				if (isActiveSignpad()) {
					((SignatureView) mLayoutElector.findViewById(R.id.viewSignpad)).stopThread();
					((SignatureMirrorView) findViewById(R.id.imgVerifySignature)).stopThread();
//					mIDScanner.backward();
					((Button) v).setText(R.string.setup_verify_button_start);
//					((ImageView) findViewById(R.id.imgVerifySignature))
//							.setImageDrawable(null);
				} else {
					((SignatureView) mLayoutElector.findViewById(R.id.viewSignpad)).startThread();
					((SignatureMirrorView) findViewById(R.id.imgVerifySignature)).startThread();
					showQuestionMessage(R.string.wait4signpad, MsgTag);
					mIsSign = true;
					((Button) v).setText(R.string.setup_verify_button_finish);
				}
				activeElectorLayout(!isActiveSignpad());
			}
			break;

		case R.id.btnVerifyCardwriter: {
			final RadioGroup rg = (RadioGroup) findViewById(R.id.radioCardwriter);
			
			if (rg.getCheckedRadioButtonId() != R.id.radio1)
				mCardIndex = rg.getCheckedRadioButtonId() == R.id.radio2 ? 2
						: 0;
			else{
				mCardIndex = 1;
				if(!mIDScanner.open()){
					showDefaultMessage(getString(R.string.err_nodev_writer, mCardIndex));
					break;
				}
				mIDScanner.forward();
			}
			if (mFactory.getSmartCard(mCardIndex) == null
					&& ResourceFactory.UI_TEST == false)
				mDeviceHandler.sendMessage(mDeviceHandler.obtainMessage(
						MSG_SMCARD_CLOSING, R.string.err_state_connect, 0));
			else{
				showQuestionMessage(R.string.wait4writer, MsgTag);
				mIsWriter = true;
				}
			break;
		}
		case R.id.btnVerifySoundguide:
			if (mMediaPlayer != null)
				onCompletion(mMediaPlayer);
			else {
				mMediaPlayer = new MediaPlayer();
				mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

				try {
					AssetFileDescriptor afd = getAssets().openFd("sound.mp3");
					mMediaPlayer.setDataSource(afd.getFileDescriptor(),
							afd.getStartOffset(), afd.getLength());
					afd.close();

					mMediaPlayer.setOnCompletionListener(this);
					mMediaPlayer.prepare();
					mMediaPlayer.start();

					mBtnSoundguide.setText(R.string.setup_verify_button_stop);
				} catch (IOException e) {
					Log.e(TAG, e.getMessage());
					mMediaPlayer.release();
					mMediaPlayer = null;
				}
			}
			break;

		case R.id.btnVerifyCleanup: {
			// 신분증 스캔 취소
			if (midCapturing)
				onClick(findViewById(R.id.btnVerifyScanner));
			// 지문인식창 소거
			if (mibCapturing)
				onClick(findViewById(R.id.btnVerifyFingerprint));
			// 서명창 소거
			if(((Button) findViewById(R.id.btnVerifySignature)).getText().equals(getString(R.string.setup_verify_button_finish))
					&&((SignatureView) mLayoutElector.findViewById(R.id.viewSignpad))!=null){
				((Button)findViewById(R.id.btnVerifySignature)).setText(R.string.setup_verify_button_start);				
				((SignatureView) mLayoutElector.findViewById(R.id.viewSignpad)).clearCanvas();
			}
			// 음성안내 취소
			if (mMediaPlayer != null)
				onCompletion(mMediaPlayer);
			// 카드장치 소거
			final int[] stateWriter = { R.id.textVerifyStateWriter1,
					R.id.textVerifyStateWriter2, R.id.textVerifyStateWriter3 };
			for (int i = 0; i < stateWriter.length; ++i)
				((TextView) findViewById(stateWriter[i])).setText("");
			break;
		}
		case R.id.status_setting:
			if (mModeSelector == null) {
				mModeSelector = ResourceFactory.UI_TEST?new SelectDialog(this, mFactory.getResources().getString(R.string.setting_title_01), new Object[]{getResources().getStringArray(R.array.listSetting01)[1]}, mSelectedMode)
						:new SelectDialog(this,
						R.string.setting_title_01, R.array.listSetting01,
						mSelectedMode);
				mModeSelector.setOnDismissListener(this);
			}

			mModeSelector.show();
			break;

		case R.id.tv_basic_print_set:
			if (mMethodSelector == null) {
				mMethodSelector = new SelectDialog(this,
						R.string.setting_title_02, R.array.listSetting03,
						mSelectedMethod);
				mMethodSelector.setOnDismissListener(this);
			}

			mMethodSelector.show();
			break;

		case R.id.obj_install:
			// 열려있는 카드장치가 관리자용이 아닌 경우 장치를 닫고 관리자용 카드장치 연결
			if (mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER) != null)
				upgrade(true);
			else
				v.setEnabled(false);
			break;
		case R.id.del_btn:

			break;

		case R.id.btnToggleBasic:
			if (v.isSelected() == false) {
				// Toggling layouts
				findViewById(R.id.btnVerifyCleanup).performClick();
				mTabBasic.setVisibility(View.VISIBLE);
				mTabVerify.setVisibility(View.GONE);
				// Toggling buttons
				v.setSelected(true);
				mBtnVerifyTab.setSelected(false);
				mBtnVerifyTab.setClickable(true);
			}
			return;

		case R.id.btnToggleVerify:
			if (v.isSelected() == false) {
				mFactory.openIBScanner(this);

				// Toggling layouts
				mTabVerify.setVisibility(View.VISIBLE);
				mTabBasic.setVisibility(View.GONE);
				// Toggling buttons
				v.setSelected(true);
				mBtnBasicTab.setSelected(false);
				mBtnBasicTab.setClickable(true);
			}
			return;
		}

		v.setClickable(true);
	}

	@Override
	public void onSignature(Bitmap bitmap) {
		if(mMessageDialog!=null&&mMessageDialog.isShowing()){
			mIsSign = false;
			mMessageDialog.dismiss();}
		dismissedMessageBox();
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		if (dialog.equals(mModeSelector)) {
			if (mSelectedMode != mModeSelector.getSelectedIndex()) {
				mDeviceMode.setText(mModeSelector.getSelectedItem().toString());
				mSelectedMode = mModeSelector.getSelectedIndex();
			}
			String strMode = null;
			switch (mSelectedMode) {
			case SERVER:
				mNetRemark.setText(getResources().getStringArray(R.array.listSetting01)[0]);
				strMode = ResourceFactory.MODE_server;
				break;
			case BOTH:
				mNetRemark.setText(getResources().getStringArray(R.array.listSetting01)[1]);
				strMode = ResourceFactory.MODE_both;
				break;
			default:
				mNetRemark.setText(getResources().getStringArray(R.array.listSetting01)[2]);
				strMode = ResourceFactory.MODE_client;
				break;
			}
			mFactory.setProperty(ResourceFactory.CONFIG_SERVER_MODE, ResourceFactory.UI_TEST?ResourceFactory.MODE_both:strMode);
		} else if (dialog.equals(mMethodSelector) && mSelectedMethod != mMethodSelector.getSelectedIndex()) {
			mOutputMethod.setText(mMethodSelector.getSelectedItem().toString());

			String strMethod;
			switch (mSelectedMethod = mMethodSelector.getSelectedIndex()) {
			case 1:
				strMethod = ResourceFactory.ISSUE_paper;
				break;
			case 2:
				strMethod = ResourceFactory.ISSUE_batch;
				break;
			default:
				strMethod = ResourceFactory.ISSUE_card;
				break;
			}

			mFactory.setProperty(ResourceFactory.CONFIG_ISSUE_METHOD, strMethod);
		} else if (dialog.equals(mRecognizeSelector) && mSelectedRecognize != mRecognizeSelector.getSelectedIndex()) {
			((TextView)findViewById(R.id.tv_basic_scan_no))
			.setText(mRecognizeSelector.getSelectedItem().toString());
			
			String strMethod;
			switch (mSelectedRecognize = mRecognizeSelector.getSelectedIndex()) {
			case 1:
				strMethod = ResourceFactory.RECOGNIZE_left;
				break;
			case 2:
				strMethod = ResourceFactory.RECOGNIZE_right;
				break;
			default:
				strMethod = ResourceFactory.RECOGNIZE_full;
				break;
			}
			mFactory.setProperty(ResourceFactory.CONFIG_RECOGNIZE, strMethod);
		}
	}

	@Override
	public void onBackPressed() {
		setResult(RESULT_OK);
		finish();
	}

	/*
	 * A handler to process state transition messages.
	 */
	@SuppressLint("HandlerLeak")
	private Handler mDeviceHandler = new Handler() {
		@SuppressLint("StringFormatInvalid")
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_IBSCAN_CLOSING:
				if (msg.arg1 == ABNORMAL) {
					showQuestionMessage("finger restart","FingerRestart");
				}else{
					new Thread(){
						public void run() {
							mFactory.stopCapture();
						};
					}.start();
					dismissedMessageBox(); // close waiting
				}
				break;

			case MSG_IDSCAN_CLOSING:
				mIDScanner.close();
				dismissedMessageBox(); // close waiting

				if (msg.arg1 == ABNORMAL) {
					showDefaultMessage(R.string.err_nores_scanner);
				}
				break;

			case MSG_SMCARD_CLOSING:
				if (msg.arg1 == R.string.setup_verify_status_fine) {
					mBeeper.playSound();
//					if(mCardIndex==1)
//					mIDScanner.backward();
				}

				if (msg.arg1 != 0)
					mFactory.releaseSmartCard();
				else if (mFactory.getSmartDeviceIndex() != ResourceFactory.SCARD_FOR_MANAGER)
					mFactory.getSmartCard(ResourceFactory.SCARD_FOR_MANAGER);

				dismissedMessageBox();// close waiting

				if (msg.arg1 != 0) {
					final int[] stateWriter = { R.id.textVerifyStateWriter1,
							R.id.textVerifyStateWriter2,
							R.id.textVerifyStateWriter3 };
					((TextView) findViewById(stateWriter[mCardIndex]))
							.setText(msg.arg1);

					if (msg.arg1 == R.string.err_state_connect)
						showDefaultMessage(getString(R.string.err_nodev_writer, mCardIndex + 1));
					else if (msg.arg1 == R.string.err_state_verify)
						showDefaultMessage(R.string.err_nores_writer);
				}
				break;
			case MSG_SQL_CONNECT:
				findViewById(R.id.btn_connect_server_test).setClickable(true);
				mServerIp.setText(mFactory.getmSqlSourceAddress());
				break;
			case MSG_SETUP_DIALOG:
				if (mMessageDialog == null) {
				mMessageDialog = new Dialog(SetupActivity.this);
				mMessageDialog.setContentView(R.layout.popup_message);
				mDefaultTextView = (TextView) mMessageDialog
						.findViewById(android.R.id.message);
				mMessageDialog.setCancelable(false);

				OnClickListener l = new OnClickListener() {
					@Override
					public void onClick(View v) {
						mMessageDialog.dismiss();
						if(mIsSign){
							mIsSign = false;
							findViewById(R.id.btnVerifySignature).performClick();
						}else if(mIsFinger){
							mIsFinger = false;
							findViewById(R.id.btnVerifyFingerprint).performClick();
						}else if(mIsScan){
							mIsScan = false;
							findViewById(R.id.btnVerifyScanner).performClick();
						}else if(mIsWriter){
							mIsWriter = false;
							mDeviceHandler.sendMessage(mDeviceHandler.obtainMessage(
									MSG_SMCARD_CLOSING, R.string.cancel, 0));
//							findViewById(R.id.btnVerifyCardwriter).performClick();
						}
					}
				};

				mMessageDialog.findViewById(android.R.id.button1)
						.setOnClickListener(l);
				mMessageDialog.findViewById(android.R.id.button2)
						.setOnClickListener(l);
				} else if (mMessageDialog.isShowing()) {
					mMessageDialog.cancel();
				}
	
				((Button) mMessageDialog.findViewById(android.R.id.button1))
						.setText(R.string.cancel);
				mMessageDialog.findViewById(android.R.id.button2)
						.setVisibility(View.GONE);
				mDefaultTextView.setText(mMsgText);
				mMessageDialog.show();
				break;
			}
		}
	};
	@Override
	public void showQuestionMessage(String message, Object tag) {
		if(MsgTag.equals(tag)){
		mMsgText = message;
		mDeviceHandler.sendEmptyMessage(MSG_SETUP_DIALOG);
		}else{
			super.showQuestionMessage(message, tag);
		}
	}
	private Handler mMenuChangeHandler = new Handler(){
		public void handleMessage(Message msg) {
			switch(msg.what) {
			case 0:
				String strMethod = mFactory
				.getProperty(ResourceFactory.CONFIG_ISSUE_METHOD);
				if (strMethod == null || strMethod.equals(ResourceFactory.ISSUE_card))
					mSelectedMethod = 0;
				else
					mSelectedMethod = strMethod.equals(ResourceFactory.ISSUE_paper)?1:2;
				mOutputMethod.setText(getResources().getStringArray(
						R.array.listSetting03)[mSelectedMethod]);
				
				
				String strMode = mFactory.getProperty(ResourceFactory.CONFIG_SERVER_MODE);
				if(strMode == null || strMode.equals(ResourceFactory.MODE_client))
					mSelectedMode = 2;
				else
					mSelectedMode = strMode.equals(ResourceFactory.MODE_server)?0:1;
				mDeviceMode.setText(getResources().getStringArray(R.array.listSetting01)[mSelectedMode]);
				
				
				String strRecognize = mFactory.getProperty(ResourceFactory.CONFIG_RECOGNIZE);
				if(strRecognize == null || strRecognize.equals(ResourceFactory.RECOGNIZE_full))
					mSelectedRecognize = 0;
				else
					mSelectedRecognize = strRecognize.equals(ResourceFactory.RECOGNIZE_right)?2:1;
				((TextView)findViewById(R.id.tv_basic_scan_no))
				.setText(getResources().getStringArray(R.array.listSetting04)[mSelectedRecognize]);
				findViewById(R.id.btn_connect_server_test).setClickable(true);
				break;
			case 1:
				mDBStart.setEnabled(!mIsDBOn);
				mDBStop.setEnabled(mIsDBOn);
				break;
			}
		};
	};

	/* *********************************************************************************************
	 * AC/Batery INTERFACE
	 * *******************************************************
	 * ************************************
	 */

	private BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			if (action.compareTo(Intent.ACTION_BATTERY_CHANGED) == 0) {
				updateDeviceStatus(intent);
			}
		}
	};

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		IPAddressObject ip = mSavedIPAddresses.get(arg2);
		mNetIp.setText(ip.getIpaddress());
		mNetGateway.setText(ip.getGateway());
		mNetDns.setText(ip.getDns());
		mNetRemark.setText(ip.getRemark());
		mNetSubnet.setText(ip.getSubnet());
		// 서버와 단말기 구분으로 서버일 경우 서버 IP주소를 변경.
		if (ip.isServer()) {
			mServerIp.setText(ip.getIpaddress());
		}
		mIPObj = ip;
	}
	private class IPSetAdapter extends BaseAdapter {
		private int p;
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				// TODO Auto-generated method stub
				p = position;
				LayoutInflater li = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				if (convertView ==null) 
					convertView= li.inflate(R.layout.cell_ip_save,parent, false);
					
					TextView tvIp = (TextView) convertView.findViewById(R.id.tv_network_save_address);
					tvIp.setText(mSavedIPAddresses.get(position).getIpaddress());

					TextView tvName = (TextView) convertView.findViewById(R.id.tv_network_save_name);
					tvName.setText(mSavedIPAddresses.get(position).getRemark());

					ImageView delBtn = (ImageView) convertView.findViewById(R.id.del_btn);
					delBtn.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							LoginActivity.IPSAVE.delete(mSavedIPAddresses.get(p));
							mSavedIPAddresses.remove(p);
							notifyDataSetChanged();
						}
					});
				
				return convertView;
			}
			@Override
			public long getItemId(int position) {
				// TODO Auto-generprated method stub
				return position;
			}
			
			@Override
			public Object getItem(int position) {
				// TODO Auto-generated method stub
				return mSavedIPAddresses.get(position);
			}
			
			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return mSavedIPAddresses.size();
			}
	}

	@Override
	public void onSqlStatus(SQLState state) {
		try {new Thread().sleep(1000);} catch (InterruptedException e) {Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();}
		dismissedMessageBox();
		if(state != SQLState.SQL_CONNECTED){
			mMenuChangeHandler.sendEmptyMessage(0);
			showDefaultMessage(R.string.ip_server_disconnect);
		}else{
			if(!mFactory.getProperty(ResourceFactory.CONFIG_SERVER_MODE).equals(ResourceFactory.MODE_client))
				mDeviceHandler.sendEmptyMessage(MSG_SQL_CONNECT);
			showDefaultMessage(R.string.ip_server_connect);
		}
	}

	@Override
	public Object[] getParameters(QueryID id) {
		return null;
	}

	@Override
	public boolean onResultSet(ResultSet rs, QueryID id)
			throws SQLException {
//		if(rs.next()&&!rs.getString("DATACHECK_DATE").equals("")){
//			dismissedMessageBox();
//			onSqlStatus(SQLState.SQL_CONNECTED);}
		return false;
	}

	@Override
	public boolean onResultSet(int rows, QueryID id)
			throws SQLException {
		return false;
	}

	@Override
	public void onEvent(int qn, String arg0) {
	}

	@Override
	public void onEvent(int qn, int updateCount) {
		
	}

	@Override
	public void onIdle(MiruSmartCard card) {
		// TODO Auto-generated method stub
//		super.onIdle(card);
	}
	
	@Override
	public void onPrepareUpdate(SQLNO qn, String[] jsonParam) {
		mFactory.jsonCommit(qn, jsonParam);
	}	
	private class DBOnOffTask extends AsyncTask<Void, Void, Void>{
		@Override
		protected Void doInBackground(Void... params) {
			try {Thread.sleep(2000);} catch (InterruptedException e) {Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();}
			File f = new File(ResourceFactory.UDB_PATH+"/tsvs16.udb");//파일이 없는 경우 null 오류 발생으로 미리 확인.
			if(f.exists()&&!mFactory.getProperty(ResourceFactory.CONFIG_SERVER_MODE).equals(ResourceFactory.MODE_client)
					&&!mIsDBOn){
				mIsDBOn = true;
				mMenuChangeHandler.sendEmptyMessage(1);
			if(mFactory.onServer(mIsDBOn)){
				dismissedMessageBox();
				showDefaultMessage(R.string.start_DB);
				return null;
				}else{
				dismissedMessageBox();
				showDefaultMessage(R.string.error_server_start);
				mIsDBOn = false;
				mMenuChangeHandler.sendEmptyMessage(1);
				}
		}else if(!f.exists()){
			mIsDBOn = false;
			mFactory.onServer(mIsDBOn);
			mMenuChangeHandler.sendEmptyMessage(1);
			dismissedMessageBox();
			showDefaultMessage(R.string.error_no_database);
			return null;
		}else if(mFactory.getProperty(ResourceFactory.CONFIG_SERVER_MODE).equals(ResourceFactory.MODE_client)){
			mIsDBOn = false;
			mFactory.onServer(mIsDBOn);
			mMenuChangeHandler.sendEmptyMessage(1);
			dismissedMessageBox();
			showDefaultMessage(R.string.error_no_servermode);
			return null;}
		mIsDBOn = false;
		mFactory.onServer(mIsDBOn);
		dismissedMessageBox();
		showDefaultMessage(R.string.stop_DB);
		mMenuChangeHandler.sendEmptyMessage(1);
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		}
	}

	@Override
	protected void onMessageResult(View v, Object tag) {
		super.onMessageResult(v, tag);
		if(tag!=null&&tag.equals("FingerRestart")){
			mFactory.ibscannerClose();
			mFactory.openIBScanner(this);}
	}

}