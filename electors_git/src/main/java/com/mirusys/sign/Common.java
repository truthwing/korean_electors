package com.mirusys.sign;

import java.io.UnsupportedEncodingException;

import com.mirusys.electors.ResourceFactory.Log;

public class Common {
private static final String TAG="Common";	
	public static boolean isTCardVerify=false;
	public static byte[] mngrCert;
	public static byte[] dnData;
	public static byte[] mngrRand;
	public static byte[] mngrKey;
	public static byte[] ctrCert;
	
	public static byte[] string2Bytearray(String str, int len){
		byte[] strt = null;
		byte[] result=null;
		try {
			strt = str.getBytes("MS949");
			result = new byte[len];
			for(int i=0; i<len;i++){
				if(i<strt.length)
					result[i]=strt[i];
				else
					result[i]=0;
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
		}
		
		return result;
	}
	public static String bytearray2String(byte[] bytearr){
		String result=null;
		try {
			result = new String(bytearr, "MS949");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
		}
		return result;
	}
	
	public static byte[] subByteArray(byte[] arr, int len){
		byte[] result=new byte[len];
			for(int i=0; i<len;i++)
				result[i]=arr[i];
		return result;
	}
	
	public static int byteArrayToInt(byte[] b, int len) 
	{
	    int value = 0;
	    for (int i = 0; i < len; i++) {
	        int shift = (len - 1 - i) * 8;
	        value += (b[i] & 0x000000FF) << shift;
	    }
	    return value;
	}
}
