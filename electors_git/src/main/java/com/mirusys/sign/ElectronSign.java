package com.mirusys.sign;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPrivateKeySpec;

import com.haesinit.common.Base64Util;
import com.mirusys.electors.ResourceFactory.Log;

import ksign.jce.provider.key.KSignRSAPrivateKey;
import ksign.jce.util.Base64;
import ksign.jce.util.JCEUtil;



public class ElectronSign {

	String alg = "SHA1WithRSA"; // SHA1WithRSA (1024bit), SHA256WithRSA (2048bit)
	
	

	private static final String TAG = "ElectronSign";


	public ElectronSign() {
		
	}

	public String substring( String data, String header, String footer ){
		return (new StringBuffer( data )).substring( header.length(), data.length()-footer.length() );
	}

	public PrivateKey loadLawPrivateKey(byte[] keyBytes) {
	    PrivateKey key = null;

		SimpleDERReader dr = new SimpleDERReader(keyBytes);

		try {
			byte[] seq = dr.readSequenceAsByteArray();

			if (dr.available() != 0)
				throw new IOException("Padding in RSA PRIVATE KEY DER stream.");

			dr.resetInput(seq);

			BigInteger version = dr.readInt();

			if ((version.compareTo(BigInteger.ZERO) != 0) && (version.compareTo(BigInteger.ONE) != 0))
				throw new IOException("Wrong version (" + version + ") in RSA PRIVATE KEY DER stream.");

			BigInteger n = dr.readInt();
			BigInteger e = dr.readInt();
			BigInteger d = dr.readInt();
			BigInteger primeP = dr.readInt();
			BigInteger primeQ = dr.readInt();
			BigInteger expP = dr.readInt();
			BigInteger expQ = dr.readInt();
			BigInteger coeff = dr.readInt();

			RSAPrivateKeySpec privSpec = new RSAPrivateCrtKeySpec(n, e, d, primeP, primeQ, expP, expQ, coeff);
			key = new KSignRSAPrivateKey((RSAPrivateKeySpec)privSpec);

//			RSAPublicKeySpec pubSpec = new RSAPublicKeySpec(n, e);
		} catch (IOException e1) {
			e1.printStackTrace();
			return null;
		}

	    return key;
	}

	public Certificate loadCertificate(byte[] certBytes){
		Certificate certificate = null;
    	InputStream keyis = new ByteArrayInputStream(certBytes);
		CertificateFactory cf;
		try {
			cf = CertificateFactory.getInstance("X.509", "Ksign");
			certificate = cf.generateCertificate(keyis);
		} catch (CertificateException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (NoSuchProviderException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		}finally{
			try {
				keyis.close();
			} catch (IOException e) {
				keyis = null;
			}
		}
	    return certificate;
	}

	public Certificate loadCertificate(String signCertPemStr){
		String tempStr = substring( signCertPemStr, "-----BEGIN CERTIFICATE-----", "-----END CERTIFICATE-----" );
		Certificate certificate = null;
		byte[] keyBytes;
		try {
			keyBytes = Base64Util.decode(tempStr);
		} catch (Exception e1) {
			e1.printStackTrace();
			return null;
		}
    	InputStream keyis = new ByteArrayInputStream(keyBytes);
		CertificateFactory cf;
		try {
			cf = CertificateFactory.getInstance("X.509", "Ksign");
			certificate = cf.generateCertificate(keyis);
		} catch (CertificateException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (NoSuchProviderException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		}finally{
			try {
				keyis.close();
			} catch (IOException e) {
				keyis = null;
			}
		}
	    return certificate;
	}

	public byte[] digitalSign(byte[] input, byte[] pkey){
		PrivateKey signPriKey = null;
		byte[] signData;
		try {
			JCEUtil.initProvider();
		} catch (Exception e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		}
		Log.d(TAG, "SignKey:"+Base64.encode2(pkey));

		signPriKey = loadLawPrivateKey(pkey);

		Signature sign = null;
		try {
			sign = Signature.getInstance(alg, "Ksign");
			sign.initSign(signPriKey);
			sign.update(input);
			signData = sign.sign();

			if(signData != null){
				byte[] encode = Base64.encode(signData);
				System.out.println("서명 값은 : " + new String(encode));
			}
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
			return null;
		} catch (NoSuchProviderException e1) {
			e1.printStackTrace();
			return null;
		} catch (InvalidKeyException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (SignatureException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		}

		return signData;
	}

	public String certDn(byte[] pcert){
		X509Certificate cert = (X509Certificate) loadCertificate(pcert);
		Principal p = cert.getSubjectDN();
		StringBuilder sb = new StringBuilder();
		String dn = p.toString();
		String[] dnSplit =dn.split(",");
		for (int i = 0; i < dnSplit.length; i++) {
			String[] dnSplitName = dnSplit[i].split("=");
			dnSplit[i]=dnSplitName[0].toLowerCase()+"="+dnSplitName[1];
			sb.append(dnSplit[i]);
			if(i!=dnSplit.length-1)
			sb.append(",");
		}
		Log.i(TAG, "dn : "+sb.toString());
		return sb.toString();
	}
	
	public boolean verifySign(byte[] signData, byte[] pcert, byte[] input) {
		Log.d(TAG, "cert: "+Base64.encode2(pcert));

		X509Certificate cert = (X509Certificate) loadCertificate(pcert);

		Signature verify = null;
		PublicKey pubkey;
		try {
			verify = Signature.getInstance(alg, "Ksign");
			pubkey=cert.getPublicKey();
			Principal p = cert.getSubjectDN();
			int len = p.toString().getBytes().length;
			
			Log.d(TAG, "DN len= "+len+"  "+p.toString());
			Log.d(TAG, "Public key str:"+pubkey.toString());
			Log.d(TAG, "Public key algorithm:"+pubkey.getAlgorithm());
			verify.initVerify(cert.getPublicKey());
			verify.update(input);
		} catch (InvalidKeyException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return false;
		} catch (NoSuchAlgorithmException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return false;
		} catch (NoSuchProviderException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return false;
		} catch (SignatureException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return false;
		}


		boolean signVerifyflag = false;;
		try {
			signVerifyflag = verify.verify(signData);
		} catch (SignatureException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return false;
		}

		if(signVerifyflag){
			System.out.println("전자서명 검증 성공");
		}else{
			System.out.println("전자서명 검증 실패");
			return false;
		}
		return true;
	}
	public boolean verifySign(byte[] signData, byte[] pcert) {
		Log.d(TAG, "cert: "+Base64.encode2(pcert));

		X509Certificate cert = (X509Certificate) loadCertificate(pcert);
		
		Signature verify = null;
		PublicKey pubkey;
		byte[] input=null;
		try {
			input = "cn=중앙선거관리위원회(S20060630),ou=NEC RootCA,o=전자투표,c=KR".getBytes("MS949");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			verify = Signature.getInstance(alg, "Ksign");
			pubkey=cert.getPublicKey();
			Principal p = cert.getIssuerDN();
//			input = p.getName().getBytes();
			int len = p.toString().getBytes().length;
			Log.d(TAG, "DN : "+p.getName() );
			Log.d(TAG, "Public key str:"+pubkey.toString());
			Log.d(TAG, "Public key algorithm:"+pubkey.getAlgorithm());

			verify.initVerify(cert.getPublicKey());
			verify.update(input);
		} catch (InvalidKeyException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return false;
		} catch (NoSuchAlgorithmException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return false;
		} catch (NoSuchProviderException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return false;
		} catch (SignatureException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return false;
		}


		boolean signVerifyflag = false;;
		try {
			signVerifyflag = verify.verify(signData);
		} catch (SignatureException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return false;
		}

		if(signVerifyflag){
			System.out.println("전자서명 검증 성공");
		}else{
			System.out.println("전자서명 검증 실패");
			return false;
		}
		return true;
	}

//	public void sealTemp(byte[] input, byte[] pkey){
//		try {
//			JCEUtil.initProvider();
//			PrivateKey key = loadLawPrivateKey(pkey);
//			Signature sign = null;
//			sign = Signature.getInstance("SHA256withRSA", "Ksign");
//			sign.initSign(key);
//			sign.update(input);
//			byte[] signData = sign.sign();
//			if(signData != null){
//				byte[] encode = Base64.encode(signData);
//				System.out.println("서명 값은 : " + new String(encode));
//			}
//			sign = Signature.getInstance("SHA1withRSA", "Ksign");
//			sign.initSign(key);
//			sign.update(input);
//			signData = sign.sign();
//			if(signData != null){
//				byte[] encode = Base64.encode(signData);
//				System.out.println("서명 값은 : " + new String(encode));
//			}
//			sign = Signature.getInstance("MD5withRSA", "Ksign");
//			sign.initSign(key);
//			sign.update(input);
//			signData = sign.sign();
//			if(signData != null){
//				byte[] encode = Base64.encode(signData);
//				System.out.println("서명 값은 : " + new String(encode));
//			}
//			sign = Signature.getInstance("SHA1withDSA", "Ksign");
//			sign.initSign(key);
//			sign.update(input);
//			signData = sign.sign();
//			if(signData != null){
//				byte[] encode = Base64.encode(signData);
//				System.out.println("서명 값은 : " + new String(encode));
//			}
//		} catch (NoSuchAlgorithmException e) {
//			e.printStackTrace();
//		} catch (NoSuchProviderException e) {
//			e.printStackTrace();
//		} catch (InvalidKeyException e) {
//			e.printStackTrace();
//		} catch (SignatureException e) {
//			e.printStackTrace();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

}
