package com.mirusys.sign;

import javax.crypto.Cipher;
import javax.crypto.SealedObject;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.mirusys.electors.ResourceFactory.Log;

import android.util.Base64;

public class SimpleCrypto{
	private static String TAG="SimpleCrypto";
	 public static byte[] Decrypt(byte[] text, byte[] key) throws Exception
     {
               Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
               byte[] ret = new byte[key.length*2];
      			System.arraycopy(key, 0, ret, 0, key.length);
      			System.arraycopy(key, 0, ret, key.length, key.length);
              SecretKeySpec keySpec = new SecretKeySpec(ret, "AES");
               IvParameterSpec ivSpec = new IvParameterSpec(key);
               cipher.init(Cipher.DECRYPT_MODE,keySpec,ivSpec);
               byte [] results = cipher.doFinal(text);
//               byte[] resultsfinal = new byte[results.length-1];
//               System.arraycopy(results, 0, resultsfinal, 0, results.length-1);
               return results;
     }

     public static byte[] Encrypt(byte[] text, byte[] key) throws Exception
     {
               Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
               byte[] ret = new byte[key.length*2];
       			System.arraycopy(key, 0, ret, 0, key.length);
       			System.arraycopy(key, 0, ret, key.length, key.length);
               SecretKeySpec keySpec = new SecretKeySpec(ret, "AES");
               IvParameterSpec ivSpec = new IvParameterSpec(key);
               cipher.init(Cipher.ENCRYPT_MODE,keySpec,ivSpec);
               byte[] results = cipher.doFinal(text);
               return results;
     }
//     public static byte[] SealEncrypt(byte[] text, byte[] key) throws Exception
//     {
//    	 Cipher cipher1 = Cipher.getInstance("DES");
//    	 SecretKeySpec keySpec = new SecretKeySpec(key, "DES");
//         IvParameterSpec ivSpec = new IvParameterSpec(key);
//         cipher1.init(Cipher.ENCRYPT_MODE,keySpec,ivSpec);
//    	 byte[] results = cipher1.doFinal(text);
//    	 Log.i(TAG, "SEAL Enc Base64 : "+Base64.encode(results, Base64.DEFAULT));
//    	 return results;
//     }
//     public static String Decrypt(String text, String key) throws Exception
//     {
//               Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//               byte[] keyBytes= new byte[16];
//               byte[] b= key.getBytes("UTF-8");
//               int len= b.length;
//               if (len > keyBytes.length) len = keyBytes.length;
//               System.arraycopy(b, 0, keyBytes, 0, len);
//               SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
//               IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
//               cipher.init(Cipher.DECRYPT_MODE,keySpec,ivSpec);
//
//               
//               byte [] results = cipher.doFinal(Base64.decode(text, 0));
//               
//               return new String(results,"UTF-8");
//     }
//
//     public static String Encrypt(String text, String key) throws Exception
//     {
//               Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//               byte[] keyBytes= new byte[16];
//               byte[] b= key.getBytes("UTF-8");
//               int len= b.length;
//               if (len > keyBytes.length) len = keyBytes.length;
//               System.arraycopy(b, 0, keyBytes, 0, len);
//               SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
//               IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
//               cipher.init(Cipher.ENCRYPT_MODE,keySpec,ivSpec);
//
//               byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
//               return Base64.encodeToString(results, 0);
//     }
}