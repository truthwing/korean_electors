package com.mirusys.sign;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;

import com.mirusys.electors.ResourceFactory.Log;

import javak.crypto.BadPaddingException;
import javak.crypto.Cipher;
import javak.crypto.IllegalBlockSizeException;
import javak.crypto.NoSuchPaddingException;
import javak.crypto.SecretKey;
import javak.crypto.SecretKeyFactory;
import javak.crypto.spec.IvParameterSpec;
import javak.crypto.spec.SecretKeySpec;
import ksign.jce.util.JCEUtil;

public class SeedCrypto {
	private static final String TAG = "SeedCrypto";
	public static byte[] encrypt(byte[] input, byte[] key) {
		try {
			JCEUtil.initProvider();
		} catch (Exception e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		}
		
		String algoname = "SEED";
		String alg = "SEED/CBC/PKCS5Padding";
		byte[] iv = key;

		SecretKeyFactory skf = null;
		SecretKey seckey = null;
		try {
			skf = SecretKeyFactory.getInstance(algoname, "Ksign");
			SecretKeySpec kspec = new SecretKeySpec(key, algoname);
			seckey = skf.generateSecret(kspec);
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
			return null;
		} catch (NoSuchProviderException e1) {
			e1.printStackTrace();
			return null;
		} catch (InvalidKeySpecException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		}
		
		IvParameterSpec ivSpec = new IvParameterSpec(iv); // iv 
		
		byte[] encBytes = null;
		Cipher enc = null;
		try {
			enc = Cipher.getInstance(alg, "Ksign");
			enc.init(Cipher.ENCRYPT_MODE, seckey, ivSpec);
			encBytes = enc.doFinal(input);
		} catch (NoSuchAlgorithmException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (NoSuchProviderException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (NoSuchPaddingException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (InvalidKeyException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (InvalidAlgorithmParameterException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (IllegalStateException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (IllegalBlockSizeException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (BadPaddingException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		}
		
		return encBytes;
	}
	
	public static byte[] decrypt(byte[] encBytes, byte[] key) {
		try {
			JCEUtil.initProvider();
		} catch (Exception e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		}
		
		String algoname = "SEED";
		String alg = "SEED/CBC/PKCS5Padding";
		byte[] iv = key;

		SecretKeyFactory skf = null;
		SecretKey seckey = null;
		try {
			skf = SecretKeyFactory.getInstance(algoname, "Ksign");
			SecretKeySpec kspec = new SecretKeySpec(key, algoname);
			seckey = skf.generateSecret(kspec);
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
			return null;
		} catch (NoSuchProviderException e1) {
			e1.printStackTrace();
			return null;
		} catch (InvalidKeySpecException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		}
		
		IvParameterSpec ivSpec = new IvParameterSpec(iv); // iv 

		byte[] decByts = null;
		Cipher dec = null;		
		try {
			dec = Cipher.getInstance(alg, "Ksign");
			dec.init(Cipher.DECRYPT_MODE, seckey, ivSpec);
			decByts = dec.doFinal(encBytes);
		} catch (NoSuchAlgorithmException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (NoSuchProviderException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (NoSuchPaddingException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (InvalidKeyException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (InvalidAlgorithmParameterException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (IllegalStateException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (IllegalBlockSizeException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		} catch (BadPaddingException e) {
			Log.e(TAG, "Error : "+e.getMessage());e.printStackTrace();
			return null;
		}
		
		return decByts;
	}
}
