package com.pacesystem.lib;



public class PaceRecID
{
	// 
	public static native int Open(String str1);
 	public static native int Close(int hRECID);
	public static native int SetImageByFile(int hRECID,int card_type,String imgnam);
	public static native int SetImage(int hRECID,int card_type, int width, int height, int byteperline,
			   							byte data[],int colortype);
	
	public static native int SetImageInt(int hRECID,int idcard_type,int width, int height, int byteperline,
			int data[],int colortype);
	
	public static native int SetImageByJpegMemory(int hRECID,int idcard_type,int length,byte data[]);
	

	public static native int Recog(int hRECID);


	public static native int GetResult(int hRECID,int position_type, int positio[], byte data[], int maxlen);
	
	public static native int FreeImage(int hRECID);
	
	public static native int SaveResultImage(int hRECID, String imgfname);

	static
	{
		System.loadLibrary("recidcard");			// 로드 순서 중요함...
	}
}
